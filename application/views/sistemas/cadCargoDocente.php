<div class="form-horizontal">
    <?php echo form_open(); ?>
    <h3 class="text-center">SELEÇÃO DOCENTES</h3>
    <hr>
    <h4>DADOS PESSOAIS</h4>
    <br>
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <label>nome</label>
            <input type="text" class="form-control" name="sp_nome" required="true">
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <label>email</label>
            <input type="email" class="form-control" name="sp_email" required="true">
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-3 col-sm-3 col-xs-6">
            <label>telefone</label>
            <input type="text" class="form-control telefone" name="sp_telefone" required="true">
        </div>
        <div class="col-md-5 col-sm-5 col-xs-6">
            <label>logradouro</label>
            <input type="text" class="form-control" name="sp_logradouro" required="true">
        </div>

        <div class="col-md-2 col-sm-2 col-xs-6">
            <label>numero</label>
            <input type="text" class="form-control" name="sp_numero" required="true">
        </div>
        <div class="col-md-2 col-sm-2 col-xs-6">
            <label>bairro</label>
            <input type="text" class="form-control" name="sp_bairro" required="true">
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <label>complemento</label>
            <input type="text" class="form-control" name="sp_complemento">
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6">
            <label>cidade</label>
            <input type="text" class="form-control" name="sp_cidade" required="true">
        </div>
        <div class="col-md-2 col-sm-2 col-xs-6">
            <label>ESTADO</label>
            <input type="text" class="form-control" name="sp_estado" required="true">
        </div>
    </div>
    <hr>
    <h4>FORMAÇÃO</h4>
    <br>
    <h5><i>GRADUAÇÃO</i></h5>
    <br>
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <label>GRADUAÇÃO EM:</label>
            <input type="text" class="form-control" name="sp_graduacaoCurso" required="true">
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            <label>InÍcio EM</label>
            <input type="text" class="form-control data" name="sp_graduacaoInicio" required="true">
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            <label>CONCLUÍDA EM</label>
            <input type="text" class="form-control data" name="sp_graduacaoFim" required="true">
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            <label>Instituicao DE ENSINO</label>
            <input type="text" class="form-control" name="sp_graduacaoInstituicao" required="true">
        </div>
    </div>
    <br>
    <h5><i>ESPECIALIZAÇÃO</i></h5>
    <br>
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <label>ESPECIALIZAÇÃO EM</label>
            <input type="text" class="form-control" name="sp_especializacaoCurso">
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            <label>InÍcio EM</label>
            <input type="text" class="form-control data" name="sp_especializacaoInicio">
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6 ">
            <label>CONCLUÍDA EM</label>
            <input type="text" class="form-control data" name="sp_especializacaoFim">
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            <label>TEMA</label>
            <input type="text" class="form-control" name="sp_especializacaoTema">
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            <label>Instituicao DE ENSINO</label>
            <input type="text" class="form-control" name="sp_especializacaoInstituicao">
        </div>
    </div>
    <br>
    <h5><i>MESTRADO</i></h5>
    <br>
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <label>MESTRADO EM</label>
            <input type="text" class="form-control" name="sp_mestradoCurso">
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            <label>InÍcio EM</label>
            <input type="text" class="form-control data" name="sp_mestradoInicio">
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            <label>CONCLUÍDO EM</label>
            <input type="text" class="form-control data" name="sp_mestradoFim">
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            <label>TEMA</label>
            <input type="text" class="form-control" name="sp_mestratoTema">
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            <label>Instituicao DE ENSINO</label>
            <input type="text" class="form-control" name="sp_mestradoInstituicao">
        </div>
    </div>
    <br>
    <h5><i>DOUTORADO</i></h5>
    <br>
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <label>DOUTORADO EM</label>
            <input type="text" class="form-control" name="sp_doutoradoCurso">
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            <label>InÍcio EM</label>
            <input type="text" class="form-control data" name="sp_doutoradoInicio">
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            <label>CONCLUÍDO EM</label>
            <input type="text" class="form-control data" name="sp_doutoradoFim">
        </div>
        <div class="col-md-12">
            <label>TEMA</label>
            <input type="text" class="form-control" name="sp_doutoradoTema">
        </div>
        <div class="col-md-12">
            <label>Instituicao DE ENSINO</label>
            <input type="text" class="form-control" name="sp_doutoradoInstituicao">
        </div>
    </div>

    <br>
    <h5><i>RESUMO DA FORMAÇÃO</i></h5>
    <br>
    <div class="form-group">
        <div class="col-md-12">
            <label>Resumo de formação</label>
            <textarea style="height: 100px;" class="form-control" name="sp_resumoFormacao" placeholder="Insira aqui dados de formação acadêmica em nível de Graduação, Especialização, Mestrado e Doutorado, identificando ênfases de formação e linhas de pesquisa."></textarea>
        </div>
    </div>

    <br>
    <h5><i>Habilidades e competências – disciplinas </i></h5>
    <br>

    <div class="form-group">
        <div class="col-md-12">
            <label>Disciplinas</label>
            <textarea style="height: 100px;" class="form-control" name="sp_habilidadeDisciplinas" placeholder="Adicione aqui disciplinas, dentre as disponíveis no edital (quando aplicável), as quais esteja apto a ministrar."></textarea>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12">
            <label>DISPONIBILIDADE</label>
            <textarea style="height: 100px;" class="form-control" name="sp_habilidadeDisponibilidade" placeholder="Digite aqui sua disponibilidade de tempo para atuar na Faculdade Guanambi, considerando o período semanal de funcionamento das 7h as 23h20 de segunda a sexta-feira e aos sábado das 7h às 12h20. "></textarea>
        </div>
    </div>

    <br>
    <h5><i>Experiência </i></h5>
    <br>
    <div class="form-group">
        <div class="col-md-12">
            <label>Experiência</label>
            <textarea style="height: 100px;" class="form-control" name="sp_habilidadeExperiencias" placeholder="[Datas], [Cargo], [Empresa]  - Este é o lugar para uma breve síntese das responsabilidades principais e conquistas mais impressionantes."></textarea>
        </div>
    </div>

    <br>
    <h5><i>PRODUÇÃO ACADÊMICA (quando aplicável) </i></h5>
    <br>

    <div class="form-group" style="margin-top: 15px;">
        <div class="col-md-12">
            <label>ARTIGOS PUBLICADOS EM REVISTAS INDEXADAS</label>
            <textarea style="height: 100px;" class="form-control" name="sp_producaoArtigos" placeholder="[Título do artigo ], [Nome da revista, ano,  número e páginas da revista]"></textarea>
        </div>
    </div>
    <div class="form-group" style="margin-top: 15px;">
        <div class="col-md-12">
            <label>CAPÍTULOS DE LIVROS</label>
            <textarea style="height: 100px;" class="form-control" name="sp_producaoCapitulosLivros" placeholder="[Nome do capítulo], [Título do livro, ano, número e páginas do livro]"></textarea>
        </div>
    </div>
    <div class="form-group" style="margin-top: 15px;">
        <div class="col-md-12">
            <label>LIVROS</label>
            <textarea style="height: 100px;" class="form-control" name="sp_producaoLiros" placeholder="[Título do livro, ano, número e páginas do livro]"></textarea>
        </div>
    </div>
    <div class="form-group" style="margin-top: 15px;">
        <div class="col-md-12">
            <label>TEXTOS COMPLETOS PUBLICADOS EM ANAIS DE EVENTOS</label>
            <textarea style="height: 100px;" class="form-control" name="sp_producaoTextoAnais" placeholder="[Nome do evento, ano, cidade do evento e instituição organizadora]"></textarea>
        </div>
    </div>
    <div class="form-group" style="margin-top: 15px;">
        <div class="col-md-12">
            <label>RESUMOS PUBLICADOS EM ANAIS DE EVENTOS INTERNACIONAIS</label>
            <textarea style="height: 100px;" class="form-control" name="sp_producaoResumoAnais" placeholder="[Nome do evento, ano, cidade do evento e instituição organizadora]"></textarea>
        </div>
    </div>


    <div class="form-group" style="margin-top: 15px;">
        <div class="col-md-12">
            <label>DEMAIS PRODUÇÕES</label>
            <textarea style="height: 100px;" class="form-control" name="sp_producaoDemaisProducoes" placeholder="[Utilize aqui normas ABNT para citações]"></textarea>
        </div>
    </div>

    <hr>
    ATENÇÃO: O candidato não selecionado terá o prazo máximo de 30 (trinta) dias após a inscrição, para retirar a sua documentação junto à coordenação do curso, mediante apresentação de 2ª via desta ficha, sob pena de descarte do mesmo.

    <hr>
    Declaro estar ciente das condições do presente processo de seleção e declaro, sob as penas da Lei, serem verdadeiras as informações prestadas.	
    <hr>
    <div class="form-group">
        <div class="col-md-4">
            <label>&nbsp;</label>
            <input type="submit" value="ENVIAR CADASTRO" class="form-control btn btn-primary">
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?php $this->util->mascaras() ?>