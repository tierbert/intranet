<!DOCTYPE html>
<html>
    <head>
        <title>Faculdade Guanambi - Formulário de pré-matrícula Pós UniFG</title>
        <meta charset="utf-8">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700" rel="stylesheet">
        <!-- Styles -->
        <!-- <link href="http://177.38.182.246/intranet/assets/tema//css/style.css" rel="stylesheet">  -->
        <style type="text/css">
            body {
                font-size: 1.4em;
                line-height: 1.85714286em;
                -webkit-font-smoothing: antialiased;
                -moz-osx-font-smoothing: grayscale;
                font-family: 'Open Sans', 'Helvetica', 'Arial', sans-serif;
                color: #666666;
                font-weight: normal;
                background-color: #f7f7f7;
            }
            .pos-logo {
                text-align: left;
            }
            .pos-link {
                text-align: right;
                padding-top: 15px;
            }
            @media (max-width: 767px) {
                .pos-logo,.pos-link {text-align: center !important;}
            }
        </style>

        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <!-- Latest compiled and minified JavaScript Bootstrap-->

    </head>
    <body>
        <div class="container" style="max-width: 960px;padding: 60px 0px;">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="pos-logo">
                        <a href="http://posunifg.com.br/">
                            <img src="http://posunifg.com.br/wp-content/themes/posunifg/img/logo-dark.png" style="max-width: 250px;">
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="pos-link">
                        <a href="http://posunifg.com.br/" class="btn btn-default">Voltar para o site da Pós UniFG</a>
                    </div>
                </div>
            </div>
        </div>

        <section style="padding: 0em 3em 3em 3em;">
            <div class="container" style="max-width: 970px;background:#fff;padding: 40px;box-shadow: 0px 0px 20px #ddd;">
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <h2>
                            <span style="font-size: 1.2em;font-weight: bold;">Pós UniFG</span> <br/>
                            <span style="font-size: 1em;">Formulário de pré-matrícula</span>
                        </h2>
                        <hr style="margin:50px 40px 40px 0px">
                    </div>
                </div>
                <?php echo form_open(); ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php
                            //echo $curso->num_rows();
                            if (!is_string($curso) && $curso->num_rows() > 0) {
                                ?>
                                <label>Curso de Interesse</label>
                                <input type="text" disabled="true" value="<?php echo $curso->row('pc_nomeCurso'); ?>" class="form-control">
                                <input name="pos_curso" type="hidden" value="<?php echo $curso->row('pc_codigo'); ?>">
                            <?php } else { ?>
                                <?php echo $this->util->select2('Curso de Interesse (obrigatório)', 'pos_curso', $cursos, $curso, '', '', '', 'required'); ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Nome (obrigatório)</label>
                            <span class="">
                                <input required="true" type="text" name="pos_nome" class="form-control" aria-required="true" aria-invalid="false" />
                            </span> 
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>
                                Data de Nascimento (obrigatório)
                            </label>
                            <input required="true" type="text" name="pos_dataNascimento" value="" class="form-control data" aria-required="true" aria-invalid="false" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>
                                Nacionalidade (obrigatório)
                            </label>
                            <input required="true" type="text" name="pos_nacionalidade" value="" size="40" class="form-control" aria-required="true" aria-invalid="false" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>
                                Naturalidade (obrigatório)
                            </label>
                            <input required="true" type="text" name="pos_naturalidade" value="" size="40" class="   form-control" aria-required="true" aria-invalid="false" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>CPF (obrigatório)</label>
                            <input required="true" type="text" name="pos_cpf" value="" size="40" class="cpf  form-control" aria-required="true" aria-invalid="false" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label >RG (obrigatório)</label>
                            <input required="true" type="text" name="pos_rg" value="" size="40" class="   form-control" aria-required="true" aria-invalid="false" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label >Órgão Emissor (obrigatório)</label>
                            <input required="true" type="text" name="pos_orgaoEmissor" value="" size="40" class="   form-control" aria-required="true" aria-invalid="false" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Estado Civil (obrigatório)</label>

                            <select required="true" name="pos_estadoSivio" class="form-control " aria-required="true" aria-invalid="false">
                                <option value="">Selecione!</option>
                                <option value="Solteiro">Solteiro</option>
                                <option value="Casado">Casado</option>
                                <option value="Divorciado">Divorciado</option>
                                <option value="Viúco">Viúco</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Empresa onde Trabalha</label>
                            <input required="true" type="text" name="pos_empTrabalho" value="" size="40" class="  form-control" aria-invalid="false" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Telefone</label>
                            <input required="true" type="tel" name="pos_telefone" value="" size="40" class="form-control telefone" aria-invalid="false" />
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Celular (obrigatório)</label>
                            <input required="true" type="tel" name="pos_celular" value="" size="40" class=" form-control telefone" aria-required="true" aria-invalid="false" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Pai (obrigatório)</label>
                            <input required="true" type="text" name="pos_pai" value="" size="40" class="   form-control" aria-required="true" aria-invalid="false" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Mãe (obrigatório)</label>
                            <input required="true" type="text" name="pos_mae" value="" size="40" class="   form-control" aria-required="true" aria-invalid="false" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Endereço (obrigatório)</label>
                            <input required="true" type="text" name="pos_endereco" value="" size="40" class="   form-control" aria-required="true" aria-invalid="false" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>CEP (obrigatório)</label>
                            <input required="true" type="text" name="pos_cep" value="" size="40" class="cep form-control" aria-required="true" aria-invalid="false" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>E-mail (obrigatório)</label>
                            <input required="true" type="email" name="pos_email" value="" size="40" class="form-control" aria-required="true" aria-invalid="false" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>
                                Graduação (obrigatório)
                            </label>
                            <input required="true" type="text" name="pos_graduacao" value="" size="40" class="   form-control" aria-required="true" aria-invalid="false" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Instituição Superior onde estuda ou estudou (obrigatório)</label>
                            <input required="true" type="text" name="pos_instituicao" value="" size="40" class="   form-control" aria-required="true" aria-invalid="false" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Forma de Pagamento (obrigatório)</label> 
                            <select required="true" name="pos_formaPagamento" class="form-control" aria-required="true" aria-invalid="false">
                                <option value="12 Parcelas">12 Parcelas</option>
                                <option value="15 Parcelas">15 Parcelas</option>
                                <option value="18 Parcelas">18 Parcelas</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>&nbsp;</label>
                            <input type="submit" class="btn btn-primary form-control" value="ENVIAR DADOS">
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </section>

        <div style="background:#194372;text-align: center;padding: 20px;color: #fff;">
            2018. UniFG
        </div>

        <script>
            $(function () {
                $('.numMatricula').mask("9999999999");
                $(".cpf").mask("999.999.999-99");
                $(".cnpj").mask("99.999.999/9999-99");
                $('.data').mask("99/99/9999");
                $('.cep').mask("99.999-999");
                $('.hora').mask("99:99");

                $('.telefone').focusout(function () {
                    var phone, element;
                    element = $(this);
                    element.unmask();
                    phone = element.val().replace(/\D/g, '');
                    if (phone.length > 10) {
                        element.mask("(99) 99999-999?9");
                    } else {
                        element.mask("(99) 9999-9999?9");
                    }
                }).trigger('focusout');
            });
            /*
             Masked Input plugin for jQuery
             Copyright (c) 2007-2013 Josh Bush (digitalbush.com)
             Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license)
             Version: 1.3.1
             */
            (function (e) {
                function t() {
                    var e = document.createElement("input"), t = "onpaste";
                    return e.setAttribute(t, ""), "function" == typeof e[t] ? "paste" : "input"
                }
                var n, a = t() + ".mask", r = navigator.userAgent, i = /iphone/i.test(r), o = /android/i.test(r);
                e.mask = {definitions: {9: "[0-9]", a: "[A-Za-z]", "*": "[A-Za-z0-9]"}, dataName: "rawMaskFn", placeholder: "_"}, e.fn.extend({caret: function (e, t) {
                        var n;
                        if (0 !== this.length && !this.is(":hidden"))
                            return"number" == typeof e ? (t = "number" == typeof t ? t : e, this.each(function () {
                                this.setSelectionRange ? this.setSelectionRange(e, t) : this.createTextRange && (n = this.createTextRange(), n.collapse(!0), n.moveEnd("character", t), n.moveStart("character", e), n.select())
                            })) : (this[0].setSelectionRange ? (e = this[0].selectionStart, t = this[0].selectionEnd) : document.selection && document.selection.createRange && (n = document.selection.createRange(), e = 0 - n.duplicate().moveStart("character", -1e5), t = e + n.text.length), {begin: e, end: t})
                    }, unmask: function () {
                        return this.trigger("unmask")
                    }, mask: function (t, r) {
                        var c, l, s, u, f, h;
                        return!t && this.length > 0 ? (c = e(this[0]), c.data(e.mask.dataName)()) : (r = e.extend({placeholder: e.mask.placeholder, completed: null}, r), l = e.mask.definitions, s = [], u = h = t.length, f = null, e.each(t.split(""), function (e, t) {
                            "?" == t ? (h--, u = e) : l[t] ? (s.push(RegExp(l[t])), null === f && (f = s.length - 1)) : s.push(null)
                        }), this.trigger("unmask").each(function () {
                            function c(e) {
                                for (; h > ++e && !s[e]; )
                                    ;
                                return e
                            }
                            function d(e) {
                                for (; --e >= 0 && !s[e]; )
                                    ;
                                return e
                            }
                            function m(e, t) {
                                var n, a;
                                if (!(0 > e)) {
                                    for (n = e, a = c(t); h > n; n++)
                                        if (s[n]) {
                                            if (!(h > a && s[n].test(R[a])))
                                                break;
                                            R[n] = R[a], R[a] = r.placeholder, a = c(a)
                                        }
                                    b(), x.caret(Math.max(f, e))
                                }
                            }
                            function p(e) {
                                var t, n, a, i;
                                for (t = e, n = r.placeholder; h > t; t++)
                                    if (s[t]) {
                                        if (a = c(t), i = R[t], R[t] = n, !(h > a && s[a].test(i)))
                                            break;
                                        n = i
                                    }
                            }
                            function g(e) {
                                var t, n, a, r = e.which;
                                8 === r || 46 === r || i && 127 === r ? (t = x.caret(), n = t.begin, a = t.end, 0 === a - n && (n = 46 !== r ? d(n) : a = c(n - 1), a = 46 === r ? c(a) : a), k(n, a), m(n, a - 1), e.preventDefault()) : 27 == r && (x.val(S), x.caret(0, y()), e.preventDefault())
                            }
                            function v(t) {
                                var n, a, i, l = t.which, u = x.caret();
                                t.ctrlKey || t.altKey || t.metaKey || 32 > l || l && (0 !== u.end - u.begin && (k(u.begin, u.end), m(u.begin, u.end - 1)), n = c(u.begin - 1), h > n && (a = String.fromCharCode(l), s[n].test(a) && (p(n), R[n] = a, b(), i = c(n), o ? setTimeout(e.proxy(e.fn.caret, x, i), 0) : x.caret(i), r.completed && i >= h && r.completed.call(x))), t.preventDefault())
                            }
                            function k(e, t) {
                                var n;
                                for (n = e; t > n && h > n; n++)
                                    s[n] && (R[n] = r.placeholder)
                            }
                            function b() {
                                x.val(R.join(""))
                        }
                        function y(e) {
                            var t, n, a = x.val(), i = -1;
                            for (t = 0, pos = 0; h > t; t++)
                                if (s[t]) {
                                                for (R[t] = r.placeholder; pos++
                            < a.length; )
                            if (n = a.charAt(pos - 1), s[t].test(n)) {
                                                            R[t] = n, i = t;
                                                    break
                            }
                            if (pos > a.length)
                            break
                            } else
                            R[t] === a.charAt(pos) && t !== u && (pos++, i = t);
                        return e ? b() : u > i + 1 ? (x.val(""), k(0, h)) : (b(), x.val(x.val().substring(0, i + 1))), u ? t : f
                            }
                            var x = e(this), R = e.map(t.split(""), function (e) {
                                            return"?" != e ? l[e] ? r.placeholder : e : void 0
                            }), S = x.val();
                        x.data(e.mask.dataName, function () {
                                            return e.map(R, function (e, t) {
                                                        return s[t] && e != r.placeholder ? e : null
                            }).join("")
                            }), x.attr("readonly") || x.one("unmask", function () {
                                                        x.unbind(".mask").removeData(e.mask.dataName)
                                }).bind("focus.mask", function () {
                                                                clearTimeout(n);
                                                        var e;
                                                        S = x.val(), e = y(), n = setTimeout(function () {
                                                                    b(), e == t.length ? x.caret(0, e) : x.caret(e)
                        }, 10)
                        }).bind("blur.mask", function () {
                                                                    y(), x.val() != S && x.change()
                            }).bind("keydown.mask", g).bind("keypress.mask", v).bind(a, function () {
                                                                            setTimeout(function () {
                                                                                var e = y(!0);
                                                                                x.caret(e), r.completed && e == x.val().length && r.completed.call(x)
                            }, 0)
                            }), y()
                        }))
                    }})
                    })(jQuery);
                        

                function removeAll(selectBox) {
                    for(let option of selectBox.options) {
                        selectBox.remove(0);
                    }
                }
                // tratando numero de parcelas;

                const curso = document.getElementsByName('pos_curso');
                
                let opt21 = new Option("21 Parcelas", "21 Parcelas");
                let opt24 = new Option("24 Parcelas", "24 Parcelas");

                if (curso[0].value == "ENGSECTRAB"){
                    const pag = document.getElementsByName("pos_formaPagamento")    
                    removeAll(pag[0])
                    pag[0].add(opt21)
                    pag[0].add(opt24)
                }
                curso[0].addEventListener('change', () => {
                     let opt21 = new Option("21 Parcelas", "21 Parcelas");
                     let opt24 = new Option("24 Parcelas", "24 Parcelas");
                     let opt12 = new Option("12 Parcelas", "12 Parcelas");
                     let opt15 = new Option("15 Parcelas", "15 Parcelas");
                    const pag = document.getElementsByName("pos_formaPagamento")  
                    if (curso[0].value == "ENGSECTRAB"){  
                        removeAll(pag[0])
                        pag[0].add(opt21)
                        pag[0].add(opt24)
                    } else {
                        removeAll(pag[0])
                        pag[0].add(opt12)
                        pag[0].add(opt15)
                    }  
                }) 
                        </script>

    </body>
</html>




