<!DOCTYPE html>
<html>
    <head>
        <meta name = "viewport" content = "initial-scale = 1.0, user-scalable = yes" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Refresh" content="300">
        <script type="text/javascript" src="<?php echo base_url('assets'); ?>/tv/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets'); ?>/tv/tv.js"></script>
        <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/tv/tv.css">
        <title>MURAL FG</title>
    </head>
    <body>
        <div class="row" style="width:auto;">
            <div id="tv_fram">
                <div id="tvbox_conten">
                    <div id="tvbox" class="tvbox">
                        <div class="tv_screen">

                            <?php for ($j = 1; $j < 6; $j++) { ?>
                                <div class="screen">
                                    <img src="<?php echo base_url('assets'); ?>/tv/c<?php echo $numCorredor; ?>-<?php echo $j; ?>.jpg">
                                </div>
                            <?php } ?>
                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>


<script type="text/javascript">
    $(function () {
        $('#tvbox').tv({
            play: 6000,
            //pause: 2500,
            mousehoverPause: false,
            transitionFX: 'slide',
            onClickNext: true,
            hideNextPrev: true,
        });
    });
</script>