<!DOCTYPE html>
<html>
    <head>
        <title>Faculdade Guanambi - Intranet</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Crimson+Text:400,400i" rel="stylesheet">
        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/tema/'); ?>/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/tema/'); ?>/css/slick.css">        
        <link href="<?php echo base_url('assets/tema/'); ?>/css/style.css" rel="stylesheet"> 

        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="js/smooth-scroll.min.js"></script>
        <script type="text/javascript" src="js/modernizr.mq.js"></script>
        <script type="text/javascript" src="js/slick.min.js"></script>
    </head>
    <body>
        <div class="container" style="width: 90%">
            <?php $this->load->view($pagina); ?>
        </div>
    </body>
</html>
