<link href="<?php echo base_url('assets/tema/'); ?>bootstrap/css/bootstrap.css" rel="stylesheet"> 



<div class="row" style="padding: 10px;">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <form role="form" class="form-inline" method="post" action="" id="formulario">
            <select class="form-control" name="horario">
                <?php
                if (isset($_POST['horario'])) {
                    ?>
                    <option value = "<?php echo $_POST['horario']; ?>"><?php echo $horarios[$_POST['horario']];
                    ?></option>
                    <?php
                }
                ?>
                <option>Selecione o Curso/Período!</option>
                <?php
                foreach ($horarios as $key => $value) {
                    ?>
                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                    <?php
                }
                ?>
            </select>
            <button class="btn btn-primary" type="submit">Consultar</button>
        </form>
    </div>
</div>
<div class="row" style="padding: 30px; font-size: 8px; font-weight: bold">
    <div class="table-responsive">


        <?php
        if (isset($horario)) {

            $tabela = $dados[$horario];
            $tabela = str_replace('<table', "<table class='table table-condensed' style=' font-size: 12px;font-weight: bold'", $tabela);

            echo $tabela;
        }
        ?>
    </div>
</div>