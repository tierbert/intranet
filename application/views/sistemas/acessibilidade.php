<div class="form-horizontal">
    <?php echo form_open(); ?>
    <h3 class="text-center">REQUERIMENTO PARA ACESSIBILIDADE</h3>

    <hr>
    <p>
        OBSERVAÇÃO: Este requerimento é exclusivo para os estudantes que possuem alguma necessidade especial 
        (deficiência física, auditiva, visual, intelectual) e deverá ser entregue no Protocolo ou pessoalmente no 
        Centro Pedagógico e Psicopedagógico (CPP), situada no Campus, Faculdade Guanambi.
    </p>

    <hr>
    <h4>DADOS PESSOAIS</h4>
    <br>

    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <label>nome</label>
            <input type="text" class="form-control" name="ace_nome" required="true">
        </div>

        <div class="col-md-2 col-sm-2 col-xs-12">
            <label>Sexo</label>
            <select class="form-control" name="ace_sexo" required="true">
                <option value="">SELECIONE O SEXO</option>
                <option value="MASCULINO">MASCULINO</option>
                <option value="FEMININO">FEMININO</option>

            </select>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <label>email</label>
            <input type="email" class="form-control" name="ace_email" required="true">
        </div>
    </div>


    <div class="form-group">
        <div class="col-md-3 col-sm-3 col-xs-12">
            <label>cpf</label>
            <input type="text" class="form-control cpf" name="ace_cpf" required="true">
        </div>

        <div class="col-md-3 col-sm-3 col-xs-12">
            <label>Sexo</label>
            <select class="form-control" name="ace_vinculo" required="true">
                <option value="">VÍNCULO COM A FACULDADE GUANAMBI</option>
                <option value="Estudante">Estudante</option>
                <option value="Servidor">Servidor</option>
                <option value="Sem_Vinculo">Sem Vínculo</option>
            </select>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-6">
            <label>telefone</label>
            <input type="text" class="form-control telefone" name="ace_fixo" required="true">
        </div>

        <div class="col-md-3 col-sm-3 col-xs-6">
            <label>telefone</label>
            <input type="text" class="form-control telefone" name="ace_celular" required="true">
        </div>
    </div>


    <div class="form-group">

        <div class="col-md-8 col-sm-8 col-xs-6">
            <label>logradouro</label>
            <input type="text" class="form-control" name="ace_logradouro" required="true">
        </div>

        <div class="col-md-2 col-sm-2 col-xs-6">
            <label>numero</label>
            <input type="text" class="form-control" name="ace_numero" required="true">
        </div>
        <div class="col-md-2 col-sm-2 col-xs-6">
            <label>bairro</label>
            <input type="text" class="form-control" name="ace_bairro" required="true">
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <label>complemento</label>
            <input type="text" class="form-control" name="ace_complemento">
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6">
            <label>cidade</label>
            <input type="text" class="form-control" name="ace_cidade" required="true">
        </div>
        <div class="col-md-2 col-sm-2 col-xs-6">
            <label>ESTADO</label>
            <input type="text" class="form-control" name="ace_estado" required="true">
        </div>
    </div>
    <hr>
    <h4>DEFICIÊNCIA/NECESSIDADES ESPECÍFICAS</h4>
    <br>


    <div class="form-group">


        <div class="col-md-12 col-sm-12 col-xs-12">
            <label>DEFICIÊNCIA/NECESSIDADES ESPECÍFICAS</label>
            <select class="form-control" name="ace_necessidade" required="true">
                <option value="">SELECIONE A DEFICIÊNCIA/NECESSIDADES ESPECÍFICAS</option>
                <option value="Deficiência Auditiva">Deficiência Auditiva</option>
                <option value="Surdez">Surdez</option>
                <option value="Baixa Visão">Baixa Visão</option>
                <option value="Cegueira">Cegueira</option>
                <option value="Deficiência Física">Deficiência Física</option>
                <option value="Dislexia">Dislexia</option>
                <option value="Autismo">Autismo</option>
                <option value="TDAH">TDAH</option>
                <option value="Altas Habilidade/Superdotação">Altas Habilidade/Superdotação</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12">
            <label>OUTRAS DEFICIÊNCIA/NECESSIDADES ESPECÍFICAS</label>
            <textarea style="height: 100px;" class="form-control" name="ace_necessidadeOutra" placeholder=""></textarea>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <label>REQUER</label>
            <select class="form-control" name="ace_requer" required="true">
                <option value="">SELECIONE</option>
                <option value="Tradutor e Intérprete de Libras">Tradutor e Intérprete de Libras</option>
                <option value="Áudio-descritor">Áudio-descritor</option>
                <option value="Bolsista de Apoio">Bolsista de Apoio</option>
                <option value="Suporte em Tecnologia Assistiva (especificar)">Suporte em Tecnologia Assistiva (especificar)</option>
            </select>
        </div>
    </div>


    <div class="form-group">
        <div class="col-md-12">
            <label>OUTROS ESPECIFICAR</label>
            <textarea style="height: 100px;" class="form-control" name="ace_requerEspecificar" placeholder=""></textarea>
        </div>
    </div>


    <div class="form-group">
        <div class="col-md-12">
            <label>OUTROS ESPECIFICAR</label>
            <textarea style="height: 100px;" class="form-control" name="outrosEspecificar" placeholder=""></textarea>
        </div>
    </div>


    <br>
    <h5><i>INFORMAÇÕES NECESSÁRIAS SOBRE SOLICITAÇÃO DE TRADUTOR E INTÉPRETE DE LIBRAS</i></h5>
    <br>

    <div class="form-group">
        <div class="col-md-12">
            <label>Disciplinas</label>
            <textarea style="height: 100px;" class="form-control" name="ace_habilidadeDisciplinas" placeholder="Adicione aqui disciplinas, dentre as disponíveis no edital (quando aplicável), as quais esteja apto a ministrar."></textarea>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-4">
            <label>&nbsp;</label>
            <input type="submit" value="ENVIAR CADASTRO" class="form-control btn btn-primary">
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?php $this->util->mascaras() ?>