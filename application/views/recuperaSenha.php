<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Faculdade Guanambi - Intranet</title>
        <meta name="description" content="Responsive HTML5 Template">
        <meta name="author" content="webthemez">

        <!-- Mobile Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Favicon -->
        <link rel="shortcut icon" href="<?php echo base_url('assets/tema/'); ?>images/favicon.ico">

        <link href="<?php echo base_url('assets/tema/'); ?>bootstrap/css/bootstrap.css" rel="stylesheet"> 
        <link href="<?php echo base_url('assets/tema/'); ?>fonts/font-awesome/css/font-awesome.css" rel="stylesheet"> 
        <link href="<?php echo base_url('assets/tema/'); ?>css/animations.css" rel="stylesheet"> 
        <link href="<?php echo base_url('assets/tema/'); ?>css/styleLogin.css" rel="stylesheet"> 
        <link href="<?php echo base_url('assets/tema/'); ?>css/custom.css" rel="stylesheet">

        <link href="<?php echo base_url('assets/tema/'); ?>css/style4.css" rel="stylesheet">
        <link href="<?php echo base_url('assets/tema/'); ?>css/demo.css" rel="stylesheet">


        <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>jsc/index.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>plugins/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>plugins/modernizr.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>plugins/isotope/isotope.pkgd.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>plugins/jquery.backstretch.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>plugins/jquery.appear.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>js/custom.js"></script>


        <!-- VIDEO -->
        <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>/js/plugins/jquery.fitvids.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>/js/jquery.bxslider.js"></script>


        <style type="text/css">




            #box{


                font-style:normal;
                color:#ffffff;
                background:#c50000;
                border:0px solid #ea326f;
                text-shadow:0px 0px 0px #ffffff;
                box-shadow:0px 0px 6px #454545;
                -moz-box-shadow:0px 0px 6px #454545;
                -webkit-box-shadow:0px 0px 6px #454545;
                border-radius:45px 7px 45px 7px;
                -moz-border-radius:45px 7px 45px 7px;
                -webkit-border-radius:45px 7px 45px 7px;

                padding:10px 30px;
                cursor:pointer;
                margin:0 auto;

                active{
                    cursor:pointer;
                    position:relative;
                    top:2px;
                }
            }

            #box2{


                font-style:normal;
                color:#ffffff;
                background:#c50000;
                border:0px solid #ea326f;
                text-shadow:0px 0px 0px #ffffff;
                box-shadow:0px 0px 6px #454545;
                -moz-box-shadow:0px 0px 6px #454545;
                -webkit-box-shadow:0px 0px 6px #454545;
                border-radius:45px 7px 45px 7px;
                -moz-border-radius:45px 7px 45px 7px;
                -webkit-border-radius:45px 7px 45px 7px;
                text-align: center; 
                padding:10px 30px;
                cursor:pointer;
                margin:0 auto;

                active{
                    cursor:pointer;
                    position:relative;
                    top:2px;
                }
            }
            #logoe{
                width: 400px;
                height: 92px;

            }  


            #formc {
                position: absolute;
                top: 0px;
                right: px;
                bottom: 0px;
                left: 230px;
                width: 360px;
                height: 260px;
                margin: auto;
                box-shadow: 0px 0px 10px black;
                padding: 20px;
                box-sizing: border-box;


            }

            #modalc{
                opacity:0.97;
                display:none;

                left:0;
                top:0;
                z-index:8000;
                background-color:#000;


            }





            .caption-insc {position: absolute; top: 50%; left: 34%; z-index: 20; text-align: left; -webkit-transform: translate(-50%,-50%); -moz-transform: translate(-50%,-50%); transform: translate(-50%,-50%);text-shadow: 2px 2px 2px rgba(0, 0, 0, 0.24);}
            .caption-insc h1 {text-transform: uppercase;}
            .caption-insc h1 span {font-size: inherit; line-height: inherit; font-weight: inherit;}
            .caption-insc h3{line-height:34px;font-size: 30px;}
            #ban {
                filter: gray; /* IE6-9 */
                -webkit-filter: grayscale(1); /* Google Chrome, Safari 6+ & Opera 15+ */
                -webkit-box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
                -moz-box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
                box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
                margin-bottom:20px;
            }

            #ban:hover {
                filter: none; /* IE6-9 */
                -webkit-filter: grayscale(0); /* Google Chrome, Safari 6+ & Opera 15+ */

            }



        </style>




    </head>

    <body class="no-trans">
        <!-- scrollToTop --> 
        <div class="scrollToTop"><i class="icon-up-open-big"></i></div>

        <!-- header start --> 
        <header class="header fixed clearfix navbar navbar-fixed-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">

                        <!-- header-left start --> 
                        <div class="header-left">

                            <!-- logo -->
                            <div class="logo smooth-scroll">
                                <a href="<?php echo site_url(); ?>"><img id="logo" src="<?php echo base_url('assets/tema/'); ?>images/logo.jpg" alt="Worthy"></a>
                            </div>

                            <!-- name-and-slogan -->


                        </div>
                        <!-- header-left end -->

                    </div>
                    <div class="col-md-9">

                        <!-- header-right start --> 
                        <div class="header-right">

                            <!-- main-navigation start --> 
                            <div class="main-navigation animated">

                                <!-- navbar start --> 
                                <nav class="navbar navbar-default" role="navigation">
                                    <div class="container-fluid">

                                        <!-- Toggle get grouped for better mobile display -->
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                                                <span class="sr-only"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>

                                        <!-- Collect the nav links, forms, and other content for toggling -->
                                        <div class="collapse navbar-collapse scrollspy smooth-scroll" id="navbar-collapse-1">
                                            <ul class="nav navbar-nav navbar-right">

                                                <li class="active"><a href="#banner">Home</a></li>
                                                <li><a href="#cursos">Ajuda</a></li>

                                            </ul>
                                        </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <ul class="cb-slideshow">
            <li><span>Image 01</span><div><h3></h3></div></li>
            <li><span>Image 02</span><div><h3></h3></div></li>
            <li><span>Image 03</span><div><h3></h3></div></li>
            <li><span>Image 04</span><div><h3></h3></div></li>
        </ul>
        <div class="login-container">
            <div id="output"></div>
            <label>RECUPERAÇÃO DE SENHA</label>
            <br><br>
            <br>
            <div class="form-box">
                <div>
                    <?php echo validation_errors(); ?>
                </div>

                <?php echo form_open() ?>
                <select name="tipo" id="tipo" class="form-control" required="true">
                    <option value="1">ALUNO</option>
                </select>
                <BR><BR>
                <div>
                    <div class="col-sm-14">

                        <label>NÚMERO DE MATRÍCULA</label>
                        <input name="matricula" class="form-control matricula" type="text" required="true"> 
                    </div>
                </div>
                <br>
                <a href="<?php echo site_url(); ?>">
                    Voltar
                </a>
                <div>
                    <button class="btn btn-info btn-block" type="submit" value=ENTRAR >RECUPERAR</button>
                </div>            
                <?php echo form_close() ?>
            </div>
        </div>
    </body>
</html>
