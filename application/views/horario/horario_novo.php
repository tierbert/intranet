
<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            NOVO HORÁRIO
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('adm/horario'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal>    
        <div>
            <?php echo validation_errors(); ?>
        </div>

        <?php echo form_open() ?>
        <?php $this->util->input2('Hora disciplina:', 'hor_disciplina', $object->hor_disciplina); ?>
        <?php $this->util->input2('Hora turno:', 'hor_turno', $object->hor_turno); ?>
        <?php $this->util->input2('Hora turma:', 'hor_turma', $object->hor_turma); ?>
        <?php $this->util->input2('Hora prof:', 'hor_prof', $object->hor_prof); ?>

        <div>
            <div class=form-group>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <input type=submit  value=Salvar class='btn btn-success form-control'>
                </div>

            </div>            
            <?php echo form_close() ?>
        </div>
    </div>
</div>