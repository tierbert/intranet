
<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            Vincular Professor
        </div>
        <div class="col-md-6 col-xs-6 text-right">
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal>    
        <div>
            <?php echo validation_errors(); ?>
        </div>
        <?php echo form_open() ?>
        <div class="form-group">
            <?php $this->util->input2('Disciplina', 'hor_disciplina', $object->hor_disciplina); ?>
            <?php $this->util->input2('Professor', 'hor_prof', $object->hor_prof); ?>
            <?php $this->util->input2('CH Teórica', 'hor_chTeorica', $object->hor_chTeorica, '', 2); ?>
        </div>
        <div class="form-group">
            <br>
            <?php $this->util->select2('Cod. Professor', 'hor_idProfessor', $professores, $object->hor_idProfessor); ?>
        </div>

        <div class="col-md-2">
            <label>&nbsp;</label>
            <input type=submit  value=Salvar class='btn btn-success form-control'>
        </div>  
    </div>          
    <?php echo form_close() ?>
</div>
