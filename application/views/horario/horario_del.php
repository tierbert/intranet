<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            EXCLUIR DISCIPLINA
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/horario/'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal>    
        <div>
            <?php echo validation_errors(); ?>
        </div>
        <?php echo form_open() ?>
        <div class="form-group">
            <?php echo form_open($this->uri->uri_string(), 'post') ?>
            Tem certeza que quer excluir: <?php echo $object->hor_id ?>?
            <br>
            <?php echo form_submit('agree', 'Sim') ?>
            <?php echo form_submit('disagree', 'Não') ?>
            <?php echo form_close() ?>
        </div>  
    </div>          
    <?php echo form_close() ?>
</div>




