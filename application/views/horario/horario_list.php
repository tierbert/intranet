<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            DISCIPLINAS
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a class='btn btn-info' href='<?php echo site_url('adm/horario/criar/') ?>'>
                NOVO HORÁRIO
            </a>    
            <a href="<?php echo site_url('adm/painel/solicitacoes'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <?php $this->load->view('botoes') ?>


    <div class="row">
        <div class="form-horizontal">
            <?php echo form_open(); ?>
            <div class="form-group">
                <div class="col-md-5">
                    <label>PROFESSOR</label>
                    <input name="nome" value="<?php if (isset($post) && $post['nome'] != '') echo $post['nome']; ?>" type="text" class="form-control" >
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <input class=" btn btn-success form-control" type="submit" value="PESQUISAR">
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>



    <div class="row" style="margin-top: 10px;">

        <?php if (count($dados) != 0) { ?>
            <table class='table table-bordered table-striped table-condensed table-hover table-responsive tabela-aredondada'>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome da Disciplina</th>
                        <th>Semestre / Turno</th>
                        <th>Turma</th>
                        <th>Nome do Professor</th>
                        <th>Código Prof.</th>
                        <th>CH Teórica</th>
                        <th>Semestre</th>
                        <th>Status</th>
                        <th>Solicitaçes</th>
                        <th>Ação</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;
                    foreach ($dados as $dado) {

                        
                        
                        $this->db->where('sd_idDisciplina', $dado->hor_id);
                        $solic = $this->db->get('solicitacao_disciplinas');

                        if ($solic->num_rows() == 0) {
                            continue;
                        }
                        
                        if ($dado->hor_idProfessor != NULL) {
                            continue;
                        }
                        
                        $i++;
                        
                        ?><tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $dado->hor_disciplina ?> </td>
                            <td><?php echo $dado->hor_turno ?> </td>
                            <td><?php echo $dado->hor_turma ?> </td>
                            <td><?php echo $dado->hor_prof ?> </td>
                            <td><?php echo $dado->hor_idProfessor ?> </td>
                            <td><?php echo $dado->hor_chTeorica ?> </td>
                            <td><?php echo $dado->hor_semestre ?> </td>

                            <td>
                                <?php
                                if ($dado->hor_status == 1) {
                                    echo 'ENTREGUE';
                                }
                                if ($dado->hor_status == '0') {
                                    echo 'NÃO ENTREGUE';
                                }
                                //echo $dado->hor_status;
                                ?> 

                            </td>
                            <td><?php echo $solic->num_rows(); ?></td>
                            <td>
                                <a class='btn btn-xs btn-warning' href="<?php echo site_url('adm/horario/horProf/' . $dado->hor_id) ?>">
                                    Editar
                                </a>
                                <a class='btn btn-xs btn-primary' href="<?php echo site_url('adm/horario/horStatus/' . $dado->hor_id) ?>">
                                    Status
                                </a>
                                <a class='btn btn-xs btn-danger' href="<?php echo site_url('adm/horario/delete/' . $dado->hor_id) ?>">
                                    Deletar
                                </a>
                            </td>

                        </tr>
                    <?php } ?>

                </tbody>
            </table>
            <ul class='pagination pagination-small'>
                <li>
                    <?php echo $paginacao; ?>
                </li>
            </ul>
        <?php } else { ?>
            <h4>Nenhum registro encontrado</h4>
        <?php } ?>

    </div>