<!DOCTYPE html>
<html>
    <head>
        <title>Faculdade Guanambi - Intranet</title>
        <meta charset="utf-8">
        <!-- Latest compiled and minified CSS 
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/bootstrap337/css/bootstrap.min.css" >



        <!-- Fonts 
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Crimson+Text:400,400i" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        -->
        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/tema/'); ?>/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/tema/'); ?>/css/slick.css">        
        <link href="<?php echo base_url('assets/tema/'); ?>/css/style.css" rel="stylesheet"> 



        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

     
        <!-- Latest compiled and minified JavaScript Bootstrap
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        -->
        <script src="<?php echo base_url(); ?>/assets/bootstrap337/js/bootstrap.min.js"></script>
    </head>
    <body>
        <header id="header">
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">               
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand smoothScroll" href="<?php echo site_url('aluno/indexaluno'); ?>">
                            <img alt="Modus" src="<?php echo base_url(); ?>/assets/img/logo.png">
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="main-navbar">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"><a class="smoothScroll" href="<?php echo site_url('/aluno/indexaluno'); ?>"> Home <span class="sr-only">(current)</span></a></li>
                            <li><a class="smoothScroll" href="<?php echo site_url('aluno/indexaluno/sair'); ?>"> Sair</a></li>
                            <!--<li><a class="smoothScroll" href="#services">Services</a></li>
                            <li><a class="smoothScroll" href="#clients">Clients</a></li>
                            <li><a class="smoothScroll" href="#contact">Contact</a></li> -->
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <main style="background: #f4f4f4">
            <section>
                <!-- NÃO PODE RETIRAR-->
            </section>

            <section id="services" style="margin-bottom: 50px">

                <section  class="home-section bg-gray container" style="background: #f4f4f4; width: 90%">
                    <?php $this->load->view($pagina); ?>
                </section> 

            </section>

            <section id="contact">          
                <div class="container">                 
                    <div class="row">                   
                        <div class="col-md-6"> 
                            <small>
                                <br><br><br>
                                Desenvolvido por:<br> Setor de Tecnologia da Informação
                            </small> 
                        </div>
                        <div class="col-md-6">                      
                            <div class="contact-subsection">                            
                                <h2 class="sub-section-title">Fale Conosco</h2> <br>
                                <small> 
                                    <i class="glyphicon glyphicon-home " aria-hidden="true"></i> Av. Pedro Felipe Duarte, nº 4911, Bairro São Sebastião.<br>
                                    <i class="glyphicon glyphicon-phone-alt " aria-hidden="true"></i> Alunos: (77) 3451-8429 <br>
                                    <i class="glyphicon glyphicon-phone-alt " aria-hidden="true"></i> Comunidade Externa: (77) 3451-8400
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <footer class="navbar navbar-inverse navbar-fixed-bottom rodapeN">
                <div class="container">
                    <div class="row">                        
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-4">
                            <small class="copyright">Copyright © 2017 - Faculdade Guanambi</small>
                        </div>
                        <div class="col-sm-4">
                        </div>
                    </div>          
                </div> 
            </footer> 

            <!-- Scripts -->


            <!-- Custom JS -->
            <script src="js/custom.js"></script>
        </main>
    </body>
</html>


<!-- Section: works -->

<!-- Rodapé-->




