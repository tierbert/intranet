<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            ESTÁGIO DE PSICOLOGIA
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('aluno/indexaluno'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">


    <h3 class="panel-title" style="font-weight: bold">
        <br>
        SELEÇÃO DOS CAMPOS DE PSICOLOGIA ESPECÍFICO I , II e III – 2018.1
        <br>
        &nbsp;
    </h3>
    <div class="row">
        <div class="col-md-12">

            <table class="table table-striped table-bordered" style="color: black" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th class="text-center">Tipo</th>
                        <th class="text-center">Local</th>
                        <th class="text-center">Dia / Período</th>
                        <th class="actions"></th>
                    </tr>
                </thead>
                <tbody >
                    <tr class=" ">
                        <?php
                        $dados1 = $this->estag__agendadosmodel->getByAlunoTipo($idAluno, 'campo');
                        $agendamento1 = $dados1->row();
                        if ($agendamento1) {
                            ?>
                            <td>Campo / Orientação</td>
                            <td><?php echo $agendamento1->el_nome; ?></td>
                            <td><?php echo $agendamento1->ev_periodo; ?></td>
                            <td> 
                                
                            </td>
                        <?php } else { ?>
                            <td>Campo / Orientação</td>
                            <td></td><td></td>
                            <td>
                                <a href="<?php echo site_url('aluno/EstagioPsic/seleciona/1/1'); ?>" class="btn btn-primary btn-xs">  
                                    SELECIONAR ESPECÍFICO I 
                                </a>
                                <a href="<?php echo site_url('aluno/EstagioPsic/seleciona/1/2'); ?>" class="btn btn-primary btn-xs">  
                                    SELECIONAR ESPECÍFICO II
                                </a>
                                <a href="<?php echo site_url('aluno/EstagioPsic/seleciona/1/3'); ?>" class="btn btn-primary btn-xs">  
                                    SELECIONAR ESPECÍFICO III
                                </a>
                            </td>
                        <?php } ?>
                    </tr>
                    

                </tbody>
            </table>
        </div>
    </div>
</div>
