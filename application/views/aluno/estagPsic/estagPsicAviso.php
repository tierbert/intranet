<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            ESTÁGIO DE PSICOLOGIA
        </div>
        <div class="col-md-6 col-xs-6 text-right">


        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">




    <h2>
        SELEÇÃO DOS CAMPOS DE ESTÁGIO DE PSICOLOGIA 
        ESPECÍFICO I E II – 2018.1
    </h2>
    <br>
    <p>

        Prezados (as) alunos (as),

    </p>
    <br>
    <p>
        O Núcleo de Estágios inicia o processo de organização dos Estágios Específicos I e II do semestre 2018.1 e, para tanto, cada acadêmico deverá fazer a escolha do campo de estágio através do sistema Intranet no site da FG, sendo que antes de participar do processo seletivo, precisam estar cientes dos seguintes critérios:
    </p>
    <ul style="list-style-type: inherit; padding-left: 40px;">
        <li>
            A vaga previamente selecionada estará diretamente vinculada à aprovação nas disciplinas que são pré-requisitos;
        </li>
        <li>
            Este procedimento não substitui a obrigatoriedade da matrícula acadêmica e financeira no estágio escolhido, portanto, o(a) aluno(a)deverá efetuar a matrícula, via sistema Jacad, antes de realizar a escolha; 
        </li>
        <li>
            Alunos (as) com pendências, como quebra de pré-requisito, deverão procurar a Coordenação do Curso ou o Núcleo de Estágios antes de fazer a escolha do campo, pois serão automaticamente eliminados da seleção, tendo em vista que não constam na relação de acadêmicos previstos para ingressarem os respectivos estágios;
        </li>
        <li>
            A conclusão da escolha do campo de estágio será feita quando o aluno entregar o Termo de Compromisso (Contrato de Estágio), o que deverá acontecer na sala 05 do campus da FG, entre os dias 15/01/18 a 26/01/18, em horário comercial (das 08 às 12h e de 14 às 18h).

        </li>
    </ul>
    <br>
    <p>
        Atenciosamente,
    </p>
    <br>
    <p>
        Núcleo de Estágios da FG.
    </p>
    <br>
    <a href="<?php echo site_url('aluno/EstagioPsic'); ?>" class="btn btn-success btn-lg ok disabled">
        CONTINUAR
    </a>

    <script>
        $(function () {
            setTimeout(function () {
                $('.ok').removeClass('disabled');
            }, 3000);
        })


    </script>