<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            ESTÁGIO DE PSICOLOGIA
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('aluno/EstagioPsic'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <h3 class="panel-title" style="font-weight: bold">
                    SELECIONE O LOCAL E PERÍODO
                </h3>
                <hr>
                <?php foreach ($locais->result() as $local) { ?>
                    <div class="panel panel-default">
                        <div class="panel-heading tour-step tour-step-7">
                            <br>
                            <h3 class="panel-title" style="font-weight: bold">
                                <?php echo $local->el_nome; ?>
                            </h3>
                            <h5 style="margin-top: 15px;"><?php echo $local->el_status ?></h5>
                            <br>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-hover table-striped table-bordered" style="color: black" cellspacing="0" cellpadding="0">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Tipo</th>
                                                <th class="text-center">Local</th>
                                                <th class="text-center">Dia / Período</th>
                                                <th class="text-center">Vagas</th>
                                                <th class="text-center"></th>
                                            </tr>
                                        </thead>
                                        <tbody >
                                            <?php
                                            $agendamento1 = $this->estag__vagasmodel->getBylocal($local->el_id, $tipo);
                                            foreach ($agendamento1->result() as $vaga) {
                                                $agendados = $this->estag__agendadosmodel->agendadosVaga($vaga->ev_id, $tipo);
                                                ?>
                                                <tr class=" <?php echo $agendados->num_rows() >= $vaga->ev_vagas ? 'danger' : 'success'; ?> ">
                                                    <td>Campo / Orientação</td>
                                                    <td><?php echo $vaga->el_nome; ?></td>
                                                    <td><?php echo $vaga->ev_periodo; ?></td>
                                                    <td><?php echo ($vaga->ev_vagas - $agendados->num_rows()) . ' de ' . $vaga->ev_vagas; ?></td>
                                                    <td> 
                                                        <?php if ($agendados->num_rows() >= $vaga->ev_vagas) { ?>
                                                            <a class="btn btn-danger btn-xs">
                                                                VAGAS ESGOTADAS
                                                            </a>
                                                        <?php } else { ?>
                                                            <a href="<?php echo site_url('aluno/EstagioPsic/selecionar/' . $vaga->ev_id); ?>" class="btn btn-primary btn-xs ">  
                                                                &nbsp;
                                                                SELECIONAR
                                                            </a>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            <?php }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> 
                    </div> 
                <?php }
                ?>
            </div>
        </div>
    </div>    
</div>
