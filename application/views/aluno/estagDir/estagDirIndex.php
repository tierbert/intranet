<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            ESTÁGIO DE DIREITO
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('aluno/indexaluno'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">


    <h3 class="panel-title" style="font-weight: bold">
        <br>
        SELEÇÃO DOS CAMPOS DE DIREITO SAJU I, II, III e VI – 2019.2
        <br>
        &nbsp;
    </h3>
    <div class="row">
        <div class="col-md-12">

            <div>
                    <a href="<?php echo site_url('aluno/EstagioDir/seleciona/1/1'); ?>" class="btn btn-primary btn-xs">  
                        SELECIONAR SAJU I - CEJUSC
                    </a>
                    <a href="<?php echo site_url('aluno/EstagioDir/seleciona/1/2'); ?>" class="btn btn-primary btn-xs">  
                        SELECIONAR SAJU II - NPJ
                    </a>
                    <a href="<?php echo site_url('aluno/EstagioDir/seleciona/1/3'); ?>" class="btn btn-primary btn-xs">  
                        SELECIONAR SAJU III - NPJ
                    </a>
                    <a href="<?php echo site_url('aluno/EstagioDir/seleciona/1/4'); ?>" class="btn btn-primary btn-xs">  
                        SELECIONAR SAJU IV - NPJ
                    </a>
            </div>

            <?php
            $dados1 = $this->estag__agendados_dirmodel->getByAlunoTipo($idAluno, 'campo');
            $agendamento1 = $dados1->result();
            if ($agendamento1) { ?>

            <hr/>

            <h3 class="panel-title" style="font-weight: bold">ESTÁGIOS JÁ AGENDADOS</h3>

            <br>

            <table class="table table-striped table-bordered" style="color: black" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th class="text-center">Tipo</th>
                        <th class="text-center">Local</th>
                        <th class="text-center">Dia / Período</th>
                        <th class="actions"></th>
                    </tr>
                </thead>
                <tbody >
                    <?php 
                    $tipagemSaju = array(
                        array('tipo' => 1, 'nome' => 'SAJU I - CEJUSC'),
                        array('tipo' => 2, 'nome' => 'SAJU II - NPJ'),
                        array('tipo' => 3, 'nome' => 'SAJU III - NPJ'),
                        array('tipo' => 4, 'nome' => 'SAJU IV - NPJ'),
                    ); 
                    ?>
                    <?php foreach($agendamento1 as $i => $agendamento){ ?>
                    <tr class=" ">
                            
                            <td><?php echo $tipagemSaju[$agendamento->el_tipo-1]['nome']; ?></td>
                            <td><?php echo $agendamento->el_nome; ?></td>
                            <td><?php echo $agendamento->ev_periodo; ?></td>
                            <td><a href="<?php echo (site_url('aluno/EstagioDir/delAgendamento/').$agendamento->ea_id); ?>" class="btn btn-primary btn-xs" id="excluir-agendamento"> Excluir Agendamento </a>

                       
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
             <?php } ?>
        </div>
    </div>
</div>

<script type="">
    
    function stopDefAction(event) {
        evt.preventDefault();   
    }
</script>
