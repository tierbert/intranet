<section id="intro" class="intro">
    <div class="slogan" class="col-md-12 col-md-12 ">
        <div style="background: rgba(254,254,254,0.3);" align="center"> 
            <img class="img-circle" id="img_logo" src="<?php echo base_url('assets/'); ?>img/logo.png">
            <h1 style="color:#ffffff; text-shadow: 1px 1px 1px #000000">LOGIN ALUNO </h1>

            <div class="container" style="width: 40%">


                <div id="div-forms">

                    <!-- Begin # Login Form -->
                    <form id="login-form" action="" method="post">
                        <div class="modal-body">
                            <div id="div-login-msg">

                                <span id="text-login-msg"><b>Digite seu Login e senha.</b></span>
                            </div>
                            <input name="matricula" id="login_username" class="form-control" type="text" placeholder="Nº de Matricula" required>
                            <input name="senha" id="login_password" class="form-control" type="password" placeholder="Senha" required>
                            <div class="checkbox" style="text-align:left">
                                <label>
                                    <b>
                                        <a href="<?php echo site_url('inicio/recuperaAl'); ?>">
                                            Esqueci minha senha.</a>
                                    </b>
                                </label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div>
                                <button type="submit" class="btn btn-skin scroll btn-lg btn-block">Login</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>

            <br/>
            <br/>
            <br/>
        </div>
        <div class="col-md-3"></div>



    </div>
</section>




