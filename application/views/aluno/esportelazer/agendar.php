<?php
/*
  if ($semana == '') {
  $semana = date('W') - 1;
  $day = date('w');
  $domingo = date('m-d-Y', strtotime('-' . $day . ' days'));
  $datas = array(
  '1' => date('d-m-Y', strtotime('+' . (1 - $day) . ' days')), //segunda
  '2' => date('d-m-Y', strtotime('+' . (2 - $day) . ' days')), // terca
  '3' => date('d-m-Y', strtotime('+' . (3 - $day) . ' days')), // quarta
  '4' => date('d-m-Y', strtotime('+' . (4 - $day) . ' days')), // quinta
  '5' => date('d-m-Y', strtotime('+' . (5 - $day) . ' days')), //sexta
  '6' => date('d-m-Y', strtotime('+' . (6 - $day) . ' days')), //sabado
  );
  } else {

  $dias = ($semana * 7) - 1;
  $diaDomingo = date('Y-m-d', strtotime('2018-01-01' . " + $dias  days"));

  $day = date('w', strtotime($diaDomingo));
  $domingo = date('m-d-Y', strtotime('-' . $day . ' days'));
  $datas = array(
  '1' => date('d-m-Y', strtotime($diaDomingo . '+' . (1 - $day) . ' days')), //segunda
  '2' => date('d-m-Y', strtotime($diaDomingo . '+' . (2 - $day) . ' days')), // terca
  '3' => date('d-m-Y', strtotime($diaDomingo . '+' . (3 - $day) . ' days')), // quarta
  '4' => date('d-m-Y', strtotime($diaDomingo . '+' . (4 - $day) . ' days')), // quinta
  '5' => date('d-m-Y', strtotime($diaDomingo . '+' . (5 - $day) . ' days')), //sexta
  '6' => date('d-m-Y', strtotime($diaDomingo . '+' . (6 - $day) . ' days')), //sabado
  );
  }
 */

$semana = date('W') - 1;
$day = date('w');
$domingo = date('m-d-Y', strtotime('-' . $day . ' days'));
$datas = array(
    '1' => date('d-m-Y', strtotime('+' . (1 - $day) . ' days')), //segunda
    '2' => date('d-m-Y', strtotime('+' . (2 - $day) . ' days')), // terca
    '3' => date('d-m-Y', strtotime('+' . (3 - $day) . ' days')), // quarta
    '4' => date('d-m-Y', strtotime('+' . (4 - $day) . ' days')), // quinta
    '5' => date('d-m-Y', strtotime('+' . (5 - $day) . ' days')), //sexta
    '6' => date('d-m-Y', strtotime('+' . (6 - $day) . ' days')), //sabado
);

$volta = '';
$avanca = '';
$semanaAtual = date('W') - 1;
if ($semana == $semanaAtual) {
    $volta = 'disabled';
} elseif ($semana > $semanaAtual + 1) {
    $avanca = 'disabled';
}
if ($semana > $semanaAtual + 2) {
    echo "<script>;window.location.href='" . site_url('aluno/esportelazer/agendar/') . "'</script>";
}
?>
<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            <span class="glyphicon glyphicon-flag"></span>
            Esporte e Lazer
        </div>
        <div class="col-md-2">

            <?php
            $hidden = 1;
            if ($hidden == 0) {
                ?>
                <a href="<?php echo site_url('aluno/esportelazer/agendar/' . ($semana - 1)); ?>" class=" btn btn-primary <?php echo $volta; ?>">
                    <span class="glyphicon glyphicon-backward"></span>
                </a>
                <a href="<?php echo site_url('aluno/esportelazer/agendar/'); ?>" class=" btn btn-primary ">
                    <span class="glyphicon glyphicon-home"></span>
                </a>
                <a href="<?php echo site_url('aluno/esportelazer/agendar/' . ($semana + 1)); ?>" class=" btn btn-primary <?php echo $avanca; ?>">
                    <span class="glyphicon glyphicon-forward"></span>
                </a>
            <?php } ?>


        </div>
        <div class="col-md-4 text-right">

            <a href="<?php echo site_url('aluno/esportelazer'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">

    <table class="table table-striped table-bordered table-hover">
        <tbody style="background: white">
            <tr >
                <th></th>
                <th class="text-center">SEGUNDA <br> <?php echo $datas['1']; ?></th>
                <th class="text-center">TERÇA <br> <?php echo $datas['2']; ?></th>
                <th class="text-center">QUARTA <br> <?php echo $datas['3']; ?></th>
                <th class="text-center">QUINTA <br> <?php echo $datas['4']; ?></th>
                <th class="text-center">SEXTA <br> <?php echo $datas['5']; ?></th>
                <th class="text-center">SÁBADO <br> <?php echo $datas['6']; ?></th>
            </tr>
        </tbody>
        <?php
        foreach ($turnos->result() as $turno) {
            ?>
            <tr>
                <th>
                    <?php echo '<br>' . $turno->tur_horario; ?>
                </th>
                <?php
                for ($dia = 1; $dia < 7; $dia++) {
                    $horario = $this->esportemodel->getHorario($turno->tur_id, $dia);
                    ?>
                    <td>
                        <?php
                        if ($horario->num_rows() > 0) {
                            $dados = $horario->row();
                            $agenda = $this->esportemodel->getByHorarioData($dados->hor_id, $this->util->data($datas[$dia]));
                            if ($agenda->num_rows() > 0) {
                                echo '<span style="color: blue">RESERVADO</span>';
                                ?>
                                <hr style="margin: 5px 0 5px 0;">
                                <?php
                            } else {
                                echo '<span style="color: green">HORÁRIO LIVRE</span>';

                                echo form_open();
                                ?>
                                <hr style="margin: 5px 0 5px 0;">

                                <select required="true" name="eag_modalidade">
                                    <option value="">Modalidade</option>
                                    <option value="Futsal">Futsal</option>
                                    <option value="Volei">Volei</option>
                                    <option value="Basquete">Basquete</option>
                                </select>

                                <input type="hidden" value="<?php echo $dados->hor_id; ?>" name="eag_idHorario">
                                <input type="hidden" value="<?php echo $this->util->data($datas[$dia]); ?>" name="eag_data">
                                <button type="submit"  class = "btn btn-success btn-xs" style = "font-size: 12px;">
                                    Reservar
                                </button>
                                <?php
                                echo form_close();
                            }
                            ?>
                            <!--
                            <a href="<?php echo site_url('adm/tcc//horDel/' . $dados->hor_id); ?>"  class="btn btn-danger btn-xs" style="font-size: 12px;">
                                Excluir
                            </a>
                            -->
                            <?php
                        } else {
                            ?>

                        <?php } ?>

                    </td>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>
</div>



<!--
<style>
    .table-striped>tbody>tr:nth-child(odd)>td, 
    .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #5cb85c;
    }
    .table-striped2>tbody>tr:nth-child(odd)>td, 
    .table-striped2>tbody>tr:nth-child(odd)>th {
        background-color: #5cb85c;
    }

</style>

-->