<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            ESPORTE E LAZER - AGENDAMENTO DE QUADRA POLIESPORTIVA
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('aluno/indexaluno'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 300px;">
    <div class="row">

        <div class="col-md-8 col-sm-7 col-xs-7 text-center">
            <h2 class="section-heading">Minhas Reservas</h2>
            <hr>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <table class="table table-striped table-bordered" style="color: black" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th class="text-center">DATA</th>
                            <th class="text-center">HORÁRIO</th>
                            <th class="text-center">MODALIDADE</th>
                        </tr>
                    </thead>
                    <tbody >
                        <?php
                        $dias = $this->util->diasSemana();
                        foreach ($reservas->result() as $dado) {
                            ?>
                            <tr>
                                <td><?php echo $this->util->data($dado->eag_data) ?> </td>
                                <td><?php echo $this->util->data($dado->tur_horario) ?> </td>
                                <td><?php echo $dado->eag_modalidade ?> </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div> 
        </div>

        <div class="col-md-4 col-sm-5 col-xs-5 text-center"> 
            <a href="<?php echo site_url('aluno/esportelazer/agendar'); ?>" class="btn btn-primary btn-lg" style="font-size: 20px;">  
                <span class="glyphicon glyphicon-flag"></span>
                <br>
                RESERVE AQUI!
            </a>                           
            <hr>                
        </div>
    </div>
</div>