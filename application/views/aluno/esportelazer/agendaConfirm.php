<link href="<?php echo base_url(); ?>assets/css/css/cucco.css" media="screen" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<section id="ACC"  >
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="container">
                    <div class="row">

                        <div id="calendar" class="col-sm-10  animated animated-sm bounceInUp">
                            <div class="pull-right tour-step tour-step-1">
                                <h6 style="">
                                    <a href="<?php echo site_url('aluno/esportelazer/agendar'); ?>" class="btn btn-primary"> 
                                        Voltar
                                    </a>
                                </h6>
                                <br>
                            </div>
                            <h2>
                                AGENDAMENTO
                            </h2>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div >
                                        Estou ciente do Regulamento para utilização de espaços e uso da Quadra e/ou campo abaixo: 
                                        O responsável por esta autorização deverá estar presente, no local, dia e horário da reserva, se por qualquer motivo isto não ocorrer, esta autorização ficará sem efeito e será recolhida pelo vigilante na portaria. 
                                        Em caso de necessidade, a Coordenação de Cultura, Esporte e Lazer poderá suspender temporariamente o horário. 
                                        O espaço reservado destina-se exclusivamente ao fim solicitado.
                                        Não fixar equipamentos que danifiquem a pintura da quadra (exemplo: fitas adesivas e fitas crepes no solo). 
                                    </div>
                                    <div>
                                        Declaro ter recebido o(s) material(s) listado. O(s) material(s) permanecerá sob minha total 
                                        responsabilidade, caso ocorra extravio ou dano após a retirada do material(s), 
                                        providenciarei o reparo ou reposição do mesmo.
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <?php echo form_open(); ?>
                        <label>
                            <input type="hidden" value="<?php echo $idhorario; ?>" name="aceite">
                            <input type="checkbox" required="true" name="aceite">
                            LI E CONCORDO COM OS TERMOS DE USO.
                        </label>
                        <br><br>
                        <input type="submit" class="btn btn-primary btn-lg" value="CONFIRMAR AGENDAMENTO">

                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
</section>
<style>
    .table-striped>tbody>tr:nth-child(odd)>td, 
    .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #67b168;
    }

    .table-striped2>tbody>tr:nth-child(odd)>td, 
    .table-striped2>tbody>tr:nth-child(odd)>th {
        background-color: #176e61
    }

</style>

<script>
    $(function () {
        $('.confirma').click(function () {
            /*
             var confirma = confirm('CONFIRMA AGENDAMENTO?');
             if (confirma) {
             } else {
             return false;
             }
             */
            var idHorario = $(this).attr('id');
            $('#idHorario').val(idHorario);
            $('#idHorario2').val(idHorario);
        });
    })

</script>

