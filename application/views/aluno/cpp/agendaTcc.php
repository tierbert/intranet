<div class="col-md-12">
    <div class="painel">
        <div class="container">
            <div class="row">
                <div  class="col-md-8 col-sm-7 col-xs-7">
                    <h2><span style="border-left: 4px solid#13B3D2; font-size: 35px;">Módulo <small>Agendamento</small></span></h2>
                    <br><br>
                    <h5>Seja Bem-vindo ao sistema de agendamento CPP.<br><br> Neste módulo será possivel a realização de agendamento para CPP.</h5>
                </div>
                <div class="col-md-4 col-sm-5 col-xs-5">
                    <!--<button type="button" class="btn btn-skin scroll"><a href="#listarsolicalu" class="btn btn-skin scroll"> Consulte seus Agendamentos</a></button>-->
                    <a href="<?php echo site_url('/aluno/indexaluno'); ?>" class="btn btn-primary btn-xs pull-right" style="margin-left: 15px;">
                        VOLTAR
                    </a>
                </div>

                <div class="col-md-8 col-sm-7 col-xs-7 text-center">
                    <hr>
                    <h2 class="section-heading">Meus Agendamentos</h2>                             
                </div>

                <div class="col-md-4 col-sm-5 col-xs-5 text-center"> 
                    <hr>                
                    <a href="<?php echo site_url('aluno/cpp/agendarCpp/2'); ?>" class="btn btn-primary">  
                        AGENDE AQUI!
                    </a>                           
                </div>
            </div>

            <div class="row">
                <br>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <table class="table table tour-step tour-step-8" style="color: black" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <th></th>
                               <!-- <th>Nº do Agendamento</th> -->
                                <th class="text-center">Data</th>
                                <th>Período</th>
                                <th class="text-center"></th>
                                <th class="actions"></th>
                            </tr>
                        </thead>
                        <tbody >
                            <tr class=" ">


                                <?php
                                $agend1 = $this->tca->getByAluno($idAluno);
                                if ($agend1->num_rows() < 2) {
                                    ?>


                                    <?php
                                }

                                foreach ($agend1->result() as $agendamento1) {
                                    if ($agendamento1) {
                                        ?>
                                    <tr>
                                        <td>Agendamento CPP</td>
                                       <!-- <td><?php echo $agendamento1->aha_id; ?></td> -->
                                        <td class="text-center"><?php echo $this->util->data($agendamento1->tch_data); ?></td>
                                        <td><?php echo $agendamento1->tch_horario; ?></td>
                                        <!--<td><?php echo $agendamento1->ahh_professor; ?></td> 
                                        <td><?php echo $agendamento1->tch_numSemana; ?>ª semana de 2017</td> 
                                        -->
                                        <td> 
                                            <!--
                                            <a href="<?php echo site_url('aluno/agendamento/agend_cancel/' . $agendamento1->tca_id); ?>" class="btn btn-danger ">  
                                                <span class="glyphicon glyphicon-trash"></span>
                                                &nbsp;
                                                CANCELAR 
                                            </a>
                                            -->
                                        </td>
                                    <?php } ?>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>            
                </div>                
            </div>
        </div>
    </div>
</div>

<style>
    .table-striped>tbody>tr:nth-child(odd)>td, 
    .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #67b168;
    }

    .table-striped2>tbody>tr:nth-child(odd)>td, 
    .table-striped2>tbody>tr:nth-child(odd)>th {
        background-color: #176e61
    }

</style>