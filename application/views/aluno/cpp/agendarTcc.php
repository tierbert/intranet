<div class="col-md-12">
    <div class="painel">
        <div class="container">
            <div class="row">
                <div  class="col-md-8 col-sm-7 col-xs-7">
                    <h2><span style="border-left: 4px solid#13B3D2; font-size: 35px;">Agendamento <small></small></span></h2>
                    <br><br>
                    <h5>Clique sobre o dia que deseja agendar com o orientador</h5>
                </div>

                <div class="col-md-4 col-sm-5 col-xs-5">
                  <!--  <button type="button" class="btn btn-skin scroll"><a href="#listarsolicalu" class="btn btn-skin scroll"> Consulte seus Agendamentos</a></button>-->
                  <a href="<?php echo site_url('/aluno/cpp'); ?>" class="btn btn-primary btn-xs pull-right" style="margin-left: 15px;">
                        VOLTAR
                    </a>
                </div>

                <div class="col-md-12 col-sm-7 col-xs-7 text-center">
                    <hr>
                    <h2> <?php  echo 'Agendamento CPP'; ?></h2>                            
                </div>
            </div>

            <div class="row">
                <br>
                <div class="col-md-12 col-sm-12 col-xs-12">

                    <table class="table table-bordered table-condensed">
                                        <thead>
                                            <tr>
                                                <th>DATA</th>
                                                <th>HORARIO</th>
                                                <th>VAGAS</th>
                                                <th>AÇÃO</th>
                                                <!--<th>PROFESSOR</th>  -->
                                                
                                            </tr>
                                        </thead>

                                        <?php
                                        foreach ($horarios->result() as $horario) {
                                            /*
                                              if ($tipo == 1) {

                                              } else {
                                              $agendados = $this->Agendamento__agendadosmodel->getByHorario($horario->ahh_id);
                                              }
                                              $total = $agendados->num_rows();
                                             * 
                                             */
                                            //echo $horario->tch_id;
                                            $agendados = $this->tca->getByHorario($horario->tch_id);
                                            $total = $agendados->num_rows();
                                            //echo ' - ' . $total . ' | ';
                                            ?>
                                            <tr>

                                                <td><?php echo $this->util->data($horario->tch_data); ?></td>
                                                <td><?php echo ($horario->tch_horario); ?></td>
                                                <td><?php echo $horario->tch_vagas - $total . ' de ' . $horario->tch_vagas; ?></td>
                                                <!--
                                                <td><?php echo ($horario->tch_professor); ?></td> 
                                                <td><?php echo $horario->tch_numSemana; ?></td>
                                                -->
                                                <td>
                                                    <?php
                                                    if ($total >= $horario->tch_vagas) {
                                                        echo "VAGAS ESGOTADAS";
                                                    } else {
                                                        ?>
                                                        <a href="<?php echo site_url('aluno/cpp/agendarHorarioCpp/' . $horario->tch_id); ?>" class="btn btn-success btn-xs" style="padding: 3px; color: white; margin-right: 5px;" >
                                                            &nbsp;&nbsp;&nbsp;
                                                            AGENDAR
                                                            &nbsp;&nbsp;&nbsp;
                                                        </a>
                                                    <?php } ?>
                                                </td>
                                            </tr>

                                            <?php
                                        }


                                        /*
                                          for ($i = 10; $i <= 21; $i++) {
                                          $hor = $this->Agendamento__horariomodel->getBy("ahh_tipo = $tipo and ahh_data = '2017-01-" . $i . "'");
                                          $horario = $hor->row();

                                          if (isset($horario->ahh_id)) {
                                          if ($tipo == 1) {
                                          $agendados = $this->Agendamento__agendadosmodel->getByHorario($horario->ahh_id, $idCurso);
                                          } else {
                                          $agendados = $this->Agendamento__agendadosmodel->getByHorario($horario->ahh_id);
                                          }
                                          //$total = $agendados->num_rows();
                                         */
                                        ?>

                                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<style>
    .table-striped>tbody>tr:nth-child(odd)>td, 
    .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #67b168;
    }

    .table-striped2>tbody>tr:nth-child(odd)>td, 
    .table-striped2>tbody>tr:nth-child(odd)>th {
        background-color: #176e61
    }

</style>

<script>
    $(function () {
        $('.confirma').click(function () {
            /*
             var confirma = confirm('CONFIRMA AGENDAMENTO?');
             if (confirma) {
             } else {
             return false;
             }
             */
            var idHorario = $(this).attr('id');
            $('#idHorario').val(idHorario);
            $('#idHorario2').val(idHorario);
        });
    })

</script>


