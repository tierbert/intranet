<?php
if ($semana == '') {
    $semana = date('W') - 1;
    $day = date('w');
    $domingo = date('m-d-Y', strtotime('-' . $day . ' days'));
    $datas = array(
        '1' => date('d-m-Y', strtotime('+' . (1 - $day) . ' days')), //segunda
        '2' => date('d-m-Y', strtotime('+' . (2 - $day) . ' days')), // terca
        '3' => date('d-m-Y', strtotime('+' . (3 - $day) . ' days')), // quarta
        '4' => date('d-m-Y', strtotime('+' . (4 - $day) . ' days')), // quinta
        '5' => date('d-m-Y', strtotime('+' . (5 - $day) . ' days')), //sexta
        '6' => date('d-m-Y', strtotime('+' . (6 - $day) . ' days')), //sabado
    );
} else {
    $dias = ($semana * 7) - 1;
    $diaDomingo = date('Y-m-d', strtotime('2018-01-01' . " + $dias  days"));

    $day = date('w', strtotime($diaDomingo));
    $domingo = date('m-d-Y', strtotime('-' . $day . ' days'));
    $datas = array(
        '1' => date('d-m-Y', strtotime($diaDomingo . '+' . (1 - $day) . ' days')), //segunda
        '2' => date('d-m-Y', strtotime($diaDomingo . '+' . (2 - $day) . ' days')), // terca
        '3' => date('d-m-Y', strtotime($diaDomingo . '+' . (3 - $day) . ' days')), // quarta
        '4' => date('d-m-Y', strtotime($diaDomingo . '+' . (4 - $day) . ' days')), // quinta
        '5' => date('d-m-Y', strtotime($diaDomingo . '+' . (5 - $day) . ' days')), //sexta
        '6' => date('d-m-Y', strtotime($diaDomingo . '+' . (6 - $day) . ' days')), //sabado
    );
}

$volta = '';
$avanca = '';
$semanaAtual = date('W') - 1;

if ($semana == $semanaAtual) {
    $volta = 'disabled';
} elseif ($semana > $semanaAtual + 1) {
    $avanca = 'disabled';
}
if ($semana > $semanaAtual + 2) {
    echo "<script>;window.location.href='" . site_url() . "/aluno/tcc/agendarTcc'</script>";
}
?>
<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 box-titulo" style="">
            TCC HORÁRIOS 
        </div>

        <div class="col-md-2">
            <a href="<?php echo site_url('aluno/tcc/agendarTcc/' . ($semana - 1)); ?>" class=" btn btn-primary <?php echo $volta; ?>">
                <span class="glyphicon glyphicon-backward"></span>
            </a>
            <a href="<?php echo site_url('aluno/tcc/agendarTcc/'); ?>" class=" btn btn-primary ">
                <span class="glyphicon glyphicon-home"></span>
            </a>
            <a href="<?php echo site_url('aluno/tcc/agendarTcc/' . ($semana + 1)); ?>" class=" btn btn-primary <?php echo $avanca; ?>">
                <span class="glyphicon glyphicon-forward"></span>
            </a>
        </div>
        <div class="col-md-4 ">

            <a href="<?php echo site_url('aluno/tcc'); ?>" class=" btn btn-primary pull-right">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">

    <table class="table table-striped table-bordered table-hover">
        <tr>
            <th></th>
            <th>SEGUNDA - <?php echo $datas['1']; ?></th>
            <th>TERÇA - <?php echo $datas['2']; ?></th>
            <th>QUARTA - <?php echo $datas['3']; ?></th>
            <th>QUINTA - <?php echo $datas['4']; ?></th>
            <th>SEXTA - <?php echo $datas['5']; ?></th>
        </tr>
        <?php
        foreach ($turnos->result() as $turno) {
            ?>
            <tr>
                <td>
                    <?php echo $turno->tur_turno . '<br>' . $turno->tur_horario; ?>
                </td>
                <?php
                for ($dia = 1; $dia < 6; $dia++) {
                    $horario = $this->tcch->getHorario($turno->tur_id, $dia);
                    ?>
                    <td>
                        <?php //echo $horario->num_rows();   ?>
                        <?php
                        if ($horario->num_rows() > 0) {

                            $dados = $horario->row();

                            echo 'PROF: ' . $dados->tcp_nome;
                            echo "<hr style='margin: 5px 0 5px 0;'>";


                            $agenda = $this->tcch->getByHorarioData($dados->hor_id, $this->util->data($datas[$dia]) . ' 00:00:00');
                            if ($agenda->num_rows() > 0) {
                                echo '<a class="btn btn-danger btn-xs" style="font-size: 12px;">HORÁRIO RESERVADO</a>';
                            } else {
                                ?>

                                <?php echo form_open(); ?>

                                <input value="<?php echo $dados->hor_id; ?>" type="hidden" name="tca_idHorario">
                                <input value="<?php echo strtotime($datas[$dia] . ' 00:00:00'); ?>" type="hidden" name="tca_data">

                                <button class="btn btn-primary btn-xs" style="font-size: 12px;">
                                    agendar
                                </button>
                                <?php echo form_close(); ?>
                                <?php
                            }
                        } else {
                            ?>

                        <?php } ?>

                    </td>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>
</div>

