<div class="box"  style="">
    <div class="row">
        <div class="col-md-10 col-xs-6 box-titulo" style="">
            TERMOS DE COMPROMISSO DE ESTÁGIO OBRIGATÓRIO
        </div>
        <div class="col-md-2 col-xs-6 text-right">
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class="row">
        <div class="col-md-8">
            <?php
            $perfil = $this->session->userdata('loginPainelAdm');
            $this->load->helper('file');
            $pasta = FCPATH . 'assets/estagio';
            $arquivos = get_filenames($pasta);
            //$arquivos = array_map('strtolower', $arquivos);
            asort($arquivos);
            $arquivos2 = array();
            foreach ($arquivos as $arquivo) {
                $dados = explode('_', $arquivo);
                $curso = strtoupper(trim($dados[0]));
                $semestre = $dados[1];
                if (isset($arquivos2[$curso][$semestre])) {
                    array_push($arquivos2[$curso][$semestre], array('nome' => $dados[2], 'arquivo' => $arquivo,));
                } else {
                    $arquivos2[$curso][$semestre][] = array(
                        'nome' => $dados[2],
                        'arquivo' => $arquivo,
                    );
                }
            }
            ?>
            <div class="btn-group-vertica" role="group" aria-label="...">
                <ul >
                    <?php
                    foreach ($arquivos2 as $curso => $cursos) {
                        ?>
                        <div class="col-md-12">
                            <div class="panel panel-primary"> 
                                <div class="panel-heading"> 
                                    <h3 class="panel-title">
                                        <?php echo $curso; ?>
                                    </h3> 
                                </div> 
                                <div class="panel-body"> 
                                    <?php foreach ($cursos as $semestre => $curso) { ?>
                                        <h4> <?php echo $semestre; ?>º Semestre</h4>
                                        <?php foreach ($curso as $cursoArq) { ?>
                                            <li class="btn btn-default btn-block" style="text-align: left">
                                                <a href="<?php echo base_url('assets/estagio/' . $cursoArq['arquivo']); ?>" target="_blanck" class="text-left">
                                                    <span class="glyphicon glyphicon-download-alt"></span> &nbsp;&nbsp;&nbsp;
                                                    <?php echo $cursoArq['nome']; ?>
                                                </a>

                                                <?php
                                                if ($perfil) {
                                                    echo form_open('adm/painel/docsEstagioDel', array('class' => 'pull-right'));
                                                    ?>
                                                    <input type="hidden" name="arquivo" value="<?php echo $cursoArq['arquivo']; ?>">
                                                    <button type="submit">
                                                        <span class="glyphicon glyphicon-trash"></span>
                                                    </button>
                                                    <?php
                                                    echo form_close();
                                                }
                                                ?> 
                                            </li>
                                        <?php } ?>
                                        <hr>
                                    <?php } ?>
                                </div> 
                            </div>
                        </div>
                    <?php } ?>
                </ul>
            </div>
        </div>

        <?php if ($perfil) { ?>
            <div class="col-md-4">
                <label>Enviar:</label>
                <div class="alert alert-info back-widget-set text-center">
                    <form role="form" class="form-horizontal" method="post" action="" id="formulario" enctype="multipart/form-data">
                        <input type="hidden" name="envia">
                        <div class="form-group">
                            <div class="col-md-12">
                                <input class="form-control" type="file" required="true" name="arquivo"/>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary form-control">Enviar Arquivo</button>
                            </div>
                        </div>
                        <br>
                        Padrão: curso_semestre_nome do documento (nutricao_4_documento 1)
                    </form>
                </div>
            </div>
        <?php } ?>

    </div>
</div>
