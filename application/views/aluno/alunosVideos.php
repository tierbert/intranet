<div class="painel">
<a href="<?php echo site_url('aluno/indexaluno'); ?>" class="btn btn-primary"> Voltar </a>
<div class="container">
    <div class="row">
                <div class="col-lg-12">
                        <h3 class="section-heading" style="text-align: center;">O vídeo abaixo têm como objetivo instruir os discentes no acesso ao novo Sistema Acadêmico.</h3>
                        <h3 class="section-subheading text-muted" style="text-align: center;">Acesse abaixo.</h3>
                    </div>

                <div class="col-lg-12">
                    <div class="timeline-heading" style="text-align: center">
                        <h4 class="subheading"> SWA.JACAD - PORTAL DO ALUNO.</h4>
                    </div>                
                
                    <div class="thumbnail">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" width="440" height="280" 
                                src="https://www.youtube.com/embed/JAzd_nksVms" frameborder="0" allowfullscreen>
                            </iframe>  
                        </div>
                    </div>
                </div>
    </div>
</div>     
</div>
