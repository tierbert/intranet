<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            Módulo de Agendamento FÓRUM DE APRENDIZAGEM
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('aluno/indexaluno'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class="row">
        <div class="col-md-8 col-sm-7 col-xs-7 text-center">
            <hr>
            <h2 class="section-heading">Meus Agendamentos</h2>                             
        </div>

        <div class="col-md-4 col-sm-5 col-xs-5 text-center"> 
            <hr>                
            <a href="<?php echo site_url('aluno/forumAprend/agendarForAprend'); ?>" class="btn btn-primary">  
                AGENDE AQUI!
            </a>                           
        </div>
    </div>
    <hr>
    <div class="row">
        <br>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <?php if ($agendas->num_rows() > 0) { ?>

                <table class="table table tour-step tour-step-8" style="color: black" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th class="text-center">DATA</th>
                            <th>HORÁRIO</th>
                            <th class="actions"></th>
                        </tr>
                    </thead>
                    <tbody >
                        <tr class=" ">
                            <?php
                            foreach ($agendas->result() as $agendamento1) {
                                if ($agendamento1) {
                                    ?>
                                <tr>
                                    <td class="text-center">
                                        <?php echo substr($this->util->data3($agendamento1->tca_data), 0, 11); ?>
                                    </td>
                                    <td><?php echo $agendamento1->tur_horario; ?></td>
                                    <td> 
                                        <!--
                                        <a href="<?php echo site_url('aluno/agendamento/agend_cancel/' . $agendamento1->tca_id); ?>" class="btn btn-danger ">  
                                            <span class="glyphicon glyphicon-trash"></span>
                                            &nbsp;
                                            CANCELAR 
                                        </a>
                                        -->
                                    </td>
                                <?php } ?>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>  
            <?php } else { ?>
                <h4>NENHUM AGENDAMENTO FUTURO</h4>
            <?php } ?>
        </div>                
    </div>
</div>
