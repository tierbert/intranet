<html>
    <head>
        <title>Comprovante De Solicitação</title>
        <meta content="">
        <link href="<?php echo base_url('assets/'); ?>/bootstrap337/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <style>
            .table1 { 
                border-left: 1px solid #000;
                border-top: 1px solid #000;
                border-right: 1px solid #000;
                border-bottom: 1px solid #000;
            }
            .negrito{
                font-weight: bold;
            }

            td{
                padding: 5px;
            }

        </style>
    </head>
    <body>

        <?php for ($i = 1; $i < 3; $i++) { ?>

            <div class="container">

                <table class="table1" >
                    <tr>
                        <td>
                            <img style="width: 20%; padding: 15px;" src="<?php echo base_url('assets/tema/images/logo.jpg'); ?>" >

                        </td>
                        <td class="text-center" style="font-weight: bold; font-size: 10px;">

                            <h4 style="font-size: 15px;">
                                Solicitação de Avaliação Substitutiva
                            </h4>
                            <br>

                            <?php
                            date_default_timezone_set('America/Bahia');
                            $data = Date('d/m/Y H:i:s');
                            ?>

                            EMITIDO EM: <?php echo $data; ?>
                        </td>
                        <td style="width: 180px;">
                            &nbsp;</td>
                    </tr>
                </table>



                <div class="row" style="margin-top: 10px; margin-bottom: 0px;">
                    <div class="col-xs-5">
                        <table class="table table-bordered" style="font-size: 10px;">
                            <tr>
                                <td style="" class="text-rig">
                                    Nº da Solicitação:
                                </td>
                                <td class="text-right">
                                    <?php echo $idSolicitacao; ?>
                                </td>
                            </tr>   
                            <tr>
                                <td class="negrit">Período Letivo:</td>
                                <td class="text-right">
                                    2019-1
                                </td>
                            </tr>

                            <tr>
                                <td class="text-righ">
                                    Matrícula:
                                </td>
                                <td class="text-right">
                                    <?php echo $matriculaAluno; ?>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-righ">
                                    Aluno:
                                </td>
                                <td class="text-right">
                                    <?php echo $nomeAluno; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px;" >
                                    Disciplinas: 
                                </td>
                                <td class="text-right">
                                    <?php
                                    $total = ($disciplinas->num_rows());
                                    echo '00' . $total;
                                    ?>  
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Valor unitário: 
                                </td>
                                <td class="text-right">R$ 75,00</td>
                            </tr>
                            <tr>
                                <td>
                                    Valor total: 
                                </td>
                                <td class="text-right">
                                    R$ <?php echo number_format($total * 75, 2, ',', '.'); ?>
                                </td>
                            </tr>
                        </table>

                    </div>

                    <div class="col-xs-5" style="width: 49%">

                        <table class="table table-bordered" style="font-size: 10px;">
                            <tr>
                                <th>
                                    Disciplina
                                </th>
                                <th>
                                    Turma
                                </th>

                            </tr>
                            <?php foreach ($disciplinas->result() as $disciplina) { ?>
                                <tr>
                                    <td><?php echo $disciplina->hor_disciplina; ?></td>
                                    <td><?php echo $disciplina->hor_turno; ?></td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>

                </div>

                <div class="row" style=" margin-top: 0px;">
                    <div class="col-xs-12" style="margin-top: 0px;">
                        <h4 style="font-size: 12px;" class="text-justify">
                            O aluno deverá apresentar o comprovante no financeiro para efetivar 
                            sua solicitação.
                            Para efetivá-la, você deve imprimir este comprovante e efetuar o pagamento junto ao setor financeiro até o dia 03/06/2019. 
                            Somente após o processamento de pagamento seu nome constará na relação de alunos aptos para à realização da prova.
                            Atenção: você pode acompanhar o andamento de sua solicitação. Acesse o sistema interno e confira o status a ela vinculado. 
                            Caso o pagamento já tenha sido efetuado e o processamento não tenha sido feito, favor se dirigir ao setor financeiro munido do respectivo comprovante.
                        </h4>


                        <p style="text-align: justify" style="font-size: 12px;">
                            A <span style="font-weight: bold">CESG - CENTRO DE EDUCAÇÃO SUPERIOR DE GUANAMBI</span>, DECLARA TER 
                            RECEBIDO A IMPORTÂNCIA SUPRA DE R$ <?php echo number_format($total * 75, 2, ',', '.'); ?>, REFERENTE
                            A SOLICITAÇÃO DA AVALIAÇÃO SUBSTITUTIVA CONFORME DESCRIÇÃO ACIMA.

                        </p>

                        <p>
                            Guanambi, _______de ______________________ de 2019.
                        </p>
                    </div>
                </div>
            </div>

            <?php
            if ($i == 1) {
                echo '<hr>';
            }
        }
        ?>
    </body>
</html>