<div class="col-md-12">
    <div class="painel">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-6 col-xs-6 text-center">
                    <h2 class="section-heading">Solicitação Aguardando <br>
                        Pagamento</h2>
                    <br>
                </div>

                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <img class="img-responsive img-circle " src="<?php echo base_url('assets/tema/'); ?>images/about/subsalu.png" alt="">
                    </div>
                    <div class="col-md-8 col-sm-6 col-xs-6">
                        <div class="textm" style="text-align:justify">
                            <p> Recebemos sua solicitação. Para efetivá-la, você deve imprimir este comprovante e efetuar o pagamento junto ao setor financeiro até o dia <b> 
                                    03/06/2019</b>. Somente após o processamento de pagamento seu nome constará na relação de alunos aptos para à realização da prova.</p>
                            <p>
                                <br><b>Atenção:</b> você pode acompanhar o andamento de sua solicitação. Acesse o sistema interno e confira o status a ela vinculado. Caso o pagamento já tenha sido efetuado e o processamento não tenha sido feito, favor se dirigir ao setor financeiro munido do respectivo comprovante.
                            </p>
                            <hr>
                            <div style="text-align: center;">
                                <a href="<?php echo site_url('aluno/solicitacoesalu/comprovante/' . $idSolicitacao); ?>" target="_blanck" class="btn btn-lg btn-info" > <div id="box2">IMPRIMIR SOLICITAÇÃO DE PAGAMENTO</div></a><br>
                                <br>
                                <a href="<?php echo site_url('aluno/Solicitacoesalu'); ?>" class="btn btn-lg btn-primary">
                                    PÁGINA INICIAL
                                </a>
                            </div>
                        </div>
                    </div>                
                </div>
            </div>
        </div>
    </div>
</div>


























