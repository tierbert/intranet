<div class="box"  style="">
    <div class="row">
        <div class="col-md-10 col-xs-10 box-titulo" style="">
            SOLICITAÇÃO DE Avaliação Substitutiva
        </div>
        <div class="col-md-2 col-xs-2 text-right">
            <a href="<?php echo site_url('aluno/indexaluno'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" >
    <div class="row">
        <div  class="col-md-8 col-sm-7 col-xs-7">
            <div class="textm">
                <p> A solicitação da Avaliação Substitutiva é realizada na aba "Nova
                    Solicitação". Neste espaço, o aluno pode solicitar individualmente a
                    disciplina escolhida ou fazer uma única solicitação contendo todas
                    as disciplinas desejadas. </p> 
                <p> ** Uma vez efetuada a solicitação, o débito será gerado na ficha
                    financeira do aluno independente da realização da prova solicitada; </p>
                <p> ** Cancelamentos só serão admitidos até o dia 04/12/2017. Para
                    solicitar o cancelamento, o aluno deve acessar sua solicitação através do
                    botão "listar solicitações" e clicar em "Cancelar"; </p>
                <p> ** Não é possível remover ou alterar disciplinas de solicitações já
                    efetuadas. Caso necessário, o aluno deve cancelar a solicitação em
                    questão e efetuar um novo processo.
                </p>
                </a>
            </div>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-4">
            <img  style="height: 250px;" class="img-responsive img-circle " src="<?php echo base_url('assets/tema/'); ?>images/about/subsalu.png" alt="">
        </div>            
    </div>
</div>


<div class="painel" style="min-height: 400px;">
    <div class="row">
        <hr>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Nova Solicitação</h2> <br>
                    <h3 class="section-subheading text-muted">Solicite sua Prova Substitutiva dentro do prazo</h3>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <section class="cards-section" style="padding-bottom: 0px; padding-top: 15px;">
                        <div id="cards-wrapper" class="cards-wrapper row">
                            <div class="item item-blue col-md-3 col-sm-6 col-xs-6">
                                <div class="item-inner text-center">
                                    <div class="icon-holder text-center" style="font-size: 35px;">
                                        <span class="glyphicon glyphicon-education"></span>
                                    </div>
                                    <!--//icon-holder-->
                                    <h3 class="title">Nova Solicitação</h3>
                                    <!--<p class="intro">Cadastro, Edição</p>-->
                                    <a class="link" href="<?php echo site_url('aluno/Solicitacoesalu/solicitacao'); ?>"><span></span></a>
                                </div><!--//item-inner-->
                            </div><!--//item-->
                        </div>
                    </section>

                </div>
            </div>
        </div>
        <hr>

    </div>
</div>
<div class="painel" style="min-height: 400px;">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <h2 class="section-heading">Solicitação</h2> <br>
            <h3 class="section-subheading text-muted">Acompanhe sua solicitação</h3> <br>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
            <table class="table table-hover" style="color: black" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th>Nº da Solicitaçao</th>
                        <th>Data</th>
                        <th>Tipo</th>
                        <th>Valor</th>
                        <th>Status</th>
                        <th>Ação</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    foreach ($solicitacoes->result() as $solicitacao) {

                        //$selec = $this->horariomodel->getById($selecionada);
                        $cor = '';
                        if ($solicitacao->sol_status == 'cancelado') {
                            $cor = 'danger';
                        }
                        ?>

                        <tr class="<?php echo $cor; ?>">
                            <td><?php echo $solicitacao->sol_id; ?></td>
                            <td><?php //echo $solicitacao->sol_data;             ?></td>
                            <td>Solic. Prova Substitutiva</td>

                            <td><?php
                                $disciplinas = $this->solicitacao_disciplinasmodel->getBySolic($solicitacao->sol_id);
                                $valor = $disciplinas->num_rows() * 75;
                                echo number_format($valor, 2, ',', '.');
                                ?>
                            </td>
                            <td><?php echo $solicitacao->sol_status; ?></td>
                            <td style="width: 400px;">
                                <?php if ($solicitacao->sol_status != 'cancelado') { ?>
                                    <a href="#sol<?php echo $solicitacao->sol_id; ?>" class="btn btn-primary btn-xs" role="button" data-toggle="collapse"  aria-expanded="false" aria-controls="collapseExample">
                                        Listar Disciplinas
                                    </a>
                                    <div class="collapse" style="" id="sol<?php echo $solicitacao->sol_id; ?>">
                                        <div class="well">
                                            <table class="table table-condensed table-striped table-hover" style="width: 100%; font-size: 12px;">
                                                <?php
                                                foreach ($disciplinas->result() as $disciplina) {
                                                    ?>
                                                    <tr class="">
                                                        <td><?php echo $disciplina->hor_disciplina; ?></td>
                                                        <td><?php echo $disciplina->hor_turno; ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </table>
                                        </div>
                                    </div>
                                    <a href="<?php echo site_url('aluno/solicitacoesalu/comprovante/' . $solicitacao->sol_id); ?>" class="btn btn-success btn-xs" role="button" >
                                        REIMPRIMIR
                                    </a>
                                    <a href="<?php echo site_url('aluno/Solicitacoesalu/cancelSolic/' . $solicitacao->sol_id); ?>" class="btn btn-danger btn-xs" role="button" >
                                        Cancelar 
                                    </a>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<style>
    .table-striped>tbody>tr:nth-child(odd)>td, 
    .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #67b168;
    }

    .table-striped2>tbody>tr:nth-child(odd)>td, 
    .table-striped2>tbody>tr:nth-child(odd)>th {
        background-color: #176e61
    }
</style>






















