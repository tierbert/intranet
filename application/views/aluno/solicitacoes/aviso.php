<div class="col-md-12">
    <div class="painel">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-6 col-xs-6 text-center">


                    <div class="alert alert-danger" role="alert">


                        <h2 class="section-heading">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            Prazo de solicitações encerrado <br>
                        </h2>


                    </div>
                    <br>
                    <a href="<?php echo site_url('aluno/solicitacoesalu'); ?>" class="btn btn-primary btn-lg">
                        VOLTAR                            
                    </a>
                    <br>
                </div>

            </div>
        </div>                
    </div>
</div>





