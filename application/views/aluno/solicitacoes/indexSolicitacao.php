<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Extensão Universitária - FG</title>
        <meta name="description" content="Responsive HTML5 Template">
        <meta name="author" content="webthemez">

        <!-- Mobile Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Favicon -->
        <link rel="shortcut icon" href="<?php echo base_url('assets/tema/'); ?>images/favicon.ico">

        <link href="<?php echo base_url('assets/tema/'); ?>bootstrap/css/bootstrap.css" rel="stylesheet"> 
        <link href="<?php echo base_url('assets/tema/'); ?>fonts/font-awesome/css/font-awesome.css" rel="stylesheet"> 
        <link href="<?php echo base_url('assets/tema/'); ?>css/animations.css" rel="stylesheet"> 
        <link href="<?php echo base_url('assets/tema/'); ?>css/style.css" rel="stylesheet"> 
        <link href="<?php echo base_url('assets/tema/'); ?>css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url('assets/tema/'); ?>cssc/style.css" rel="stylesheet">

        <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>jsc/index.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>plugins/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>plugins/modernizr.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>plugins/isotope/isotope.pkgd.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>plugins/jquery.backstretch.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>plugins/jquery.appear.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>js/custom.js"></script>






        <style type="text/css">




            #box{


                font-style:normal;
                color:#ffffff;
                background:#c50000;
                border:0px solid #ea326f;
                text-shadow:0px 0px 0px #ffffff;
                box-shadow:0px 0px 6px #454545;
                -moz-box-shadow:0px 0px 6px #454545;
                -webkit-box-shadow:0px 0px 6px #454545;
                border-radius:45px 7px 45px 7px;
                -moz-border-radius:45px 7px 45px 7px;
                -webkit-border-radius:45px 7px 45px 7px;

                padding:10px 30px;
                cursor:pointer;
                margin:0 auto;

                active{
                    cursor:pointer;
                    position:relative;
                    top:2px;
                }
            }

            #box2{


                font-style:normal;
                color:#ffffff;
                background:#1b4999;
                border:0px solid #ea326f;
                text-shadow:0px 0px 0px #ffffff;
                box-shadow:0px 0px 6px #454545;
                -moz-box-shadow:0px 0px 6px #454545;
                -webkit-box-shadow:0px 0px 6px #454545;
                border-radius:45px 7px 45px 7px;
                -moz-border-radius:45px 7px 45px 7px;
                -webkit-border-radius:45px 7px 45px 7px;
                text-align: center; 
                padding:10px 30px;
                cursor:pointer;
                margin:0 auto;

                active{
                    cursor:pointer;
                    position:relative;
                    top:2px;
                }
            }
            #logoe{
                width: 400px;
                height: 92px;

            }  


            #formc {
                position: absolute;
                top: 0px;
                right: px;
                bottom: 0px;
                left: 230px;
                width: 360px;
                height: 260px;
                margin: auto;
                box-shadow: 0px 0px 10px black;
                padding: 20px;
                box-sizing: border-box;


            }

            #modalc{
                opacity:0.97;
                display:none;

                left:0;
                top:0;
                z-index:8000;
                background-color:#000;


            }





            .caption-insc {position: absolute; top: 50%; left: 34%; z-index: 20; text-align: left; -webkit-transform: translate(-50%,-50%); -moz-transform: translate(-50%,-50%); transform: translate(-50%,-50%);text-shadow: 2px 2px 2px rgba(0, 0, 0, 0.24);}
            .caption-insc h1 {text-transform: uppercase;}
            .caption-insc h1 span {font-size: inherit; line-height: inherit; font-weight: inherit;}
            .caption-insc h3{line-height:34px;font-size: 30px;}
            #ban {
                filter: gray; /* IE6-9 */
                -webkit-filter: grayscale(1); /* Google Chrome, Safari 6+ & Opera 15+ */
                -webkit-box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
                -moz-box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
                box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
                margin-bottom:20px;
            }

            #ban:hover {
                filter: none; /* IE6-9 */
                -webkit-filter: grayscale(0); /* Google Chrome, Safari 6+ & Opera 15+ */

            }









        </style>




    </head>

    <body class="no-trans">
        <!-- scrollToTop --> 
        <div class="scrollToTop"><i class="icon-up-open-big"></i></div>

        <!-- header start --> 
        <header class="header fixed clearfix navbar navbar-fixed-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">

                        <!-- header-left start --> 
                        <div class="header-left">

                            <!-- logo -->
                            <div class="logo smooth-scroll">
                                <a href="<?php echo site_url('aluno/indexaluno'); ?>">
                                    <img id="logo" src="<?php echo base_url('assets/tema/'); ?>images/logo.jpg" alt="Worthy">
                                </a>
                            </div>

                            <!-- name-and-slogan -->


                        </div>
                        <!-- header-left end -->

                    </div>
                    <div class="col-md-9">

                        <!-- header-right start --> 
                        <div class="header-right">

                            <!-- main-navigation start --> 
                            <div class="main-navigation animated">

                                <!-- navbar start --> 
                                <nav class="navbar navbar-default" role="navigation">
                                    <div class="container-fluid">

                                        <!-- Toggle get grouped for better mobile display -->
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                                                <span class="sr-only"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>

                                        <!-- Collect the nav links, forms, and other content for toggling -->
                                        <div class="collapse navbar-collapse scrollspy smooth-scroll" id="navbar-collapse-1">
                                            <ul class="nav navbar-nav navbar-right">

                                                <li class="active"><a href="<?php echo site_url('/aluno/indexaluno'); ?>">Home</a></li>
                                                <!--<li><a href="#cursos">Manual</a></li> -->


                                                <li>
                                                     <a href="<?php echo site_url('aluno/indexaluno/sair'); ?>" id="loginform">
                                                        <span>Sair</span>
                                                    </a>
                                                </li>

<!-- <a href="<?php //echo site_url('site/inscricoesLogin');   ?> ">Login</a> -->
                                                <div class="login" style="opacity:0.95" class=form-horizontal >
                                                    <div class="arrow-up"></div>
                                                    <div class="formholder">
                                                        <div class=form-horizontal>    
                                                            <div>
                                                                <?php echo validation_errors(); ?>
                                                            </div>
                                                            <?php echo form_open('inicio/loginAluno') ?>

                                                            <div class="form-group">
                                                                <div class=" col-md-12">
                                                                    <?php $this->util->input2('NÚMERO DE MATRÍCULA', 'matricula', '', 'matricula', 12); ?>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-12">
                                                                    <div class="col-sm-12">
                                                                        <label>SENHA</label>
                                                                        <input required="true" name="senha" class="form-control " type="password"> 
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class=form-group>
                                                                <input type=submit  value=ENTRAR class='btn btn-primary'>
                                                            </div>            
                                                            <?php echo form_close() ?>
                                                        </div>

                                                    </div>

                                                </div>

                                            </ul>
                                        </div>


                                </nav>
                                <!-- navbar end -->

                            </div>
                            <!-- main-navigation end -->

                        </div>
                        <!-- header-right end -->

                    </div>
                </div>
            </div>
        </header>
        <!-- header end -->

        <!-- banner start --> 
        <div class="banner1">    	   
            <img src="<?php echo base_url('assets/tema/'); ?>images/Banner4.png" alt="" class="img-responsive" alt="deslizar">

            
            
            
            <div class="welcome-message">
                <div class="wrap-info">
                    <div class="caption-insc">
                        <h1 class="animated fadeInDown"><font><font></font></font></h1>
                        <p class="animated fadeInUp"><font><font></font></font></p>                
                    </div>
                    <a href="#modulo" class="arrow-nav scroll wowload fadeInDownBig animated" style="visibility: visible; 
                       animation-name: fadeInDownBig;"><i class="fa fa-angle-down"></i></a>
                </div>
            </div>
        </div>

        <br><br><br>
        
        <section id="about">
            <div class="container">
                <div class="row">
                    
                    <?php $this->load->view($pagina); ?>
                    
                    
                </div>
                
            </div>
        </section>










    </body>
    <script>
        $(document).ready(function () {
            //$('#modalc').modal('show');
            $('.matricula').mask('9999999999');

        });
    </script>

    <?php $this->util->mascaras(); ?>
    <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>jsc/index.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>js/jquery.cookie.js"></script>
    <script>
        //$("input").prop('required', true);

        if ($.cookie('atualizacoes') != 'false') {
            $('#modalc').modal('show');
            $.cookie('atualizacoes', 'false');
        }


        /* Author:WebThemez
         * Author URI:http://webthemez.com
         * License: Creative Commons Attribution 3.0 License (https://creativecommons.org/licenses/by/3.0/)
         */

        (function ($) {
            $(document).ready(function () {


                $(".banner-image").backstretch('<?php echo base_url('/assets/tema/'); ?>images/banner_s.png');

                // Fixed header
                //-----------------------------------------------
                $(window).scroll(function () {
                    if (($(".header.fixed").length > 0)) {
                        if (($(this).scrollTop() > 0) && ($(window).width() > 767)) {
                            $("body").addClass("fixed-header-on");
                        } else {
                            $("body").removeClass("fixed-header-on");
                        }
                    }
                    ;
                });

                $(window).load(function () {
                    if (($(".header.fixed").length > 0)) {
                        if (($(this).scrollTop() > 0) && ($(window).width() > 767)) {
                            $("body").addClass("fixed-header-on");
                        } else {
                            $("body").removeClass("fixed-header-on");
                        }
                    }
                    ;
                });

                $('#quote-carousel').carousel({
                    pause: true,
                    interval: 4000,
                });
                //Scroll Spy
                //-----------------------------------------------
                if ($(".scrollspy").length > 0) {
                    $("body").addClass("scroll-spy");
                    $('body').scrollspy({
                        target: '.scrollspy',
                        offset: 152
                    });
                }

                //Smooth Scroll
                //-----------------------------------------------
                if ($(".smooth-scroll").length > 0) {
                    $('.smooth-scroll a[href*=#]:not([href=#]), a[href*=#]:not([href=#]).smooth-scroll').click(function () {
                        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                            var target = $(this.hash);
                            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                            if (target.length) {
                                $('html,body').animate({
                                    scrollTop: target.offset().top - 151
                                }, 1000);
                                return false;
                            }
                        }
                    });
                }

                // Animations
                //-----------------------------------------------
                if (($("[data-animation-effect]").length > 0) && !Modernizr.touch) {
                    $("[data-animation-effect]").each(function () {
                        var $this = $(this),
                                animationEffect = $this.attr("data-animation-effect");
                        if (Modernizr.mq('only all and (min-width: 768px)') && Modernizr.csstransitions) {
                            $this.appear(function () {
                                setTimeout(function () {
                                    $this.addClass('animated object-visible ' + animationEffect);
                                }, 400);
                            }, {accX: 0, accY: -130});
                        } else {
                            $this.addClass('object-visible');
                        }
                    });
                }
                ;

                // Isotope filters
                //-----------------------------------------------
                if ($('.isotope-container').length > 0) {
                    $(window).load(function () {
                        $('.isotope-container').fadeIn();
                        var $container = $('.isotope-container').isotope({
                            itemSelector: '.isotope-item',
                            layoutMode: 'masonry',
                            transitionDuration: '0.6s',
                            filter: "*"
                        });
                        // filter items on button click
                        $('.filters').on('click', 'ul.nav li a', function () {
                            var filterValue = $(this).attr('data-filter');
                            $(".filters").find("li.active").removeClass("active");
                            $(this).parent().addClass("active");
                            $container.isotope({filter: filterValue});
                            return false;
                        });
                    });
                }
                ;

                //Modal
                //-----------------------------------------------
                if ($(".modal").length > 0) {
                    $(".modal").each(function () {
                        $(".modal").prependTo("body");
                    });
                }

            }); // End document ready
        })(this.jQuery);

        //Slider Video
        $('.bxslider').bxSlider({
            video: true,
            useCSS: false
        });



        // novo carrosel





    </script>

</html>
