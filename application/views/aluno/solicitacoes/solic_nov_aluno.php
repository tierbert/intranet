<?php
$turnos = array();
$turnos[''] = 'Selecione a turma!';

$sql = $this->db
        ->select('SUBSTRING(hor_turno, 1,2) as periodo')
        ->select('SUBSTRING(hor_turno, 4) as turma')
        ->select('hor_semestre')
        ->select('hor_turno')
        ->where('hor_semestre', '2019-1')
        ->order_by('turma, periodo')
        ->group_by('hor_turno')
        ->get('horario');


foreach ($sql->result() as $sq) {
    //echo $sq->periodo . ' - ' . $sq->turma . '<br>';
    $turnos[$sq->hor_turno] = 'Período: ' . $sq->periodo . ' - Turma: ' . $sq->turma;
}
?>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<link href="<?php echo base_url('assets'); ?>/js/selectize.bootstrap3.css" rel="stylesheet">
<script src="<?php echo base_url('assets'); ?>/js/selectize.js"></script>

<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            Avaliação  Substitutiva
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('aluno/Solicitacoesalu/'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <hr>
    <div class="row">
        <div class="col-md-6">
            <div class="form-horizontal">
                <?php echo form_open('', site_url('aluno/solicitacoesalu/finalizaSolic')); ?>
                <h5 style="font-weight: bold; font-size: 20px;" for="Add1">Adicione uma nova disciplina:</h5>
                <hr>   
            </div>

            <?php echo form_dropdown('', $turnos, '', array('class' => 'form-control selectize turma')); ?>
            <hr>
            <table class="table table-striped table-bordered table-hover table-condensed" style="" cellspacing="0" cellpadding="0" id="tabel">
                <thead>
                    <tr>
                        <th>Disciplina</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="divDisciplinas">
                    <tr><td colspan="2">Selecione a Turma</td></tr>
                </tbody>
            </table>
        </div>

        <div class="col-md-5">
            <div class="form-horizontal">
                <?php //echo form_open();  ?>
                <h5 style="font-weight: bold; font-size: 20px;" for="Add1">disciplinas Selecionadas</h5>
                <hr>   
            </div>


            <table class="table" style="color: black" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th>Nome da Diciplina</th>
                        <th>Turma</th>
                        <th class="actions">Ações</th>
                    </tr>
                </thead>
                <tbody class="divSelecionadas">
                    <?php
                    echo form_open('aluno/solicitacoesalu/finalizaSolic');
                    if (count($selecionadas) > 0) {
                        foreach ($selecionadas as $selecionada) {
                            $selec = $this->horariomodel->getById($selecionada);
                            ?>
                            <tr>
                                <td class="discSelecionada"><?php echo $selec->hor_disciplina; ?></td>
                                <td><?php echo $selec->hor_prof; ?></td>
                                <td><?php echo $selec->hor_turno; ?></td>
                                <td class="actions">

                                    <a onclick='remove(<?php echo $selec->hor_id ?>)' class="btn btn-danger"  style="margin:   0px;" >
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
            <hr>
            <div style="text-align:center">
                Total de disciplinas: 
                <span class="quantidade">
                    <?php
                    $total = count($selecionadas);

                    echo $total;
                    ?>
                </span>
                <br>
                Valor Unitario: 75,00
                <br>
                Valor Total: 
                <span class="total">
                    <?php echo number_format($total * 75, 2, ',', '.'); ?>
                </span>
                <hr>
            </div>
            <div class="text-center"> 
                <a type="submit" class="btn btn-primary scroll finaliza"   >
                    Finalizar Solicitação
                </a>
            </div>
            <?php //echo form_close();  ?>
        </div>              
    </div>  
</div>

<style>
    /*
    .table-striped>tbody>tr:nth-child(odd)>td, 
    .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #67b168;
    }

    .table-striped2>tbody>tr:nth-child(odd)>td, 
    .table-striped2>tbody>tr:nth-child(odd)>th {
        background-color: #176e61
    }
    .selectProf{
        height: 80px;

    }
    */

    .hr4{
        padding: 0px;
        margin: 5px 0px;
        border: 1px solid gray;
    }
</style>



<script>
    $('.selectize').selectize({
        selectOnTab: true,
    });

    $('.turma').on("change", function (e) {
        //console.log($(this).val());
        refreshDisciplinas();
    });



    function add(idDisciplina) {
        $.ajax({
            type: 'POST',
            url: "<?php echo site_url('aluno/solicitacoesalu/api_add'); ?>",
            data: {idDisciplina: idDisciplina},
            success: function (result) {
                //console.log(result);
                refreshDisciplinas();
                refresSelecionadas();
            }, error: function (result) { },
        });
    }

    function remove(idDisciplina) {
        $.ajax({
            type: 'POST',
            url: "<?php echo site_url('aluno/solicitacoesalu/api_remove'); ?>",
            data: {idDisciplina: idDisciplina},
            success: function (result) {
                //console.log(result);
                refreshDisciplinas();
                refresSelecionadas();
            }, error: function (result) { },
        });
    }


    function refresSelecionadas() {
        $.ajax({
            type: 'POST',
            url: "<?php echo site_url('aluno/solicitacoesalu/api_getSelecionadas'); ?>",
            data: {turma: ''},
            success: function (result) {
                //console.log(result);
                $('.divSelecionadas').html(result);
                refreshTotais();
            }, error: function (result) { },
        });
    }

    function refreshDisciplinas() {
        var turma = $('.turma').val();
        $.ajax({
            type: 'POST',
            url: "<?php echo site_url('aluno/solicitacoesalu/api_getDisciplinas'); ?>",
            data: {turma: turma},
            success: function (result) {
                //console.log(result);
                $('.divDisciplinas').html(result);
            }, error: function (result) { },
        });
    }

    function refreshTotais() {
        var quantidade = $(".discSelecionada").length;
        var total = quantidade * 75;

        console.log(total);
        console.log(quantidade);

        $('.total').html(total);
        $('.quantidade').html(quantidade);
    }

    // refresSelecionadas();

    $('.finaliza').click(function () {
        var total = $(".discSelecionada").length;
        if (total === 0) {
            //alert('Nenhuma disciplina selecionada');
            Swal.fire({
                type: 'error',
                title: 'Nenhuma Disciplina Selecionada!',
                text: 'Selecione uma ou mais disciplinas!',
                //footer: ''
            })
            return false;
        }

        Swal.fire({
            title: 'Uma vez efetuada a solicitação, o débito será gerado na ficha financeira do aluno independente da realização da prova solicitada. Deseja confirmar sua solicitação?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Concordo'
        }).then((result) => {
            if (result.value) {
                window.location.href = '<?php echo site_url('aluno/solicitacoesalu/finalizaSolic'); ?>';
            }
        })
        return false;
    });
</script>