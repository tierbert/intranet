<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            Avaliação  Substitutiva
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('aluno/indexaluno'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class="row">
        <div class="col-md-8 col-sm-7 col-xs-7 text-center">
            <hr>
            <h2 class="section-heading">
                Módulo Avaliação  Substitutiva
            </h2>                             
        </div>

        <div class="col-md-4 col-sm-5 col-xs-5 text-center"> 
            <hr>                
            <a href="<?php echo site_url('aluno/solicitacoesalu/solicitacao'); ?>"  class="btn btn-primary btn-lg" style="margin-top: 10px;"> 
                NOVA SOLICITAÇÃO
            </a>                           
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="pull-right tour-step tour-step-1">
                <h6 style="">
                    Usuário logado:
                    <span style="font-weight: bold">
                        <?php echo $nomeAluno; ?> 
                    </span>
                    - 
                    Nº de Matrícula: 
                    <span style="font-weight: bold">
                        <?php echo $matriculaAluno; ?>
                    </span>
                </h6>
                <br>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading tour-step tour-step-7">
                    <h3 class="panel-title" style="font-weight: bold">
                        Minhas Solicitações
                    </h3>
                </div>
                <div class="panel-body">


                    <div class="row">
                        <div class="col-md-12">

                            <table class="table table-bordered table-hover" style="color: black" cellspacing="0" cellpadding="0">
                                <thead>
                                    <tr>
                                        <th>Nº da <br>Solicitaçao</th>
                                        <th>Data</th>
                                        <th>Tipo</th>
                                        <th>Valor</th>
                                        <th>Status</th>
                                        <th class="actions"></th>
                                    </tr>
                                </thead>
                                <tbody >

                                    <?php
                                    foreach ($solicitacoes->result() as $solicitacao) {
                                        // if ($solicitacao->sol_status != 'cancelado') {

                                        $status = $solicitacao->sol_status;

                                        $cor = '';
                                        if ($solicitacao->sol_status == 'pago') {
                                            $cor = 'success';
                                        } elseif ($solicitacao->sol_status == 'cancelado') {
                                            $cor = 'danger';
                                        }
                                        ?>

                                        <tr class=" <?php echo $cor; ?>">
                                            <td><?php echo $solicitacao->sol_id; ?></td>
                                            <td><?php
                                                $data = substr($solicitacao->sol_dataCriacao, 0, 10);
                                                $data = $this->util->data($data);
                                                echo $data;
                                                ?></td>
                                            <td>Solic. Avaliação Substitutiva</td>

                                            <td><?php
                                                $disciplinas = $this->solicitacao_disciplinasmodel->getBySolic($solicitacao->sol_id);
                                                $valor = $disciplinas->num_rows() * 75;
                                                echo number_format($valor, 2, ',', '.');
                                                ?>
                                            </td>
                                            <td><?php
                                                if ($solicitacao->sol_status == '') {
                                                    echo 'AGUARDANDO PAGAMENTO';
                                                } else {
                                                    echo mb_strtoupper($solicitacao->sol_status);
                                                }
                                                ?></td>

                                            <td style="width: 400px;" class="text-left">
                                                <?php if ($status != 'cancelado') { ?>
                                                    <a href="<?php echo site_url('aluno/solicitacoesalu/comprovante/' . $solicitacao->sol_id); ?>" class="btn btn-info btn-xs" >
                                                        Comprovante
                                                    </a>
                                                <?php } else { ?>
                                                    Cancelada - 
                                                <?php } ?>
                                                <a href="#sol<?php echo $solicitacao->sol_id; ?>" class="btn btn-primary btn-xs" role="button" data-toggle="collapse"  aria-expanded="false" aria-controls="collapseExample">
                                                    Disciplinas
                                                </a>


                                                <?php if ($solicitacao->sol_status == '') { ?>

                                                    <a href="<?php echo site_url('aluno/Solicitacoesalu/cancelSolic/' . $solicitacao->sol_id); ?>" class="btn btn-danger btn-xs" >
                                                        <span class="glyphicon glyphicon-trash"></span> 
                                                        Cancelar
                                                    </a>

                                                <?php } ?>


                                                <div class="collapse" style="" id="sol<?php echo $solicitacao->sol_id; ?>">
                                                    <div class="well">

                                                        <table class="table table-condensed" style="width: 100%; font-size: 12px;">
                                                            <?php
                                                            foreach ($disciplinas->result() as $disciplina) {
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $disciplina->hor_disciplina; ?></td>
                                                                    <td><?php echo $disciplina->hor_turno; ?></td>
                                                                </tr>
                                                            <?php } ?>

                                                        </table>

                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                        // }
                                    }
                                    ?>


                                </tbody>
                            </table>

                            <hr>

                        </div>



                        <hr>
                    </div>
                </div> <!-- /#list -->
            </div> <!-- /#main -->

        </div>
    </div>
</div>    
<div class="text-justify"  style= "margin-top: 50px; margin-bottom: 0px; padding: 100px;">
    <div style="border: 1px solid#eb5d1e; padding: 25px;">
        INFORMAÇÕES IMPORTANTES:
        <br><br>
        - Você deve solicitar a AVALIAÇÃO SUBSTITUTIVA clicando no botão "Nova Solicitação". Neste espaço você pode solicitar individualmente a disciplina escolhida, ou fazer uma única solicitação contendo todas as disciplinas desejadas. Atenção: <br>
        - Uma vez efetuada a solicitação, o débito será gerado na ficha financeira do aluno independente da realização da avaliação solicitada;<br>
        - Cancelamentos só serão admitidos até o dia 02/12/2016, para solicitações não pagas e se efetuados pelo próprio aluno. Para solicitar o cancelamento o aluno deve acessar sua solicitação através do botão "listar solicitações"  e clicar em  "Cancelar";<br>
        - Não é possível remover ou alterar disciplinas de solicitações já efetuadas. Em caso de necessidade, o aluno deve cancelar a solicitação em questão e efetuar um novo processo.<br>
        - Após escolhida a(s) disciplina(s), o aluno deve clicar no botão "Enviar solicitação" , imprimir a "solicitação de pagamento" e quitá-lo junto ao setor financeiro da Instituição. Informação:<br>
        - Conforme estipulado em calendário acadêmico, o pagamento só será admitido até o dia 02/12/2016;<br>
        - O pagamento só será recebido pelo setor financeiro e em seu horário de funcionamento (07h00 - 21h30)<br>
        - Você pode acompanhar o andamento de sua solicitação. Caso o pagamento já tenha sido efetuado e o processamento não tenha sido feito, favor se dirigir ao setor financeiro munido do respectivo comprovante.
    </div>
</div>

</div>
</section>
</div>



<style>
    .table-striped>tbody>tr:nth-child(odd)>td, 
    .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #67b168;
    }

    .table-striped2>tbody>tr:nth-child(odd)>td, 
    .table-striped2>tbody>tr:nth-child(odd)>th {
        background-color: #176e61
    }

</style>

</section>
<!--Tuor-->

