<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            RENOVAÇÃO DA CARTEIRINHA ESTUDANTIL
            
        </div>
        <div class="col-md-6 col-xs-6 text-right">
           <!-- <a href="<?php echo site_url('adm/rh/Iniciorh'); ?>" class=" btn btn-primary">
                Voltar
            </a> -->
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal> 
        <h2 style="text-align: center;"> Agendamento para RENOVAÇÃO DA CARTEIRINHA ESTUDANTIL</h2> <br>
        <table class='table table-bordered table-striped table-condensed table-hover table-responsive tabela-aredondada'> 
            <thead>
                <tr>
                    <th></th>
                    <th>DATA</th>
                    <th>PERÍODO</th>
                    <th>OPÇÕES</th>
                    
                </tr>
            </thead>
            <tbody >

                <tr>
                
                    <?php
                    $agendamento1 = $this->Agendamento__agendadosmodel->getByAluno($idAluno, 3);
                    
                    //print_r($agendamento1);
                    
                    if ($agendamento1) {
                        ?>
                        <td>RENOVAÇÃO DA CARTEIRINHA ESTUDANTIL</td>
                        <td><?php echo $this->util->data($agendamento1->ahh_data); ?></td>
                        <td><?php echo $agendamento1->ahh_turno; ?></td>
                        <td> 
                            <a href="<?php echo site_url('aluno/agendamento/agend_cancel/' . $agendamento1->aha_id); ?>" class="btn btn-danger ">  
                                <span class="glyphicon glyphicon-trash"></span>
                                &nbsp;
                                CANCELAR 
                            </a>
                        </td>
                    <?php } else { ?>
                        <td>RENOVAÇÃO DA CARTEIRINHA ESTUDANTIL</td>
                        <td></td><td></td>
                        <td>
                            <a href="<?php echo site_url('aluno/agendamento/agendar/3'); ?>" class="btn btn7 btn-success">  
                                AGENDE AQUI!
                            </a>
                        </td>
                    <?php } ?>
                </tr> 
            </tbody>
        </table>
    </div>
</div>

