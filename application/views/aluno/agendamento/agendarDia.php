<link href="<?php echo base_url(); ?>assets/css/css/cucco.css" media="screen" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<section id="ACC"  >
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="container">
                    <div class="row">
                        <div id="overview" class="col-sm-4 col-md-3 animated bounceInUp">
                            <h2>Calendário</h2><br><br>
                            <h5>Clique sobre o dia que seja agendar com seu coordenador de curso</h5>
                            <p></p>
                            <button type="button" class="btn btn-skin scroll"><a href="#listarsolicalu" class="btn btn-skin scroll"> Consulte seus Agendamentos</a></button>
                        </div>
                        <!-- End Column -->
                        <div id="calendar" class="col-sm-7 col-md-8 animated animated-sm bounceInUp">
                            <div class="pull-right tour-step tour-step-1">
                                <h6 style="">
                                    <a href="<?php echo site_url('aluno/agendamento/agendar/' . $tipo); ?>" class="btn btn-primary"> 
                                        Voltar
                                    </a>

                                </h6>
                                <br>
                            </div>
                            <h2>
                                <?php
                                if ($tipo == 1) {
                                    echo 'Coordenador de Curso<br>';
                                    echo 'Curso: ' . $curso;
                                } elseif ($tipo == 2) {
                                    echo 'Agendamento Financeiro';
                                } elseif ($tipo == 3) {
                                    echo 'Agendamento Negociação';
                                }
                                ?>
                            </h2>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="calendar fc fc-ltr">

                                        <h3>Horários disponíveis para o dia <?php echo $dia; ?>/01/2017</h3>

                                        <table class="table table-bordered">


                                            <tr>
                                                <th>Data</th>
                                                <th>Horário</th>
                                                <th>Vagas</th>
                                                <th></th>
                                            </tr>


                                            <?php
                                            foreach ($horarios->result() as $horario) {
                                                //$hor = $this->Agendamento__horariomodel->getBy("ahh_tipo = $tipo and ahh_data = '2017-01-" . $dia . "'");
                                                //$horari = $hor->row();
                                                if ($tipo == 1) {
                                                    $agendados = $this->Agendamento__agendadosmodel->getByHorario($horario->ahh_id, $idCurso);
                                                } else {
                                                    $agendados = $this->Agendamento__agendadosmodel->getByHorario($horario->ahh_id);
                                                }
                                                $total = $agendados->num_rows();
                                                ?>
                                                <tr>
                                                    <td><?php echo $this->util->data($horario->ahh_data); ?></td>
                                                    <td><?php echo $horario->ahh_turno; ?></td>
                                                    <td>   
                                                        <?php
                                                        $vagas = $horario->ahh_vagas;
                                                        echo $total . ' / ' . $vagas;
                                                        ?>
                                                    </td>
                                                    <td>


                                                        <?php
                                                        if ($total >= $vagas) {
                                                            ?>
                                                            
                                                                ESGOTADO
                                                            
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <a href="<?php echo site_url('aluno/agendamento/agendaDia/' . $horario->ahh_id); ?>" class="btn btn-primary btn-xs" style="padding: 3px; color: white; margin-right: 5px;">
                                                                Agendar
                                                            </a>

                                                        <?php } ?>

                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
    </div>
</section>
<style>
    .table-striped>tbody>tr:nth-child(odd)>td, 
    .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #67b168;
    }

    .table-striped2>tbody>tr:nth-child(odd)>td, 
    .table-striped2>tbody>tr:nth-child(odd)>th {
        background-color: #176e61
    }

</style>

<script>
    $(function () {
        $('.confirma').click(function () {
            /*
             var confirma = confirm('CONFIRMA AGENDAMENTO?');
             if (confirma) {
             } else {
             return false;
             }
             */
            var idHorario = $(this).attr('id');
            $('#idHorario').val(idHorario);
            $('#idHorario2').val(idHorario);
        });
    })

</script>



<?php if ($tipo == 2) { ?>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="pull-right btn btn-xs btn-primary" data-dismiss="modal" aria-label="Close">
                        CANCELAR  
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">Agendamento</h4>
                </div>
                <div class="modal-body">
                    <h4>
                        CONFIRMA AGENDAMENTO?
                    </h4>
                </div>
                <div class="modal-footer">
                    <div class="row center">
                        <div class="col-md-6">
                            <?php ?>
                            <button type="button" class="btn btn-default btn-block" data-dismiss="modal" aria-label="Close">
                                CANCELAR  
                            </button>
                            <?php
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo form_open();
                            ?>
                            <input type="hidden" name="idHorario" id="idHorario2">
                            <button type = "submit" class = "btn btn-primary btn-block">SIM</button>
                            <?php
                            echo form_close();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



<?php } else { ?>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="pull-right btn btn-xs btn-primary" data-dismiss="modal" aria-label="Close">
                        CANCELAR  
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">Agendamento</h4>
                </div>
                <div class="modal-body">
                    <h4>
                        DESEJA AGENDAR HORARIO PARA O FINANCEIRO NESSE MESMO DIA?
                    </h4>
                </div>
                <div class="modal-footer">
                    <div class="row center">
                        <div class="col-md-6">
                            <?php
                            echo form_open();
                            ?>
                            <input type="hidden" name="financeiro" value="0">
                            <input type="hidden" name="idHorario" id="idHorario">
                            <button type = "submit" class = "btn btn-default btn-block" >NAO</button>
                            <?php
                            echo form_close();
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo form_open();
                            ?>
                            <input type="hidden" name="financeiro" value="1">
                            <input type="hidden" name="idHorario" id="idHorario2">
                            <button type = "submit" class = "btn btn-primary btn-block">SIM</button>
                            <?php
                            echo form_close();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>