<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            Cancelar Agendamento

        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('aluno/agendamento'); ?>" class=" btn btn-primary">
                Voltar
            </a> 
        </div>
    </div>
</div>



<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal> 
        <?php echo form_open($this->uri->uri_string(), 'post') ?>
        CONFIRMA O CANCELAMENTO DO AGENDAMENTO?
        <br>
        <?php echo form_submit('agree', 'Sim') ?>
        <?php echo form_submit('disagree', 'Não') ?>
        <?php echo form_close() ?>
    </div>
</div>

