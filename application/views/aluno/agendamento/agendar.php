<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-10 col-xs-10 box-titulo" style="">
            RENOVAÇÃO DA CARTEIRINHA ESTUDANTIL
        </div>
        <div class="col-md-2 col-xs-2 text-right">
            <a href="<?php echo site_url('aluno/agendamento'); ?>" class=" btn btn-primary">
                Voltar
            </a> 
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class="form-horizontal text-center"> 
        <h4>SELECINE O DIA E HORÁRIO</h4>

        <ul class="nav nav-tabs">
            <?php foreach ($horarios->result() as $k => $horario) { ?>
                <li class="<?php echo $k == 0 ? 'active' : ''; ?>"><a href="#<?php echo $horario->ahh_data; ?>" data-toggle="tab"><?php echo substr($horario->ahh_data, 8, 9) . "/" . substr($horario->ahh_data, 5, 2); ?></a></li>
            <?php } ?>
        </ul>

        <div class="tab-content">
            <?php foreach ($horarios->result() as $k => $horario) { ?>
                <div class="tab-pane fade <?php echo $k == 0 ? 'active in' : ''; ?>" id="<?php echo $horario->ahh_data; ?>" >
                    <table class="table table-bordered table-striped table-condensed table-hover">
                        <thead>
                        <th>Dia</th>
                        <th>Horário</th>
                        <th>Disponível</th>
                        <th></th>
                        </thead>
                        <tbody>
                            <?php
                            $sql = $this->db->query(""
                                    . "SELECT * from  agendamento__horario "
                                    . "WHERE ahh_data = '$horario->ahh_data' "
                                    . "AND ahh_data >= current_date() "
                                    . "AND ahh_tipo = $tipo "
                                    . "ORDER BY ahh_data, ahh_turno");


                            foreach ($sql->result() as $dados) {
                                ?>
                                <tr>
                                    <td><?php echo $this->util->data($horario->ahh_data); ?></td>
                                    <td><?php echo $dados->ahh_turno; ?></td>
                                    <td><?php
                                        $vagas = $dados->ahh_vagas;
                                        $agendados = $this->db->query("select * from agendamento__agendados where aha_idHorario = $dados->ahh_id");
                                        echo $vagas - $agendados->num_rows() . " / ";
                                        echo $vagas;
                                        ?>
                                    </td>
                                    <td>
                                        <?php if ($agendados->num_rows() < $vagas) { ?>
                                            <a class='btn btn-primary btn-xs' href='<?php echo site_url("aluno/agendamento/agendaDia/" . $dados->ahh_id); ?> '>Agendar</a> 
                                        <?php } else { ?>
                                            Esgotado
                                        <?php }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<!--
<style>
    .table-striped>tbody>tr:nth-child(odd)>td, 
    .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #67b168;
    }

    .table-striped2>tbody>tr:nth-child(odd)>td, 
    .table-striped2>tbody>tr:nth-child(odd)>th {
        background-color: #176e61
    }

</style> 
</section>

<style>
    .table-striped>tbody>tr:nth-child(odd)>td, 
    .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #67b168;
    }

    .table-striped2>tbody>tr:nth-child(odd)>td, 
    .table-striped2>tbody>tr:nth-child(odd)>th {
        background-color: #176e61
    }

</style>

-->