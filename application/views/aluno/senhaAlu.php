<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            ALTERAR SENHA PARA PRIMEIRO ACESSO
        </div>
        <div class="col-md-6 col-xs-6 text-right">
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px;">
    <div>
        <?php echo validation_errors(); ?>
    </div>
    <?php echo form_open() ?>
    <div class="form-group">
        <?php $this->util->input2('E-mail', 'alu_email', '', '', 4, '', 'required', 2, 'email'); ?>
        <?php $this->util->input2('Nova Senha', 'alu_senha', '', 's1', 2, '', 'required', 2, 'password'); ?>
        <?php $this->util->input2('Repetir Senha', '', '', 's2', 2, '', 'required', 2, 'password'); ?>
        <input type="hidden" value="<?php echo $alu_id; ?>" name="alu_id">
    </div>

    <div class="col-md-2">
        <label>&nbsp;</label>
        <input type=submit  value=Alterar class='btn btn-success form-control'>
    </div> 

    <?php echo form_close() ?>
</div>
<style>
    label{
        color: black;
    }

</style>

<script>
    $('.form-horizontal').submit(function () {
        var s1, s2;
        s1 = $('.s1').val();
        s2 = $('.s2').val();

        if (s1 !== s2) {
            alert('Senhas não conferem');
            return false;
        }

        if ((s1.length) < 6) {
            alert('Mínimo de 6 Dígitos');
            return false;
        }
    });
</script>
