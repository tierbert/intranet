<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            HORÁRIOS 
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a class='btn btn-info' href='<?php echo site_url('adm/painelcpp/horarioCppNovo') ?>'>
                NOVO HORÁRIO
            </a>    
            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <?php if (count($horarios) != 0) { ?>
        <table class='table table-bordered table-striped table-condensed table-hover'>
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Horário</th>
                    <th>Quantidade Vagas</th>
                    <th>Agendados</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($horarios->result() as $dado) {
                    ?><tr>
                        <td><?php echo $this->util->data($dado->tch_data) ?> </td>

                        <td><?php echo $dado->tch_horario ?> </td>
                        <td><?php echo $dado->tch_vagas ?> </td>
                        <td>
                            <?php
                            $agendados = $this->tcaa->getByhorario($dado->tch_id);
                            if (count($agendados) != 0) {
                                ?>
                                <table class='table table-bordered table-striped table-condensed table-hover'>
                                    <thead>
                                        <tr>
                                            <th>Horário</th>
                                            <th>Aluno</th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($agendados->result() as $dado) {
                                            ?><tr>
                                                <td><?php echo $dado->tch_horario ?> </td>
                                                <td><?php echo $dado->alu_nome ?> </td> 
                                            </tr>
                                        <?php } ?>

                                    </tbody>
                                </table>
                                <ul class='pagination pagination-small'>
                                    <li>
                                        <?php //echo $paginacao;  ?>
                                    </li>
                                </ul>
                            <?php } else { ?>
                                <h4>Nenhum registro encontrado</h4>
                            <?php } ?> 
                        </td>
                        <td>
                            <a class='btn btn-xs btn-success' href="<?php echo site_url('adm/painelcpp/horarioCppEdit/' . $dado->tch_id) ?>">Alterar</a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <ul class='pagination pagination-small'>
            <li>
                <?php //echo $paginacao;  ?>
            </li>
        </ul>
    <?php } else { ?>
        <h4>Nenhum registro encontrado</h4>
    <?php } ?>
</div>  
</div>


