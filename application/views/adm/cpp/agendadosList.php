<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            ALUNOS AGENDADOS PARA O HORÁRIO
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('adm/painelcpp'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <?php if (count($agendados) != 0) { ?>
        <table class='table table-bordered table-striped table-condensed table-hover'>
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Horário</th>
                    <th>Aluno</th> 
                </tr>
            </thead>
            <tbody>
                <?php foreach ($agendados->result() as $dado) {
                    ?><tr>
                        <td><?php echo $this->util->data($dado->tch_data) ?> </td>
                        <td><?php echo $dado->tch_horario ?> </td>
                        <td><?php echo $dado->alu_nome ?> </td> 
                    </tr>
                <?php } ?>

            </tbody>
        </table>
        <ul class='pagination pagination-small'>
            <li>
                <?php //echo $paginacao; ?>
            </li>
        </ul>
    <?php } else { ?>
        <h4>Nenhum registro encontrado</h4>
    <?php } ?> 
</div>

