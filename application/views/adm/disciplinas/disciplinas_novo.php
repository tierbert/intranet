
<h2>Editando Cadastro</h2>
<hr>
<div class=form-horizontal>    
    <div>
        <?php echo validation_errors(); ?>
    </div>

    <?php echo form_open() ?>
      <?php $this->util->input('dis_idCurso', 'dis_idCurso', $object->dis_idCurso); ?>
      <?php $this->util->input('dis_codigo', 'dis_codigo', $object->dis_codigo); ?>
      <?php $this->util->input('dis_nome', 'dis_nome', $object->dis_nome); ?>
      <?php $this->util->input('dis_turma', 'dis_turma', $object->dis_turma); ?>
      <?php $this->util->input('dis_nivel', 'dis_nivel', $object->dis_nivel); ?>
      <?php $this->util->input('dis_obs', 'dis_obs', $object->dis_obs); ?>
      <?php $this->util->input('dis_dataProva', 'dis_dataProva', $object->dis_dataProva); ?>
      <?php $this->util->input('dis_horaProva', 'dis_horaProva', $object->dis_horaProva); ?>
      <?php $this->util->input('dis_salaProva', 'dis_salaProva', $object->dis_salaProva); ?>
      <?php $this->util->input('dis_idProfessor', 'dis_idProfessor', $object->dis_idProfessor); ?>
  
    
    <div class=form-group>
        <input type=submit  value=Salvar class='btn btn-primary'>
    </div>            
    <?php echo form_close() ?>
    <div>
