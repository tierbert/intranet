<div class="painel">
    <div class="row">
        <h3 class='pull-left'>CADASTRAR HORÁRIO</h3>
        <div class="pull-right">
            <a class='btn btn-primary' href='<?php echo site_url('adm/esportelazer/horarios') ?>'>
                VOLTAR
            </a>
        </div>
    </div>
    <div class="row">
        <?php echo form_open(); ?>
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-md-2">
                    <label>DIA DA SEMANA</label>
                    <select name="eho_diaSemana" required="true" class="form-control">
                        <?php
                        $dias = $this->util->diasSemana();
                        if ($dados->eho_diaSemana != NULL) {
                            ?>
                            <option value="<?php echo $dados->eho_diaSemana; ?>"><?php echo $dias[$dados->eho_diaSemana]; ?></option>
                        <?php } ?>
                        <option value="">SELECIONE O DIA</option>
                        <?php foreach ($this->util->diasSemana() as $key => $dia) { ?>
                            <option value="<?php echo $key; ?>"><?php echo $dia; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3">
                    <label>HORÁRIO</label>
                    <input value="<?php echo $dados->eho_horario; ?>" name="eho_horario" type="text" class="form-control" required="true">
                </div>
                <div class="col-md-3    ">
                    <label>MODALIDADE</label>
                    <select name="eho_modalidade" required="true" class="form-control">
                        <?php if ($dados->eho_modalidade) { ?>
                            <option value="<?php echo $dados->eho_modalidade; ?>"><?php echo $dados->eho_modalidade; ?></option>
                        <?php } ?>
                        <option value="">SELECIONE O MODALIDADE</option>
                        <option value="FUTSAL_HANDEBOL_BASQUETE">FUTSAL / HANDEBOL / BASQUETE</option>
                        <option value="VOLEI">VÔLEI</option>
                    </select>
                </div>
                <div class="col-md-1">
                    <label>&nbsp;</label>
                    <input value="CADASTRAR" type="submit" class="form-control btn btn-primary ">
                </div>
            </div>
        </div>    
        <?php echo form_close(); ?>
    </div>