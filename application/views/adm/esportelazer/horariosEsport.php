<div class="painel">
    <div class="row">
        <h3 class='pull-left'>HORÁRIOS</h3>
        <div class="pull-right">

            <a class='btn btn-primary' href='<?php echo site_url('adm/esportelazer/horarioCad') ?>'>
                CADASTRAR HORÁRIO
            </a>
            <a class='btn btn-primary' href='<?php echo site_url('adm/esportelazer') ?>'>
                VOLTAR
            </a>
        </div>
    </div>

    <?php if ($horarios->num_rows() > 0) { ?>
        <table class='table table-bordered table-striped table-condensed table-hover'>
            <thead>
                <tr>
                    <th>DIA DA SEMANA</th>
                    <th>HORÁRIO</th>
                    <th>MODALIDADE</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($horarios->result() as $dado) {
                    $dias = $this->util->diasSemana();
                    ?><tr>
                        <td><?php echo $dias[$dado->eho_diaSemana] ?> </td>
                        <td><?php echo $dado->eho_horario ?> </td>
                        <td><?php echo $dado->eho_modalidade ?> </td>
                        <td>
                            <a class='btn btn-xs btn-primary' href="<?php echo site_url('adm/esportelazer/horarioEdit/' . $dado->eho_id) ?>">
                                EDITAR
                            </a>
                            <a class='btn btn-xs btn-danger' href="<?php //echo site_url('adm/professores/delete/' . $dado->pro_id)          ?>">
                                EXCLUIR
                            </a>
                        </td>
                    </tr>
                <?php } ?>

            </tbody>
        </table>
    <?php } else { ?>
        <h4>Nenhum registro encontrado</h4>
    <?php } ?>
</div>