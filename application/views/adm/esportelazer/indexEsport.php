<div class="painel">
    <div class="row">

        <div class="col-md-6">
            <h2 class="">SOLICITAÇÕES DE UTILIZAÇÃO DA QUADRA</h2>
        </div>
        <div class="col-md-6 text-right">
            <a class='btn btn-info ' href='<?php echo site_url('adm/esportelazer/lancar/') ?>'>
                LANÇAR HORÁRIOS P/RESERVA
            </a>
            <a class='btn btn-success ' href='<?php echo site_url('adm/esportelazer/horarios') ?>'>
                HORÁRIOS
            </a>
            <a class='btn btn-danger ' href='<?php echo site_url('adm/painel') ?>'>
                VOLTAR
            </a>
        </div>
    </div>
    <legend style="padding: 5px;"></legend>
    <div class="row">
        <div class="col-xl-8 col-lg-12">
            <?php if ($horarios->num_rows() > 0) { ?>
                <div class="table-responsive">
                    <table id="recent-orders" class="table table-hover table-striped mb-0">
                        <thead>
                            <tr>
                                <th>DIA DA SEMANA</th>
                                <th>DATA</th>
                                <th>HORÁRIO</th>
                                <th>MODALIDADE</th>
                                <th>ALUNO</th>
                                <th>OPÇÕES</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $dias = $this->util->diasSemana();
                            foreach ($horarios->result() as $dado) {
                                $cor = '';
                                if ($dado->ere_status == 'CONFIRMADA') {
                                    $cor = 'success';
                                }
                                ?>
                                <tr class="<?php echo $cor; ?>">
                                    <td class="text-truncate"><?php echo $dias[$dado->eho_diaSemana] ?> </td>
                                    <td class="text-truncate"><?php echo $this->util->data2($dado->ere_data) ?> </td>
                                    <td class="text-truncate"><?php echo $dado->eho_horario ?> </td>
                                    <td class="text-truncate"><?php echo $dado->eho_modalidade ?> </td>
                                    <td class="text-truncate"><?php echo $dado->alu_nome ?> </td>
                                    <td class="text-truncate">
                                        <?php
                                        if ($dado->ere_idAluno) {
                                            if ($dado->ere_status == 'CONFIRMADA') {
                                                ?>
                                                CONFIRMADA
                                            <?php } else { ?>
                                                <a class='btn btn-xs btn-success' href="<?php echo site_url('adm/esportelazer/confirmar/' . $dado->ere_id) ?>">
                                                    CONFIRMAR
                                                </a>
                                            <?php } ?>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                <?php } else { ?>
                    <h4>NENHUM REGISTRO ENCONTRADO</h4>
                <?php } ?>
                </table>
            </div>
        </div>
    </div>
</div>