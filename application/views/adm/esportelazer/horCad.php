
<h2>CADASTRO DE HORÁRIO</h2>
<hr>
<div class=form-horizontal>    
    <div>
        <?php echo validation_errors(); ?>
    </div>

    <?php echo form_open() ?>
    <div class=form-group>
        <?php $this->util->select2('Modalidade', 'hor_idModalidade', $professores, '', '', '4', '', '', '', ''); ?>
    </div>

    <div class=form-group>
        <div class="col-md-2">
            <br>
            <input name="hor_idTurno" type="hidden" value="<?php echo $idTurno; ?>">
            <input name="hor_dia" type="hidden" value="<?php echo $dia; ?>">
            <input type=submit  value=Salvar class='btn btn-primary form-control'>
        </div>

    </div>            
    <?php echo form_close() ?>
</div>
