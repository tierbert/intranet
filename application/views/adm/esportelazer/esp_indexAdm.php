<?php
if ($semana == '') {
    $semana = date('W') - 1;
    $day = date('w');
    $domingo = date('m-d-Y', strtotime('-' . $day . ' days'));
    $datas = array(
        '1' => date('d-m-Y', strtotime('+' . (1 - $day) . ' days')), //segunda
        '2' => date('d-m-Y', strtotime('+' . (2 - $day) . ' days')), // terca
        '3' => date('d-m-Y', strtotime('+' . (3 - $day) . ' days')), // quarta
        '4' => date('d-m-Y', strtotime('+' . (4 - $day) . ' days')), // quinta
        '5' => date('d-m-Y', strtotime('+' . (5 - $day) . ' days')), //sexta
        '6' => date('d-m-Y', strtotime('+' . (6 - $day) . ' days')), //sabado
    );
} else {

    $dias = ($semana * 7) - 1;
    $diaDomingo = date('Y-m-d', strtotime('2018-01-01' . " + $dias  days"));

    $day = date('w', strtotime($diaDomingo));
    $domingo = date('m-d-Y', strtotime('-' . $day . ' days'));
    $datas = array(
        '1' => date('d-m-Y', strtotime($diaDomingo . '+' . (1 - $day) . ' days')), //segunda
        '2' => date('d-m-Y', strtotime($diaDomingo . '+' . (2 - $day) . ' days')), // terca
        '3' => date('d-m-Y', strtotime($diaDomingo . '+' . (3 - $day) . ' days')), // quarta
        '4' => date('d-m-Y', strtotime($diaDomingo . '+' . (4 - $day) . ' days')), // quinta
        '5' => date('d-m-Y', strtotime($diaDomingo . '+' . (5 - $day) . ' days')), //sexta
        '6' => date('d-m-Y', strtotime($diaDomingo . '+' . (6 - $day) . ' days')), //sabado
    );
}

$volta = '';
$avanca = '';
$semanaAtual = date('W') - 1;
if ($semana == $semanaAtual) {
    //$volta = 'disabled';
} elseif ($semana > $semanaAtual + 1) {
    //$avanca = 'disabled';
}
if ($semana > $semanaAtual + 2) {
    //echo "<script>;window.location.href='" . site_url() . "/aluno/tcc/agendarTcc'</script>";
}
?>
<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            <span class="glyphicon glyphicon-flag"></span>
            Esporte e Lazer
        </div>
        <div class="col-md-2">
            <a href="<?php echo site_url('adm/esportelazer/index/' . ($semana - 1)); ?>" class=" btn btn-primary <?php echo $volta; ?>">
                <span class="glyphicon glyphicon-backward"></span>
            </a>
            <a href="<?php echo site_url('adm/esportelazer/index/'); ?>" class=" btn btn-primary ">
                <span class="glyphicon glyphicon-home"></span>
            </a>
            <a href="<?php echo site_url('adm/esportelazer/index/' . ($semana + 1)); ?>" class=" btn btn-primary <?php echo $avanca; ?>">
                <span class="glyphicon glyphicon-forward"></span>
            </a>
        </div>
        <div class="col-md-4 text-right">

            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">

    <table class="table table-striped table-bordered table-hover">
        <tr id="navba">
            <th>HORÁRIOS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
            <th class="text-center">SEGUNDA FEIRA <br> <?php echo $datas['1']; ?></th>
            <th class="text-center">TERÇA <br> <?php echo $datas['2']; ?></th>
            <th class="text-center">QUARTA <br> <?php echo $datas['3']; ?></th>
            <th class="text-center">QUINTA <br> <?php echo $datas['4']; ?></th>
            <th class="text-center">SEXTA <br> <?php echo $datas['5']; ?></th>
            <th class="text-center">SÁBADO <br> <?php echo $datas['6']; ?></th>
        </tr>
        <?php
        foreach ($turnos->result() as $turno) {
            ?>
            <tr>
                <th class="">
                    <?php echo $turno->tur_horario; ?>
                </th>
                <?php
                for ($dia = 1; $dia < 7; $dia++) {
                    $horario = $this->esportemodel->getHorario($turno->tur_id, $dia);
                    ?>
                    <td>
                        <?php
                        if ($horario->num_rows() > 0) {
                            $dados = $horario->row();
                            $agenda = $this->esportemodel->getByHorarioData($dados->hor_id, $this->util->data($datas[$dia]));
                            if ($agenda->num_rows() > 0) {
                                if ($agenda->row('eag_idAluno') == 0) {
                                    echo '<span style="color: blue">RESERVADO</span>';
                                    ?>
                                    <hr style="margin: 5px 0 5px 0;">
                                    <?php
                                } else {
                                    echo '<span style="color: red">' . $agenda->row('alu_nome') . '</span>';
                                    ?>
                                    <hr style="margin: 5px 0 5px 0;">
                                    <?php echo '<span style="color: red">' . strtoupper($agenda->row('eag_modalidade')) . '</span>'; ?>

                                    <!--
                                    <a href = "<?php echo site_url('adm/tcc//reservDel/' . $agenda->row('tca_id')); ?>" class = "btn btn-danger btn-xs pull-right" style = "font-size: 12px;">
                                        x
                                    </a>
                                    -->
                                    <?php
                                }
                            } else {
                                echo '<span style="color: green">HORÁRIO LIVRE</span>';
                                ?>

                                <hr style="margin: 5px 0 5px 0;">
                                <a href = "<?php echo site_url('adm/esportelazer/reservar/' . $dados->hor_id . '/' . $this->util->data($datas[$dia])); ?>" class = "btn btn-primary btn-xs" style = "font-size: 12px;">
                                    Reservar
                                </a>

                                <?php
                            }
                            ?>


                            <!--
                        <a href="<?php echo site_url('adm/tcc//horDel/' . $dados->hor_id); ?>"  class="btn btn-danger btn-xs" style="font-size: 12px;">
                            Excluir
                        </a>
                            -->

                            <?php
                        } else {
                            ?>
                            <a href="<?php echo site_url('adm/esportelazer/horCad/' . $turno->tur_id . '/' . $dia); ?>" class="btn btn-primary btn-xs" style="font-size: 12px;">
                                +
                            </a>
                        <?php } ?>

                    </td>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>
</div>




<style>
    /*
    .table-striped>tbody>tr:nth-child(odd)>td, 
    .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #337ab7;
    }
    .table-striped2>tbody>tr:nth-child(odd)>td, 
    .table-striped2>tbody>tr:nth-child(odd)>th {
        background-color: #337ab7;
    }
    */


    /* Style the navbar */
    #navbar {
        overflow: hidden;
    }

    /* Navbar links */
    #navbar a {
        float: left;
        display: block;
        color: #f2f2f2;
        text-align: center;
        padding: 14px;
        text-decoration: none;
    }

    /* Page content */
    .content {
        padding: 16px;
    }

    /* The sticky class is added to the navbar with JS when it reaches its scroll position */
    .sticky {
        margin-top: 100px;
        position: fixed;
        top: 0;
        width: 100%;
    }

    /* Add some top padding to the page content to prevent sudden quick movement (as the navigation bar gets a new position at the top of the page (position:fixed and top:0) */
    .sticky + .content {
        padding-top: 60px;
    }

</style>


<script>

    // When the user scrolls the page, execute myFunction 
    window.onscroll = function () {
        myFunction()
    };

// Get the navbar
    var navbar = document.getElementById("navbar");

// Get the offset position of the navbar
    var sticky = navbar.offsetTop;

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
    function myFunction() {
        if (window.pageYOffset >= sticky) {
            navbar.classList.add("sticky")
        } else {
            navbar.classList.remove("sticky");
        }
    }
</script>



