<div class="painel">
    <div class="row">

        <h3 style="text-align: center;">LANÇAR HORÁRIOS PARA RESERVA</h3>
        <div class="pull-right">
            <a class='btn btn-primary' href='<?php echo site_url('adm/esportelazer') ?>'>
                VOLTAR
            </a>
        </div>
    </div>

    <div class="row">
        <?php echo form_open(); ?>
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-md-3">
                    <label>ESCOLHA O PERÍODO</label>
                    <input value="<?php if (isset($periodo)) echo $periodo; ?>" name="periodo" type="text" class="form-control periodo" required="true">
                </div>
                <div class="col-md-1">
                    <input name="control" value="gerar" type="hidden">

                    <label>&nbsp;</label>

                    <input value="GERAR" type="submit" class="form-control btn btn-success ">
                </div>
            </div>
        </div>    
        <?php echo form_close(); ?>
    </div>

    <div class="row">
        <?php
        if (isset($horariosDia)) {
            echo form_open();
            ?>
            <table class="table table-bordered table-striped table-hover">
                <tr>
                    <th></th>
                    <th>DATA</th>
                    <th>DIA DA SEMANA</th>
                    <th>HORÁRIO</th>
                    <th>MODALIDADE</th>
                </tr>
                <?php
                $dias = $this->util->diasSemana();
                foreach ($horariosDia as $hd) {
                    $valor = $hd['idHorario'] . '|' . $hd['data'] . '|' . $hd['diaSemana'] . '|' . $hd['horario'] . '|' . $hd['modalidade'];
                    ?>
                    <tr>
                        <td><input value="<?php echo $valor; ?>" type="checkbox" name="diasSelecionados[]" checked="true"></td>
                        <td><?php echo $this->util->data2($hd['data']) ?></td>
                        <td><?php echo $dias[$hd['diaSemana']]; ?></td>
                        <td><?php echo $hd['horario']; ?></td>
                        <td><?php echo $hd['modalidade']; ?></td>
                    </tr>
                <?php } ?>
                <tr></tr>
            </table>
            <input type="hidden" name="control" value="salvar" class="btn btn-primary">
            <input type="submit" value="CONFIRMAR" class="btn btn-primary">

            <?php echo form_close(); ?>
            <hr>

            <hr>

        <?php } ?>
    </div>
    <link href="<?php echo base_url('assets'); ?>/js/datepick5/css/redmond.datepick.css" rel="stylesheet">
    <script src="<?php echo base_url('assets'); ?>/js/datepick5/js/jquery.plugin.min.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/datepick5/js/jquery.datepick.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/datepick5/js/jquery.datepick-pt.js"></script>
    <script>
        $(function () {
            $('.periodo').datepick({
                rangeSelect: true,
                onClose: null,
                showTrigger: '#calImg'
            });
        });
    </script>