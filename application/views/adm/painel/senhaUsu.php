<hr>
<div class=form-horizontal style="margin-left: 100px;">    
    <h4>Alterar Senha para primeiro acesso</h4>
    <div>
        <?php echo validation_errors(); ?>
    </div>
    <?php echo form_open() ?>
    <?php $this->util->input('E-mail', 'usu_email', '', '', 4, '', 'required', 2, 'email'); ?>
    <?php $this->util->input('Nova Senha', 'usu_senha', '', 's1', 2, '', '', 2, 'password'); ?>
    <?php $this->util->input('Repetir Senha', '', '', 's2', 2, '', '', 2, 'password'); ?>

    <input type="hidden" value="<?php echo $usu_id;?>" name="usu_id">

    <div class=form-group>
        <label class="col-md-2"></label>
        <input type=submit  value=Alterar class='btn btn-primary bt'>
    </div>            
    <?php echo form_close() ?>
</div>
<style>
    label{
        color: black;
    }

</style>

<script>




    $('.form-horizontal').submit(function () {
        var s1, s2;
        s1 = $('.s1').val();
        s2 = $('.s2').val();

        if (s1 !== s2) {
            alert('Senhas não conferem');
            return false;
        }

        if ((s1.length) < 6) {
            alert('Mínimo de 6 Dígitos');
            return false;
        }



    });



</script>
