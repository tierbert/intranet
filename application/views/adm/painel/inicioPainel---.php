<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Sistema Acadêmico - Interno</title>

        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url('assets/'); ?>css/bootstrap.min.css" rel="stylesheet" type="text/css">

        <!-- Fonts -->
        <link href="<?php echo base_url('assets/'); ?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/'); ?>css/nivo-lightbox.css" rel="stylesheet" />
        <link href="<?php echo base_url('assets/'); ?>css/nivo-lightbox-theme/default/default.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/'); ?>css/animate.css" rel="stylesheet" />
        <!-- Squad theme CSS -->
        <link href="<?php echo base_url('assets/'); ?>css/style.css" rel="stylesheet">
        <link href="<?php echo base_url('assets/'); ?>color/default.css" rel="stylesheet">


        <script src="<?php echo base_url('assets/'); ?>js/jquery.min.js"></script>

        <!-- =======================================================
            Theme Name: Ninestars
            Theme URL: https://bootstrapmade.com/ninestars-free-bootstrap-3-theme-for-creative/
            Author: BootstrapMade
            Author URL: https://bootstrapmade.com
        ======================================================= -->


    </head>
    <style>



        hr {
            border-top: 1px dashed ;
            border-bottom: 1px solid ;


            height: 4px;
        }

        #intro{	
            background-image: url(<?php echo base_url('assets/'); ?>img/img-bg.jpg);
            background-size: cover;
        }


    </style>
    <body data-spy="scroll">

        <div class="container">
            <ul id="gn-menu" class="gn-menu-main">
                <li class="gn-trigger">
                    <a class="gn-icon gn-icon-menu"><span>Menu</span></a>
                    <nav class="gn-menu-wrapper">
                        <div class="gn-scroller">
                            <ul class="gn-menu">
                                <li>
                                    <a href="<?php echo site_url('painel'); ?>" class="gn-icon gn-icon-archive">Painel Administrativo</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('inicio/loginAluno'); ?>" class="gn-icon gn-icon-archive">Painel Aluno</a>
                                </li>
                                <li>
                                    <a href="#" class="gn-icon gn-icon-archive">Painel Professor</a>
                                </li>
                                <li>
                                    <a href="#intro" class="gn-icon gn-icon-archive">Sair</a>
                                </li>
                            </ul>
                        </div><!-- /gn-scroller -->
                    </nav>
                </li>
                <li>
                    <a href="<?php echo site_url(); ?>">
                        Faculdade Guanambi</a></li>
                <li class="hidden-xs">
                    <ul class="company-social">

                    </ul>	
                </li>
            </ul>
        </div>
        <!-- Section: works -->

        <section id="intro" class="intro">
            <div class="slogan" class="col-md-12 col-md-12 ">
                <div style="background: rgba(254,254,254,0.3);" align="center"> 
                    <img class="img-circle" id="img_logo" src="<?php echo base_url('assets/'); ?>img/logo.png">
                    <h1 style="color:#ffffff; text-shadow: 1px 1px 1px #000000">LOGIN ADMINISTRATIVO </h1>

                    <div class="container" style="width: 40%">


                        <div id="div-forms">

                            <!-- Begin # Login Form -->
                            <form id="login-form" action="" method="post">
                                <div class="modal-body">
                                    <div id="div-login-msg" class="checkbox">
                                        <span id="text-login-msg"><b>Digite seu Login e senha.</b></span>
                                    </div>
                                        <input  name="login" id="login_username" class="form-control" type="text" placeholder="Login" required>
                                        <input  name="senha" id="login_password" class="form-control" type="password" placeholder="Senha" required>
                                    <div class="checkbox" style="text-align:left">
                                        <label>
                                            <input type="checkbox"> <b>Lembrar-me </b>
                                        </label>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div>
                                        <button type="submit" class="btn btn-skin scroll btn-lg btn-block">Login</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <br/>
                    <br/>
                    <br/>
                </div>
                <div class="col-md-3"></div>



            </div>
        </section>










        <!-- Core JavaScript Files -->

        <script src="<?php echo base_url('assets/'); ?>js/bootstrap.min.js"></script>
        <script src="<?php echo base_url('assets/'); ?>js/jquery.easing.min.js"></script>	
        <script src="<?php echo base_url('assets/'); ?>js/classie.js"></script>
        <script src="<?php echo base_url('assets/'); ?>js/gnmenu.js"></script>
        <script src="<?php echo base_url('assets/'); ?>js/jquery.scrollTo.js"></script>
        <script src="<?php echo base_url('assets/'); ?>js/nivo-lightbox.min.js"></script>
        <script src="<?php echo base_url('assets/'); ?>js/stellar.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="<?php echo base_url('assets/'); ?>js/custom.js"></script>
        <script src=<?php echo base_url('assets/'); ?>"contactform/contactform.js"></script>




    </div>	

</body>


</html>
