<div class="row painel hidden-print">
    <?php echo form_open_multipart(); ?>
    <h3>REMESSAS</h3>
    <hr>
    <div class="form-group">
        <div class="col-md-3">   
            <label>ARQUIVO</label> 
            <input name="anexos" type="file" class="form-control" required="true">
            <input type="hidden" value="imp" name="imp">
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-2">
            <label>&nbsp;</label>
            <input type="submit" class="form-control btn btn-success" value="ENVIAR ARQUIVO">
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<div class="row">
    <hr>
</div>
<div class="row painel">
    <?php
    if ($arquivo != NULL) {
        ?>
        <table class='table table-bordered table-striped table-condensed table-hover'>
            <tbody>
                <tr >
                    <th class="text-center">data_mov</th>
                    <th class="text-center">cod_fies</th>
                    <th class="text-center">CPF	</th>
                    <th class="text-center">vlr_copart_ies</th>
                    <th class="text-center">Mora</th>	
                    <th class="text-center">Multa</th>
                </tr>
                <?php
                $dados = explode("\n", $arquivo);
                unset($dados[0]);
                foreach ($dados as $dado) {
                    if ($dado == '')
                        continue;
                    $linha = explode(';', $dado);
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $linha[0] ?> </td>
                        <td class="text-center"><?php echo $linha[4] ?> </td>
                        <td class="text-center"><?php echo $linha[5] ?> </td>
                        <td class="text-right"><?php echo $linha[7] ?> </td>
                        <td class="text-right"><?php echo $linha[8] ?> </td>
                        <td class="text-right"><?php echo $linha[9] ?> </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php } ?>
</div>