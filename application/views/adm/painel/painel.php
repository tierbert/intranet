
<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            ACESSO INTERNO
        </div>
        <div class="col-md-6 col-xs-6 text-justify">
            <?php
            $nome = $this->session->userdata('usu_nome');
            ?>

            <h4> Olá <?php echo $nome; ?>,</h4> Seja Bem-Vindo a intranet. 
            <p>Na Intranet FG, estão disponíveis todos os sistema internos.  </p>
        </div>
    </div>
</div>

<style type="text/css" >
    .textoVertical {
        writing-mode:tb-rl;
        -webkit-transform:rotate(180deg);
        -moz-transform:rotate(270deg);
        -o-transform: rotate(270deg);
        font-weight: bold;
        color: #337ab7;
        font-size: 18px;
    }
</style>




<div class="painel" style="min-height: 150px;">
    <div class="col-md-1 textoVertical" style="border-left: 4px solid; border-color: #337ab7; width: 50px">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        INÍCIO 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div class="col-md-2 text-center">
        <a href="<?php echo site_url('adm/painel/solicitacoes'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-plus"></span>
            <br>
            CADASTROS
        </a>
    </div>

<!-- 
    <div class="col-md-2 text-center">
        <a href="<?php echo site_url('/adm/EstagioPsicAdm/index/1'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-book"></span>
            <br>
            ESTÁGIO DE <br>PSICOLOGIA
        </a>
    </div>
 -->
  <!--   <div class="col-md-2 text-center">
        <a href="<?php echo site_url('/adm/EstagioDirAdm/index/1'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-book"></span>
            <br>
            ESTÁGIO DE <br>DIREITO
        </a>
    </div>
-->
    <div class="col-md-2 text-center">
        <a href="<?php echo site_url('/adm/painel/docsEstagio'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-download-alt"></span>
            <br>
            TERMOS DE COMPROMISSO DE ESTÁGIO
        </a>
    </div>

<!-- 
    <div class="col-md-2 text-center">
        <a href="<?php echo base_url('assets'); ?>/materialApoio/manual_procedimentos.pdf" target="_blanck">


            <span style="font-size: 60px;" class="glyphicon glyphicon-file"></span>
            <br>
            MANUAL DE <br>
            PROCEDIMENTOS ACADÊMICOS
        </a>
    </div>
-->
<!-- 
    <div class="col-md-2 text-center">
        <a href="<?php echo site_url('adm/agendamento/agend_agendados'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-file"></span>
            AGENDADOS<br>
            CARTEIRINHA ESTUDANTIL
        </a>
    </div>
-->


</div>


<div class="painel" style="min-height: 150px;">
    <div class="col-md-1 textoVertical" style="border-left: 4px solid; border-color: #337ab7; width: 50px">
        &nbsp;&nbsp;  AGENDA &nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <!-- 
    <div class="col-md-2 text-center">
        <a href="<?php echo site_url('adm/tcc/tccAgendados'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-calendar"></span>
            <br>
            AGENDAMENTO PLANTÃO DE TCC
        </a>
    </div>
    <div class="col-md-2 text-center">
        <a href="<?php echo site_url('adm/painelcpp'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-comment"></span>
            <br>
            AGENDAMENTO<br> CPP
        </a>
    </div>
-->
    <div class="col-md-2 text-center">

        <a href="<?php echo site_url('adm/horarios/horario/calendario/sala'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-blackboard"></span>
            <br>
            AGENDAMENTO SALA DE AULA
        </a>

    </div>

    <div class="col-md-2 text-center">

        <a href="<?php echo site_url('adm/horarios/horario/calendario/lab'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-alert"></span>
            <br>
            AGENDAMENTO<br> LAB DE SAÚDE
        </a>

    </div>

    <div class="col-md-2 text-center">

        <a href="<?php echo site_url('adm/horarios/horario/calendario/labsaude2'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-alert"></span>
            <br>
            AGENDAMENTO <br>LAB DE SAÚDE II
        </a>

    </div>
</div>



<div class="painel" style="min-height: 150px;">
    <div class="col-md-1 textoVertical" style="border-left: 4px solid; border-color: #337ab7; width: 50px">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  RH &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
<!-- 
    <div class="col-md-2 text-center">
        <a href="<?php echo site_url('adm/rh/selecao'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-copy"></span>
            <br>
            SELEÇÃO RH
        </a>
    </div>

-->
    <div class="col-md-2 text-center">

        <a href="<?php echo site_url('adm/rh/prosoft'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-share-alt"></span>
            <br>
            PROSOFT
        </a>
    </div>
    <div class="col-md-2 text-center">
        <a href="<?php echo site_url('adm/capacita'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-refresh"></span>
            <br>
            CICLO DE <br>CAPACITAÇÃO
        </a>
    </div>

<!-- 
    <div class="col-md-2 text-center">

        <a href="<?php echo site_url('adm/rh/capacitaRh'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-refresh"></span>
            <br>
            RELATÓRIOS<br>
            CAPACITAÇÃO
        </a>
    </div>
-->
    <?php if (BASEPATH == '/home/www/html/intranet1/system/') { ?>
        <div class="col-md-2 text-center ">                      



            <a href="<?php echo site_url('adm/rh/Iniciorh'); ?>">
                <span style="font-size: 60px;" class="glyphicon glyphicon-briefcase"></span>
                <br>
                SOLICITAÇÕES<br>
                PARA RH
            </a>
        </div>

    <?php } ?>



</div>



<div class="painel" style="min-height: 150px;">
    <div class="col-md-1 textoVertical" style="border-left: 4px solid; border-color: #337ab7; width: 50px">
        &nbsp;&nbsp;  OUTROS &nbsp;&nbsp;&nbsp;&nbsp;
    </div>





<!-- 
    <div class="col-md-2 text-center">                      


        <a href="<?php echo site_url('adm/ProjetosAdm'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-duplicate"></span>
            <br>
            ATIVIDADES COMP. E APOIO EM EVENTOS
        </a>
    </div>
-->

    <div class="col-md-2 text-center">                      
        <a href="<?php echo site_url('adm/painel/arquivos'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-file"></span>
            <br>
            ARQUIVOS
        </a>
    </div>
<!-- 
    <div class="col-md-2 text-center">                      
        <a href="<?php echo site_url('adm/programas/posList'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-education"></span>
            <br>
            PÓS-GRADUAÇÃO
        </a>
    </div>

    <div class="col-md-2 text-center">                      
        <a href="<?php echo site_url('adm/esportelazer'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-flag"></span>
            <br>
            ESPORTE LAZER
        </a>
    </div>
-->
    <div class="col-md-2 text-center">                      
        <a href="<?php echo site_url('adm/Solicitacoesadm'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-bullhorn"></span>
            <br>
            RELATÓRIOS SUBSTITUTIVA
        </a>
    </div>

</div>




<div class="painel" style="min-height: 150px;">
    <div class="col-md-1 textoVertical" style="border-left: 4px solid; border-color: #337ab7; width: 50px">
        &nbsp;&nbsp;  ASCOM &nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="col-md-2 text-center">                      
        <a href="<?php echo site_url('adm/programas/tvs'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-modal-window"></span>
            <br>
            TV'S
        </a>
    </div>

    <div class="col-md-2 text-center">                      
        <a href="<?php echo site_url('adm/programas/anuncios'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-modal-window"></span>
            <br>
            ANÚNCIOS
        </a>
    </div>
<!-- 
    <div class="col-md-2 text-center">                      
        <a href="<?php echo site_url('adm/painel/foto'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-camera"></span>
            <br>
            FOTOS ALUNOS
        </a>
    </div>


    <div class="col-md-2 text-center">                      
        <a href="<?php echo site_url('adm/painel/fotoFunc'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-camera"></span>
            <br>
            FOTOS FUNCIONÁRIOS
        </a>
    </div>
</div>
-->

<!-- 
<div class="painel" style="min-height: 150px;">
    <div class="col-md-1 textoVertical" style="border-left: 4px solid; border-color: #337ab7; width: 50px">
        &nbsp;&nbsp;  SISTEMAS &nbsp;&nbsp;
    </div>
    <div class="col-md-2 text-center">                      
        <a href="<?php echo site_url('adm/salalivre/recepcao'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-blackboard"></span>
            <br>
            SALA LIVRE
        </a>
    </div>
    <div class="col-md-2 text-center">                      
        <a href="<?php echo site_url('adm/programas/anuncios'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-book"></span>
            <br>
            BIBLIOTECA
        </a>
    </div>
</div>
-->

<?php
$imgProf = base_url() . 'assets/tv/anuncios/func.jpg';
?>
<button type="button" class="btn btn-primary btn-lg btn-modal hidden" data-toggle="modal" data-target="#myModal">
    &nbsp;
</button>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin: 50px; width: 92%; height: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Fechar</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <?php $link = $this->db->where('anu_perfil', 'prof')->get('anuncios')->row() ?>
                <a href="<?php echo $link->anu_link; ?>" target="_blanck">
                    <img src="<?php echo $imgProf; ?>" class="img-responsive">
                </a>
            </div>
        </div>
    </div>
</div>
<?php
if (@getimagesize($imgProf)) {
    ?>
    <script>
        $('.btn-modal').click();
    </script>
<?php } ?>