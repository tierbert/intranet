
<div class="painel" style="min-height: 400px;">
       <label>Enviar:</label>
<div class="alert alert-info back-widget-set text-center">
            <form role="form" class="form-inline" method="post" action="" id="formulario" enctype="multipart/form-data">             
                <input type="hidden" name="envia">
                <input class="form-control" type="file" required="true" name="arquivo"/>
                <button type="submit" class="btn btn-primary">Enviar Arquivo</button>
            </form>
        </div>
        <ul class="list-group">
            <?php
            /*
              $path = base_url() . "assets/arquivos/";
              $diretorio = dir($path);
              $arquivos = array();
             * 
             */
            echo "Lista de Arquivos: <br>";

            foreach ($arquivos as $arqu) {
                if ($arqu == 'index.html') {
                    continue;
                }
                ?>
                <div class="col-md-9 col-sm-10 col-xs-10">
                    <?php
                    echo "<a href='" . $pastaRemota . $arqu . "'><li class='list-group-item'>" . $arqu . "</li></a>";
                    ?>
                </div>
                <div class="col-md-3 col-sm-2 col-xs-2" style="margin-top: 15px;">
                    <a class="exclui" id="<?php echo $arqu; ?>" ><span class="glyphicon glyphicon-trash"></span></a>
                </div>
            <?php } ?>
        </ul>
    </div>
</div>
<script type='text/javascript'>
    $(function () {
        $('.exclui').click(function () {
            var confirma = confirm('Confirma a exclusão');

            if (confirma) {
                var arq = this.id;
                $.post('<?php echo site_url('adm/painel/arquivosDel') ?>', {arquivo: arq}, function (data) {
                    window.location.reload();
                });
            }
        })

        if (window.localStorage) {
            if (!localStorage.getItem('firstLoad'))
            {
                localStorage[ 'firstLoad' ] = true;
                window.location.reload();
            } else
                localStorage.removeItem('firstLoad');
        }

    });


</script>
</div>



