
<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            MÓDULOS
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <section class="cards-section ">
        <div class="container">    


            <br>    
            <div id="cards-wrapper" class="cards-wrapper row">
                <div class="item item-green col-md-3 col-sm-6 col-xs-6">
                    <div class="item-inner text-center">
                        <div class="icon-holder text-center" style="font-size: 35px;">
                            <span class="glyphicon glyphicon-education"></span>
                        </div>
                        <!--//icon-holder-->
                        <h3 class="title">Cadastro de Alunos</h3>
                        <!--<p class="intro">Cadastro, Edição</p>-->
                        <a class="link" href="<?php echo site_url('adm/alunos/'); ?>"><span></span></a>
                    </div><!--//item-inner-->
                </div><!--//item-->
                <div class="item item-pink item-2 col-md-3 col-sm-6 col-xs-6">
                    <div class="item-inner">
                        <div class="icon-holder text-center" style="font-size: 35px;">
                            <span aria-hidden="true" class="glyphicon glyphicon-blackboard"></span>
                        </div><!--//icon-holder-->
                        <h3 class="title">Professores Cadastrados</h3>
                       <!-- <p class="intro">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</p>-->
                        <a class="link" href="<?php echo site_url('adm/professores'); ?>"><span></span></a>
                    </div><!--//item-inner-->
                </div><!--//item-->
                <div class="item item-blue col-md-3 col-sm-6 col-xs-6">
                    <div class="item-inner">
                        <div class="icon-holder text-center" style="font-size: 35px;">
                            <span aria-hidden="true" class="glyphicon glyphicon-user"></span>
                        </div><!--//icon-holder-->
                        <h3 class="title">Funcionários Cadastrados</h3>
                      <!-- <p class="intro">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</p>-->
                        <a class="link" href="<?php echo site_url('adm/usuarios'); ?>"><span></span></a>
                    </div><!--//item-inner-->
                </div><!--//item-->
                                <!--
                <div class="item item-purple col-md-3 col-sm-6 col-xs-6">
                    <div class="item-inner">
                        <div class="icon-holder text-center" style="font-size: 35px;">
                            <span aria-hidden="true" class="glyphicon glyphicon-search"></span>
                        </div>
                        <h3 class="title">Consultar Solicitações</h3>
                      
                        <a class="link" href="<?php echo site_url('adm/solicitacoesadm'); ?>"><span></span></a>
                    </div>
                </div>
                <div class="item item-primary col-md-3 col-sm-6 col-xs-6">
                    <div class="item-inner">
                        <div class="icon-holder text-center" style="font-size: 35px;">
                            <span aria-hidden="true" class="glyphicon glyphicon-book"></span>
                        </div>
                        <h3 class="title">Cadastro de Disciplina</h3>
                     
                        <a class="link" href="<?php echo site_url('adm/horario'); ?>"><span></span></a>
                    </div>
                </div>
                <div class="item item-orange col-md-3 col-sm-6 col-xs-6">
                    <div class="item-inner">
                        <div class="icon-holder text-center" style="font-size: 35px;">
                            <span aria-hidden="true" class="glyphicon glyphicon-th-list"></span>
                        </div>
                        <h3 class="title">Lista de Assinatura</h3>
                      
                        <a class="link" href="<?php echo site_url('adm/solicitacoesadm/relatorioAssinat'); ?>"><span></span></a>
                    </div>
                </div>
                <div class="item item-green col-md-3 col-sm-6 col-xs-6">
                    <div class="item-inner">
                        <div class="icon-holder text-center" style="font-size: 35px;">
                            <span aria-hidden="true" class="glyphicon glyphicon-print"></span>
                        </div>
                        <h3 class="title">Relatórios Por Status</h3>
                      
                        <a class="link" href="<?php echo site_url('adm/solicitacoesadm/relatorioStatus'); ?>"><span></span></a>
                    </div>
                </div>
                <div class="item item-red col-md-3 col-sm-6 col-xs-6">
                    <div class="item-inner">
                        <div class="icon-holder text-center" style="font-size: 35px;">
                            <span aria-hidden="true" class="glyphicon glyphicon-print"></span>
                        </div>
                        <h3 class="title">Relatórios Professor</h3>
                      
                        <a class="link" href="<?php echo site_url('adm/solicitacoesadm/relatorios'); ?>"><span></span></a>
                    </div>
                </div>

                <div class="item item-red col-md-3 col-sm-6 col-xs-6">
                    <div class="item-inner text-center">
                        <div class="icon-holder text-center" style="font-size: 35px;">
                            <span aria-hidden="true" class="glyphicon glyphicon-cloud-download"></span>
                        </div>
                        <h3 class="title">Remessa</h3>
                      
                        <a class="link" href="<?php echo site_url('adm/painel/remessa'); ?>"><span></span></a>
                    </div>
                </div>
                -->


            </div><!--//cards-->
        </div><!--//container-->
    </section><!--//cards-section-->
</div>


