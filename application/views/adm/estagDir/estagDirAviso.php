<h2>
    SELEÇÃO DOS CAMPOS DE DIREITO ESPECÍFICO I E II – 2017.2
</h2>
<h3>
    Prezados (as) alunos (as),
</h3>
<p>
    Estamos iniciando mais um semestre e o Núcleo de Estágios é responsável pelo processo de distribuição dos grupos dos Estágios Supervisionados Específicos I e II do semestre 2017.2. Para a escolha do campo de estágio, o aluno deverá efetuar a matrícula em Estágio Específico I ou II, via sistema Jacad e, posteriormente, participar do processo seletivo para o campo.
</p>
<p>
    A vaga previamente selecionada estará diretamente vinculada à aprovação nas disciplinas que são pré-requisitos. Caso o aluno tenha alguma pendência a resolver, como quebra de pré-requisito, por exemplo, deverá procurar a Coordenação do Curso ou o Núcleo de Estágios antes de fazer a escolha do campo. É importante ressaltar que os dias e horários de campo e supervisão poderão sofrer alterações.
</p>
<p>
    Informamos ainda, que esse procedimento não substitui a obrigatoriedade da matrícula acadêmica e financeira no estágio escolhido. A conclusão da escolha do campo de estágio será feita quando o aluno entregar o Termo de Compromisso (Contratos de Estágio), que pode ser realizada na sala 05 do campus da FG, até o dia 31/07/17, em horário comercial (das 08 às 12h e de 14 às 18h). 
</p>

<p>
    Atenciosamente,<br>
    Núcleo de Estágios da FG.
<p>

    <a href="<?php echo site_url('aluno/EstagioDir'); ?>" class="btn btn-success btn-lg ok disabled">
        CONTINUAR
    </a>

    <script>
        $(function () {
            setTimeout(function () {
                $('.ok').removeClass('disabled');
            }, 3000);
        })


    </script>