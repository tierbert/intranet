<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            <?php echo ucfirst($titulo); ?> 
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/EstagioDirAdm/index/1'); ?>" class=" btn btn-primary">
                CADASTRAR LOCAIS
            </a>
            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">

                <div>
                    <hr style="margin: 0 0 0 0">


                    <ul class="nav nav-tabs">
                        <li  role="presentation" class="pendentes <?php if ($tipo == 'campo') echo 'active'; ?>" >
                            <a href="<?php echo site_url('adm/EstagioDirAdm/index/1'); ?>" >
                                CAMPO / ORIENTAÇÃO
                            </a>
                        </li>


                        <!--
                        <li role="presentation" class="aprovados <?php if ($tipo == 'orientacao') echo 'active'; ?>">
                            <a href="<?php echo site_url('adm/EstagioDirAdm/index/2'); ?>">
                                ORIENTAÇÃO
                            </a>
                        </li>
                        -->
                    </ul>
                    <hr style="margin: 0 0 10px  0">
                </div>




                <?php foreach ($locais as $local) { ?>

                    <div class="panel panel-default">
                        <div class="panel-heading tour-step tour-step-7">
                            <br>
                            <a href="<?php echo site_url('adm/EstagioDirAdm/cadVaga/' . $local->el_id); ?>" class="btn btn-primary btn-xs  pull-right">
                                CADASTRAR VAGAS
                            </a>
                            <h3 class="panel-title" style="font-weight: bold">
                                <?php echo $local->el_nome; ?>
                            </h3>
                            <h5><?php echo $local->el_status ?></h5>


                            <br>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">

                                    <table class="table table-bordered" style="color: black" cellspacing="0" cellpadding="0">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Tipo</th>
                                                <th class="text-center">Local</th>
                                                <th class="text-center">Dia / Período</th>
                                                <th class="text-center">Vagas</th>
                                                <th class="text-center"></th>
                                            </tr>
                                        </thead>
                                        <tbody >
                                            <?php
                                            $agendamento1 = $this->estag__vagas_dirmodel->getBylocal($local->el_id, $tipo)->result();
                                            foreach ($agendamento1 as $vaga) {
                                                $agendados = $this->estag__agendados_dirmodel->agendadosVaga($vaga->ev_id, $tipo);
                                                $cor = '';
                                                if ($vaga->ev_tipo == 'campo' && $vaga->ev_tipo2 != 'INATIVA') {
                                                    $cor = 'success';
                                                } elseif ($vaga->ev_tipo == 'orientacao' && $vaga->ev_tipo2 != 'INATIVA' || $vaga->ev_tipo2 == 'INATIVA') {
                                                    $cor = 'danger';
                                                }
                                                ?>
                                                <tr class=" <?php echo $cor; ?>">
                                                    <td> CAMPO / ORIENTAÇÃO <?php //echo $tipoExt; ?></td>
                                                    <td><?php echo $vaga->el_nome; ?></td>
                                                    <td><?php echo $vaga->ev_periodo; ?></td>
                                                    <td><?php echo ($vaga->ev_vagas - $agendados->num_rows()) . ' de ' . $vaga->ev_vagas; ?></td>
                                                    <td> 
                                                        <a href="<?php echo site_url('adm/EstagioDirAdm/editVaga/' . $local->el_id . '/' . $vaga->ev_id); ?>" class="btn btn-primary btn-xs">
                                                            <i class="glyphicon glyphicon-edit"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5" class="text-center">
                                                        INSCRITOS
                                                        <table class="table table-hover table-striped table-condensed table-bordered" style="width: 80%; text-align: left">
                                                            <tr>
                                                                <th>#</th>
                                                                <th>matrícula</th>
                                                                <th>Nome</th>
                                                                <th>Data Inscrição</th>
                                                                <th></th>
                                                            </tr>
                                                            <?php
                                                            $i = 0;
                                                            foreach ($agendados->result() as $agendado) {
                                                                $i++;
                                                                ?>
                                                                <tr style="text-align: left">
                                                                    <td>
                                                                        <?php echo $i; ?></td>
                                                                    <td><?php echo $agendado->alu_matricula; ?></td>
                                                                    <td><?php echo $agendado->alu_nome; ?></td>
                                                                    <td><?php echo $agendado->ea_data; ?></td>
                                                                    <td>
                                                                        <a href="<?php echo site_url('adm/EstagioDirAdm/delete/' . $agendado->ea_id); ?>" class="hidden-print">
                                                                            <i class="glyphicon glyphicon-trash"></i>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                        </table>
                                                    </td>
                                                </tr>
                                            <?php }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!--/#list -->
                    </div> <!--/#main -->
                <?php }
                ?>
            </div>
        </div>
    </div>    
</div>
<style>
    .active li{
        background: #337ab7;
    }


    tabs>li.active>a:focus, .nav-tabs>li.active>a {
        background: #337ab7;
        border: 1px solid #337ab7;
        color: white;
        cursor: pointer

    }

    tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
        background: #337ab7;
        border: 1px solid #337ab7;
        color: white;
        cursor: pointer

    }


</style>