<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            EDITAR COLABORADOR
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/usuarios'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal>    
        <div>
            <?php echo validation_errors(); ?>
        </div>

        <?php echo form_open() ?>
        <div class=form-group>
            <?php $this->util->input2('NOME', 'usu_nome', $object->usu_nome, '', 7, '', 'required'); ?>
        </div>
        <div class=form-group>
            <br>
            <?php
            $this->util->select2('SETOR DE LOTAÇÃO', 'usu_setor', $setores, $object->usu_setor, '', 3, '', 'required');
            $this->util->input2('Nº DE REGISTRO   RH', 'usu_login', $object->usu_login, '', 2, '', 'required', 2, 'NUMBER');
            $this->util->input2('SENHA', 'usu_senha', $object->usu_senha, '', 2, '', 'required', 2, 'password');
            ?>
            <?php
            $perfil = array(
                '' => '',
                'secretaria' => 'SECRETARIA',
                'financeiro' => 'FINANCEIRO',
                'coordenacao' => 'COORDENACAO',
                'coordenador' => 'COORDENADOR',
                'recepcao' => 'RECEPCAO',
                'recep_coord' => 'RECEPCAO DA COORDENACAO',
                'cpp' => 'cpp',
                'CAA' => 'CAA',
                'TI' => 'TI',
                'POS-GRADUACAO' => 'POS-GRADUACAO',
                'COORD.NUPEX' => 'COORD.NUPEX',
                'PROGPEX' => 'PROGPEX',
                'COORD.FINANCEIRO' => 'COORD.FINANCEIRO',
            );
            ?>
        </div>
        <hr>
        <div class=form-group>
            <?php
            $this->util->select2('PERFIL (SOMENTE SE FOR UTILIZAR SISTEMAS ESPECÍFICOS)', 'usu_perfil', $perfil, $object->usu_perfil, '', 5);
            $this->util->select2('STATUS', 'usu_status', array('ATIVO' => 'ATIVO', 'INATIVO' => 'INATIVO'), $object->usu_status, '', 2, '', 'required');
            $grupos = array(
                '' => 'Selecione o Grupo',
                'administrativo' => 'administrativo',
                'coord_setor' => 'coord_setor',
                'coord_curso' => 'coord_curso',
            );
            $this->util->select2('SETOR GRUPO', 'usu_grupo', $grupos, $object->usu_grupo, '', 2, '', 'required');
            ?>
        </div>
        <div class=form-group>
            <div class="col-md-2">
                <label>&nbsp;</label>
                <input type=submit  value=Salvar class='btn btn-success form-control'>
            </div>
        </div>            
        <?php echo form_close() ?>
    </div>
</div>