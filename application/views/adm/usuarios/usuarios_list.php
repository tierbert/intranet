<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            COLABORADORES
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a class='btn btn-info' href='<?php echo site_url('adm/usuarios//criar/') ?>'>
                NOVO COLABORADOR
            </a>    
            <a href="<?php echo site_url('adm/painel/solicitacoes'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class="row">
        <div class="form-horizontal">
            <?php echo form_open(); ?>
            <div class="form-group">
                <div class="col-md-2">
                    <label>Nº REGISTRO RH</label>
                    <input value="<?php echo (isset($post['regRh'])) ? $post['regRh'] : ''; ?>" class="form-control" type="number" name="regRh">
                </div>
                <div class="col-md-4">
                    <label>NOME</label>
                    <input value="<?php echo (isset($post['nome'])) ? $post['nome'] : ''; ?>" class="form-control" type="text" name="nome" >
                </div>
                <div class="col-md-2">
                    <label>PERFIL</label>
                    <input value="<?php echo (isset($post['perfil'])) ? $post['perfil'] : ''; ?>" class="form-control" type="text" name="perfil" >
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <input class=" btn btn-success form-control" type="submit" value="PESQUISAR">
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>

    </div>

    <div class="row" style="margin-top: 10px;">

        <?php if (count($dados) != 0) { ?>
            <table class='table table-bordered table-striped table-condensed table-hover' style="text-align: justify;">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>REG RH</th>
                        <th>PERFIL</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dados as $dado) {
                        ?><tr><td><?php echo $dado->usu_nome ?> </td>
                            <td><?php echo $dado->usu_login ?> </td>
                            <td><?php echo $dado->usu_perfil ?> </td>
                            <td>
                                <a  href="<?php echo site_url('adm/usuarios/editar/' . $dado->usu_id) ?>" class='btn btn-xs btn-warning'>
                                    Editar
                                </a>
                            </td>
                        </tr>
                    <?php } ?>

                </tbody>
            </table>
            <ul class='pagination pagination-small'>
                <li>
                    <!--<?php echo $paginacao; ?> -->
                </li>
            </ul> 

        <?php } else { ?>
            <h4>Nenhum registro encontrado</h4>
        <?php } ?>
    </div>
</div> 