<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            <?php echo ucfirst($titulo); ?>
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a class='btn btn-info' href='<?php echo site_url('adm/professores/criar/') ?>'>
                NOVO <?php echo ucfirst(substr($titulo, 0, -2)) ?>
            </a>    
            <a href="<?php echo site_url('adm/painel/solicitacoes'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class="row">
        <div class="form-horizontal hidden-print" style="margin-bottom: 10px;">
            <?php echo form_open(); ?>

            <div class="form-group">
                <div class="col-md-3" >
                    <label>Num. Registro:</label>
                    <input name="pro_registroRh" style="color: black" value="<?php echo $this->input->post('pro_registroRh') ?>" type="text" class="form-control" >
                </div>
                <div class="col-md-3" >
                    <label>Professor:</label>
                    <input name="nome" style="color: black" value="<?php echo $this->input->post('nome') ?>" type="text" class="form-control" >
                </div>

                <div class="col-md-2">
                    <label> &nbsp;</label> 
                    <button type="submit" class="form-control btn btn-success">Pesquisar</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>       
    </div>

    <div class="row">
        <?php if (count($dados) != 0) { ?>
            <table class='table table-bordered table-striped table-condensed table-hover'>
                <thead>
                    <tr>
                        <th>Num Registro</th>
                        <th>NOME PROFESSOR</th>
                        <th>CPF </th>
                        <th>AÇÃO</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dados as $dado) {
                        ?><tr>
                            <td class="text-center"><?php echo $dado->pro_registroRh ?> </td>
                            <td><?php echo $dado->pro_nome ?> </td>
                            <td><?php echo $dado->pro_cpf ?> </td>
                            <td>
                               <!-- <a class='btn btn-xs btn-success' href="<?php echo site_url('adm/professores/discProf/' . $dado->pro_id) ?>">
                                    Disciplinas
                                </a> -->

                                <a class='btn btn-xs btn-warning' href="<?php echo site_url('adm/professores/editar/' . $dado->pro_id) ?>">
                                    Editar
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <ul class='pagination pagination-small'>
                <li>
                    <?php echo $paginacao; ?>
                </li>
            </ul>
        <?php } else { ?>
            <h4>Nenhum registro encontrado</h4>
        <?php } ?>
    </div>
</div>

