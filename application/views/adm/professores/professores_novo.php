<style>
    .form-group{
        margin-bottom: 15px;
    }

    p{
        background: red;
        color: white;
        padding: 5px;
        font-weight: bold;
        margin: 10px 0;
    }
</style>
<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            NOVO PROFESSOR
        </div>
        <div class="col-md-6 col-xs-6 text-right">   
            <a href="<?php echo site_url('adm/professores'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>     
        </div>
    </div>
</div>
<div class="painel" style="min-height: 300px;">

    <div class=form-horizontal>    
        <div>
            <?php echo validation_errors(); ?>
        </div>
        <?php echo form_open() ?>
        <div class="form-group">
            <?php $this->util->input2('Registro Rh', 'pro_registroRh', set_value('pro_registroRh'), '', '2', '', 'required'); ?>
            <?php $this->util->input2('Nome', 'pro_nome', set_value('pro_nome'), '', 8, '', 'required'); ?>
        </div>
        <div class="form-group">
            <?php $this->util->input2('CPF', 'pro_cpf', set_value('pro_cpf'), 'cpf', 2); ?>
            <?php $this->util->input2('Senha Intranet', 'pro_senhaIntra', set_value('pro_senhaIntra'), '', '4', '', 'required', '2', 'password'); ?>
            <?php $this->util->input2('Senha Sala Livre (somente números)', 'pro_senha', set_value('pro_senha'), '', '4', '', 'required', '2', 'password'); ?>
        </div>
        <?php
        $titulacao = array(
            '' => '',
            "GRADUADO" => 'GRADUADO',
            "ESPECIALISTA" => 'ESPECIALISTA',
            "MESTRE" => 'MESTRE',
            "DOUTOR" => 'DOUTOR'
        );

        $regime = array(
            '' => '',
            "INTEGRAL" => 'INTEGRAL',
            "PARCIAL_PROPORCIONAL" => 'PARCIAL_PROPORCIONAL',
            "PARCIAL_REGULAR" => 'PARCIAL_REGULAR',
            "HORISTA" => 'HORISTA'
        );
        ?>
        <div class=form-group>
            <?php $this->util->select2('titulação', 'pro_titulacao', $titulacao, '', '', 5); ?>
            <?php $this->util->select2('regime', 'pro_regime', $regime, '', '', 5); ?>
        </div>

        <div class=form-group>
            <div class="col-md-2">
                <label>&nbsp;</label>
                <input type=submit  value=Salvar class='btn btn-success form-control'>
            </div>
        </div>    
    </div>
    <?php echo form_close() ?>
</div>
<?php $this->util->mascaras() ?>

