<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Sala livre</title>
        <meta name="description" content="description">
        <meta name="author" content="DevOOPS">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="<?php echo base_url() . 'assets/'; ?>plugins/bootstrap/bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url() . 'assets/'; ?>plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
        <link href="<?php echo base_url() . 'assets/'; ?>plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
        <link href="<?php echo base_url() . 'assets/'; ?>plugins/select2/select2.css" rel="stylesheet">
        <link href="<?php echo base_url() . 'assets/'; ?>css/style.css" rel="stylesheet">
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
                        <script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
                        <script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
        <![endif]-->
        
        <script src="<?php echo base_url() . 'assets/'; ?>plugins/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url() . 'assets/'; ?>plugins/jquery-ui/jquery-ui.min.js"></script>
        <script src="<?php echo base_url() . 'assets/'; ?>plugins/bootstrap/bootstrap.min.js"></script>

    </head>
    <body style="background: #e4b9b9 url(<?php echo base_url('assets'); ?>/img/devoops_pattern_b10.png) 0 0 repeat;">
        <!--Start Header-->
        <div id="screensaver">
            <canvas id="canvas"></canvas>
            <i class="fa fa-lock" id="screen_unlock"></i>
        </div>
        <div id="modalbox">
            <div class="devoops-modal">
                <div class="devoops-modal-header">
                    <div class="modal-header-name">
                        <span>Basic table</span>
                    </div>
                    <div class="box-icons">
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="devoops-modal-inner">
                </div>
                <div class="devoops-modal-bottom">
                </div>
            </div>
        </div>
        <header class="navbar">
            <div class="container-fluid expanded-panel">
                <div class="row">
                    <div id="logo" class="col-xs-12 col-sm-2">
                        <a href="<?php echo site_url('recepcao')  ?>">Sala Livre</a>
                    </div>
                    <div id="top-panel" class="col-xs-12 col-sm-10">
                        <div class="row">
                            <div class="col-xs-8 col-sm-8 text-center">
                                <h2>FACULDADE GUANAMBI</h2>
                            </div>
                            <div class="col-xs-4 col-sm-4 top-panel-right">
                                <ul class="nav navbar-nav pull-right panel-menu">
                                    <li class="dropdown">
                                        <a href="<?php echo site_url('auth/sair'); ?>" >
                                            <div class="avatar">
                                                <img src="<?php echo base_url() . 'assets/'; ?>img/avatar.jpg" class="img-rounded" alt="avatar" />
                                            </div>
                                            <div class="user-mini pull-right">
                                               
                                                <span>Sair</span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!--End Header-->
        <!--Start Container-->
        <div id="main" class="container-fluid" >
            <div class="row">
                <div id="sidebar-left " class="col-xs-2 col-sm-1" >
                    
                </div>
                <!--Start Content-->
                <div id="content" class="col-xs-12 col-sm-10"  >
                    <!--Start Breadcrumb-->
                    <div class="row" >
                        <div id="breadcrumb" class="col-xs-12" style="background: #e4b9b9 url(<?php echo base_url('assets'); ?>/img/devoops_pattern_b10.png) 0 0 repeat;">
                            <ol class="breadcrumb" style="margin-top: 10px;">
                                <li>
                                    <a href="<?php echo site_url('recepbiblio/inicio'); ?>" class="btn btn-primary">
                                        CHAVES
                                    </a>
                                    <a href="<?php echo site_url('recepbiblio/salasEstudo'); ?>" class="btn btn-primary">
                                        SALAS DE ESTUDO
                                    </a>
                                </li>
                            </ol>
                        </div>
                    </div>
                    <!--End Breadcrumb-->

                    <?php $this->load->view("$pagina"); ?>

                    <!--
                    <div id="dashboard_tabs" class="col-xs-12 col-sm-12" style="min-height: 500px">
                    </div>
                    -->
                </div>
                <!--End Content-->
            </div>
        </div>
    </body>
</html>
