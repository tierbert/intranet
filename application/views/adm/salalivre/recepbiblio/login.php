<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Faculdade Guanambi</title>
        <meta name="description" content="description">
        <meta name="author" content="Evgeniya">
        <meta name="keyword" content="keywords">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="<?php echo base_url() . 'assets/'; ?>plugins/bootstrap/bootstrap.css" rel="stylesheet">

        <link href="<?php echo base_url() . 'assets/'; ?>css/style.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
                        <script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
                        <script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body style="background: #e4b9b9 url(<?php echo base_url('assets'); ?>/img/devoops_pattern_b10.png) 0 0 repeat;">
        <div class="container-fluid">
            <div id="page-login" class="row">
                <div class="col-xs-12 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                    <div class="box">
                        <div class="box-content">
                            <?php $this->load->view('botoes'); ?>
                            <?php echo form_open(); ?>
                            <div class="text-center">
                                <h3 class="page-header">
                                    FACULDADE GUANAMBI
                                    <br>
                                    LABORATÓRIO LIVRE
                                </h3>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Usuário</label>
                                <input type="text" class="form-control" name="nome" required="true" autofocus="true"/>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Senha</label>
                                <input type="password" class="form-control" name="senha"  required="true    "/>
                            </div>
                            <div class="text-center">
                                <button type="submit" href="../index.html" class="btn btn-primary">Entrar</button>
                            </div>
                            <?php echo form_close(); ?>
                       
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
