<div class="box ui-draggable ui-droppable">
    <div class="box-header hidden-print">
        <div class="box-name">
            CHAVE
            <span class="pull-right">
                <a href="<?php echo site_url('recepbiblio/inicio'); ?>" class="btn btn-primary">
                    Voltar                    
                </a>
            </span>
        </div>
    </div>
    <div class="box-content">



        <div class="row">
            <div class="col-md-6">
                <div class=form-horizontal>    

                    <?php echo form_open(); ?>


                    <?php echo $this->util->input('Nº da Chave', '', $chave->cha_numero, '', 4); ?>
                    <input type="hidden" name="cm_id" value="<?php echo $locados->cm_id; ?>">

                    <?php echo $this->util->input('Nº de Matricula', '', $locados->cm_matricula, '', 8); ?>
                    <?php echo $this->util->input('Nome do Aluno', '', $locados->cm_nomeAluno, '', 8); ?>

                    <label class="col-md-2"></label>
                    <input type="submit" value="Devolver" class="btn btn-primary">

                    <?php echo form_close(); ?>

                </div>

            </div>
            <div class="col-md-6">

                Entregue: 
                <br>
                <?php echo $locados->cm_retirada; ?>
                <br><br>
                Tempo: 
                <br>
                <?php
                $data1 = $locados->cm_retirada;

                date_default_timezone_set('America/Bahia');
                $date = date('Y/m/d H:i:s ', time());
                $data2 = $date;

                $unix_data1 = strtotime($data1);
                $unix_data2 = strtotime($data2);

                $nHoras = ($unix_data2 - $unix_data1) / 3600;
                $nMinutos = (($unix_data2 - $unix_data1) % 3600) / 60;

                printf('%02d:%02d:00', $nHoras, $nMinutos);
                ?>

            </div>


        </div>
    </div>
</div>
<hr>
<div class="box ui-draggable ui-droppable">
    <div class="box-content">
        <div class="box-name">
            <h4>Histórico</h4>
            <table class="table table-hover table-striped">
                <tbody>
                    <tr>
                        <th>RETIRADA</th>
                        <th>ENTREGA</th>
                        <th>MATRICULA</th>
                        <th>NOME</th>
                    </tr>
                    <?php foreach ($dadosMov->result() as $his) { ?>
                        <tr>
                            <td><?php echo $this->util->data3($his->cm_retirada); ?></td>
                            <td><?php echo $his->cm_entrega ? $this->util->data3($his->cm_entrega) : ''; ?></td>
                            <td><?php echo $his->cm_matricula; ?></td>
                            <td><?php echo $his->cm_nomeAluno; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets') . '/plugins/select2/select2.js'; ?>"></script>
<link  src="<?php echo base_url('assets') . '/plugins/select2/select2.css'; ?>">

<script type="text/javascript">
    $(function () {
        $('select').select2();
    });


</script>