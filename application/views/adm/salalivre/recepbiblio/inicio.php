<div class="box ui-draggable ui-droppable">
    <div class="box-header">
        <div class="box-name">
            CHAVES
            <span class="pull-right">
                <a href="<?php echo site_url('recepcao/inicio'); ?>" class="btn btn-primary">
                    Voltar                    
                </a>
            </span>
        </div>
    </div>
    <div class="box-content" style="margin: 0px;">
        <div class="row">
            <?php
            //unset($kits['']);
            foreach ($chaves->result() as $chave) {
                $desabilita = '';
                $cor = '';
                $link = '';
                if ($chave->cha_idMovimentacao == 0) {
                    $cor = 'btn-primary';
                    $link = 'retira';
                } else {
                    $cor = 'btn-danger';
                    //$desabilita = 'disabled';
                    $link = 'devolver';
                }
                ?>            
                <a href="<?php echo site_url("recepbiblio/" . $link . "Chave/" . $chave->cha_id . '/' . $chave->cha_numero); ?>" style="font-size: 20px; padding: 5px; color: white" class="btn btn-lg <?php echo $cor; ?>" <?php echo $desabilita; ?>>
                    <?php echo str_pad($chave->cha_numero, 3, 0, STR_PAD_LEFT); ?>
                </a>
            <?php } ?>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets') . '/plugins/select2/select2.js'; ?>"></script>
<link  src="<?php echo base_url('assets') . '/plugins/select2/select2.css'; ?>">
<script type="text/javascript">
    $(function () {
        $('select').select2();
    });
</script>