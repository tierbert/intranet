<div class="box ui-draggable ui-droppable">
    <div class="box-header">
        <div class="box-name">
            CHAVE
            <span class="pull-right">
                <a href="<?php echo site_url('recepbiblio/inicio'); ?>" class="btn btn-primary">
                    Voltar                    
                </a>
            </span>
        </div>
    </div>
    <div class="box-content">




        <div class=form-horizontal>    

            <?php echo form_open('', array('class' => 'formulario')); ?>


            <?php echo $this->util->input('Nº da Chave', '', $numChave, '', 1); ?>
            <input type="hidden" name="cm_idChave" value="<?php echo $idChave; ?>">

            <?php echo $this->util->input('Nº de Matricula', 'cm_matricula', '', 'matricula'); ?>
            <?php echo $this->util->input('Nome do Aluno', 'cm_nomeAluno', '', 'nome'); ?>

            <label class="col-md-2"></label>
            <input type="submit" value="Salvar" class="btn btn-primary">

            <?php echo form_close(); ?>

        </div>


        <div class="row">


        </div>
    </div>
</div>
<hr>
<div class="box ui-draggable ui-droppable">
    <div class="box-content">
        <div class="box-name">
            <h4>Histórico</h4>
            <table class="table">
                <tbody>
                    <tr>
                        <th>RETIRADA</th>
                        <th>ENTREGA</th>
                        <th>MATRICULA</th>
                        <th>NOME</th>
                    </tr>
                    <?php foreach ($locados->result() as $his) { ?>
                        <tr>
                            <td><?php echo $this->util->data3($his->cm_retirada); ?></td>
                            <td><?php echo $his->cm_entrega ? $this->util->data3($his->cm_entrega) : ''; ?></td>
                            <td><?php echo $his->cm_matricula; ?></td>
                            <td><?php echo $his->cm_nomeAluno; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets') . '/plugins/select2/select2.js'; ?>"></script>
<link  src="<?php echo base_url('assets') . '/plugins/select2/select2.css'; ?>">

<script type="text/javascript">
    $(function () {
        $('select').select2();

        $('.formulario').submit(function () {
            var nome = $('.nome').val();
            var matricula = $('.matricula').val();

            if (nome == '' && matricula == '') {
                alert('Informe o Nome ou Nº de Matrícula');
                return false;
            }


        });




    });


</script>