
<h3>Reservar Sala</h3>
<hr>
<div class=form-horizontal>    
    <div>
        <?php echo validation_errors(); ?>
    </div>

    <?php echo form_open() ?>

    <div class="form-group">
        <label class="col-sm-1 control-label">Sala:</label>
        <div class="col-sm-4">
            <div class="form-control" style="background: #dadada">
                <?php echo $sala->sal_nome; ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="inputPassword" class="col-sm-1 control-label">Horário</label>
        <div class="col-sm-4">
            <div type="password" class="form-control" style="background: #dadada">
                <?php echo $horario->sh_horario; ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="inputPassword" class="col-sm-1 control-label">Alunos</label>
        <div class="col-sm-4">
            <input type="hidden" name="sala" value="<?php echo $sala->sal_id; ?>">
            <input type="hidden" name="horario" value="<?php echo $horario->sh_id; ?>">
            <textarea disabled="true" class="form-control" rows="5"><?php echo $movimentacao->sm_aluno; ?></textarea>
        </div>
    </div>





    <div class="form-group">
        <label for="inputPassword" class="col-sm-1 control-label">Entrada</label>
        <div class="col-sm-3">
            <div type="password" class="form-control" style="background: #dadada">
                <?php echo $movimentacao->sm_retirada; ?>

            </div>
        </div>
        <div class="col-sm-1">
            <?php if ($movimentacao->sm_retirada == NULL) { ?>
                <a href="<?php echo site_url('recepbiblio/sala_retirar/' . $movimentacao->sm_id . '/' . $idSala . '/' . $idHorario); ?>" class="btn btn-primary">
                    Entregar
                </a>
            <?php } ?>
        </div>
    </div>

    <div class="form-group">
        <label for="inputPassword" class="col-sm-1 control-label">Saída</label>
        <div class="col-sm-3">
            <div type="password" class="form-control" style="background: #dadada">
                <?php echo $movimentacao->sm_entrega; ?>

            </div>
        </div>
        <div class="col-sm-1">
            <?php if ($movimentacao->sm_retirada != NULL && $movimentacao->sm_entrega == NULL) { ?>
                <a href="<?php echo site_url('recepbiblio/sala_entregar/' . $movimentacao->sm_id . '/' . $idSala . '/' . $idHorario); ?>" class="btn btn-primary">
                    Devolver
                </a>
            <?php } ?>
        </div>
    </div>

    <div class="form-group">
        <label for="inputPassword" class="col-sm-1 control-label">&nbsp;</label>
        <div class="col-sm-4">
            <a href="<?php echo site_url('recepbiblio/salasEstudo'); ?>" class="btn btn-success">Voltar</a>
        </div>
    </div>

    <?php echo form_close() ?>
    <div>
