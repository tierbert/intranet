<div class="box ui-draggable ui-droppable" style="padding: 0px; margin: 0px;">
    <div class="box-content" style="margin: 0px;">
        <div class="row" style="padding: 0px;">
            <table class="table table-hover table-striped text-center">
                <tr>
                    <th>&nbsp;&nbsp;&nbsp;
                        <?php
                        echo date('d/m');
                        ?>
                    </th>
                    <?php foreach ($horarios->result() as $horario) { ?>
                        <th><?php echo $horario->sh_horario; ?></th>
                    <?php } ?>
                </tr>
                <?php
                $data = date('Y-m-d');
                foreach ($salas->result() as $sala) {
                    ?>
                    <tr>
                        <td><?php echo $sala->sal_nome; ?></td>
                        <?php foreach ($horarios->result() as $horario) { ?>
                            <td>
                                <?php
                                $reserva = $this->sala_movimentacaomodel->getByData($horario->sh_id, $sala->sal_id, $data);
                                if ($reserva->num_rows() > 0) {
                                    if ($reserva->row('sm_retirada') != NULL && $reserva->row('sm_entrega') != NULL) {
                                        ?>
                                        <a class="btn disabled btn-danger btn-block" style="margin: 0px;" alt="Reservar" >
                                            Utilizado
                                        </a>
                                    <?php } elseif ($reserva->row('sm_retirada') != NULL && $reserva->row('sm_entrega') == NULL) { ?>
                                        <a href="<?php echo site_url('recepbiblio/salaMovimentar/' . $sala->sal_id . '/' . $horario->sh_id); ?>" class="btn btn-warning btn-block" style="margin: 0px; color: #d9534f" alt="Reservar" >
                                            
                                            <?php
                                            $now = strtotime('NOW');
                                            $restante = 7200 - ( $now - strtotime($reserva->row('sm_retirada')));
                                            if ($restante < 0) {
                                                echo 'Esgotado!!';
                                            } else {
                                                $horas = floor($restante / 3600);
                                                $minutos = floor(($restante - ($horas * 3600)) / 60);
                                                //echo $restante . ' - ';
                                                echo $horas . ':' . $minutos;
                                            }
                                            ?>

                                        </a>
                                    <?php } else { ?>

                                        <a href="<?php echo site_url('recepbiblio/salaMovimentar/' . $sala->sal_id . '/' . $horario->sh_id); ?>" class="btn btn-danger btn-block" style="margin: 0px;" alt="Reservar" >
                                            Reservado
                                        </a>
                                    <?php } ?>
                                <?php } else { ?>
                                    <a href="<?php echo site_url('recepbiblio/salaReservar/' . $sala->sal_id . '/' . $horario->sh_id); ?>" class="btn btn-success btn-block" style="margin: 0px;" alt="Reservar" >
                                        Livre
                                    </a>
                                <?php } ?>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets') . '/plugins/select2/select2.js'; ?>"></script>
<link  src="<?php echo base_url('assets') . '/plugins/select2/select2.css'; ?>">
<script type="text/javascript">
    $(function () {
        $('select').select2();
    });
</script>