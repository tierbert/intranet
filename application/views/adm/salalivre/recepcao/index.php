<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            SALA LIVRE - CAMPUS: 
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('adm/painel/solicitacoes'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px; padding: 0">
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
            <?php echo form_open(); ?>
            <div class="text-center">
                <h3 class="page-header">Entrega de Kits</h3>
            </div>
            <div class="form-group">
                <label class="control-label">Nº de Registro</label>
                <input type="text" class="form-control" name="registro" autofocus required="true"/>
            </div>
            <div class="form-group">
                <label class="control-label">Senha</label>
                <input type="password" class="form-control" name="senha" required="true"/>
            </div>
            <div class="form-group">
                <div class=" text-center">
                    <label>&nbsp;</label>
                    <button type="submit" href="" class="btn btn-primary form-control">Entrar</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        <div class="col-md-7 col-md-offset-1" style="padding: 30px;">
            <div class="col-md-4">
                <a href="<?php echo site_url('adm/salalivre/recepcao/kits'); ?>" class="btn btn-primary btn-lg btn-block botao" >
                    Kits  
                </a>
            </div>
            <div class="col-md-4">
                <a class="btn btn-primary btn-lg btn-block botao" >
                    Professores
                </a>
            </div>
            <div class="col-md-4">
                <a class="btn btn-primary btn-lg btn-block botao" >
                    histórico
                </a>
            </div>

            <style>
                .botao{
                    padding: 30px;
                    margin-top: 10px
                }
            </style>

        </div>
    </div>
</div>
