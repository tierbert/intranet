<div class="box ui-draggable ui-droppable">
    <div class="box-header">
        <div class="box-name">

            <span></span>
        </div>
    </div>
    <div class="box-content">

        <div class="row">

            <div class="col-md-4">
                <i class="glyphicon glyphicon-user"></i>
                <?php echo $this->session->userdata('pro_nome'); ?>
                <hr>
                <h2>
                    <i class=""></i>
                    Kit nº: <?php echo $prof->row('kit_numero'); ?>
                </h2>
                <hr>
                <i class=""></i>
                <?php echo $prof->row('fre_inicio'); ?>
            </div>
            <div class="col-md-4">
                <div class="center">

                    <?php
                    $disabel1 = '';
                    $disabel2 = '';
                    $cor1 = 'btn-primary';
                    $cor2 = 'btn-primary';
                    if ($prof->num_rows() == 0) {
                        $disabel1 = '';
                        $disabel2 = 'pointer-events: none';
                        $cor2 = 'btn-danger';
                    } else {
                        $disabel1 = 'pointer-events: none';
                        $cor1 = 'btn-danger';
                    }
                    ?>

                    <a href="<?php echo site_url('adm/salalivre/recepcao/retiraKit'); ?>"  class="btn <?php echo $cor1; ?> btn-block" style="font-size: 25px; <?php echo $disabel1; ?>">
                        <br>
                        <i class="glyphicon glyphicon-arrow-up"></i>
                        RETIRAR KIT
                        <br>&nbsp;
                    </a>      
                    <a href="<?php echo site_url('adm/salalivre/recepcao/devolverKit') . '/' . $prof->row('fre_id'); ?>"  class="btn <?php echo $cor2; ?> btn-block" style="font-size: 25px; <?php echo $disabel2; ?>">
                        <br>
                        <i class="glyphicon glyphicon-arrow-down"></i>
                        DEVOLVER KIT
                        <br>&nbsp;
                    </a>      
                    <a href="<?php echo site_url('adm/salalivre/recepcao'); ?>" class="btn btn-primary btn-block" style="font-size: 25px;">
                        <br>
                        <i class="glyphicon glyphicon-arrow-left"></i>
                        CONCLUIR
                        <br>&nbsp;</a>      
                </div>

            </div>

            <div class="col-md-4">

            </div>




        </div>
    </div>
</div>