<div class="box ui-draggable ui-droppable">
    <div class="box-header">
        <div class="box-name">
            HORÁRIO
            <span class="pull-right">
                <a href="<?php echo site_url('recepcao/inicio'); ?>" class="btn btn-primary">
                    Voltar                    
                </a>
            </span>
        </div>
    </div>
    <div class="box-content">




        <div class=form-horizontal>    
            <div>
                <?php echo validation_errors(); ?>
            </div>

            <?php echo form_open() ?>
            <?php $this->util->select('Nome do Prof.', 'fre_idKit', $prof, 'select', '', 8); ?>


            <div class=form-group>
                
            </div>            
            <?php echo form_close() ?>
        </div>



        <div class="row">

            <hr>
            <?php
            if (isset($dados)) {
                $tabela = $dados;
                $tabela = str_replace('<table', "<table class='table table-condensed' style='width=100%; font-size: 10px;'", $tabela);

                echo $tabela;
            }
            ?>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets') . '/plugins/select2/select2.js'; ?>"></script>
<link  src="<?php echo base_url('assets') . '/plugins/select2/select2.css'; ?>">

<script type="text/javascript">
    $(function () {
        $('select').select2();
    });


</script>