<div class="box ui-draggable ui-droppable">
    <div class="box-header">
        <div class="box-name">
            HORÁRIO
            <span class="pull-right">
                <a href="<?php echo site_url('adm/salalivre/recepcao/inicio'); ?>" class="btn btn-primary">
                    Voltar                    
                </a>
            </span>
        </div>
    </div>
    <div class="box-content">




        <div class=form-horizontal>    

        </div>



        <div class="row">

            <hr>
            <?php
            if (isset($dados)) {
                $tabela = $dados;
                $tabela = str_replace('<table', "<table class='table table-condensed' style='width=100%; font-size: 10px;'", $tabela);

                echo $tabela;
            }
            ?>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                unset($kits['']);
                foreach ($kits as $key => $kit) {
                    $status = $this->db->query("select * from salalivre.frequencia where fre_idKit = $key  and fre_fim is null");
                    $desabilita = '';
                    $cor = '';
                    if ($status->num_rows() == 0) {
                        $cor = 'btn-primary';
                    } else {
                        $cor = 'btn-danger';
                        $desabilita = "pointer-events: none";
                    }
                    //echo $status->num_rows();
                    ?>            
                    <a href="<?php echo site_url('adm/salalivre/recepcao/retirarKit/' . $key); ?>" 
                       class="btn btn-lg <?php echo $cor; ?>"  
                       style="min-width: 130px; margin-top: 10px; <?php echo $desabilita; ?>">
                        Kit: <?php echo $kit; ?>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets') . '/plugins/select2/select2.js'; ?>"></script>
<link  src="<?php echo base_url('assets') . '/plugins/select2/select2.css'; ?>">

<script type="text/javascript">
    $(function () {
        $('select').select2();
    });


</script>