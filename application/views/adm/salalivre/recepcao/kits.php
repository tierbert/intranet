<div class="box ui-draggable ui-droppable">
    <div class="box-header">
        <div class="box-name">
            HORÁRIO
            <span></span>
        </div>
    </div>
    <div class="box-content">
        <h3 class='pull-left'>Kits Entregues: <?php echo $dados->num_rows(); ?> </h3>
        <?php if (count($dados) != 0) { ?>
            <table class='table table-bordered table-striped table-condensed table-hover'>
                <thead>
                    <tr>
                        <th>Professor</th>
                        <th>Nº do Kit</th>
                        <th>Retirada</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dados->result() as $dado) {
                        ?><tr>
                            <td><?php echo $dado->pro_nome ?> </td>
                            <td><?php echo $dado->kit_numero ?> </td>
                            <td><?php echo $dado->fre_inicio ?> </td>
                            <td>
                                <a class='btn btn-sm btn-danger' href="<?php echo site_url('recepcao/devolverkit/' . $dado->fre_id); ?>">Devolver</a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } else { ?>
            <h4>Nenhum registro encontrado</h4>
        <?php } ?>
    </div>
</div>