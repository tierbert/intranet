
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo base_url('assets'); ?>/bootstrap337/css/bootstrap.css" rel="stylesheet"> 
<link href="<?php echo base_url('assets/tema/'); ?>fonts/font-awesome/css/font-awesome.css" rel="stylesheet"> 
<script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>plugins/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/bootstrap337/js/bootstrap.js"></script>
<div class="row" style="padding: 20px;">
    <div class="row">
        <div class="col-lg-12 text-center">
            <a href="<?php echo site_url('adm/painel'); ?>" class="btn btn-primary pull-right" style="margin-left: 10px;">
                Voltar
            </a>
            <a href="<?php echo site_url('adm/horarios/horario'); ?>" class="btn btn-primary pull-right">
                Reservas
            </a>
            <h2 class="section-heading">Agendamento Sala de Aula</h2>
            <button class="btn btn-success btn-xs">Sala Disponível</button>
            <button class="btn btn-primary btn-xs">Agendamento Confirmado</button>
            <button class="btn btn-warning btn-xs">Aguardando Confirmação</button>
            <button class="btn btn-danger btn-xs">Sala Ocupada</button>

        </div> 
    </div>
    <br>
    <div class="col-md-12">   
        <?php
        $day = date('w');
        $domingo = date('m-d-Y', strtotime('-' . $day . ' days'));
        $datas = array(
            '1' => date('d-m-Y', strtotime('+' . (1 - $day) . ' days')), //segunda
            '2' => date('d-m-Y', strtotime('+' . (2 - $day) . ' days')), // terca
            '3' => date('d-m-Y', strtotime('+' . (3 - $day) . ' days')), // quarta
            '4' => date('d-m-Y', strtotime('+' . (4 - $day) . ' days')), // quinta
            '5' => date('d-m-Y', strtotime('+' . (5 - $day) . ' days')), //sexta
            '6' => date('d-m-Y', strtotime('+' . (6 - $day) . ' days')), //sabado
        );
        ?>
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <th></th>
                <th>SEGUNDA - <?php echo $datas['1']; ?></th>
                <th>TERÇA - <?php echo $datas['2']; ?></th>
                <th>QUARTA - <?php echo $datas['3']; ?></th>
                <th>QUINTA - <?php echo $datas['4']; ?></th>
                <th>SEXTA - <?php echo $datas['5']; ?></th>
                <th>SÁBADO - <?php echo $datas['6']; ?></th>
                <th></th>
            </tr>
            <?php
            foreach ($turnos->result() as $turno) {
                ?>
                <tr>
                    <td>
                        <?php echo $turno->tur_turno . '<br>' . $turno->tur_horario; ?>
                    </td>
                    <?php
                    for ($i = 1; $i < 7; $i++) {
                        ?>
                        <td>
                            <?php
                            $salas = $this->horariom->getSalas($turno->tur_id, $i, $tipo);
                            foreach ($salas->result() as $sala) {
                                $nomeSala = $sala->sal_abreviatura;
                                //$agendada = $this->horariom->getAgendaByDia($turno->tur_id, $sala->sal_id, $i, $datas[$i]);
                                ?>

                                <?php
                                //$nomeSala = str_replace('SALA ', 'S', $sala->sal_nome);
                                //$reserv = $this->horariom->getByHorario($turno->tur_id, $sala->sal_id, $i);
                                //$agendada = $this->horariom->getAgendaByDia($turno->tur_id, $sala->sal_id, $i, $datas[$i]);
                                if ($sala->res_id != NULL) {
                                    if ($sala->res_status == 'AGUARDANDO') {
                                        ?>
                                        <a class="btn btn-warning btn-xs" >
                                            <?php echo $nomeSala; ?>
                                        </a>
                                    <?php } else { ?>
                                        <a href="<?php echo site_url('adm/horarios/horario/horarioEdit/' . $sala->sal_id . '/' . $turno->tur_id . '/' . $i); ?>" class="btn btn-primary btn-xs"   data-container="body" data-toggle="popover" data-placement="top" data-content=".">

                                            <?php echo $nomeSala; ?>
                                        </a>
                                        <?php
                                    }
                                } elseif ($sala->hor == NULL) {
                                    ?>
                                    <a href="<?php echo site_url('adm/horarios/horario/horarioEdit/' . $sala->sal_id . '/' . $turno->tur_id . '/' . $i); ?>" class="btn btn-success btn-xs"   data-container="body" data-toggle="popover" data-placement="top" data-content=".">
                                        <?php echo $nomeSala; ?>
                                    </a>
                                    <?php
                                } else {
                                    $conteudo = ""
                                            . "<b>DISCIPLINA:</b><br>" . $sala->disciplina
                                            . '<br><b>TURMA(S):</b><br> ' . $sala->turmas
                                            . '<br><b>PROFESSOR(A):</b><br> ' . $sala->pro_nome;
                                    ?>

                                    <a href="<?php echo site_url('adm/horarios/horario/horarioDel/' . $sala->hor); ?>" id="example"  rel="popover"  data-content="<?php echo $conteudo; ?>" data-original-title="<?php echo '<b>' . $sala->sal_nome . '</b>'; ?>" data-placement="bottom"  class="btn btn-danger btn-xs" >
                                        <?php echo $nomeSala; ?>
                                    </a>

                                    <?php
                                }
                            }
                            ?>
                        </td>
                    <?php } ?>
                    <td></td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>

<!--<a href="<?php //echo site_url('adm/horarios/horario/horarioDel/' . $reserv->row('hor_id'));                                       ?>" class="btn btn-danger btn-xs"  rel="popover" data-content="" >
<?php echo $nomeSala; ?>
</a> -->
<div id="conteudo" style="display: none">
    <div>Esta sala já está agendada</div>
    <div class="sala-titulo">..</div>
</div>
<style>
    .btn-xs2{
        min-width: 0px; 
        min-height: 0px; 
        padding: 0 3px  0 3px;
        margin: 0 0 0 0;
    }
</style>

<script>
    $(function () {
        $('[rel=popover]').popover({
            placement: 'auto right',
            trigger: 'manual',
            html: true,
            trigger: ' hover | focus | manual',
            content: function () {
                // $('.sala-titulo').html('');
                // return $('#conteudo').html();
            }
        });

    })
</script>