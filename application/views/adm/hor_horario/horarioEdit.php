<h2>Novo Horário</h2>
<hr>
<div class=form-horizontal>    
    <div>
        <?php echo validation_errors(); ?>
    </div>
    <?php echo form_open() ?>
    <input type="hidden" name="hor_idSala" value="<?php echo $sala; ?>">
    <input type="hidden" name="hor_idTurno" value="<?php echo $turno; ?>">
    <input type="hidden" name="hor_dia" value="<?php echo $dia; ?>">
     
    <?php $this->util->input('Disciplina', 'hor_disciplina', ''); ?>
    <?php $this->util->input('Turma(s)', 'hor_turmas', ''); ?>


    <div class=form-group>
        <input type=submit  value=Salvar class='btn btn-primary'>
    </div>            
    <?php echo form_close() ?>
</div>
<?php $this->util->mascaras() ?>
<link type="text/css" href="<?php echo base_url('assets'); ?>/js/select2/css/select2.css"
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/select2/js/select2.js"></script>
<script>
    $(function (){
       $('.select2').select2(); 
    });

</script>