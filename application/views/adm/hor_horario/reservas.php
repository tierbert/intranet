<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            CONFIRMAÇÃO DE RESERVAS
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a class='btn btn-primary' href='<?php echo site_url('adm/horarios/horario/calendario/' . $tipo) ?>'>
                VOLTAR
            </a> 


        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">

    <h5>    Prezado Funcionário, nesta área você pode visualizar os agendamentos solicitados pelos professores, é neste momento em que você CONFIRMA ou NÃO o agendamento.</h5> <br>

    <?php if (count($dados) != 0) { ?>
        <table class='table table-bordered table-striped table-condensed table-hover'>
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Professor</th>
                    <th>Disciplina</th>
                    <th>Turma</th>
                    <th>Sala</th>
                    <th>Turno</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($dados->result() as $dado) {
                    $cor = '';
                    if ($dado->res_status == 'CONFIRMADA') {
                        $cor = 'success';
                    } elseif ($dado->res_status == 'CANCELADA') {
                        $cor = 'danger';
                    } elseif($dado->res_status == 'AGUARDANDO') {
                        $cor = 'warning';
                    }
                    ?>
                    <tr class="<?php echo $cor; ?> ">
                        <td ><?php echo $this->util->data($dado->res_data) ?> </td>
                        <td><?php echo $dado->pro_nome ?> </td>
                        <td><?php echo $dado->res_disciplina ?></td>
                        <td><?php echo $dado->res_turma ?></td>
                        <td><?php echo $dado->sal_nome ?> </td>
                        <td><?php echo $dado->tur_horario ?> </td>
                        <td><?php echo $dado->res_status ?> </td>
                        <td>
                            <a href="<?php echo site_url('adm/horarios/horario/confirmar/' . $dado->res_id . '/1/' . $tipo); ?>" class="btn btn-primary">
                                CONFIRMAR
                            </a>
                            <a href="<?php echo site_url('adm/horarios/horario/confirmar/' . $dado->res_id . '/2/' . $tipo); ?>" class="btn btn-danger">
                                CANCELAR
                            </a>
                        </td>
                    </tr>
                <?php } ?>

            </tbody>
        </table>
        <ul class='pagination pagination-small'>
            <li>
                <?php //echo $paginacao;  ?>
            </li>
        </ul>
    <?php } else { ?>
        <h4>Nenhum registro encontrado</h4>
    <?php } ?>
</div>



