<div class="painel" style="min-height: 400px;">
    <div class="row">
        <div class="col-md-6 ">
            <h2>RESERVAR SALA</h2>
        </div>
        <div class="col-md-6 text-right">
            <a href="<?php echo site_url('adm/horarios/horario/index/sala'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
    <legend style="padding-top: 5px; padding-bottom: 5px;"></legend>


    <div class="form-horizontal">
        <?php echo form_open(); ?>
        <hr>
        <div class="form-group">
            <div class="col-md-4">
                <label><?php echo $sala->sal_nome; ?></label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-4">
                <label>DIA DA SEMANA: </label>
                <?php echo $diaSemana; ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-4">
                <label>TURNO: </label>
                <?php echo $turno->tur_turno; ?> - 
                <?php echo $turno->tur_horario; ?>
                <input type="hidden" name="turno" value="<?php echo $turno->tur_id; ?>">
                <input type="hidden" name="dia" value="<?php echo $dia; ?>">
                <input type="hidden" name="sala" value="<?php echo $sala->sal_id; ?>">

            </div>  
        </div>
        <hr>

        <div class="form-group">
            <div class="col-md-3">
                <label>PROFESSOR</label>
                <?php $this->util->selectClean('hor_idProfessor', $professores, '', 'select2'); ?>
            </div>
        </div>
        <br>
        <div class="form-group">
        <div class="col-md-3">
                <label>DISCIPLINA</label>
                <input type="text" name="res_disciplina" class="form-control " value="">
            </div>
        </div>
        <br>
        <div class="form-group">
        <div class="col-md-3">
                <label>TURMA</label>
                <input type="text" name="res_turma" class="form-control" value="">
            </div>
        </div>
        <br>
        <div class="form-group">
            <div class="col-md-3">
                <label>DATA</label>
                <input type="text" name="data" class="form-control data" value="<?php echo $data; ?>">
            </div>
        </div> 
        <div class="form-group">
                        <div class="col-md-2">
                <label>&nbsp;</label>
                <input type="submit" value="CONFIRMAR" class="form-control btn btn-primary">
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
    <?php $this->util->mascaras() ?>