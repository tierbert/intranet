<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo base_url('assets'); ?>/bootstrap337/css/bootstrap.css" rel="stylesheet"> 
<link href="<?php echo base_url('assets/tema/'); ?>fonts/font-awesome/css/font-awesome.css" rel="stylesheet"> 
<script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>plugins/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/bootstrap337/js/bootstrap.js"></script>
<div class="row" style="padding: 20px;">
    <div class="row">
        <div class="col-lg-12 text-center">

            <a href="<?php echo site_url('adm/painel'); ?>" class="btn btn-primary pull-right" style="margin-left: 10px;">
                Voltar
            </a>
            <a href="<?php echo site_url('adm/horarios/horario/index/' . $tipo); ?>" class="btn btn-primary pull-right" style="margin-left: 10px;">
                Reservas
            </a>
            <a href="<?php echo site_url('adm/salas'); ?>" class="btn btn-primary pull-right" style="margin-left: 10px;" >
                Salas
            </a>
            <?php //14 = TI_COORD || 22 = LAB  || 23 = LAB_COORD
            if($user_setor == 14 || $user_setor == 22 || $user_setor == 23) { ?>
            <a href="<?php echo site_url('adm/horarios/horario/bloquearSala/'); ?>" class="btn btn-danger pull-right" style="margin-left: 10px;">
                BLOQUEAR SALA
            </a>
            <?php } ?>

            <h2 class="section-heading">Agendamento Sala de Aula</h2>
            
            <span class="label label-success">SALA DISPONÍVEL</span>
            <span class="label label-primary">AGENDAMENTO CONFIRMADO</span>
            <span class="label label-danger">SALA BLOQUEADA</span>
        </div> 
    </div>
    <br>
    <div class="col-md-12">   
        <?php
        
        // $firstDayofTheYear = '2020-01-01';
        $firstDayofTheYear = date('Y-m-d', strtotime('first day of january this year'));

        if ($semana == '') {
            $semana = date('W') - 1;
            $day = date('w');
            $domingo = date('m-d-Y', strtotime('-' . $day . ' days'));
            $datas = array(
                date('d-m-Y', strtotime('+' . (1 - $day) . ' days')), //segunda
                date('d-m-Y', strtotime('+' . (2 - $day) . ' days')), // terca
                date('d-m-Y', strtotime('+' . (3 - $day) . ' days')), // quarta
                date('d-m-Y', strtotime('+' . (4 - $day) . ' days')), // quinta
                date('d-m-Y', strtotime('+' . (5 - $day) . ' days')), //sexta
                date('d-m-Y', strtotime('+' . (6 - $day) . ' days')), //sabado
            );
        } else {
            $dias = ($semana * 7) - 1;
            $diaDomingo = date('Y-m-d', strtotime($firstDayofTheYear . " + $dias  days"));
            $day = date('w', strtotime($diaDomingo));
            $domingo = date('m-d-Y', strtotime('-' . $day . ' days'));
            $datas = array(
                date('d-m-Y', strtotime($diaDomingo . '+' . (1 - $day) . ' days')), //segunda
                date('d-m-Y', strtotime($diaDomingo . '+' . (2 - $day) . ' days')), // terca
                date('d-m-Y', strtotime($diaDomingo . '+' . (3 - $day) . ' days')), // quarta
                date('d-m-Y', strtotime($diaDomingo . '+' . (4 - $day) . ' days')), // quinta
                date('d-m-Y', strtotime($diaDomingo . '+' . (5 - $day) . ' days')), //sexta
                date('d-m-Y', strtotime($diaDomingo . '+' . (6 - $day) . ' days')), //sabado
            );
        }
        
        $volta = '';
        $avanca = '';
        $semanaAtual = date('W') - 1;

        if ($semana == $semanaAtual) {
            $volta = 'disabled';
        }

        $days_to_exclude = array();
        if($tipo == 'sala'){
            $days_to_exclude = array(
                '22-08-2019', '23-08-2019', '08-08-2019', '09-08-2019', '15-08-2019', '16-08-2019', '01-08-2019', '02-08-2019',
                '05-09-2019', '06-09-2019', '12-09-2019', '13-09-2019', '19-09-2019', '20-09-2019', '30-10-2019', '31-10-2019', 
                '03-10-2019', '03-10-2019', '21-11-2019', '22-11-2019', '28-11-2019', '29-11-2019', '05-12-2019', '06-12-2019', '
                12-12-2019', '13-12-2019'
            );
        }
        $dias_da_semana = array(
            'DOMINGO',
            'SEGUNDA',
            'TERÇA',
            'QUARTA',
            'QUINTA',
            'SEXTA',
            'SÁBADO'
        );

        

        $valid_days = array_values($datas);

        ?>
        <div class="box"  style="">
            <div class="row">
                <div class="col-md-6 box-titulo" style="">
                </div>

                <div class="col-md-2">
                    <a href="<?php echo site_url('adm/horarios/horario/calendario/' . $tipo . '/' . ($semana - 1)); ?>" class=" btn btn-primary <?php echo $volta; ?>">
                        <span class="glyphicon glyphicon-backward"></span>
                    </a>
                    <a href="<?php echo site_url('adm/horarios/horario/calendario/' . $tipo); ?>" class=" btn btn-primary ">
                        <span class="glyphicon glyphicon-home"></span>
                    </a>
                    <a href="<?php echo site_url('adm/horarios/horario/calendario/' . $tipo . '/' . ($semana + 1)); ?>" class=" btn btn-primary <?php echo $avanca; ?>">
                        <span class="glyphicon glyphicon-forward"></span>
                    </a>
                </div>
                <div class="col-md-4 ">

                    <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary pull-right">
                        VOLTAR
                    </a>
                </div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <th></th>

                <?php for ($i = 0; $i < sizeof($valid_days) ; $i++) { ?>
                            <th <?php echo in_array($valid_days[$i], $days_to_exclude)?'hidden':''; ?>><?php echo $dias_da_semana[date('w', strtotime($valid_days[$i]))] ?> - <span class=""><?php echo $valid_days[$i]; ?></span></th>        
                <?php } ?>
            </tr>
            <?php
            foreach ($turnos->result() as $turno) {
                ?>
                <tr>
                    <td>
                        <?php echo $turno->tur_turno . '<br>' . $turno->tur_horario; ?>
                    </td>
                    <?php
                    for ($i = 0; $i < sizeof($valid_days); $i++) {
                        ?>
                        <td <?php echo in_array($valid_days[$i], $days_to_exclude)?'hidden':'';?>>
                            <?php
                            
                            
                            $array_size = sizeof($valid_days);
                            $salas = $this->horariom->getSalasV2($turno->tur_id, $i, $tipo, $valid_days[0], $valid_days[$array_size-1]);
                            // var_dump($salas->result());die;
                            foreach ($salas->result() as $sala) {
                                $nomeSala = $sala->sal_abreviatura;
                                //$agendada = $this->horariom->getAgendaByDia($turno->tur_id, $sala->sal_id, $i, $datas[$i]);
                                ?>

                                <?php
                                //$nomeSala = str_replace('SALA ', 'S', $sala->sal_nome);
                                //$reserv = $this->horariom->getByHorario($turno->tur_id, $sala->sal_id, $i);
                                //$agendada = $this->horariom->getAgendaByDia($turno->tur_id, $sala->sal_id, $i, $datas[$i]);
                                if ($sala->res_id != NULL) {
                                    if ($sala->res_status == 'CONFIRMADA'){ ?>

                                        <a href="<?php echo site_url("adm/horarios/horario/horarioDel/$sala->res_id/sala"); ?>" class="btn btn-primary btn-xs"   data-container="body" data-toggle="popover" data-placement="top">
                                            <?php echo $nomeSala; ?>
                                        </a>
                                        <?php
                                    }elseif ($sala->res_status == 'BLOQUEADA') { ?>
                                        <a href="#" class="btn btn-danger btn-xs"   data-container="body" data-toggle="popover" data-placement="top">
                                            <?php echo $nomeSala; ?>
                                        </a>

                              <?php }
                                    else{
                                        ?>
                                        <a href="<?php echo site_url('adm/horarios/horario/reserva/' . $turno->tur_id . '/' . $sala->sal_id . '/' . $i . '/' . $datas[$i] . '/' . $tipo); ?>" class="btn btn-success btn-xs"   data-container="body" data-toggle="popover" data-placement="top" data-content=".">
                                            <?php echo $nomeSala; ?>
                                        </a>
                                        <?php
                                    }   
                                } else{
                                    ?>
                                    <a href="<?php echo site_url('adm/horarios/horario/reserva/' . $turno->tur_id . '/' . $sala->sal_id . '/' . $i . '/' . $datas[$i] . '/' . $tipo); ?>" class="btn btn-success btn-xs"   data-container="body" data-toggle="popover" data-placement="top" data-content=".">
                                        <?php echo $nomeSala; ?>
                                    </a>
                                    <?php
                                } 
                            }
                            ?>
                        </td>
                    <?php } ?>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>

<!--<a href="<?php //echo site_url('adm/horarios/horario/horarioDel/' . $reserv->row('hor_id'));                                               ?>" class="btn btn-danger btn-xs"  rel="popover" data-content="" >
<?php echo $nomeSala; ?>
</a> -->
<div id="conteudo" style="display: none">
    <div>Esta sala já está agendada</div>
    <div class="sala-titulo">..</div>
</div>
<style>
    .btn-xs2{
        min-width: 0px; 
        min-height: 0px; 
        padding: 0 3px  0 3px;
        margin: 0 0 0 0;
    }
</style>

<script>
    $(function () {
        $('[rel=popover]').popover({
            placement: 'auto right',
            trigger: 'manual',
            html: true,
            trigger: ' hover | focus | manual',
            content: function () {
                // $('.sala-titulo').html('');
                // return $('#conteudo').html();
            }
        });

    })
</script>