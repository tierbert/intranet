<h3 class='pull-left'><?php echo ucfirst($titulo); ?> - Listando: </h3>
<a class='btn btn-primary pull-right' href='<?php echo site_url($this->uri->segment(1) . '/criar/') ?>'>Nova <?php echo ucfirst(substr($titulo, 0, -2)) ?></a>
<?php if (count($dados) != 0) { ?>
    <table class='table table-bordered table-striped table-condensed table-hover'>
        <thead>
            <tr>
                <th>hor_idSala</th>
                <th>hor_idTurno</th>
                <th>hor_idProfessor</th>
                <th>hor_dia</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dados as $dado) {
                ?><tr><td><?php echo $dado->hor_idSala ?> </td>
                    <td><?php echo $dado->hor_idTurno ?> </td>
                    <td><?php echo $dado->hor_idProfessor ?> </td>
                    <td><?php echo $dado->hor_dia ?> </td>
                    <td><a class='btn btn-sm btn-primary' href="<?php echo site_url($this->uri->segment(1) . '/editar/' . $dado->hor_id)
                ?>">Editar</a>
                        <a class='btn btn-sm btn-danger' href="<?php echo site_url($this->uri->segment(1) . '/delete/' . $dado->hor_id) ?>">Deletar</a>
                    </td>

                </tr>
            <?php } ?>

        </tbody>
    </table>
    <ul class='pagination pagination-small'>
        <li>
            <?php echo $paginacao; ?>
        </li>
    </ul>
<?php } else { ?>
    <h4>Nenhum registro encontrado</h4>
<?php } ?>

