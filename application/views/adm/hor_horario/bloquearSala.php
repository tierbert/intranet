<div class="painel" style="min-height: 400px;">
    <div class="row">
        <div class="col-md-6 ">
            <h2>BLOQUEAR SALA</h2>
        </div>
        <div class="col-md-6 text-right">
            <a href="<?php echo site_url('adm/horarios/horario/index/sala'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
    <legend style="padding-top: 5px; padding-bottom: 5px;"></legend>

     <div class="form-horizontal">
        <?php echo form_open(); ?>
        <hr>
        <div class="form-group">
            <div class="col-md-4">
                <label></label>
            </div>
        </div>
        <div class="form-group">
        </div>
        <hr>

        <div class="form-group">
            <div class="col-md-6">
                <label>DIA DA SEMANA: </label><br/ >
                <table class="table">
                    <tr>
                        <td>
                            <input class="form-check-input" type="checkbox" name="dia[]" value="monday">
                            <label class="form-check-label" for="dias">SEGUNDA</label>
                        </td>
                        <td>
                            <input class="form-check-input" type="checkbox" name="dia[]" value="tuesday">
                            <label class="form-check-label" for="dias">TERÇA</label>
                        </td>
                        <td>
                            <input class="form-check-input" type="checkbox" name="dia[]" value="wednesday">
                            <label class="form-check-label" for="dias">QUARTA</label>
                        </td>
                        <td>
                            <input class="form-check-input" type="checkbox" name="dia[]" value="thursday">
                            <label class="form-check-label" for="dias">QUINTA</label>
                        </td>
                        <td>
                            <input class="form-check-input" type="checkbox" name="dia[]" value="friday">
                            <label class="form-check-label" for="dias">SEXTA</label>
                        </td>
                        <td>
                            <input class="form-check-input" type="checkbox" name="dia[]" value="saturday">
                            <label class="form-check-label" for="dias">SÁBADO</label>
                        </td>
                    </tr>
                </table>
                <hr />                    
            </div>
        </div>
        
        <br />
         <div class="form-group">
            <div class="col-md-3">
                <label>Horário: </label>      
                <?php 
                    foreach ($horarios as $horario) { ?> 
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="horario[]" value="<?php echo $horario->tur_id?>">
                            <label class="form-check-label" for="horario"><?php echo $horario->tur_horario ?></label>
                        </div> 
                <?php } ?>
               
            </div>
        
            <div class="col-md-3">
                <label>SALA: </label><br/>
                <select name="sala" class="form-controlselect2" id="">
                <?php 
                foreach ($salas as $sala) {
                    ?><option value="<?php echo $sala->sal_id?>"><?php echo $sala->sal_nome ?></option> <?php
                 } 
                ?>
                </select>
                <br />
                <br />
                <label>DATA LIMITE PARA BLOQUEIO: </label>
                <input type="date" name="dataLimite" required="true">
                <br />
                <div class="col-md-3">
                </div>
                <div class="col-md-6">
                    <label>&nbsp;</label>
                    <input type="submit" value="CONFIRMAR" class="form-control btn btn-primary">
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
<?php $this->util->mascaras() ?>