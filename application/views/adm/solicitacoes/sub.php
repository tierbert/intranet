
<link href="../../assets/css/css/metisMenu.min.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/css/sb-admin-2.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/css/css/morris.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/css/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>




<div>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Intranet</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">0</div>
                                    <div>Novos comentários!!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Ver detalhes</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">0</div>
                                    <div>Novas tarefas!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Ver detalhes</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-shopping-cart fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">0</div>
                                    <div>Notificação!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Ver detalhes</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-support fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">0</div>
                                    <div>Informativos!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Ver detalhes</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                   
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-clock-o fa-fw"></i> Sistemas Internos
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <ul class="timeline">
                                <li>
                                    <div class="timeline-badge"><i class="fa fa-check"></i>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Avaliação Substitutiva</h4>
                                            <p><small class="text-muted"><i class="fa fa-clock-o"></i> Painel Adminsitrativo</small>
                                            </p>
                                        </div>
                                        <div class="timeline-body">
                                            <p> - Cadastro dos alunos.</p>
                                            <p> - Cadastro das Disciplinas.</p>
                                            <p> - Cadastro dos Professores.</p>
                                            <p> - Relatórios.</p>
                                            <div class="text-center"> 
                            <a href="<?php echo site_url('solicitacoes'); ?>" type="button" class="btn btn-skin scroll " > Acessar</a>                                                                       
                        </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="timeline-inverted">
                                    <div class="timeline-badge warning"><i class="fa fa-credit-card"></i>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Suporte TI - FG</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p> - Aberturas de Demandas Internas.</p>
                                            <p> - Solicitação de Novos Equipamentos.</p>
                                            <p> - Informações e Dúvidas.</p>
                                            <div class="text-center"> 
                            <a href="<?php echo site_url('solicitacoes'); ?>" type="button" class="btn btn-skin scroll " > Acessar</a>                                                                       
                        </div>
                                            
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="timeline-badge danger"><i class="fa fa-bomb"></i>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Sistema de Eventos - Extensão</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p> - Cadastro de Novos Cursos .</p>
                                            <p> - Alteração de Valores.</p>
                                            <p> - Cadastros dos usuários.</p> 
                                        <div class="text-center"> 
                            <a href="<?php echo site_url('solicitacoes'); ?>" type="button" class="btn btn-skin scroll " > Acessar</a>                                                                       
                        </div>
                                        </div>
                                    </div>
                                </li>
                                 <li class="timeline-inverted">
                                    <div class="timeline-badge success"><i class="fa fa-graduation-cap"></i>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Alea</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p>- Agenda de Atendimentos</p>
                                            <div class="text-center"> 
                            <a href="<?php echo site_url('solicitacoes'); ?>" type="button" class="btn btn-skin scroll " > Acessar</a>                                                                       
                        </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="timeline-badge info"><i class="fa fa-save"></i>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Sala Live</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p>- Controle das Sala de Aula</p>
                                            <p>- Controle Biblioteca</p>
                                            <div class="text-center"> 
                            <a href="<?php echo site_url('solicitacoes'); ?>" type="button" class="btn btn-skin scroll " > Acessar</a>                                                                       
                        </div>
                                            
                                        </div>
                                    </div>
                                </li>
                                
                               
                            </ul>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bell fa-fw"></i> Painel de notificações
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div>                           </div>
                            <!-- /.list-group -->
                            <a href="#" class="btn btn-default btn-block">Ver Todos os Alertas</a>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Donut Chart Example
                        </div>
                        <div class="panel-body">
                            <div id="morris-donut-chart"></div>
                            <a href="#" class="btn btn-default btn-block">Ver Detalhes</a>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    <div class="chat-panel panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-fw"></i> Bate-papo
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-chevron-down"></i>
                                </button>
                                <ul class="dropdown-menu slidedown">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-refresh fa-fw"></i> Refresh
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-check-circle fa-fw"></i> Available
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-times fa-fw"></i> Busy
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-clock-o fa-fw"></i> Away
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-sign-out fa-fw"></i> Sign Out
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        
                        <!-- /.panel-body -->
                        <div class="panel-footer">
                            <div class="input-group">
                                <input id="btn-input" type="text" class="form-control input-sm" placeholder="Digite sua mensagem aqui..." />
                                <span class="input-group-btn">
                                    <button class="btn btn-warning btn-sm" id="btn-chat">
                                        Enviar
                                    </button>
                                </span>
                            </div>
                        </div>
                        <!-- /.panel-footer -->
                    </div>
                    <!-- /.panel .chat-panel -->
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>







<div class="container">
    <div class="row">
        <!-- Solicitações -->
        <div class="col-xs-12 col-sm-12 col-md-3">
            <div class="team boxed-grey">
                <div class="inner">
                    <h5><center> Solicitações </center></h5>
                    <div class="col-sm-12 col-md-12">
                        <div class="text-center"> 
                            <a href="<?php echo site_url('solicitacoes'); ?>" type="button" class="btn btn-skin scroll " > Consulte</a>                                                                       
                        </div>
                    </div><div class="avatar">
                        <img src="<?php echo base_url('assets/'); ?>img/team/3.png" alt="" class="img-responsive" /></div>
                </div>
            </div>
        </div>
        <!-- End Solicitações -->
        <!-- Alunos -->
        <div class="col-xs-12 col-sm-12 col-md-3">
            <div class="team boxed-grey">
                <div class="inner">
                    <h5><center> Alunos </center></h5>
                    <div class="col-sm-12 col-md-12">
                        <div class="text-center"> 
                            <a href="<?php echo site_url('alunos'); ?>" type="button" class="btn btn-skin scroll " > Consulte</a>                                                                       
                        </div>
                    </div><div class="avatar">
                        <img src="<?php echo base_url('assets/'); ?>img/team/2.png" alt="" class="img-responsive" /></div>
                </div>
            </div>
        </div>
        <!-- End Alunos -->
        <!-- Horario -->
        <div class="col-xs-12 col-sm-12 col-md-3">
            <div class="team boxed-grey">
                <div class="inner">
                    <h5><center> Disciplinas </center></h5>
                    <div class="col-sm-12 col-md-12">
                        <div class="text-center"> 
                            <a href="<?php echo site_url('horario'); ?>" type="button" class="btn btn-skin scroll " > Consulte</a>                                                                       
                        </div>
                    </div><div class="avatar">
                        <img src="<?php echo base_url('assets/'); ?>img/team/1.png" alt="" class="img-responsive" /></div>
                </div>
            </div>
        </div>
        
        



    </div>		
</div>
