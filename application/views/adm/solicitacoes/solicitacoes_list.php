<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
           SOLICITAÇÕES
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            
            <a href="<?php echo site_url('adm/painel/solicitacoes'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px;">

    <div class="row">
        <div class="col-md-8">
            <div class="form-horizontal">
                <?php echo form_open(); ?>
                
                    <div class="col-md-3">
                        <label>Nº DA SOLIC</label>                    
                        <input name="id" value="<?php if (isset($post) && $post['id'] != '') echo $post['id']; ?>" type="text" class="form-control" >
                    </div>
                    <div class="col-md-5">
                        <label>ALUNO</label>
                        <input name="nome" value="<?php if (isset($post) && $post['nome'] != '') echo $post['nome']; ?>" type="text" class="form-control" >
                    </div>
                    <div class="col-md-3">
                        <label>&nbsp;</label>
                        <button type="submit" class="btn btn-block btn-success">Pesquisar</button>
                    </div>
                
                <?php echo form_close(); ?>
            </div>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-4">
            <?php $this->load->view('botoes') ?> 
            <br><br>
            <div class="row">
                <h4 class='pull-left'>SOLICITAÇÕES TOTAIS </h4><br>
                <?php
                $pag = isset($totais['pago']) ? $totais['pago'] : 0;
                $canc = isset($totais['cancelado']) ? $totais['cancelado'] : 0;
                $baixd = isset($totais['baixado']) ? $totais['baixado'] : 0;
                $pend = isset($totais['']) ? $totais[''] : 0;
                ?>
                Pendentes:  <?php
                echo $pend;
                ?>
                &nbsp;&nbsp;&nbsp;
                Pagas: <?php echo $pag ?>
                &nbsp;&nbsp;&nbsp;
                Baixadas: <?php echo $baixd ?>
                &nbsp;&nbsp;&nbsp;
                Canceladas: <?php echo $canc; ?>
                <br>
                Total:  <?php echo $pag + $canc + $pend + $baixd ?>
                &nbsp;&nbsp;&nbsp;
                <br>
            </div>
        </div>
    </div>
   
       <div class="row" style="margin-top: 10px;">

        <div class="col-md-12 col-xs-12 col-sm-12">
        
            <?php
            if (isset($dados)) {
                if (count($dados->result()) != 0) {
                    ?>
                     <table class='table table-bordered table-striped table-condensed table-hover table-responsive tabela-aredondada'>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nº SOLIC</th>
                                <th>ALUNO</th>
                                <th>VALOR</th>
                                <th>STATUS</th>
                                <th>SEMESTRE</th>
                                <th style="width: 410px;">AÇÃO</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            foreach ($dados->result() as $dado) {
                                $i++;
                                $status = $dado->sol_status;

                                $cor = '';
                                if ($status == 'pago' || $status == 'baixado') {
                                    $cor = 'success';
                                } elseif ($status == 'cancelado') {
                                    $cor = 'danger';
                                }
                                ?><tr class="<?php echo $cor; ?>">
                                    <td><?php echo $i ?> </td>
                                    <td><?php echo $dado->sol_id ?> </td>
                                    <td><?php echo $dado->alu_nome ?> </td>
                                    <td class="text-right"><?php echo number_format($dado->sol_valor, 2, ',', '.') ?> </td>
                                    <td><?php echo $dado->sol_status ?> </td>
                                    <td><?php echo $dado->sol_semestre ?> </td>
                                    <td>
                                        <?php if ($status == '') { ?>
                                            <a class='btn btn-xs btn-success' href="<?php echo site_url('adm/solicitacoesadm/editar/' . $dado->sol_id) ?>" style="min-width: 50px;">
                                                Status
                                            </a>
                                        <?php } elseif ($status == 'cancelado') { ?>
                                            <a class='btn btn-xs btn-default' style="min-width: 50px;">
                                                STATUS
                                            </a>
                                        <?php } else { ?>
                                            <a class='btn btn-xs btn-default' href="<?php echo site_url('adm/solicitacoesadm/editar/' . $dado->sol_id) ?>" style="min-width: 50px;">
                                                STATUS
                                            </a>

                                        <?php } ?>
                                        <a class='btn btn-xs btn-warning' href="<?php echo site_url('adm/solicitacoesadm/disciplinas/' . $dado->sol_id) ?>" style="min-width: 50px;">
                                            Disciplinas
                                        </a>

                                        <a target="_blanck" class='btn btn-xs btn-primary' href="<?php echo site_url('adm/solicitacoesadm/comprovante/' . $dado->sol_id) ?>" style="min-width: 10px;">
                                            <span class="glyphicon glyphicon-print"></span> 
                                        </a>
                                        <a class='btn btn-xs btn-danger' href="<?php echo site_url('adm/solicitacoesadm/delete/' . $dado->sol_id) ?>" style="min-width: 10px;">
                                            <span class="glyphicon glyphicon-trash"></span> 
                                        </a>
                                    </td>

                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                    <ul class='pagination pagination-small'>
                        <li>
                            <?php echo $paginacao; ?>
                        </li>
                    </ul>
                <?php } else { ?>
                    <hr>
                    <h4>Nenhum registro encontrado</h4>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>