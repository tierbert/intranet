<html>
    <head>
        <title>Comprovante De Solicitação</title>
        <meta content="">
        <link href="<?php echo base_url('assets/'); ?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <style>
            .table1 { 
                border-left: 1px solid #000;
                border-top: 1px solid #000;
                border-right: 1px solid #000;
                border-bottom: 1px solid #000;
            }
            .negrito{
                font-weight: bold;
            }

        </style>
    </head>
    <body>

        <div class="container">

            <table class="table1" style="margin-top: 70px; width: 100%">
                <tr>
                    <td>
                        <img style="width: 10%; padding: 10px;" src="<?php echo base_url('assets/img/logo2.jpg'); ?>" >

                    </td>
                    <td class="text-center" style="font-weight: bold; font-size: 10px;">

                        CESG - CENTRO DE EDUCAÇÃO SUPERIOR DE <br>
                        GUANAMBI S/C FACULDADE GUANAMBI
                        <br>
                        CNPJ: 04.097.860/0001-46

                        <br><br>

                        <?php
                        date_default_timezone_set('America/Bahia');
                        $data = Date('d/m/Y h:i:s');
                        ?>

                        EMITIDO EM: <?php echo $data; ?>
                    </td>
                    <td style="width: 180px;">
                        &nbsp;</td>
                </tr>
            </table>


            <div class="text-center" style="margin-top: 50px; margin-bottom: 30px; ">
                <h4 style="font-size: 15px; font-weight: bold">
                    RELATÓRIO DE SOLICITAÇÕES INDIVIDUAL</h4>
                <hr style="margin: 0; margin-bottom: 15px; border: 1px solid #000;">
                <h4 style="font-size: 12px; font-weight: bold">
                    <strong>
                        PROFESSOR: <?php echo $professor->pro_nome; ?>
                    </strong>


                </h4>
                <hr style="margin: 0; margin-top: 15; border: 1px solid #000; ">
            </div>

            <div class="" style="">
                <center>
                    <table   style="width: 100%; "  border="1">
                        <tr>
                            <th class="text-center">#</th>
                            <th>
                                Disciplina
                            </th>
                            <th>
                                Turno
                            </th>
                            <th>Turma</th>
                            <th>Semestre</th>
                            <th class="text-center">STATUS</th>
                            <th>CH TEÓRICA</th>

                        </tr>
                        <?php
                        $i = 0;
                        $total = 0;
                        foreach ($disciplinas->result() as $disciplina) {
                            ?>
                            <tr>
                                <td style="width: 50px;" class="text-center">
                                    <?php
                                    $i++;
                                    echo $i;
                                    ?></td>
                                <td class=""><?php echo $disciplina->hor_disciplina; ?></td>
                                <td><?php echo $disciplina->hor_turno; ?></td>
                                <td class="text-center"><?php echo $disciplina->hor_turma; ?></td>
                                <td class="text-center"><?php echo $disciplina->hor_semestre; ?></td>
                                <td class="text-center">
                                    <?php
                                    //echo $disciplina->hor_status;

                                    if ($disciplina->hor_status == 1) {
                                        echo 'ENTREGUE';
                                    }
                                    if ($disciplina->hor_status == '0') {
                                        echo 'NÃO ENTREGUE';
                                    }
                                    ?>
                                </td>
                                <td class="text-center">
                                    <?php
                                    $total += $disciplina->hor_chTeorica;
                                    echo $disciplina->hor_chTeorica;
                                    ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </center>

                <hr style="margin: 0px; margin-top: 20px;">
                <h4 style="font-size: 14px; margin: 0px;">Total de disciplinas: <?php echo $i; ?></h4>
                <h4 style="font-size: 14px; margin: 0px;">
                    Total de CH Teórica: <?php echo $total; ?></h4>
                <hr style="margin: 0px;">

            </div>
        </div>
    </body>
</html>