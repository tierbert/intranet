<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            ALTERAR STATUS
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('adm/solicitacoesadm'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px;">

    <h2>Alterar Status</h2>
    <hr>
    <div class=form-horizontal>    
        <div>
            <?php echo validation_errors(); ?>
        </div>

        <?php echo form_open() ?>
        <?php echo 'Nº Solicitaçao: ' . $object->sol_idAluno; ?>
        <br>
        Aluno: 
        <br>
        <?php echo "Valor: " . $object->sol_valor; ?>
        <hr>
        <div class=form-group>
            <?php $this->util->input2('Data do Pagamento', 'sol_dataPagamento', date('d-m-Y'), 'data', 2); ?>
            <?php
            $status = array(
                'pago' => 'pago',
                'baixado' => 'baixado'
            );
            $this->util->select2('Status Solicitação', 'sol_status', $status, $object->sol_status, '', 2);
            ?>
        </div>
        <div class=form-group>
            <div class="col-md-2">
                <label>&nbsp;</label>
                <input type=submit  value=Salvar class='btn btn-success form-control'>
            </div>
        </div>
        <?php echo form_close() ?>
    </div>

</div>
<?php $this->util->mascaras(); ?>