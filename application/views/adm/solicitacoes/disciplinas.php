<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            DISCIPLNAS
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('adm/solicitacoesadm'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px;">

<div class=form-horizontal>    
    <div>
        <?php echo validation_errors(); ?>
    </div>

    <?php echo form_open() ?>
    <?php echo 'Nº Solicitaçao: ' . $object->sol_idAluno; ?>
    <br>
    Aluno: 
    <?php echo ''; ?>
    <br>
    <?php echo "Valor: " . $object->sol_valor; ?>
    <br><br>
    
    <table class='table table-bordered table-striped table-condensed table-hover table-responsive tabela-aredondada'>
        
        <tr>
            <th>DISCIPLINA</th>
            <th>TURNO</th>
            <th>TURMA</th>
            <th>PROFESSOR</th>
        </tr>
        
        <?php foreach ($disciplinas->result() as $disc){ ?>
        
        <tr>
            <td><?php echo $disc->hor_disciplina; ?></td>
            <td><?php echo $disc->hor_turno; ?></td>
            <td><?php echo $disc->hor_turma; ?></td>
            <td><?php echo $disc->hor_prof; ?></td>
        </tr>
        
        <?php } ?>
    </table>
    
    
    <hr>



           
    <?php echo form_close() ?>
</div>


</div>