<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            Relatório por Status
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('adm/painel/solicitacoes'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>


<div class="painel" style="min-height: 400px;">
    <?php echo form_open(); ?>
    <div class="row">
        <div class=form-horizontal> 
            <div class="form-group">
                <div class="col-sm-4">
                    <label>Professor:</label>
                    <?php
                    //var_dump($professores);

                    $postProf = '';
                    if (isset($post) && $post['prof'] != '') {
                        $postProf = $post['prof'];
                    }

                    $js = 'class="form-control select2" ';
                    echo form_dropdown('prof', $professores, $postProf, $js);
                    ?>
                </div>
                <div class="col-sm-4">
                    <label>Disciplina:</label>
                    <input name="disciplina" value="<?php if (isset($post) && $post['disciplina'] != '') echo $post['disciplina']; ?>" type="text" class="form-control"  placeholder="Disciplina">
                </div>

                <div class="col-sm-4">
                    <label>STATUS:</label>
                    <select class="form-control" name="status">
                        <?php
                        $status = array(
                            'TODOS' => 'TODOS',
                            'AGUARDANDO' => 'AGUARDANDO',
                            'PAGO' => 'PAGO',
                            'CANCELADO' => 'CANCELADO'
                        );
                        if (isset($post) && $post['status'] != '') {
                            ?>
                            <option><?php echo $post['status']; ?></option>
                            <?php
                        }
                        foreach ($status as $stat) {
                            ?>
                            <option value="<?php echo $stat; ?>"><?php echo $stat; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>


            &nbsp;
            <div class="form-group">
                <div class="col-sm-4">
                    <button type="submit" class="btn btn-success">Pesquisar</button>                        
                </div>
            </div>

            <?php echo form_close(); ?>
        </div>
    </div>
    <hr>
    <div class="row">
        <?php if ($disciplinas) { ?>
            <table class='table table-bordered table-striped table-condensed table-hover table-responsive tabela-aredondada'>   
                <thead>
                    <tr>
                        <th>Disciplina</th>
                        <th>Professor</th>
                        <th>Semestre / Turno</th>
                    </tr>
                </thead>
                <?php foreach ($disciplinas->result() as $disciplina) { ?>
                    <tr>
                        <td><?php echo $disciplina->hor_disciplina; ?></td>
                        <td><?php echo $disciplina->hor_prof; ?></td>
                        <td><?php echo $disciplina->hor_turno; ?></td>
                        <td>
                            <?php
                            $status = 'todos';
                            if (isset($post) && $post['status'] != '') {
                                $status = $post['status'];
                            }
                            ?>

                            <a href="<?php echo site_url('adm/solicitacoesadm/relatorioStatusPdf/' . $disciplina->hor_id . '/' . $status); ?>" target="_blanck" class="btn btn-warning btn-xs">
                                Imprimir
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        <?php } ?>
    </div>
</div>


<link href="<?php echo base_url('assets/'); ?>select2/css/select2.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('assets/'); ?>select2/js/select2.js"></script>
<script>

    $('.select2').select2();

</script>