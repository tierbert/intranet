<div class="container">
    <div class="row">
        <h3 class="pull-left">Relatorio para Assinatura</h3>
        <div class="pull-right">

            <a class='btn btn-primary ' href='<?php echo site_url('painel/solicitacoes') ?>'>
                <span class="glyphicon glyphicon-chevron-left"></span>
                Voltar
            </a>
        </div>
        <div class="row">
            <?php if ($disciplinas) { ?>
                <table class="table  table-hover table-condensed" style="color: black">
                    <thead>
                        <tr>
                            <th>Disciplina</th>
                            <th>Professor</th>
                            <th>Turno</th>
                        </tr>
                    </thead>
                    <?php foreach ($disciplinas->result() as $disciplina) { ?>
                        <tr>
                            <td><?php echo $disciplina->hor_disciplina; ?></td>
                            <td><?php echo $disciplina->hor_prof; ?></td>
                            <td><?php echo $disciplina->hor_turno; ?></td>
                            
                            <td>
                                <?php
                                $status = 'pago';
                                ?>

                                <a href="<?php echo site_url('solicitacoes/relatorioAssinatPdf/' . $disciplina->hor_id . '/' . $status); ?>" target="_blanck" class="btn btn-info btn-xs">
                                    Imprimir
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            <?php } ?>
        </div>
    </div>
</div>

<link href="<?php echo base_url('assets/'); ?>select2/css/select2.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('assets/'); ?>select2/js/select2.js"></script>
<script>

    $('.select2').select2();

</script>