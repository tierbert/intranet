<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            Relatório Individual
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('adm/painel/solicitacoes'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>


<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal> 
        <?php $this->load->view('botoes') ?>

        <div class="row">            
            <div class="col-sm-6">
                <div class="form-group">
                    <?php echo form_open('adm/solicitacoesadm/relProf'); ?>
                    <label>Professor - Relátorio solicitações Individuais</label>
                    <?php
                    //var_dump($professores);
                    $postProf = '';
                    if (isset($post) && $post['prof'] != '') {
                        $postProf = $post['prof'];
                    }
                    $js = 'class="form-control select2" ';
                    echo form_dropdown('prof', $professores, $postProf, $js);
                    ?>
          
                    <br>
                    <button type="submit" class="btn btn-success" value="Pesquisar">Pesquisar</button>
                   
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <hr>
      <div class=form-horizontal>
    <div class="row">            
            <div class="col-sm-6">
                <div class="form-group">
            <h4>Relatório Geral de Professores</h4>
            <?php echo form_open('adm/solicitacoesadm/cargaHoraria'); ?>
            <div class="form-group col-md-5">

                <!--  <label>Professor</label>
                <?php
                //var_dump($professores);

                $postProf = '';
                if (isset($post) && $post['prof'] != '') {
                    $postProf = $post['prof'];
                }

                $js = 'class="form-control select2" ';
                echo form_dropdown('prof', $professores, $postProf, $js);
                ?>
              </div> -->


                <div class="col-md-4">
                    &nbsp;
                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="Pesquisar">Pesquisar</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<link href="<?php echo base_url('assets/'); ?>select2/css/select2.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('assets/'); ?>select2/js/select2.js"></script>
<script>

    $('.select2').select2();

</script>