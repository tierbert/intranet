<html>
    <head>
        <title>Comprovante De Solicitação</title>
        <meta content="">
        <link href="<?php echo base_url('assets/'); ?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <style>
            .table1 { 
                border-left: 1px solid #000;
                border-top: 1px solid #000;
                border-right: 1px solid #000;
                border-bottom: 1px solid #000;
            }
            .negrito{
                font-weight: bold;
            }

        </style>
    </head>
    <body>

        <div class="container">

            <table class="table1" style="margin-top: 70px; width: 100%">
                <tr>
                    <td>
                        <img style="width: 10%; padding: 10px;" src="<?php echo base_url('assets/tema/images/logo.jpg'); ?>" >

                    </td>
                    <td class="text-center" style="font-weight: bold; font-size: 10px;">

                        CESG - CENTRO DE EDUCAÇÃO SUPERIOR DE <br>
                        GUANAMBI S/C FACULDADE GUANAMBI
                        <br>
                        CNPJ: 04.097.860/0001-46

                        <br><br>

                        <?php
                        date_default_timezone_set('America/Bahia');
                        $data = Date('d/m/Y h:i:s');
                        ?>

                        EMITIDO EM: <?php echo $data; ?>
                    </td>
                    <td style="width: 180px;">
                        &nbsp;</td>
                </tr>
            </table>


            <div class="text-center" style="margin-top: 50px; margin-bottom: 30px; ">
                <h4 style="font-size: 15px; font-weight: bold">
                    RELATÓRIO DE SOLICITAÇÕES POR STATUS</h4>
                <hr style="margin: 0; border: 1px solid #000; ">
            </div>




            <div class="row">
                <table style="width: 100%;" class="table">
                    <tr>
                        <td style="width: 12%;" class="text-right">
                            Disciplina: 
                        </td>
                        <td class="negrito">
                            <?php echo $solicitacoes->row('hor_disciplina'); ?>
                        </td>

                        <td style="width: 20px;" >Turno:</td>
                        <td class="negrito"><?php echo $solicitacoes->row('hor_turno'); ?></td>

                    </tr>
                    <tr>
                        <td class="text-right">
                            Professor: 
                        </td>
                        <td  class="negrito  " >
                            <?php echo $solicitacoes->row('hor_prof'); ?>
                        </td>
                        <td>Turma:</td>
                        <td class="negrito"><?php echo $solicitacoes->row('hor_turma'); ?></td>
                    </tr>



                </table>
            </div>


            <hr>
            <br>
            <div class="" style="">
                <center>
                    Total: <?php echo $solicitacoes->num_rows(); ?>
                    <table   style="width: 100%; "  border="1">
                        <tr>
                            <th class="text-center">#</th>
                            <th>
                                Nº Solic.
                            </th>
                            <th>
                                Aluno
                            </th>
                            <th>Semestre</th>
                            <th>Status</th>

                        </tr>
                        <?php
                        $i = 1;
                        foreach ($solicitacoes->result() as $solicitacao) {
                            ?>
                            <tr>
                                <td style="width: 50px;" class="text-center">
                                    <?php
                                    echo $i;
                                    $i++;
                                    ?></td>
                                <td class="text-center"><?php echo $solicitacao->sol_id; ?></td>
                                <td><?php echo $solicitacao->alu_nome; ?></td>
                                <td><?php echo $solicitacao->sol_semestre; ?></td>
                                <td><?php
                                    if ($solicitacao->sol_status == '') {
                                        echo 'AGUARDANDO';
                                    } else {
                                        echo mb_strtoupper($solicitacao->sol_status);
                                    }
                                    ?></td>
                            </tr>
                        <?php } ?>
                    </table>

                </center>

            </div>
        </div>
    </body>
</html>