<?php //$this->load->view('adm/agendamento/agend_botoes');   ?>
<h3 class='pull-left'>ALUNOS AGENDADOS PARA O HORÁRIO</h3>
<a class='btn btn-primary pull-right' href='<?php echo site_url('professor/painelprof/agendados') ?>'>
    VOLTAR
</a>
<?php if (count($agendados) != 0) { ?>
    <table class='table table-bordered table-striped table-condensed table-hover'>
        <thead>
            <tr>
                <th>Data</th>
                <th>Horário</th>
                <th>Aluno</th> 
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($agendados->result() as $dado) {
                ?><tr>
                    <td><?php echo $this->util->data($dado->tch_data) ?> </td>
                    <td><?php echo $dado->tch_horario ?> </td>
                    <td><?php echo $dado->alu_nome ?> </td> 
                    <td>
                    </td>

                </tr>
            <?php } ?>

        </tbody>
    </table>
    <ul class='pagination pagination-small'>
        <li>
            <?php //echo $paginacao; ?>
        </li>
    </ul>
<?php } else { ?>
    <h4>Nenhum registro encontrado</h4>
<?php } ?>

