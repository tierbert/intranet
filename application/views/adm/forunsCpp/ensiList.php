<?php
if ($semana == '') {
    $semana = date('W') - 1;
    $day = date('w');
    $domingo = date('m-d-Y', strtotime('-' . $day . ' days'));
    $datas = array(
        '1' => date('d-m-Y', strtotime('+' . (1 - $day) . ' days')), //segunda
        '2' => date('d-m-Y', strtotime('+' . (2 - $day) . ' days')), // terca
        '3' => date('d-m-Y', strtotime('+' . (3 - $day) . ' days')), // quarta
        '4' => date('d-m-Y', strtotime('+' . (4 - $day) . ' days')), // quinta
        '5' => date('d-m-Y', strtotime('+' . (5 - $day) . ' days')), //sexta
        '6' => date('d-m-Y', strtotime('+' . (6 - $day) . ' days')), //sabado
    );
} else {

    $dias = ($semana * 7) - 1;
    $diaDomingo = date('Y-m-d', strtotime('2018-01-01' . " + $dias  days"));

    $day = date('w', strtotime($diaDomingo));
    $domingo = date('m-d-Y', strtotime('-' . $day . ' days'));
    $datas = array(
        '1' => date('d-m-Y', strtotime($diaDomingo . '+' . (1 - $day) . ' days')), //segunda
        '2' => date('d-m-Y', strtotime($diaDomingo . '+' . (2 - $day) . ' days')), // terca
        '3' => date('d-m-Y', strtotime($diaDomingo . '+' . (3 - $day) . ' days')), // quarta
        '4' => date('d-m-Y', strtotime($diaDomingo . '+' . (4 - $day) . ' days')), // quinta
        '5' => date('d-m-Y', strtotime($diaDomingo . '+' . (5 - $day) . ' days')), //sexta
        '6' => date('d-m-Y', strtotime($diaDomingo . '+' . (6 - $day) . ' days')), //sabado
    );
}

$volta = '';
$avanca = '';
$semanaAtual = date('W') - 1;
if ($semana == $semanaAtual) {
    //$volta = 'disabled';
} elseif ($semana > $semanaAtual + 1) {
    //$avanca = 'disabled';
}
if ($semana > $semanaAtual + 2) {
    //echo "<script>;window.location.href='" . site_url() . "/aluno/tcc/agendarTcc'</script>";
}
?>
<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            FÓRUNS - CPP
        </div>
        <div class="col-md-2">
            <a href="<?php echo site_url('adm/forumAdm/ensiList/' . ($semana - 1)); ?>" class=" btn btn-primary <?php echo $volta; ?>">
                <span class="glyphicon glyphicon-backward"></span>
            </a>
            <a href="<?php echo site_url('adm/forumAdm/ensiList/'); ?>" class=" btn btn-primary ">
                <span class="glyphicon glyphicon-home"></span>
            </a>
            <a href="<?php echo site_url('adm/forumAdm/ensiList/' . ($semana + 1)); ?>" class=" btn btn-primary <?php echo $avanca; ?>">
                <span class="glyphicon glyphicon-forward"></span>
            </a>
        </div>
        <div class="col-md-4 text-right">

            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">

    <div>
        <hr style="margin: 0 0 0 0">
        <ul class="nav nav-tabs">

            <!--
            <li  role="presentation" class="pendentes" >
                <a href="<?php echo site_url('adm/forumAdm/'); ?>" >
                    FÓRUM APRENDIZAGEM
                </a>
            </li>
            -->

            <li role="presentation" class="aprovados  active">
                <a href="<?php echo site_url('adm/forumAdm/ensiList'); ?>">
                    FÓRUM ENSINAGEM
                </a>
            </li>

        </ul>
        <hr style="margin: 0 0 10px  0">
    </div>



    <table class="table table-striped table-bordered table-hover">
        <tr class="text-center">
            <th></th>
            <th class="text-center">SEGUNDA <br> <?php echo $datas['1']; ?></th>
            <th class="text-center">TERÇA <br> <?php echo $datas['2']; ?></th>
            <th class="text-center">QUARTA <br> <?php echo $datas['3']; ?></th>
            <th class="text-center">QUINTA <br> <?php echo $datas['4']; ?></th>
            <th class="text-center">SEXTA <br> <?php echo $datas['5']; ?></th>
        </tr>
        <?php
        foreach ($turnos->result() as $turno) {
            ?>
            <tr>
                <td>
                    <?php echo $turno->tur_horario; ?>
                </td>
                <?php
                for ($dia = 1; $dia < 6; $dia++) {
                    $horario = $this->fcpp->getHorario($turno->tur_id, $dia);
                    ?>
                    <td>
                        <?php //echo $horario->num_rows(); ?>
                        <?php
                        if ($horario->num_rows() > 0) {

                            $dados = $horario->row();
                            $agenda = $this->fcpp->getByHorarioData($dados->hor_id, $this->util->data($datas[$dia]) . ' 00:00:00', 'prof');
                            echo 'AGENDADOS: ' . $agenda->num_rows() . ' DE ' . $dados->hor_vagas;
                            ?>
                            <a href="<?php //echo site_url('adm/tcc/tccAgendados/horDel/' . $dados->hor_id);                             ?>"  class="btn btn-danger btn-xs pull-right disabled" style="font-size: 12px;">
                                x
                            </a>
                            <hr style="margin: 5px 0 5px 0;">

                            <?php
                            if ($agenda->num_rows() > 0) {
                                if ($agenda->row('tca_idAluno') == 0) {
                                    echo '<span style="color: blue">RESERVADO.</span>';
                                } else {

                                    echo 'PROFESSOR:<br><ul>';
                                    foreach ($agenda->result() as $ag) {
                                        echo '<li style="color: blue">' . $ag->pro_nome . '</li>';
                                    }
                                    echo '</ul>';
                                    echo "<hr style='margin: 5px 0 5px 0; '>";
                                    echo 'OBS.:<br> <span style="color: blue">' . $ag->tca_obs . '</span>';
                                }
                            } else {
                                echo '<span style="color: green">SEM PROF. AGENDADO</span>';
                            }
                        } else {
                            ?>
                            <a href="<?php //echo site_url('adm/tcc/tccAgendados/horCad/' . $turno->tur_id . '/' . $dia);                            ?>" class="btn btn-primary btn-xs disabled" style="font-size: 12px;">
                                +
                            </a>
                            <?php
                        }
                        ?>



                    </td>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>
</div>




<style>
    /*
    .table-striped>tbody>tr:nth-child(odd)>td, 
    .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #337ab7;
    }
    .table-striped2>tbody>tr:nth-child(odd)>td, 
    .table-striped2>tbody>tr:nth-child(odd)>th {
        background-color: #337ab7;
    }
    */

</style>

<style>
    .active li{
        background: #337ab7;
    }


    tabs>li.active>a:focus, .nav-tabs>li.active>a {
        background: #337ab7;
        border: 1px solid #337ab7;
        color: white;
        cursor: pointer

    }

    tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
        background: #337ab7;
        border: 1px solid #337ab7;
        color: white;
        cursor: pointer

    }


</style>