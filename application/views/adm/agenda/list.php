<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            RAMAIS / E-MAILS
        </div>

        <div class="col-md-6 col-xs-6 text-right">
            <a class='btn btn-info' href="<?php echo site_url('adm/agenda/agendaAdm/') ?>">CADASTRAR</a>
            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                VOLTAR
            </a> 
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">

        <div class="painel" style="min-height: 400px;">
            <h3 class="text-center" style="margin-bottom: 15px;">RAMAIS</h3>
            <?php if ($objects): ?>
                <table class='table table-bordered table-striped table-condensed table-hover' id="dataTables" style="margin-top: 15px;">
                    <thead>
                        <tr>
                            <th>Nome / Setor</th>
                            <th>Número / Ramal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($objects as $object): ?>
                            <tr>
                                <td><?php echo $object->ag_nome ?></td>
                                <td><?php echo $object->ag_numero ?></td>

                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            <?php else: ?>
                <h4>Nenhum registro encontrado</h4>
            <?php endif ?>

        </div>
    </div>
    <div class="col-md-6">

        <div class="painel" style="min-height: 400px;">
            <h3 class="text-center">E-MAILS</h3>

        </div>
    </div>
</div>


<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/dataTables/jquery.dataTables.js"></script>
<link type="text/css" href="<?php echo base_url('assets'); ?>/js/dataTables/dataTables.bootstrap">
<script>

    $(function () {
        $('#dataTables').DataTable({
            "paging": false,
        });
        //alert('teste');
    });



</script>