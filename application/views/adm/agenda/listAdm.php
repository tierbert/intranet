<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            RAMAIS
        </div>
         
        <div class="col-md-6 col-xs-6 text-right">
            <a class='btn btn-info' href="<?php echo site_url('adm/agenda/criar/') ?>">Novo Cadastro</a>
            <a href="<?php echo site_url('adm/agenda'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">

   
</p>
<?php if ($objects): ?>
    <table class='table table-bordered table-striped table-condensed table-hover'>
        <thead>
            <tr>
                <th>Nome / Setor</th>
                <th>Número / Ramal</th>
                <th> Ação </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($objects as $object): ?>
                <tr>
                    <td><?php echo $object->ag_nome ?></td>
                    <td><?php echo $object->ag_numero ?></td>
                    <td>
                        <a class='btn btn-sm btn-warning' href="<?php echo site_url('adm/agenda/editar/' . $object->ag_id) ?>">Editar</a>
                        <a class='btn btn-sm btn-danger' href="<?php echo site_url('adm/agenda/delete/' . $object->ag_id) ?>">Deletar</a></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
<?php else: ?>
    <h4>Nenhum registro encontrado</h4>
<?php endif ?>
        
