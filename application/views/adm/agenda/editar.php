<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
           EDITANDO RAMAIS
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/agenda/agendaAdm/'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">

    <div class=form-horizontal>    
        <div>
            <?php echo validation_errors(); ?>
        </div>

        <?php echo form_open() ?>
        <div class=form-group>
            <?php
            $this->util->input2('Nome / Setor', 'ag_nome', $object->ag_nome,'',3); 
            $this->util->input2('Número / Ramal', 'ag_numero', $object->ag_numero, '', 2);
         /*   $this->util->input2('Cidade', 'ag_cidade', $object->ag_cidade, '', 2);
            $this->util->input2('Obs', 'ag_obs', $object->ag_obs);*/
            ?>
        </div>
        <div class=form-group>
            <div class="col-md-2">
                <label>&nbsp;</label>
                <input type=submit  value=Salvar class='btn btn-success form-control'>
            </div>

        </div>            
        <?php echo form_close() ?>
    </div>
</div>