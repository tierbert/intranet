<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            <?php echo ucfirst($titulo); ?> 
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a class='btn btn-info' href='<?php echo site_url('adm/salas/criar/') ?>'>
                NOVA SALA
            </a>    
            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
 

    <div class="row">
        <?php if (count($dados) != 0) { ?>
            <table class='table table-bordered table-striped table-condensed table-hover'>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Abreviatura</th>
                        <th>Tipo</th>
                        <th>Status</th>
                        <th>Ação</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dados->result() as $dado) {
                        ?><tr><td><?php echo $dado->sal_id ?> </td>
                            <td><?php echo $dado->sal_nome ?> </td>
                            <td><?php echo $dado->sal_abreviatura ?> </td>
                            <td><?php echo $dado->sal_tipo ?> </td>
                            <td><?php echo $dado->sal_status ?> </td>
                            <td>
                                <a class='btn btn-xs btn-warning' href="<?php echo site_url('adm/salas/editar/' . $dado->sal_id) ?>">
                                    Editar
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <ul class='pagination pagination-small'>
                <li>
                    <?php echo $paginacao; ?>
                </li>
            </ul>
        <?php } else { ?>
            <h4>Nenhum registro encontrado</h4>
        <?php } ?>
    </div>
</div>