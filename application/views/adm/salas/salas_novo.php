<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            CADASTRO SALA
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('adm/salas'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 300px;">

    <div class=form-horizontal>    
        <div class="error">
            <?php echo validation_errors(); ?>
        </div>
        <?php echo form_open() ?>
        <div class="form-group">
            <?php $this->util->input2('NOME DA SALA', 'sal_nome', $dados->sal_nome, '', 3, '', 'required'); ?>
            <?php $this->util->input2('ABREVIATURA', 'sal_abreviatura', $dados->sal_abreviatura, '', 2, '', 'required'); ?>

            <div class="col-md-2"> 
                <label> TIPO </label>
                <select name="sal_tipo" class="form-control" required="true">
                    <?php
                    if ($dados->sal_tipo != '') {
                        echo "<option value='$dados->sal_tipo'> $dados->sal_tipo </option>";
                    }
                    ?>                

                    <option value="SALA"> SALA </option>
                    <option value="LAB">LAB. SAUDE</option>
                    <option value="LABSAUDE2"> LAB. SAUDE 2</option>
                </select>
            </div>
            <div class="col-md-2"> 
                <label> Status </label>
                <select name="sal_status" class="form-control" required="true">
                    <?php
                    if ($dados->sal_tipo != '') {
                        echo "<option value='$dados->sal_status'> $dados->sal_status </option>";
                    }
                    ?>                

                    <option value="">Selecione o Status!</option>
                    <option value="ATIVA">ATIVA</option>
                    <option value="INATIVA">INATIVA</option>
                    <option value="MANUTENÇÃO">MANUTENÇÃO</option>
                </select>
            </div>
        </div>

        <div class=form-group>
            <div class="col-md-2">
                <label>&nbsp;</label>
                <input type=submit  value=Salvar class='btn btn-success form-control'>
            </div>

        </div>            
        <?php echo form_close() ?>
    </div>
</div>
<?php $this->util->mascaras(); ?>
<style>
    .error p{
        color: white;
        background-color: red;
        padding: 5px;
        margin-bottom: 5px;
    }
</style>