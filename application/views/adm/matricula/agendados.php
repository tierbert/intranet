<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            MATRÍCULA / <?php echo ucfirst($titulo); ?>
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                Voltar
            </a> 
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">

    <?php $this->load->view('adm/matricula/botoes') ?>

    <h3 class='pull-left'></h3>
    <div class="row">
        <div class="col-md-8">
            <div class="form hidden-print" style="margin-left: 15px;">
                <?php echo form_open(); ?>

                <div class="form-group">
                    <div class="col-md-6" style="padding: 0; padding-left: 5px;">
                        <label>Aluno:</label>
                        <input name="nome" value="<?php if (isset($post) && $post['nome'] != '') echo $post['nome']; ?>" type="text" class="form-control" >
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-3" style="margin-top: 5px;">
                        &nbsp;
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-primary">Pesquisar</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>

            </div>
        </div>

    </div>
    <hr style="margin: 0;">

    <?php if (count($dados) != 0) { ?>
        <table class='table table-bordered table-striped table-condensed table-hover'>
            <thead>
                <tr>
                    <th>ALUNO</th>
                    <th>DATA</th>
                    <th>HORÁRIO</th>
                    <th>PROFESSOR</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($dados as $dado) {
                    ?><tr class="<?php if ($dado->aha_conferido) echo 'success'; ?>">
                        <td><?php echo $dado->alu_nome ?> </td>
                        <td><?php echo $dado->ahh_data ?> </td>
                        <td><?php echo $dado->ahh_turno ?> </td>
                        <td><?php echo $dado->ahh_professor ?> </td>
                        <td>
                            <a class='btn btn-xs btn-warning' href="<?php echo site_url('adm/agendamento/agend_agendados/conferido/' . $dado->aha_id) ?>">
                                Conferir
                            </a>
                            <a class='btn btn-xs btn-danger' href="<?php echo site_url('adm/agendamento/agend_agendados/delete/' . $dado->aha_id) ?>">
                                Cancelar
                            </a>
                        </td>
                    </tr>
                <?php } ?>

            </tbody>
        </table>
        <ul class='pagination pagination-small'>
            <li>
                <?php echo $paginacao; ?>
            </li>
        </ul>
    <?php } else { ?>
        <h4>Nenhum registro encontrado</h4>
    <?php } ?>

    <?php $this->util->mascaras() ?>