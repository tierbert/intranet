<link href="<?php echo base_url(); ?>assets/css/sb-admin-2.css" rel="stylesheet" type="text/css"/>
<div class="row">
    <div class="col-lg-2 col-md-6" style="width: 200px;">
        <a href="<?php echo site_url('adm/matricula/agendados'); ?>">
            <div class="panel panel-primary">
                <div class="panel-footer">
                    <span class="pull-left">AGENDADOS</span>
                    <span class="pull-right">
                        <i class="glyphicon glyphicon-calendar"></i></span>
                    <div class="clearfix"></div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-lg-2 col-md-6" style="width: 200px;">
        <a href="<?php echo site_url('adm/matricula/horarios'); ?>">
            <div class="panel panel-green">
                <div class="panel-footer">
                    <span class="pull-left">HORÁRIOS</span>
                    <span class="pull-right">
                        <i class="glyphicon glyphicon-dashboard"></i>
                    </span>
                    <div class="clearfix"></div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-lg-2 col-md-6" style="width: 200px;">
        <a href="<?php echo site_url('adm/matricula/senhas/1'); ?>">
            <div class="panel panel-yellow">
                <div class="panel-footer">
                    <span class="pull-left">SENHAS FINANC</span>
                    <span class="pull-right"><i class="glyphicon glyphicon-align-center"></i></span>
                    <div class="clearfix"></div>
                </div>

            </div>
        </a>
    </div>

    <div class="col-lg-2 col-md-6" style="width: 200px;">
        <a href="<?php echo site_url('adm/matricula/senhas/2'); ?>">
            <div class="panel panel-yellow">
                <div class="panel-footer">
                    <span class="pull-left">SENHAS GERENC</span>
                    <span class="pull-right"><i class="glyphicon glyphicon-align-center"></i></span>
                    <div class="clearfix"></div>
                </div>

            </div>
        </a>
    </div>
</div>
<hr style="margin:  0 0 20px 0">