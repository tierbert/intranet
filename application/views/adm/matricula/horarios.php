<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            MATRÍCULA / HORÁRIOS
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a class='btn btn-warning' href='<?php echo site_url('adm/matricula/horarioNovo') ?>'>
                NOVO HORARIO
            </a>

            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                Voltar
            </a> 
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">

    <?php $this->load->view('adm/matricula/botoes') ?>



    <?php if ($dados->num_rows() > 0) { ?>
        <table class='table table-bordered table-striped table-condensed table-hover'>
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Horário</th>
                    <th>Quantidade Vagas</th>
                    <!--<th>Professor</th>  -->
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($dados->result() as $dado) {
                    ?><tr><td><?php echo $dado->ahh_data ?> </td>
                        <td><?php echo $dado->ahh_turno ?> </td>
                        <td><?php echo $dado->ahh_vagas ?> </td>
                        <!--<td><?php echo $dado->ahh_professor ?> </td>  -->
                        <td>
                            <a class='btn btn-xs btn-primary' href="<?php echo site_url('adm/matricula/horarioEditar/' . $dado->ahh_id) ?>">Editar</a>
                            <a class='btn btn-xs btn-danger' href="<?php echo site_url('adm/agendamento/agend_horario/delete/' . $dado->ahh_id) ?>">Deletar</a>
                        </td>

                    </tr>
                <?php } ?>

            </tbody>
        </table>
        <ul class='pagination pagination-small'>
            <li>
                <?php echo $paginacao; ?>
            </li>
        </ul>
    <?php } else { ?>
        <h4>Nenhum registro encontrado</h4>
    <?php } ?>

