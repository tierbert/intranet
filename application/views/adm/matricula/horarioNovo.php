<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            MATRÍCULA
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                Voltar
            </a> 
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">

<?php $this->load->view('adm/matricula/botoes') ?>

<h2><?php echo $titulo; ?></h2>
<hr>
<div class=form-horizontal>    
    <div>
        <?php echo validation_errors(); ?>
    </div>

    <?php echo form_open() ?>
    <div class="form-group">
        <?php $this->util->input('Data', 'ahh_data', $object->ahh_data, 'data', 4, '', 'required'); ?>
        <?php $this->util->input('Horário', 'ahh_turno', $object->ahh_turno, '', 4, '', 'required'); ?>
        <?php $this->util->input('Quantidade Vagas', 'ahh_vagas', $object->ahh_vagas, '', 4, '', 'required'); ?>
        <?php $this->util->input('Tipo', 'ahh_tipo', $object->ahh_tipo, '', 4, '', 'required'); ?>
        <?php //$this->util->select('Curso', 'ahh_curso', $cursos, $object->ahh_curso,'',3);  ?>
    </div>
    <div class=form-group>
        <input type=submit  value=Salvar class='btn btn-primary'>
    </div>            
    <?php echo form_close() ?>
</div>

<?php $this->util->mascaras(); ?>