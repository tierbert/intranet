<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            ATIVIDADES COMP. E APOIO EM EVENTOS
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal> 
        <div class="col-sm-4">    
            <a href="<?php echo site_url('adm/ProjetosAdm/nupexPendentes'); ?>" class=" btn btn-primary">
                COORD NUPEX
            </a>
        </div>
        <div class="col-sm-4">   
            <a href="<?php echo site_url('adm/ProjetosAdm/academicaPendentes'); ?>" class="btn btn-success" >
                COORD GER. ACADÊMICA
            </a>
        </div>
        <div class="col-sm-4">   
            <a href="<?php echo site_url('adm/ProjetosAdm/FinanceiroPendentes'); ?>" class=" btn btn-danger">
                COORD FINANCEIRO
            </a>
            <br>
        </div>


        <div class="col-sm-4">   
            <a href="<?php echo site_url('adm/ProjetosAdm/cipePendentes'); ?>" class=" btn btn-danger">
                CIPE
            </a>
            <br>
        </div>

    </div>

</div>