<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            DETALHES DA COMPRA
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/ProjetosAdm/nupexPendentes'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px;">
    <div class="row">
        <?php $this->load->view('adm/projetos/abasNupex') ?>
        <div class="row">
            <div class="col-md-12">
                <div class="form-horizontal">
                    <div class="form-group">
                        <br>
                        <div class="col-md-5">
                            <label>TÍTULO DA COMPRA</label>
                            <input value="<?php echo $dados->pro_titulo; ?>" type="text" class="form-control" required="true" disabled>
                        </div>
                        <div class="col-md-2">
                            <label>DATA PREVISTA</label>
                            <input value="<?php echo $this->util->data2($dados->pro_dataApresentacao); ?>" type="text" class="form-control data" required="true" disabled>
                        </div>
                        <div class="col-md-2">
                            <label>CUSTO TOTAL</label>
                            <input value="<?php echo $dados->pro_custo; ?>" type="text" class="form-control" required="true" disabled>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="col-md-9">
                            <label>RESUMO DA COMPRA</label>
                            <div class="form-control" style="height: auto; background: #eee">
                                <?php echo $dados->pro_obs; ?>
                            </div>
                        </div>
                    </div>
                    <br>
                    <hr>
                    <h3>
                        ANEXO(S) 
                    </h3>
                    <div class="divAnexos" style="padding-top: 15px;">
                        <?php
                        foreach ($anexos->result() as $anexo) {
                            $pastaAnexos = '../../intranet_files/projetos/';
                            ?>
                            <div class="form-group">
                                <div class="col-md-1 text-center">   
                                    <label>ARQUIVO</label>
                                    <a href="<?php echo site_url('adm/ProjetosAdm/download/' . $anexo->ane_id); ?>" target="_blanck">
                                        <span class="glyphicon glyphicon-download" style="font-size: 28px; margin-top: 5px;"></span>
                                    </a>
                                </div>
                                <div class="col-md-8">   
                                    <label>OBSERVAÇÕES</label>  
                                    <input value="<?php echo $anexo->ane_obs; ?>" type="text" class="form-control" disabled>
                                </div>
                                <br>
                            </div>
                        <?php } ?>
                    </div>
                    <hr>
                    <h3>
                        <!-- --> PARECER
                    </h3>
                    <?php echo form_open(); ?>
                    <div class="form-group">
                        <div class="col-md-3">
                            <label>&nbsp;</label>
                            <select name="enc_parecer" class="form-control" required="true">
                                <option value=""></option>
                                <option value="DEFERIDO">DEFERIDO</option>
                                <option value="INDEFERIDO">INDEFERIDO</option>
                            </select> 
                        </div>
                        <div class="col-md-6">
                            <label>PRODUTOS SOLICITADOS</label>
                            
                     <table class='table table-bordered table-striped table-condensed table-hover'>
                    <thead>
                        <tr>
                            <th>PRODUTO</th>
                            <th>VALOR</th>
                        </tr>
                    </thead>
                    <TBODY>
                        <TR>
                            <TD>PRODUTO 1</TD>
                            <TD>R$ 1000,00</TD>
                        </TR>
                            <TR>
                            <TD>PRODUTO 2</TD>
                            <TD>R$ 563,24</TD>
                        </TR>
                    </TBODY>
                    </table>   
                    </div> 
                   
                    <br>
                </div>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3">
                            <label>&nbsp;</label>
                            <input type="submit" class="form-control btn btn-success" value="EMCAMINHAR ">
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $('.pendentes').addClass('active');

</script>