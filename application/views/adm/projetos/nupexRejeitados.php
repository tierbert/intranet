<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            PROJETOS
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class="row">
        <?php $this->load->view('adm/projetos/abasNupex') ?>




        <?php if ($projetos->num_rows() != 0) { ?>
            <table class='table table-bordered table-striped table-condensed table-hover'>
                <thead>
                    <tr>
                        <th>Nº Projeto</th>
                        <th>Professor</th>
                        <th>Título</th>
                        <th>Tipo</th>
                        <th>Ação</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($projetos->result() as $dado) {
                        $dadosProf = $this->db->get_where('salalivre.professores', array('pro_id' => $dado->pro_idProfessor))->row();
                        ?>
                        <tr>
                            <td><?php echo $dado->pro_id ?> </td>
                            <td><?php echo $dadosProf->pro_nome ?> </td>
                            <td><?php echo $dado->pro_titulo ?> </td>
                            <td><?php echo $dado->pro_tipo ?> </td>
                            <td>
                                <a class='btn btn-xs btn-warning' href="<?php echo site_url('adm/ProjetosAdm/nupexRejeitadosDetalhar/' . $dado->pro_id) ?>">
                                    DETALHAR
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>

        <?php } else { ?>
            <h4>Nenhum registro encontrado</h4>
        <?php } ?>

    </div>
</div>
<script>

    $('.rejeitados').addClass('active');

</script>