<div>
    <h3>GERÊNCIA</h3> <br>
    <hr style="margin: 0 0 0 0">
    <ul class="nav nav-tabs">
        <li  role="presentation" class="pendentes" >
            <a href="<?php echo site_url('adm/ProjetosAdm/nupexPendentes'); ?>" >
                PENDENTES
            </a>
        </li>
        <li role="presentation" class="cipe">
            <a href="<?php echo site_url('adm/ProjetosAdm/nupexCipe'); ?>" >
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                GER. TI
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </a>
        </li>
        <li role="presentation" class="academica">
            <a href="<?php echo site_url('adm/ProjetosAdm/nupexAcademica'); ?>" >
                GER. ADM
            </a>
        </li>
        <li role="presentation" class="financeiro">
            <a href="<?php echo site_url('adm/ProjetosAdm/nupexFinanceiro'); ?>"> 
                GER. FIN
            </a>
        </li>
        <li role="presentation" class="aprovados">
            <a href="<?php echo site_url('adm/ProjetosAdm/nupexAprovados'); ?>">
                APROVADOS
            </a>
        </li>
        <li role="presentation" class="rejeitados" >
            <a href="<?php echo site_url('adm/ProjetosAdm/nupexRejeitados'); ?>">
                REJEITADOS
            </a>
        </li>
        <li role="presentation" class="todos">
            <a href="<?php echo site_url('adm/ProjetosAdm/nupexTodos'); ?>">
                TODOS
            </a>
        </li>
        <li></li>
        <!--<a href="<?php echo site_url('adm/ProjetosAdm/cipePendentes'); ?>" class="btn btn-primary btn-xs pull-right" style="margin-top: 8px;">
            Painel CIPE
        </a>-->
    </ul>
    <hr style="margin: 0 0 10px  0">
</div>

<style>
    .active li{
        background: #337ab7;
    }


    tabs>li.active>a:focus, .nav-tabs>li.active>a {
        background: #337ab7;
        border: 1px solid #337ab7;
        color: white;
        cursor: pointer

    }

    tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
        background: #337ab7;
        border: 1px solid #337ab7;
        color: white;
        cursor: pointer

    }


</style>