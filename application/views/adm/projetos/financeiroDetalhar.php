<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            PROJETOS
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/ProjetosAdm/nupexFinanceiro'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px;">
    <div class="row">
        <?php $this->load->view('adm/projetos/abasNupex') ?>
        <div class="row">
            <div class="col-md-12">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-5">
                            <label>TÍTULO DO PROJETO</label>
                            <input value="<?php echo $dados->pro_titulo; ?>" type="text" class="form-control" required="true" disabled>
                        </div>
                        <div class="col-md-2">
                            <label>DATA PREVISTA</label>
                            <input value="<?php echo $dados->pro_dataApresentacao; ?>" type="text" class="form-control data" required="true" disabled>
                        </div>
                        <div class="col-md-2">
                            <label>CUSTO TOTAL</label>
                            <input value="<?php echo $dados->pro_custo; ?>" type="text" class="form-control" required="true" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-9">
                            <label>RESUMO DO PROJETO</label>
                            <div class="form-control" style="height: auto; background: #eee">
                                <?php echo $dados->pro_obs; ?>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h3>
                        ANEXO(S) 
                    </h3>
                    <div class="divAnexos" style="padding-top: 15px;">
                        <?php
                        foreach ($anexos->result() as $anexo) {
                            $pastaAnexos = '../../intranet_files/projetos/';
                            ?>
                            <div class="form-group">
                                <div class="col-md-1 text-center">   
                                    <label>ARQUIVO</label>
                                    <a href="<?php echo site_url('adm/ProjetosAdm/download/' . $anexo->ane_id); ?>" target="_blanck">
                                        <span class="glyphicon glyphicon-download" style="font-size: 28px; margin-top: 5px;"></span>
                                    </a>
                                </div>
                                <div class="col-md-8">   
                                    <label>OBSERVAÇÕES</label>  
                                    <input value="<?php echo $anexo->ane_obs; ?>" type="text" class="form-control" disabled>
                                </div>
                                <br>
                            </div>
                        <?php } ?>
                    </div>
                    <hr>
                    <h3>
                        PARECER NUPEX
                    </h3>
                    <?php
                    $idSetor = 18;
                    $parecerNupex = $this->projetosmodel->getParecerBySetor($dados->pro_id, $idSetor)
                    ?>
                    <div class="form-group">
                        <div class="col-md-3">
                            <label>PARECER</label>
                            <input value="<?php echo $parecerNupex->enc_parecer; ?>" type="text" class="form-control" required="true" disabled>

                            <label>DATA DO PARECER</label>
                            <input value="<?php echo $this->util->data3($parecerNupex->enc_dataParecer); ?>" type="text" class="form-control" required="true" disabled>
                        </div>
                        <div class="col-md-6">
                            <label>observação</label>
                            <textarea name="enc_obs" class="form-control" style="height: 100px;" disabled><?php echo ($parecerNupex->enc_obs); ?></textarea>
                        </div>
                    </div>

                    <hr>
                    <h3>
                        PARECER GER. ACADÊMICA
                    </h3>

                    <?php
                    $idSetor = 29;
                    $parecerAcad = $this->projetosmodel->getParecerBySetor($dados->pro_id, $idSetor)
                    ?>
                    <div class="form-group">
                        <div class="col-md-3">
                            <label>PARECER</label>
                            <input value="<?php echo $parecerAcad->enc_parecer; ?>" type="text" class="form-control" required="true" disabled>

                            <label>DATA DO PARECER</label>
                            <input value="<?php echo $this->util->data3($parecerAcad->enc_dataParecer); ?>" type="text" class="form-control" required="true" disabled>
                        </div>
                        <div class="col-md-6">
                            <label>observação</label>
                            <textarea name="enc_obs" class="form-control" style="height: 100px;" disabled><?php echo ($parecerAcad->enc_obs); ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3">
                            <label>&nbsp;</label>
                            <a  href="<?php echo site_url('adm/ProjetosAdm/nupexFinanceiro'); ?>" class="form-control btn btn-primary">VOLTAR</a>
                        </div>
                    </div>

                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $('.financeiro').addClass('active');

</script>