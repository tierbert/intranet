<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            DADOS DA COMPRA
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/ProjetosAdm/nupexPendentes'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px;">
    <div class="row">
        <div class="row">
            <div class="col-md-12">
                <div class="form-horizontal">
                    <div class="form-group">
                        <br>
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-6">
                            <label>REQUISIÇÃO Nº: <b> <?php echo $dados->pro_id; ?></b> </label><br>

                            <label>SETOR: <b> <?php echo $dados->pro_local; ?></b> </label><br>
                        
                            <label>TÍTULO DA REQUISIÇÃO: <b> <?php echo $dados->pro_titulo; ?></b> </label><br>
                           
                        
                            <label>DATA PREVISTA: <b><?php echo $this->util->data2($dados->pro_dataApresentacao); ?></b> </label><br>

                       
                            <label>CUSTO TOTAL: <b> R$ <?php echo $dados->pro_custo; ?></b></label> <br>
                            
                            <label>RESUMO DA COMPRA: <b> <?php echo $dados->pro_obs; ?> </b> </label>
                                                          
                            </div>
                        </div>
                    </div>
                    
                    <hr>
                    <div class="form-group">
            
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-6">
                            <label>LISTA DE PRODUTOS SOLICITADOS</label>                            
                            <table class='table table-bordered table-striped table-condensed table-hover'>
                    <thead>
                        <tr>
                            <th>PRODUTO</th>
                            <th>VALOR</th>
                        </tr>
                    </thead>
                    <TBODY>
                        <TR>
                            <TD>PRODUTO 1</TD>
                            <TD>R$ 1000,00</TD>
                        </TR>
                            <TR>
                            <TD>PRODUTO 2</TD>
                            <TD>R$ 563,24</TD>
                        </TR>
                    </TBODY>
                    </table>   
                    </div> 
                   
                    <br>
                </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
