<div>
    <h3>CIPE</h3> <br>
    <hr style="margin: 0 0 0 0">
    <ul class="nav nav-tabs">
        <li  role="presentation" class="pendentes" >
            <a href="<?php echo site_url('adm/ProjetosAdm/cipePendentes'); ?>" >
                PENDENTES
            </a>
        </li>

        <li role="presentation" class="aprovados">
            <a href="<?php echo site_url('adm/ProjetosAdm/cipeAprovados'); ?>">
                APROVADOS
            </a>
        </li>
        <li role="presentation" class="rejeitados">
            <a href="<?php echo site_url('adm/ProjetosAdm/cipeRejeitados'); ?>">
                REJEITADOS
            </a>
        </li>
    </ul>
    <hr style="margin: 0 0 10px  0">
</div>

<style>
    .active li{
        background: #337ab7;
    }


    tabs>li.active>a:focus, .nav-tabs>li.active>a {
        background: #337ab7;
        border: 1px solid #337ab7;
        color: white;
        cursor: pointer

    }

    tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
        background: #337ab7;
        border: 1px solid #337ab7;
        color: white;
        cursor: pointer

    }


</style>