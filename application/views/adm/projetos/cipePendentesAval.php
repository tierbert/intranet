<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            PROJETOS
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px;">
    <div class="row">
        <?php $this->load->view('adm/projetos/abasCipe') ?>
        <div class="row">
            <div class="col-md-12">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-5">
                            <label>TÍTULO DO PROJETO</label>
                            <input value="<?php echo $dados->pro_titulo; ?>" type="text" class="form-control" required="true" disabled>
                        </div>
                        <div class="col-md-2">
                            <label>DATA PREVISTA</label>
                            <input value="<?php echo $dados->pro_dataApresentacao; ?>" type="text" class="form-control data" required="true" disabled>
                        </div>
                        <div class="col-md-2">
                            <label>CUSTO TOTAL</label>
                            <input value="<?php echo $dados->pro_custo; ?>" type="text" class="form-control" required="true" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-9">
                            <label>RESUMO DO PROJETO</label>
                            <div class="form-control" style="height: auto; background: #eee">
                                <?php echo $dados->pro_obs; ?>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h3>
                        ANEXO(S) 
                    </h3>
                    <div class="divAnexos" style="padding-top: 15px;">
                        <?php
                        foreach ($anexos->result() as $anexo) {
                            $pastaAnexos = '../../intranet_files/projetos/';
                            ?>
                            <div class="form-group">
                                <div class="col-md-1 text-center">   
                                    <label>ARQUIVO</label>
                                    <a href="<?php echo site_url('adm/ProjetosAdm/download/' . $anexo->ane_id); ?>" target="_blanck">
                                        <span class="glyphicon glyphicon-download" style="font-size: 28px; margin-top: 5px;"></span>
                                    </a>
                                </div>
                                <div class="col-md-8">   
                                    <label>OBSERVAÇÕES</label>  
                                    <input value="<?php echo $anexo->ane_obs; ?>" type="text" class="form-control" disabled>
                                </div>
                                <br>
                            </div>
                        <?php } ?>
                    </div>
                    <hr>
                    <h3>
                        PARECER NUPEX
                    </h3>
                    <div class="form-group">
                        <div class="col-md-3">
                            <label>PARECER</label>
                            <input value="<?php echo $parecerNupex->enc_parecer; ?>" type="text" class="form-control" required="true" disabled>

                            <label>DATA DO PARECER</label>
                            <input value="<?php echo $parecerNupex->enc_dataParecer; ?>" type="text" class="form-control" required="true" disabled>
                        </div>
                        <div class="col-md-6">
                            <label>observação</label>
                            <textarea name="enc_obs" class="form-control" style="height: 100px;" disabled><?php echo $parecerNupex->enc_obs; ?></textarea>
                        </div>
                    </div>
                    <hr>
                    <h3>
                        PARECER CIPE
                    </h3>
                    <?php echo form_open(); ?>
                    <div class="form-group">
                        <div class="col-md-3">
                            <label>&nbsp;</label>
                            <select name="enc_parecer" class="form-control" required="true">
                                <option value=""></option>
                                <option value="DEFERIDO">DEFERIDO</option>
                                <option value="INDEFERIDO">INDEFERIDO</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>observação</label>
                            <textarea name="enc_obs" class="form-control" style="height: 100px;"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3">
                            <label>&nbsp;</label>
                            <input type="submit" class="form-control btn btn-primary" value="SALVAR PARECER">
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $('.pendentes').addClass('active');

</script>