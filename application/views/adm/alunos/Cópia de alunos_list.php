
<?php $this->load->view('botoes') ?>
<div class="row">
    <h3 class='pull-left'><?php echo ucfirst($titulo); ?> - Total Cadastrffados: <?php echo $totalAlunos; ?> </h3>
    <div class="pull-right">
        <a class='btn btn-primary ' href='<?php echo site_url('adm/alunos/criar/') ?>'>Cadastrar Aluno</a>
        
    </div>
</div>
<div class="row">
    <div class="form hidden-print" style="margin-left: 15px;">
        <?php echo form_open(); ?>

        <div class="form-group">
            <div class="col-md-3" style="padding: 0; padding-left: 5px;">
                <label>Aluno:</label>
                <input name="nome" value="<?php if (isset($post) && $post['nome'] != '') echo $post['nome']; ?>" type="text" class="form-control" >
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-4" style="margin-top: 5px;">
                &nbsp;
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Pesquisar</button>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>

    </div>

</div>
<?php if (count($dados) != 0) { ?>
    <table class='table table-bordered table-striped table-condensed table-hover'>
        <thead>
            <tr>
                <th>Nº Matrícula</th>
                <th>Nome</th>
                <th>CPF</th>
                <th>Ação</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dados->result() as $dado) {
                ?><tr><td><?php echo $dado->alu_matricula ?> </td>
                    <td><?php echo $dado->alu_nome ?> </td>
                    <td><?php echo $dado->alu_cpf ?> </td>
                    <td><a class='btn btn-xs btn-primary' href="<?php echo site_url('adm/alunos/editar/' . $dado->alu_id)
                ?>">Editar</a>
                        <a class='btn btn-xs btn-danger' href="<?php echo site_url('adm/alunos/delete/' . $dado->alu_id) ?>">Deletar</a>
                    </td>

                </tr>
            <?php } ?>

        </tbody>
    </table>
    <ul class='pagination pagination-small'>
        <li>
            <?php echo $paginacao; ?>
        </li>
    </ul>
<?php } else { ?>
    <h4>Nenhum registro encontrado</h4>
<?php } ?>

