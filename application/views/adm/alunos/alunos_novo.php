<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            CADASTRO NOVO ALUNO
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('adm/alunos'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 300px;">

    <div class=form-horizontal>    
        <div class="error">
            <?php echo validation_errors(); ?>
        </div>
        <?php echo form_open() ?>
        <div class="form-group">
            <?php $this->util->input2('Nº Matrícula', 'alu_matricula', set_value('alu_matricula'), '', 2); ?>
            <?php $this->util->input2('Nome', 'alu_nome', set_value('alu_nome')); ?>
            <?php $this->util->input2('CPF', 'alu_cpf', $object->alu_cpf, 'cpf', 2); ?>
        </div>
        <div class="form-group">
            <?php $this->util->input2('Senha', 'alu_senha', set_value('alu_senha'), '', 2, '', '', 2, 'password'); ?>
            <?php $this->util->input2('E-mail', 'alu_email', $object->alu_email); ?>

        </div>
        <div class=form-group>
            <div class="col-md-2">
                <label>&nbsp;</label>
                <input type=submit  value=Salvar class='btn btn-success form-control'>
            </div>

        </div>            
        <?php echo form_close() ?>
    </div>
</div>
<?php $this->util->mascaras(); ?>
<style>
    .error p{
        color: white;
        background-color: red;
        padding: 5px;
        margin-bottom: 5px;
    }
</style>