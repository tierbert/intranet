<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            <?php echo ucfirst($titulo); ?> 
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a class='btn btn-info' href='<?php echo site_url('adm/alunos/criar/') ?>'>
                NOVO ALUNO
            </a>    
            <a href="<?php echo site_url('adm/painel/solicitacoes'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class="row">
        <div class="form-horizontal hidden-print" style="margin-bottom: 10px;">
            <?php echo form_open(); ?>

            <div class="form-group">
                <div class="col-md-2" >
                    <label>MATRÍCULA:</label>
                    <input name="matricula" value="<?php if (isset($post) && $post['matricula'] != '') echo $post['matricula']; ?>" type="text" class="form-control" >
                </div>
                <div class="col-md-4" >
                    <label>NOME:</label>
                    <input name="nome" value="<?php if (isset($post) && $post['nome'] != '') echo $post['nome']; ?>" type="text" class="form-control" >
                </div>

                <div class="col-md-2" >
                    <label>&nbsp;</label>
                    <button type="submit" class="form-control btn btn-success" >Pesquisar</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>

    <div class="row">
        <?php if (count($dados) != 0) { ?>
            <table class='table table-bordered table-striped table-condensed table-hover'>
                <thead>
                    <tr>
                        <th>Nº Matrícula</th>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Ação</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dados->result() as $dado) {
                        ?><tr><td><?php echo $dado->alu_matricula ?> </td>
                            <td><?php echo $dado->alu_nome ?> </td>
                            <td><?php echo $dado->alu_cpf ?> </td>
                            <td><a class='btn btn-xs btn-warning' href="<?php echo site_url('adm/alunos/editar/' . $dado->alu_id)
                        ?>">Editar</a>
                                <a class='btn btn-xs btn-danger' href="<?php echo site_url('adm/alunos/delete/' . $dado->alu_id) ?>">Deletar</a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <ul class='pagination pagination-small'>
                <li>
                    <?php echo $paginacao; ?>
                </li>
            </ul>
        <?php } else { ?>
            <h4>Nenhum registro encontrado</h4>
        <?php } ?>
    </div>
</div>