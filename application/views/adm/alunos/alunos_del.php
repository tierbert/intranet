<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            EXCLUIR ALUNO
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('adm/alunos'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 300px;">

<div class='form'>
    <?php echo form_open($this->uri->uri_string(), 'post') ?>
    Tem certeza que quer excluir: <?php echo $object->alu_id ?>?
    <br>
    <?php echo form_submit('agree', 'Sim') ?>
    <?php echo form_submit('disagree', 'Não') ?>
    <?php echo form_close() ?>
    
</div>
