<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            <?php echo $titulo; ?>
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <section class="cards-section " style="padding: 0 0">
        <hr>
        <div class="row">
            <div class="col-md-12">
                <h3>
                    Gestão de conflitos
                </h3>        
                <br>
                <?php
                $grupo = 4;
                $inscricao = $this->capacitamodel->getUsuarioGrupo($idUsuario, $grupo);
                if ($inscricao->num_rows() == 0) {
                    ?>
                    <a href="<?php echo site_url('adm/capacita/agendaCoord_curso/' . $grupo); ?>" class="btn btn-primary">
                        Ver Horários
                    </a>
                <?php } else { ?>
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Curso</th>
                            <th>Horário</th>
                            <th>Local</th>
                            <th>Profissional</th>
                        </tr>
                        <tr class="success">
                            <td><?php echo $inscricao->row('cur_curso'); ?></td>
                            <td><?php echo $inscricao->row('cur_horario'); ?></td>
                            <td><?php echo $inscricao->row('cur_sala'); ?></td>
                            <td><?php echo $inscricao->row('cur_profissional'); ?></td>
                        </tr>
                    </table>
                <?php } ?>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <h3>
                    Gestão e Liderança
                </h3>        
                <br>
                <?php
                $grupo = 9;
                $inscricao = $this->capacitamodel->getUsuarioGrupo($idUsuario, $grupo);
                if ($inscricao->num_rows() == 0) {
                    ?>
                    <a href="<?php echo site_url('adm/capacita/agendaCoord_curso/' . $grupo); ?>" class="btn btn-primary">
                        Ver Horários
                    </a>
                <?php } else { ?>
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Curso</th>
                            <th>Horário</th>
                            <th>Local</th>
                            <th>Profissional</th>
                        </tr>
                        <tr class="success">
                            <td><?php echo $inscricao->row('cur_curso'); ?></td>
                            <td><?php echo $inscricao->row('cur_horario'); ?></td>
                            <td><?php echo $inscricao->row('cur_sala'); ?></td>
                            <td><?php echo $inscricao->row('cur_profissional'); ?></td>
                        </tr>
                    </table>
                <?php } ?>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <h3>
                    libras
                </h3>        
                <br>
                <?php
                $grupo = 5;
                $inscricao = $this->capacitamodel->getUsuarioGrupo($idUsuario, $grupo);
                if ($inscricao->num_rows() == 0) {
                    ?>
                    <a href="<?php echo site_url('adm/capacita/agendaCoord_curso/' . $grupo); ?>" class="btn btn-primary">
                        Ver Horários
                    </a>
                <?php } else { ?>
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Curso</th>
                            <th>Horário</th>
                            <th>Local</th>
                            <th>Profissional</th>
                        </tr>
                        <tr class="success">
                            <td><?php echo $inscricao->row('cur_curso'); ?></td>
                            <td><?php echo $inscricao->row('cur_horario'); ?></td>
                            <td><?php echo $inscricao->row('cur_sala'); ?></td>
                            <td><?php echo $inscricao->row('cur_profissional'); ?></td>
                        </tr>
                    </table>
                <?php } ?>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <h3>
                    Redação Oficial 
                </h3>        
                <br>
                <?php
                $grupo = 7;
                $inscricao = $this->capacitamodel->getUsuarioGrupo($idUsuario, $grupo);
                if ($inscricao->num_rows() == 0) {
                    ?>
                    <a href="<?php echo site_url('adm/capacita/agendaCoord_curso/' . $grupo); ?>" class="btn btn-primary">
                        Ver Horários
                    </a>
                <?php } else { ?>
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Curso</th>
                            <th>Horário</th>
                            <th>Local</th>
                            <th>Profissional</th>
                        </tr>
                        <tr class="success">
                            <td><?php echo $inscricao->row('cur_curso'); ?></td>
                            <td><?php echo $inscricao->row('cur_horario'); ?></td>
                            <td><?php echo $inscricao->row('cur_sala'); ?></td>
                            <td><?php echo $inscricao->row('cur_profissional'); ?></td>
                        </tr>
                    </table>
                <?php } ?>
            </div>
        </div>
        <hr>
    </section>
</div>