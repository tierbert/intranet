<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            <?php echo $titulo; ?>
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/capacita/adm'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px;">
    <section class="cards-section " style="padding: 0 0">
        <?php
        echo form_open();
        ?>
        <div class="row">
            <div class="col-md-12">
                <H3>Selecione o colaborador</H3>
                <br>
                <?php $this->util->select2('Colaborador', 'age_idUsuario', $colaboradores, 'selectize', 'selectize', '6', '', 'required') ?>
            </div>
        </div>
        <hr>
        <H3>Selecione o Horário Desejado</H3>
        <br>
        <table class="table table-bordered table-hover">
            <tr>
                <th></th>
                <th>Curso</th>
                <th>Turma</th>
                <th>Local</th>
                <th>Profissional</th>
                <th>Vagas</th>
                <?php if($user == "TI-FUNC" || $user == "RH") { ?>
                    <th></th>
                    <th></th>
                <?php } ?>
            </tr>
            <?php
            foreach ($cursos->result() as $curso) {
                $inscritos = $this->capacitamodel->getInscritos($curso->cur_id);
                ?>
                <tr>
                    <td>
                        <?php if ($inscritos->num_rows() >= $curso->cur_vagas) { ?>
                            Vagas Esgotadas
                        <?php } else { ?>
                            <input type="radio" name="age_idCurso" value="<?php echo $curso->cur_id; ?>" required="true">
                        <?php } ?>
                    </td>
                    <td><?php echo mb_strtoupper($curso->cur_curso); ?></td>
                    <td><?php echo mb_strtoupper($curso->cur_horario); ?></td>
                    <td><?php echo mb_strtoupper($curso->cur_sala); ?></td>
                    <td><?php echo mb_strtoupper($curso->cur_profissional); ?></td>
                    <td>
                        <?php echo ($curso->cur_vagas - $inscritos->num_rows()) . ' de ' . mb_strtoupper($curso->cur_vagas); ?>
                    </td>
                    <?php if($user == "TI-FUNC" || $user == "RH") { ?>
                        <td>
                            <a href="<?php echo site_url('adm/capacita/editClass/'.$curso->cur_id); ?>" class="hidden-print">
                                <i class="glyphicon glyphicon-edit"></i>
                            </a>
                        </td>
                        <td>
                            <a href="<?php echo site_url('adm/capacita/deleteClass/'.$curso->cur_id); ?>" class="hidden-print">
                                <i class="glyphicon glyphicon-trash"></i>
                            </a>
                        </td>
                        
                    <?php } ?>

                </tr>
            <?php } ?>
        </table>

        <input class="btn btn-primary" value="Confirmar" type="submit">

        <?php
        echo form_close();
        ?>
    </section>
</div>

<link href="<?php echo base_url('assets'); ?>/js/selectize/selectize.bootstrap3.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/selectize/selectize.js"></script>
<script>
    $('.selectize').selectize({
        selectOnTab: true,
        //plugins: ['remove_button'],
    });
</script>