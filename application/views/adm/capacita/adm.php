<div class="box"  style="">
    <div class="row">
        <div class="col-md-9 col-xs-6 box-titulo" style="">
            <?php echo $titulo; ?>
        </div>
        <div class="col-md-2 col-xs-6 text-right hidden-print">
            <?php if($user == 'TI-FUNC' || $user == 'RH'){ ?>
            <a href="<?php echo site_url('adm/capacitaCursos/cadastrarCurso/'); ?>" class="btn btn-primary hidden-print">
                Cadastrar Curso
            </a>  
            <?php } ?>
        </div>
        <div class="col-md-1 col-xs-6 text-right hidden-print">
            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px;">
    <div>
        <ul class="nav nav-tabs hidden-print">
            <li  role="presentation" class="pendentes " >
                <a href="<?php echo site_url('adm/capacita/coord_setor'); ?>" >
                    PARA MIM
                </a>
            </li>

            <li role="presentation" class="aprovados active">
                <a href="<?php echo site_url('adm/capacita/adm'); ?>">
                    PARA OS COLABORADORES
                </a>
            </li>

        </ul>

                 
        
    </div>

    <style>
        .active li{
            background: #337ab7;
        }


        tabs>li.active>a:focus, .nav-tabs>li.active>a {
            background: #337ab7;
            border: 1px solid #337ab7;
            color: white;
            cursor: pointer

        }

        tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
            background: #337ab7;
            border: 1px solid #337ab7;
            color: white;
            cursor: pointer

        }


    </style>


    <section class="cards-section " style="padding: 0 0">
        <hr>
        <?php 
        // insert loop
            foreach ($itemslist as $course) {
         ?>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <h3>
                        <?php echo $course->cur_name ?>
                    </h3>        
                </div>
                <div class="col-md-2">
                    <?php if($user == 'TI-FUNC' || $user == 'RH'){ ?>
                        <a href="<?php echo site_url('adm/capacita/cadastrarTurma/'. $course->cur_id); ?>" class="btn btn-primary hidden-print">
                        Cadastrar Turma
                        </a>
                    <?php } ?>     
                </div>
                <div class="col-md-2">
                    <?php if($user == 'TI-FUNC' || $user == 'RH'){ ?>
                        <a href="<?php echo site_url('adm/capacitaCursos/editCourse/'. $course->cur_id); ?>" class="btn btn-primary hidden-print">
                        Editar Curso
                        </a>
                    <?php } ?>     
                </div>
                <div class="col-md-2">
                    <?php if($user == 'TI-FUNC' || $user == 'RH'){ ?>
                        <a href="<?php echo site_url('adm/capacitaCursos/deleteCourse/'. $course->cur_id); ?>" class="btn btn-primary hidden-print">
                        Excluir Curso
                        </a>
                    <?php } ?>     
                </div>
                <br>
                <br>
                <?php
                $grupo = 5;
                $inscricoes = $this->capacitamodel->getSetorGrupo($idSetor, $course->cur_id);
                ?>
                <table class = "table table-bordered table-hover table-striped">
                    <tr>
                        <th>Colaborador</th>
                        <th>Horário</th>
                        <th>Local</th>
                        <th>Profissional</th>
                        <th></th>
                    </tr>
                    <?php foreach ($inscricoes->result() as $inscricao) { ?>
                        <tr>
                            <td><?php echo mb_strtoupper($inscricao->usu_nome); ?></td>
                            <td><?php echo $inscricao->cur_horario; ?></td>
                            <td><?php echo $inscricao->cur_sala; ?></td>
                            <td><?php echo $inscricao->cur_profissional; ?></td>
                            <td>
                                <a href="<?php echo site_url('adm/capacita/deleteAgendaAdm/' . $inscricao->age_id); ?>" class="hidden-print">
                                    <i class="glyphicon glyphicon-trash"></i>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
                <a href="<?php echo site_url('adm/capacita/agendaAdm/' . $course->cur_id); ?>" class="btn btn-primary hidden-print">
                    Incluir Colaborador
                </a>
            </div>
        </div>
        <hr>
    <?php 
        } 
    ?>
        
    </section>
</div>