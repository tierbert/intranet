<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            <?php echo $titulo; ?>
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/capacita/coord_setor'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px;">
    <section class="cards-section " style="padding: 0 0">
        <H3>Selecione o Horário Desejado</H3>
        <table class="table table-bordered table-hover">
            <tr>
                <th>Curso</th>
                <th>Turma</th>
                <th>Local</th>
                <th>Profissional</th>
                <th>Vagas</th>
                <th></th>
            </tr>
            <?php
            foreach ($cursos->result() as $curso) {
                $inscritos = $this->capacitamodel->getInscritos($curso->cur_id);
                ?>
                <tr>
                    <td><?php echo mb_strtoupper($curso->cur_curso); ?></td>
                    <td><?php echo mb_strtoupper($curso->cur_horario); ?></td>
                    <td><?php echo mb_strtoupper($curso->cur_sala); ?></td>
                    <td><?php echo mb_strtoupper($curso->cur_profissional); ?></td>
                    <td>
                        <?php echo ($curso->cur_vagas - $inscritos->num_rows()) . ' de ' . mb_strtoupper($curso->cur_vagas); ?>
                    </td>
                    <td>
                        <?php if ($inscritos->num_rows() >= $curso->cur_vagas) { ?>
                            Vagas Esgotadas
                        <?php } else { ?>
                            <a href="<?php echo site_url('adm/capacita/agendarCoord_setor/' . $curso->cur_id); ?>" class="btn btn-primary btn-xs">
                                Selecionar
                            </a>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </section>
</div>