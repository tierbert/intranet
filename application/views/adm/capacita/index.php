<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            <?php echo $titulo; ?>
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <section class="cards-section " style="padding: 0 0">

        <a href="<?php echo site_url('adm/capacita/adm'); ?>" class="btn btn-primary">
            Adm
        </a>
        <a href="<?php echo site_url('adm/capacita/coord_setor'); ?>" class="btn btn-primary">
            Coord Setor
        </a>
        <a href="<?php echo site_url('adm/capacita/coord_curso'); ?>" class="btn btn-primary">
            Coord Curso
        </a>
    </section>
</div>