<h2>Nova Turma</h2>
<hr>
<div class=form-horizontal>    
    <div>
        <?php echo validation_errors(); ?>
        <?php $grupos = ['teste']; ?>
    </div>
    <?php echo form_open() ?>
    
    <div class="col-md-8 col-xs-8">
        <div class="row">
            <div class="col-md-4 col-xs-4">
                <label for="cur_setor">Selecione o público alvo:</label>
                <select name="cur_setor" class="form-control " required="">
                    <option value="geral">Geral</option>
                    <option value="administrativo">Administrativo</option>
                </select>
            </div> 
            <input name="cur_idGrupo" class="form-control " type="hidden" value="<?php echo $classcourse; ?>">
            <div class="col-md-4 col-xs-4">
                <label for="cur_curso">Nome da Turma:</label>
                <input name="cur_curso" class="form-control " type="text">
            </div>        
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-4">
                <label for="cur_vagas">Quantidade de Vagas:</label>
                <input name="cur_vagas" class="form-control " type="number">
            </div> 
            <div class="col-md-4 col-xs-4">
                <label for="cur_sala">Sala:</label>                       
                <input name="cur_sala" class="form-control " type="text">    
            </div> 
            <div class="col-md-4 col-xs-4">
                <label for="cur_horario">Horário:</label>                       
                <input name="cur_horario" class="form-control " type="text">    
            </div>        
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-4">
                <label for="cur_profissional">Profissional:</label>                       
                <input name="cur_profissional" class="form-control " type="text">    
            </div> 
            <div class="col-md-4 col-xs-4">
                <label for="cur_ch">Carga Horária::</label>                       
                <input name="cur_ch" class="form-control " type="text">   
            </div> 
            <div class="col-md-4 col-xs-4">
                <br />
                 <input type=submit  value=Salvar class='btn btn-primary' style="">
            </div> 
        </div>
    </div>    

    <?php echo form_close() ?>
</div>
<link type="text/css" href="<?php echo base_url('assets'); ?>/js/select2/css/select2.css"
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/select2/js/select2.js"></script>
<script>
    $(function (){
       $('.select2').select2(); 
    });

</script>