<h2>Nova Turma</h2>
<hr>
<div class=form-horizontal>    
    <div>
        <?php echo validation_errors(); ?>
    </div>
    <?php echo form_open() ?>
    
    <div class="col-md-12 col-xs-12">
        <div class="row">
            <div class="col-md-4 col-xs-4">
                <label for="cur_type">Selecione o público alvo:</label>
                <select name="cur_type" class="form-control " required="">
                    <option value="geral">Geral</option>
                    <option value="administrativo">Administrativo</option>
                </select>
            </div> 
            <div class="col-md-4 col-xs-4">
                <label for="cur_name">Informe o nome do Curso:</label>
                <input name="cur_name" class="form-control " type="text" value="<?php echo $object->cur_name ?>">
            </div> 
            <div class="col-md-4 col-xs-4">
                <br />
                 <input type=submit  value=Salvar class='btn btn-primary'>
            </div>     
        </div>
    </div>    



    <?php echo form_close() ?>
</div>
<link type="text/css" href="<?php echo base_url('assets'); ?>/js/select2/css/select2.css"
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/select2/js/select2.js"></script>
<script>
    $(function (){
       $('.select2').select2(); 
    });

</script>