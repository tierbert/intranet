<h3 class='pull-left'><?php echo ucfirst($titulo); ?> - Listando: </h3>
<?php if (count($dados) != 0) { ?>
    <table class='table table-bordered table-striped table-condensed table-hover'>
        <thead>
            <tr>
                <th>Cod Curso</th>
                <th>cur_nome</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dados as $dado) {
                ?><tr>
                    <td><?php echo $dado->cur_id ?> </td>
                    <td><?php echo $dado->cur_nome ?> </td>
                    <td><a class='btn btn-xs btn-primary' href="<?php echo site_url('adm/cursos/editar/' . $dado->cur_id)
                ?>">Editar</a>
                        <a class='btn btn-xs btn-danger' href="<?php echo site_url($this->uri->segment(1) . '/delete/' . $dado->cur_id) ?>">Deletar</a>
                    </td>

                </tr>
            <?php } ?>

        </tbody>
    </table>
    <ul class='pagination pagination-small'>
        <li>
            <?php echo $paginacao; ?>
        </li>
    </ul>
<?php } else { ?>
    <h4>Nenhum registro encontrado</h4>
<?php } ?>

