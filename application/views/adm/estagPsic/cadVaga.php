<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            <?php echo ucfirst($titulo); ?> 
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/EstagioPsicAdm/index/1'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class="row">
        <div class="form-horizontal">
            <?php echo form_open(); ?>
            <div class="form-group">

                <div class="col-md-3">
                    <label>PERÍODO</label>
                    <input value="<?php echo $dados->ev_periodo; ?>" type="text" name="ev_periodo" class="form-control" required="true">
                    (EX. 2ª Feira - Matutino)
                </div>
                <div class="col-md-2">
                    <label>VAGAS</label>
                    <input value="<?php echo $dados->ev_vagas; ?>" type="text" name="ev_vagas" class="form-control" required="true">
                </div>
                <div class="col-md-2">
                    <label>TPO</label>
                    <select name="ev_tipo" class="form-control" required="true">
                        <?php if ($dados->ev_tipo != '') { ?>
                            <option value="<?php echo $dados->ev_tipo; ?>"><?php echo $dados->ev_tipo; ?></option>
                        <?php } ?>
                        <!--<option value="">SELECIONE O TIPO</option>-->
                        <option value="campo">CAMPO / ORIENTAÇÃO</option>
                        <!--<option value="orientacao">ORIENTAÇÃO</option>-->
                    </select>
                </div>

            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <input value="<?php echo $idLocal; ?>" name="ev_idLocal" type="hidden" >
                    <input value="CADASTRAR" type="submit" class="form-control btn btn-primary">
                </div>
            </div>
            <?php form_close() ?>

        </div>
    </div>
</div>