<link href="../../assets/css/css/metisMenu.min.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/css/sb-admin-2.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/css/css/morris.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/css/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/css/css/claro.css" media="screen" rel="stylesheet" type="text/css">
<link href="../../assets/css/css/BusyButton.css" media="screen" rel="stylesheet" type="text/css">
<link href="../../assets/css/css/cucco.css" media="screen" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--Css calendario-->
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
<section id="IN"  >
    <div class=row>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="container">
                                <div class="row">
                                    <div id="overview" class="col-sm-12 col-md-12 animated bounceInUp">
                                        <h2><span style="border-left: 4px solid#13B3D2; font-size: 35px;">Módulo: <small>ESTÁGIO PSICOLOGIA</small></span></h2>
                                        <br>
                                        <h5>
                                            Acontece em dois dias na semana: 01 dia no campo de 04 horas + 01 dia de orientação acadêmica na FG de 04 horas;
                                        </h5>
                                        <p></p>

                                    </div>
                                    <!-- End Column -->
                                    <div id="calendar" class="col-sm-4 col-md-3 animated animated-sm bounceInUp">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- /#list -->
                </div> <!-- /#main -->
            </div>
        </div>
    </div>
</section>
<section id="listarsolicalu" class="home-section text-center" >
    <div class="">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading tour-step tour-step-7">
                            <h3 class="panel-title" style="font-weight: bold">
                                <br>
                                SELEÇÃO DOS CAMPOS DE PSICOLOGIA ESPECÍFICO I E II – 2017.2
                                <br>
                                &nbsp;
                            </h3>
                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-12">

                                    <table class="table table tour-step tour-step-8" style="color: black" cellspacing="0" cellpadding="0">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Tipo</th>
                                                <th class="text-center">Local</th>
                                                <th class="text-center">Dia / Período</th>

                                                <th class="actions"></th>
                                            </tr>
                                        </thead>
                                        <tbody >
                                            <tr class=" ">
                                                <?php
                                                $dados1 = $this->estag__agendadosmodel->getByAlunoTipo($idAluno, 'campo');
                                                $agendamento1 = $dados1->row();
                                                if ($agendamento1) {
                                                    ?>
                                                    <td>Campo</td>
                                                    <td><?php echo $agendamento1->el_nome; ?></td>
                                                    <td><?php echo $agendamento1->ev_periodo; ?></td>
                                                    <td> 
                                                     
                                                    </td>
                                                <?php } else { ?>
                                                    <td>Campo</td>
                                                    <td></td><td></td>
                                                    <td>
                                                        <a href="<?php echo site_url('aluno/EstagioPsic/seleciona/1/1'); ?>" class="btn btn-primary btn-xs">  
                                                            SELECIONAR ESPECÍFICO I 
                                                        </a>
                                                        <a href="<?php echo site_url('aluno/EstagioPsic/seleciona/1/2'); ?>" class="btn btn-primary btn-xs">  
                                                            SELECIONAR ESPECÍFICO II
                                                        </a>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <tr >
                                                <?php
                                                $dados2 = $this->estag__agendadosmodel->getByAlunoTipo($idAluno, 'orientacao');
                                                $agendamento2 = $dados2->row();
                                                if ($agendamento2) {
                                                    ?>
                                                    <td>Orientação acadêmica</td>
                                                    <td><?php echo $agendamento2->el_nome; ?></td>
                                                    <td><?php echo $agendamento2->ev_periodo; ?></td>
                                                    <td> 
                                                      
                                                    </td>
                                                <?php } else { ?>
                                                    <td>Orientação acadêmica</td>
                                                    <td></td><td></td>
                                                    <td>
                                                        <a href="<?php echo site_url('aluno/EstagioPsic/seleciona/2/1'); ?>" class="btn btn-info btn-xs">  
                                                            SELECIONAR ESPECÍFICO I 
                                                        </a>
                                                        <a href="<?php echo site_url('aluno/EstagioPsic/seleciona/2/2'); ?>" class="btn btn-info btn-xs">  
                                                            SELECIONAR ESPECÍFICO II
                                                        </a>
                                                    </td>
                                                <?php } ?>

                                            </tr>


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- /#list -->
                    </div> <!-- /#main -->
                </div>
            </div>
        </div>    
    </div>
</section>
<style>
    .table-striped>tbody>tr:nth-child(odd)>td, 
    .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #67b168;
    }

    .table-striped2>tbody>tr:nth-child(odd)>td, 
    .table-striped2>tbody>tr:nth-child(odd)>th {
        background-color: #176e61
    }

</style>