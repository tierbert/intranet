<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css" integrity="sha384-PDle/QlgIONtM1aqA2Qemk5gPOE7wFq8+Em+G/hmo5Iq0CCmYZLv3fVRDJ4MMwEA" crossorigin="anonymous">


    </head>
    <body>

        <?php
        if (isset($_GET['ra_aluno'])) {

            $ra_aluno = $_GET['ra_aluno'];
            $image_link = "";

            $data = array(
                'token' => '5e0d3b28bd55aa5bbb1d35ff89a08b93',
                'format' => 'json',
                // 'tipo' => 'COMPLETA',
                'filtrarPor' => 'RA',
                'termo' => $ra_aluno
            );
            $URI = "http://guanambi.jacad.com.br:80/academico/api/v1/academico/aluno/";

            $ch = curl_init($URI);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_URL, $URI);

            $result = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($result, true);

            $image_link = "http://guanambi.jacad.com.br/academico/images/perfil/" . $result[0]["idPerfil"] . "/640";

            // foreach ($result as $key) {
            // 	echo $key['ra'] . " - " . $key['nome'] . " - " . $key['foto'] . " - " . $key['login'] . " - " . $key['senha'] . "<br/>";
            // }
        }
        ?>
        <div class="container">

            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <form class="form_aluno form-inline" style="padding: 20px 0px 0px 0px;">
                        <div class="form-group mx-sm-3 mb-2" style="margin-left: 0px !important;width: 80%">
                            <input type="text" class="form-control" style="width: 100%" id="ra_aluno" name="ra_aluno" placeholder="Informe o RA do aluno" autofocus="on" autocomplete="off">
                        </div>
                        <button type="submit" class="btn btn-primary mb-2">Consultar</button>
                    </form>
                </div>
                <div class="col-md-8 offset-md-2">
                    <span style="font-size: .7em">RAs para teste: 1510450131, 2019119244053, 1710380009, 2019118643958, 2019119244090</span>
                    <hr>
                    <?php
                        if (isset($_GET['ra_aluno'])) {
                    ?>
                            <p>
                                <b>Nome: </b><?php echo $result[0]['nome'] ?> <br> 
                                <b>RA: </b><?php echo $result[0]['ra'] ?>
                            </p>
                            <p style="text-align: center;">
                                <img src="<?php echo $image_link; ?>">	
                            </p>
                    <?php } ?>
                </div>
            </div>

        </div>
    </body>
</html>