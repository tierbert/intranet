<?php
if ($semana == '') {
    $semana = date('W') - 1;
    $day = date('w');
    $domingo = date('m-d-Y', strtotime('-' . $day . ' days'));
    $datas = array(
        '1' => date('d-m-Y', strtotime('+' . (1 - $day) . ' days')), //segunda
        '2' => date('d-m-Y', strtotime('+' . (2 - $day) . ' days')), // terca
        '3' => date('d-m-Y', strtotime('+' . (3 - $day) . ' days')), // quarta
        '4' => date('d-m-Y', strtotime('+' . (4 - $day) . ' days')), // quinta
        '5' => date('d-m-Y', strtotime('+' . (5 - $day) . ' days')), //sexta
        '6' => date('d-m-Y', strtotime('+' . (6 - $day) . ' days')), //sabado
    );
} else {

    $dias = ($semana * 7) - 1;
    $diaDomingo = date('Y-m-d', strtotime('2018-01-01' . " + $dias  days"));

    $day = date('w', strtotime($diaDomingo));
    $domingo = date('m-d-Y', strtotime('-' . $day . ' days'));
    $datas = array(
        '1' => date('d-m-Y', strtotime($diaDomingo . '+' . (1 - $day) . ' days')), //segunda
        '2' => date('d-m-Y', strtotime($diaDomingo . '+' . (2 - $day) . ' days')), // terca
        '3' => date('d-m-Y', strtotime($diaDomingo . '+' . (3 - $day) . ' days')), // quarta
        '4' => date('d-m-Y', strtotime($diaDomingo . '+' . (4 - $day) . ' days')), // quinta
        '5' => date('d-m-Y', strtotime($diaDomingo . '+' . (5 - $day) . ' days')), //sexta
        '6' => date('d-m-Y', strtotime($diaDomingo . '+' . (6 - $day) . ' days')), //sabado
    );
}

$volta = '';
$avanca = '';
$semanaAtual = date('W') - 1;
if ($semana == $semanaAtual) {
    //$volta = 'disabled';
} elseif ($semana > $semanaAtual + 1) {
    //$avanca = 'disabled';
}
if ($semana > $semanaAtual + 2) {
    //echo "<script>;window.location.href='" . site_url() . "/aluno/tcc/agendarTcc'</script>";
}
?>
<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            TCC HORÁRIOS 
        </div>
        <div class="col-md-2">
            <a href="<?php echo site_url('adm/tcc/tccAgendados/index/' . ($semana - 1)); ?>" class=" btn btn-primary <?php echo $volta; ?>">
                <span class="glyphicon glyphicon-backward"></span>
            </a>
            <a href="<?php echo site_url('adm/tcc/tccAgendados/index/'); ?>" class=" btn btn-primary ">
                <span class="glyphicon glyphicon-home"></span>
            </a>
            <a href="<?php echo site_url('adm/tcc/tccAgendados/index/' . ($semana + 1)); ?>" class=" btn btn-primary <?php echo $avanca; ?>">
                <span class="glyphicon glyphicon-forward"></span>
            </a>
        </div>
        <div class="col-md-4 text-right">

            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">

    <table class="table table-striped table-bordered table-hover">
        <tr>
            <th></th>
            <th>SEGUNDA - <?php echo $datas['1']; ?></th>
            <th>TERÇA - <?php echo $datas['2']; ?></th>
            <th>QUARTA - <?php echo $datas['3']; ?></th>
            <th>QUINTA - <?php echo $datas['4']; ?></th>
            <th>SEXTA - <?php echo $datas['5']; ?></th>
        </tr>
        <?php
        foreach ($turnos->result() as $turno) {
            ?>
            <tr>
                <td>
                    <?php echo $turno->tur_turno . '<br>' . $turno->tur_horario; ?>
                </td>
                <?php
                for ($dia = 1; $dia < 6; $dia++) {
                    $horario = $this->tcch->getHorario($turno->tur_id, $dia);
                    ?>
                    <td>
                        <?php //echo $horario->num_rows(); ?>
                        <?php
                        if ($horario->num_rows() > 0) {

                            $dados = $horario->row();
                            echo 'PROF: ' . $dados->tcp_nome;
                            ?>
                            <a href="<?php echo site_url('adm/tcc/tccAgendados/horDel/' . $dados->hor_id); ?>"  class="btn btn-danger btn-xs pull-right" style="font-size: 12px;">
                                x
                            </a>
                            <hr style="margin: 5px 0 5px 0;">

                            <?php
                            $agenda = $this->tcch->getByHorarioData($dados->hor_id, $this->util->data($datas[$dia]) . ' 00:00:00');
                            if ($agenda->num_rows() > 0) {
                                if ($agenda->row('tca_idAluno') == 0) {
                                    echo '<span style="color: blue">RESERVADO PELO PROF.</span>';
                                    ?>
                                    <a href = "<?php echo site_url('adm/tcc/tccAgendados/reservDel/' . $agenda->row('tca_id')); ?>" class = "btn btn-danger btn-xs pull-right" style = "font-size: 12px;">
                                        x
                                    </a>
                                    <?php
                                } else {
                                    echo 'ALUNO: <span style="color: red">' . $agenda->row('alu_nome') . '</span>';
                                    ?>
                                    <a href = "<?php echo site_url('adm/tcc/tccAgendados/reservDel/' . $agenda->row('tca_id')); ?>" class = "btn btn-danger btn-xs pull-right" style = "font-size: 12px;">
                                        x
                                    </a>
                                    <?php
                                }
                            } else {
                                echo '<span style="color: green">HORÁRIO LIVRE</span>';
                            }
                            ?>
                            <hr style="margin: 5px 0 5px 0;">

                            <?php
                        } else {
                            ?>
                            <a href="<?php echo site_url('adm/tcc/tccAgendados/horCad/' . $turno->tur_id . '/' . $dia); ?>" class="btn btn-primary btn-xs" style="font-size: 12px;">
                                +
                            </a>
                        <?php } ?>

                    </td>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>
</div>




<style>
    /*
    .table-striped>tbody>tr:nth-child(odd)>td, 
    .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #337ab7;
    }
    .table-striped2>tbody>tr:nth-child(odd)>td, 
    .table-striped2>tbody>tr:nth-child(odd)>th {
        background-color: #337ab7;
    }
    */

</style>

