<?php $this->load->view('adm/agendamento/agend_botoes'); ?>
<h3 class='pull-left'>Horários</h3>
<a class='btn btn-primary pull-right' href='<?php echo site_url('adm/agendamento/agend_horario/criar/') ?>'>
    NOVO HORARIO
</a>
<?php if (count($dados) != 0) { ?>
    <table class='table table-bordered table-striped table-condensed table-hover'>
        <thead>
            <tr>
                <th>Data</th>
                <th>Horário</th>
                <th>Quantidade Vagas</th>
                <!--<th>Professor</th>  -->
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dados as $dado) {
                ?><tr><td><?php echo $dado->ahh_data ?> </td>
                    <td><?php echo $dado->ahh_turno ?> </td>
                    <td><?php echo $dado->ahh_vagas ?> </td>
                    <!--<td><?php echo $dado->ahh_professor ?> </td>  -->
                    <td>
                        <a class='btn btn-xs btn-primary' href="<?php echo site_url('adm/agendamento/agend_horario/editar/' . $dado->ahh_id) ?>">Editar</a>
                        <a class='btn btn-xs btn-danger' href="<?php echo site_url('adm/agendamento/agend_horario/delete/' . $dado->ahh_id) ?>">Deletar</a>
                    </td>

                </tr>
            <?php } ?>

        </tbody>
    </table>
    <ul class='pagination pagination-small'>
        <li>
            <?php echo $paginacao; ?>
        </li>
    </ul>
<?php } else { ?>
    <h4>Nenhum registro encontrado</h4>
<?php } ?>

