<link href="<?php echo base_url(); ?>assets/css/sb-admin-2.css" rel="stylesheet" type="text/css"/>

<div class="row">
    <div class="col-lg-2 col-md-6" style="width: 200px;">
        <a href="<?php echo site_url('adm/agendamento/agend_agendados'); ?>">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="glyphicon glyphicon-align-justify fa-3x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php //echo $totais['solicitacoes']; ?></div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <span class="pull-left">AGENDADOS</span>
                    <span class="pull-right">
                        <i class="glyphicon glyphicon-dashboard"></i></span>
                    <div class="clearfix"></div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-lg-2 col-md-6" style="width: 200px;">
        <a href="<?php echo site_url('adm/agendamento/agend_horario'); ?>">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="glyphicon glyphicon-user fa-3x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php //echo $totais['alunos']; ?></div>
                        </div>
                    </div>
                </div>

                <div class="panel-footer">
                    <span class="pull-left">HORÁRIOS</span>
                    <span class="pull-right">
                        <i class="glyphicon glyphicon-apple"></i>
                    </span>
                    <div class="clearfix"></div>
                </div>

            </div>
        </a>
    </div>

    <!--
    <div class="col-lg-2 col-md-6" style="width: 200px;">
        <a href="<?php echo site_url('adm/agendamento/Agend_inicio/senhas/1'); ?>">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="glyphicon glyphicon-th fa-3x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php //echo $totais['horario']; ?></div>
                        </div>
                    </div>
                </div>

                <div class="panel-footer">
                    <span class="pull-left">SENHAS FINANC</span>
                    <span class="pull-right"><i class="glyphicon glyphicon-align-center"></i></span>
                    <div class="clearfix"></div>
                </div>

            </div>
        </a>
    </div>


      <div class="col-lg-2 col-md-6" style="width: 200px;">
        <a href="<?php echo site_url('adm/agendamento/Agend_inicio/senhas/2'); ?>">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="glyphicon glyphicon-th fa-3x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php //echo $totais['horario']; ?></div>
                        </div>
                    </div>
                </div>

                <div class="panel-footer">
                    <span class="pull-left">SENHAS GERENC</span>
                    <span class="pull-right"><i class="glyphicon glyphicon-align-center"></i></span>
                    <div class="clearfix"></div>
                </div>

            </div>
        </a>
    </div>
    -->
    
</div>