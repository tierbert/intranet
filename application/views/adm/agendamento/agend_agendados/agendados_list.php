<?php $this->load->view('adm/agendamento/agend_botoes') ?>

<h3 class='pull-left'><?php echo ucfirst($titulo); ?></h3>
<br><br><br>
<div class="row">
    <div class="col-md-8">
        <div class="form hidden-print" style="margin-left: 15px;">
            <?php echo form_open(); ?>
            <div class="form-group">
                <div class="col-md-3" style="padding: 0">
                    <label>Data:</label>
                    <input name="data" value="<?php if (isset($post) && $post['data'] != '') echo $post['data']; ?>" type="text" class="form-control data" >
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6" style="padding: 0; padding-left: 5px;">
                    <label>Aluno:</label>
                    <input name="nome" value="<?php if (isset($post) && $post['nome'] != '') echo $post['nome']; ?>" type="text" class="form-control" >
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-3" style="margin-top: 5px;">
                    &nbsp;
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-primary">Pesquisar</button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>

        </div>
    </div>
    <div class="col-md-4">
        <h4 style="margin: 0">Totais</h4>
        <hr style="margin-top: 5px; margin-bottom: 5px;">
        <?php
        ?>
        Pendentes:  <?php
        //echo $pend;
        ?>
        &nbsp;&nbsp;&nbsp;
        Pagas: <?php //echo $pag          ?>
        &nbsp;&nbsp;&nbsp;
        Canceladas: <?php //echo $canc;          ?>
        <br>
        Total:  <?php //echo $pag + $canc + $pend;          ?>
    </div>
</div>
<hr style="margin: 0;">

<?php if (count($dados) != 0) { ?>
    <table class='table table-bordered table-striped table-condensed table-hover'>
        <thead>
            <tr>
                <th>ALUNO</th>
                <th>DATA</th>
                <th>HORÁRIO</th>
                <th>PROFESSOR</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dados as $dado) {
                ?><tr class="<?php if ($dado->aha_conferido) echo 'success'; ?>">
                    <td><?php echo $dado->alu_nome ?> </td>
                    <td><?php echo $dado->ahh_data ?> </td>
                    <td><?php echo $dado->ahh_turno ?> </td>
                    <td><?php echo $dado->ahh_professor ?> </td>
                    <td>
                        <a class='btn btn-xs btn-warning' href="<?php echo site_url('adm/agendamento/agend_agendados/conferido/' . $dado->aha_id) ?>">
                            Conferir
                        </a>
                        <a class='btn btn-xs btn-danger' href="<?php echo site_url('adm/agendamento/agend_agendados/delete/' . $dado->aha_id) ?>">
                            Cancelar
                        </a>
                    </td>
                </tr>
            <?php } ?>

        </tbody>
    </table>
    <ul class='pagination pagination-small'>
        <li>
            <?php echo $paginacao; ?>
        </li>
    </ul>
<?php } else { ?>
    <h4>Nenhum registro encontrado</h4>
<?php } ?>

<?php $this->util->mascaras() ?>