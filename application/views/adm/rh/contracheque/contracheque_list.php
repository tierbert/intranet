<?php $this->load->view('adm/rh/contracheque/botoes'); ?>
<div class="row">
    <div class="col-md-8">
        <h4 class='pull-left'>Hollerichs Anexadas </h4>
        <br><br>
        <div class="form hidden-print" style="margin-left: 15px;">

            <?php echo form_open(); ?>
            <div class="form-group">
                <div class="col-md-3" style="padding: 0">
                    <label>Nº de Solic.:</label>
                    <input name="id" value="<?php if (isset($post) && $post['id'] != '') echo $post['id']; ?>" type="text" class="form-control" >
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6" style="padding: 0; padding-left: 5px;">
                    <label>Aluno:</label>
                    <input name="nome" value="<?php if (isset($post) && $post['nome'] != '') echo $post['nome']; ?>" type="text" class="form-control" >
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-3" style="margin-top: 5px;">
                    &nbsp;
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-primary">Pesquisar</button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>

        </div>
    </div>
    <div class="col-md-4">

    </div>
</div>
<hr style="margin: 0;">

<div class="row">
    <?php if (count($dados) != 0) { ?>
        <table class='table table-bordered table-striped table-condensed table-hover'>
            <thead>
                <tr>
                    <th>rhc_registroRh</th>
                    <th>rhc_mes</th>
                    <th>rhc_ano</th>
                    <th>rhc_arquivo</th>
                    <th>rhc_observacao</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($dados as $dado) {
                    ?><tr><td><?php echo $dado->rhc_registroRh ?> </td>
                        <td><?php echo $dado->rhc_mes ?> </td>
                        <td><?php echo $dado->rhc_ano ?> </td>
                        <td><?php echo $dado->rhc_arquivo ?> </td>
                        <td><?php echo $dado->rhc_observacao ?> </td>
                        <td><a class='btn btn-sm btn-primary' href="<?php echo site_url($this->uri->segment(1) . '/editar/' . $dado->rhc_id)
                    ?>">Editar</a>
                            <a class='btn btn-sm btn-danger' href="<?php echo site_url($this->uri->segment(1) . '/delete/' . $dado->rhc_id) ?>">Deletar</a>
                        </td>

                    </tr>
                <?php } ?>

            </tbody>
        </table>
        <ul class='pagination pagination-small'>
            <li>
                <?php echo $paginacao; ?>
            </li>
        </ul>
    <?php } else { ?>
        <h4>Nenhum registro encontrado</h4>
    <?php } ?>
</div>
