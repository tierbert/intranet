<link href="<?php echo base_url(); ?>assets/css/css/metisMenu.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/css/sb-admin-2.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/css/css/morris.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/css/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<div class="row">
    <div class="col-lg-2 col-md-6" style="width: 200px;">
        <a href="<?php echo site_url('adm/rh/contracheque/index'); ?>">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="glyphicon glyphicon-new-window fa-3x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php //echo $totais['solicitacoes'];  ?></div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <span class="pull-left">Hollerichs</span>
                    <span class="pull-right">
                        <i class="glyphicon glyphicon-dashboard"></i></span>
                    <div class="clearfix"></div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-lg-2 col-md-6" style="width: 200px;">
        <a href="<?php echo site_url('adm/rh/contracheque/mes'); ?>">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="glyphicon glyphicon-align-justify fa-3x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php //echo $totais['alunos'];  ?></div>
                        </div>
                    </div>
                </div>

                <div class="panel-footer">
                    <span class="pull-left">Hollerichs Por Mes</span>
                    <span class="pull-right">
                        <i class="glyphicon glyphicon-apple"></i>
                    </span>
                    <div class="clearfix"></div>
                </div>

            </div>
        </a>
    </div>


    
</div>