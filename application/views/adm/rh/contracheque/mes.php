<?php $this->load->view('adm/rh/contracheque/botoes'); ?>
<div class="row">
    <div class="col-md-2">
        <br>
        <h4 >Anexos </h4>
    </div>
    <div class="col-md-8">
        <div class="form hidden-print" style="margin-left: 15px;">
            <?php
            echo form_open();
            ?>
            <div class="form-group">
                <div class="col-md-1" style="padding: 0; padding-right: 5px;">
                    <label>Mês:</label>
                    <input name="mes" value="<?php echo $mes; ?>" type="text" class="form-control" >
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-1" style="padding: 0; padding-right: 5px;">
                    <label>Ano:</label>
                    <input name="ano" value="<?php echo $ano; ?>" type="text" class="form-control" >
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3" style="padding: 0">
                    <label>Tipo:</label>
                    <select class="form-control" name="tipo">
                        <option value="professores">Professores</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3" style="margin-top: 5px;">
                    &nbsp;
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-primary">Pesquisar</button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<hr style="margin: 0;">

<div class="row">
    <?php if (isset($professores) && count($professores) != 0) { ?>
        <table class='table table-bordered table-striped table-condensed table-hover'>
            <thead>
                <tr>
                    <th>Registro</th>
                    <th>Nome</th>
                    <th>Mês</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($professores->result() as $professor) {
                    $contracheque = $this->rh_contrachequemodel->getByFunCompetencia($professor->pro_registroRh, $mes, $ano);
                    ?><tr>
                        <td><?php echo $professor->pro_registroRh ?> </td>
                        <td><?php echo $professor->pro_nome ?> </td>
                        <td>

                            <?php echo $mes . '/' . $ano ?> 
                        </td>
                        <td>
                            <?php if ($contracheque->row('rhc_arquivo') != '') { ?>
                                <?php
                                $attr = array('width' => '1000', 'height' => '500', 'class' => 'btn btn-primary btn-xs', 'type' => "button");
                                echo anchor_popup('adm/rh/contracheque/download/' . $contracheque->row('rhc_id'), "<i class='glyphicon glyphicon-file'></i>", $attr)
                                ?>
                            <?php } ?>
                        </td>
                        <td>
                            <?php if ($contracheque->row('rhc_arquivo') == '') { ?>
                                <form action="<?php echo site_url('adm/rh/contracheque/upload'); ?>" class="form-inline" enctype="multipart/form-data" method="post"> 

                                    <input name="reg" value="<?php echo $professor->pro_registroRh; ?>" type="hidden">
                                    <input name="mes" value="<?php echo $mes; ?>" type="hidden">
                                    <input name="ano" value="<?php echo $ano; ?>" type="hidden">

                                    <input type="file" name="arquivo" required class="form-control input-sm" style="width: 200px;">
                                    <input type="submit" name="upload" class="form-control input-sm btn-primary">
                                </form>
                            <?php } else { ?>

                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php } else { ?>
        <h4>Click em pesquisar</h4>
    <?php } ?>
</div>
