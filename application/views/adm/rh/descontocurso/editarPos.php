<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            SOLICITAÇÃO DESCONTO CURSO DE PÓS
        </div>
        <div class="col-md-6 col-xs-6 text-right">
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal> 
        <?php echo form_open(); ?>
        <fieldset>
            <div>
                <?php echo validation_errors(); ?>
            </div>
            <?php echo form_open() ?>

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <?php $this->util->input2('TIPO DE CURSO', '', $object->dc_tipoCurso, '', '4', '', 'disabled'); ?>
                        <?php $this->util->input2('BENEFICIÁRIO', '', $object->dc_beneficiario, '', '4', '', 'disabled'); ?>
                        <?php $this->util->input2('NOME DO FAMILIAR', '', $object->dc_familiarNome, '', '4', '', 'disabled'); ?>
                    </div>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <?php $this->util->input2('PARENTESCO DO FAMILIAR', '', $object->dc_familiarParentesco, '', 4, '', 'disabled'); ?>
                        <?php $this->util->input2('CURSO', '', $object->dc_curso, '', 5, '', 'disabled'); ?>
                        <?php $this->util->input2('SEMESTRE', '', $object->dc_semestre, '', 3, '', 'disabled'); ?>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <?php $this->util->input2('OBSERVAÇÕES', '', $object->dc_observacoes, '', 12, '', 'disabled'); ?>                                     
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <h4 style="text-align: center;">PARECER RH</h4>
                    <div class="form-group">
                        <?php $this->util->input2('TEMPO DE SERVIÇO', '', $object->dc_rhTempoServico, '', 3, '', 'disabled'); ?>
                        <?php $this->util->select2('PARECER', '', array('' => '', 'DEFERIDO' => 'DEFERIDO', 'INDEFERIDO' => 'INDEFERIDO'), $object->dc_rhParecer, '', 3, '', 'disabled'); ?>                                   
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <h4 style="text-align: center;">PARECER COORDENAÇÃO</h4>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <?php $this->util->select2('PARECER', 'dc_gerParecer', array('' => '', 'DEFERIDO' => 'DEFERIDO', 'INDEFERIDO' => 'INDEFERIDO'), $object->dc_gerParecer, 'parecer', 12); ?>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group hidden exibe">
                            <?php $this->util->select2('PERCENTUAL', 'dc_gerPercentual', array('' => '', '0' => '0', '25%' => '25%', '50%' => '50%', '75%' => '75%', '100%' => '100%'), $object->dc_gerPercentual, '', 12); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class=form-group>
                <input type=submit  value=Salvar class='btn btn-success pull-right'>
            </div>            
            <?php echo form_close() ?>
          
    </div>
</div>
<script>
    $(function () {

        /* FUNÇÃO PARA MOSNTRAR O RESTO DO FORMULARIO CASO SEJA DEFERIDO - ATENTAR PARA OS NOMES DAS CLASSES NO FORMULÁRIO*/
        $('.parecer').click(function () {

            if ($('.parecer').val() == 'DEFERIDO') {
                $('.exibe').removeClass('hidden');
            } else {
                $('.exibe').addClass('hidden');
            }

        });


    });//
</script>