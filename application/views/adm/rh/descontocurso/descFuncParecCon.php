<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            HISTÓRICO DO PROTOCOLO
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a  href='<?php echo site_url('adm/rh/descontoCurso/descFuncList') ?>' class=" btn btn-danger">
                Início
            </a> 
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal> 
        <div class="col-sm-4">          
            <div class="alert alert-dismissible alert-success">
                <h5> PARECER RH: </h5>
                <?php if (count($object) != 0) { ?>       
                    <?php
                    if ($object->dc_rhParecer == null) {
                        echo "AGUARDANDO PARECER<br>";
                    } else {
                        ?>
                        <?php echo $object->dc_rhParecer ?> <br>

                    <?php } ?>
                    <?php ?>
                <?php }
                ?>
            </div>
        </div>

        <div class="col-sm-4">
               <div class="alert alert-dismissible alert-info">
                <h5> PARECER COORDENAÇÃO: </h5>
                <?php if (count($object) != 0) { ?>       
                    <?php
                    if ($object->dc_gerParecer == null) {
                        echo "AGUARDANDO PARECER<br>";
                    } else {
                        ?>
                        <?php echo $object->dc_gerParecer ?> <br> <br>
                        <h5> PERCENTUAL DESCONTO:</h5>
                        <?php echo $object->dc_gerPercentual ?>
                    <?php } ?>
                    <?php ?>
                <?php }
                ?>
            </div>
        </div>
 
        <div class="col-sm-4"> 

        </div> 

    </div>
</div>


