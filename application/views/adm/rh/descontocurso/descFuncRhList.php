<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            SOLICITAÇÕES DE DESCONTO EM CURSOS
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/rh/Iniciorh'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal> 
        <?php if ($dados->num_rows() > 0) { ?>
            <div class="col-sm-2"><b>TIPO DE CURSO</div>
            <div class="col-sm-2">BENEFICIÁRIO</div>
            <div class="col-sm-3">CURSO</div>
            <div class="col-sm-2">PARECER RH</div>
            <div class="col-sm-1">PERCENTUAL DESCONTO</div>
            <div class="col-sm-2">OPÇÕES</b></div>
            <br>
            <br>
            <hr>
            <?php foreach ($dados->result() as $dado) { ?> 
                <table class='table table-bordered table-striped table-condensed table-hover table-responsive tabela-aredondada'> 

                    <div class="col-sm-2"><?php echo $dado->dc_tipoCurso ?> </div>
                    <div class="col-sm-2"><?php echo $dado->dc_beneficiario ?> </div>
                    <div class="col-sm-3"><?php echo $dado->dc_curso ?> </div>
                    <div class="col-sm-2"><?php echo $dado->dc_rhParecer ?> </div>
                    <div class="col-sm-1"><?php echo $dado->dc_gerPercentual ?> </div>

                    <div class="col-sm-2"><a class='btn btn7 btn-danger' href="<?php echo site_url('adm/rh/descontoCurso/editarRh/' . $dado->dc_id) ?>">
                            INFO
                        </a>  </div> <br><br>
                </table>
            <?php } ?>
        </div>
    <?php } else { ?>
        <h4 style="text-align: center;">NENHUM REGISTRO ENCONTRADO</h4>
    <?php } ?>
</div>
