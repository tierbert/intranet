<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            ANÁLISE DESCONTO CURSO
        </div>
        <div class="col-md-6 col-xs-6 text-right">
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal> 
        <?php echo form_open(); ?>
        <fieldset>
            <div>
                <?php echo validation_errors(); ?>
            </div>
            <?php echo form_open() ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <?php $this->util->input2('COLABORADOR', '', $object->usu_nome, '', 8, '', 'disabled'); ?>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <?php $this->util->input2('TIPO DE CURSO', '', $object->dc_tipoCurso, '', 4, '', 'disabled'); ?>
                        <?php $this->util->input2('CURSO', '', $object->dc_curso, '', 4, '', 'disabled'); ?>
                        <?php $this->util->input2('SEMESTRE', '', $object->dc_semestre, '', 2, '', 'disabled'); ?>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12">              
                    <div class="form-group">
                        <?php $this->util->input2('BENEFICIÁRIO', '', $object->dc_beneficiario, '', 2, '', 'disabled'); ?>
                        <?php $this->util->input2('NOME DO FAMILIAR', '', $object->dc_familiarNome, '', 4, '', 'disabled'); ?>
                        <?php $this->util->input2('PARENTESCO DO FAMILIAR', '', $object->dc_familiarParentesco, '', 3, '', 'disabled'); ?>
                        <?php $this->util->input2('OBSERVAÇÕES', '', $object->dc_observacoes, '', 3, '', 'disabled'); ?>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12">              
                    <div class="form-group">
                        <?php $this->util->input2('TEMPO DO SERVIÇO', 'dc_rhTempoServico', $object->dc_rhTempoServico); ?>
                        <?php $this->util->select2('PARECER', 'dc_rhParecer', array('' => '', 'DEFERIDO' => 'DEFERIDO', 'INDEFERIDO' => 'INDEFERIDO'), $object->dc_rhParecer, '', 3); ?>
                        <?php $this->util->select2('PERCENTUAL', 'dc_gerPercentual', array('' => '', '0' => '0', '25%' => '25%', '50%' => '50%', '75%' => '75%', '100%' => '100%'), $object->dc_gerPercentual, '', 2); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                &nbsp;
                <div class="form-group">
                    <button type="submit" class="btn btn-success" value="Salvar">Salvar</button>
                </div>
            </div>
            <?php echo form_close() ?>
    </div>
    <?php echo $this->util->mascaras(); ?>
</div>