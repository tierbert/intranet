<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            MINHAS SOLICITAÇÕES DE DESCONTO
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href='<?php echo site_url('adm/rh/descontoCurso/descFuncParecRh/' . $object->dc_id) ?>' class=" btn btn-primary">
                Voltar
            </a> 
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal> 
        <h4 style="text-align: center;">PARECER COORDENAÇÃO</h4> <br>
        <?php if (count($object) != 0) { ?>           
            <?php // echo $object->usu_nome?>

            <div class="row">
                <b> STATUS DA SOLICITAÇÃO:</b><br>
                <?php
                if ($object->dc_gerParecer == null) {
                    ?> <div class=" col-md-3 col-xs-3 col-sm-3 alert alert-warning"> 
                    <?php echo "AGUARDANDO PARECER<br>"; ?>
                    </div> <?php
                } else {
                    ?>
                    <?php if ($object->dc_gerParecer == 'DEFERIDO') {
                        ?> <div class=" col-md-3 col-xs-3 col-sm-3 alert alert-success"> 
                            <?php echo $object->dc_gerParecer ?><br>
                        </div> 
                    <?php } ?>
                    <?php if ($object->dc_gerParecer == 'INDEFERIDO') {
                        ?> <div class=" col-md-3 col-xs-3 col-sm-3 alert alert-danger"> 
                            <?php echo $object->dc_gerParecer ?><br>
                        </div> 
                    <?php } ?>
                </div>
                <div class="row">
                    <b> DESCONTO: <br></b>
                    <p><?php echo $object->dc_gerPercentual ?> </p> 
                </div>
            <?php } ?>
        <?php } ?>
        <?php ?>
        <?php //} 
        ?>          
        <?php if ($object->dc_gerParecer == 'DEFERIDO') { ?>
            <a class='btn btn-info pull-right' href='<?php echo site_url('adm/rh/descontoCurso/descFuncParecCon/' . $object->dc_id) ?>'>PRÓXIMO</a> 
        <?php } ?>
    </div>
</div> 

