<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            SOLICITAÇÃO DE DESCONTO EM CURSO
        </div>
        <div class="col-md-6 col-xs-6 text-right">
           <!-- <a class='btn btn-warning' href='<?php echo site_url('adm/rh/adiantamento/adiantamentoFuncNovo') ?>'>SOLICITAR ADIANTAMENTO</a> 
            <a href="<?php echo site_url('adm/rh/Iniciorh'); ?>" class=" btn btn-primary">
                Voltar
            </a> -->
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal> 
        <?php echo form_open(); ?>                                       
        <?php echo validation_errors(); ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <?php //$this->util->select2('TIPO DE CURSO', 'dc_tipoCurso', $tipoCursos, $object->dc_tipoCurso, '', 4, '', 'required'); ?>
                    <div class="col-sm-4">
                        <label>TIPO DE CURSO</label>
                        <select class="form-control" name="dc_tipoCurso" id="tipoCurso" required="true">
                            <?php foreach ($tipoCursos as $key => $tipo) { ?>
                                <option value="<?php echo $key; ?>"><?php echo $tipo; ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="col-sm-6">
                        <label> CURSO</label>
                        <select class="form-control" name="dc_curso" id="dc_curso" required="true">

                        </select>
                    </div>
                    <?php //$this->util->input2('CURSO:', 'dc_curso', $object->dc_curso, '', 6, '', 'required'); ?>
                    <?php //$this->util->input2('SEMESTRE:', 'dc_semestre', $object->dc_semestre, '', 2, '', 'required'); ?>

                    <div class="col-sm-2">
                        <?php $this->util->select2('SEMESTRE', 'dc_semestre', array('' => 'SELECIONE', '1' => '1°', '2' => '2°', '3' => '3°', '4' => '4°', '5' => '5°', '6' => '6°', '7' => '7°', '8' => '8°', '9' => '9°', '10' => '10°'), $object->dc_semestre, '', '', 'required'); ?>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-2">
                    <div class="form-group">
                        <?php $this->util->select2('BENEFICIÁRIO', 'dc_beneficiario', array('' => 'SELECIONE', 'PROPRIO' => 'PRÓPRIO', 'PARENTE' => 'PARA UM PARENTE'), $object->dc_beneficiario, 'parecer', '', 2); ?>
                    </div>
                </div>

                <div class="col-sm-10">
                    <div class="form-group hidden exibe">
                        <?php $this->util->input2('NOME DO FAMILIAR:', 'dc_familiarNome', $object->dc_familiarNome, '', 8); ?>
                        <?php $this->util->input2('PARENTESCO:', 'dc_familiarParentesco', $object->dc_familiarParentesco, '', 4); ?>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <?php $this->util->input2('OBSERVAÇÕES', 'dc_observacoes', $object->dc_observacoes, '', 12, '', 'required'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            &nbsp;
            <div class="form-group">
                <input type=submit  value=Salvar class='btn btn-success'>
            </div>
            <?php echo form_close() ?>  
        </div>            
        <?php echo form_close() ?>
    </div>
</div>
<script>
    $(function () {

        /* FUNÇÃO PARA MOSNTRAR O RESTO DO FORMULARIO CASO SEJA DEFERIDO - ATENTAR PARA OS NOMES DAS CLASSES NO FORMULÁRIO*/
        $('.parecer').click(function () {

            if ($('.parecer').val() == 'PARENTE') {
                $('.exibe').removeClass('hidden');
            } else {
                $('.exibe').addClass('hidden');
            }

        });


        $('#tipoCurso').click(function () {
            if (this.value == 'GRADUACAO') {
                $.post("<?php echo site_url('adm/cursos/listByTipo/'); ?>", {tipo: this.value}, function (data) {
                    //alert(data);
                    $('#dc_curso').html(data);
                });
            }
            if (this.value == 'POS_GRADUACAO') {
                $.post("<?php echo site_url('adm/cursos/listByTipo/'); ?>", {tipo: this.value}, function (data) {
                    $('#dc_curso').html(data);
                });
            }
            if (this.value == 'MESTRADO') {
                $.post("<?php echo site_url('adm/cursos/listByTipo/'); ?>", {tipo: this.value}, function (data) {
                    $('#dc_curso').html(data);
                });
            }
        })


    });//
</script>