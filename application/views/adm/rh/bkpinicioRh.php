<br><br><br>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header" style="text-align: center;">SERVIÇOS PARA FUNCIONÁRIOS </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <!--<div class="huge">26</div> -->
                            <div>ADIANTAMENTO SALÁRIO</div>
                        </div>
                    </div>
                </div>
                <a href="<?php echo site_url('adm/rh/adiantamento/adiantamentoFunc'); ?>">
                    <div class="panel-footer">
                        <span class="pull-left">SOLICITAR</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                           <!-- <i class="fa fa-comments fa-5x"></i> -->
                        </div>
                        <div class="col-xs-12 text-right">
                            <!--<div class="huge">26</div> -->
                            <div> SOLICITAÇÃO DE DESCONTO</div>
                        </div>
                    </div>
                </div>
                <a href="<?php echo site_url('adm/rh/descontoCurso/descFuncList'); ?>" class="">
                    <div class="panel-footer">
                        <span class="pull-left">SOLICITAR</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                           <!-- <i class="fa fa-comments fa-5x"></i> -->
                        </div>
                        <div class="col-xs-12 text-right">
                            <!--<div class="huge">26</div> -->
                            <div>JUSTIFICATIVA DE AUSÊNCIA</div>
                        </div>
                    </div>
                </div>
                <a href="<?php echo site_url('adm/rh/ausencia'); ?>">
                    <div class="panel-footer">
                        <span class="pull-left">SOLICITAR</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                           <!-- <i class="fa fa-comments fa-5x"></i> -->
                        </div>
                        <div class="col-xs-9 text-right">
                            <!--<div class="huge">26</div> -->
                            <div>AJUSTE DE PONTO</div>
                        </div>
                    </div>
                </div>
                <a href="<?php echo site_url('adm/rh/AjustePonto'); ?>">
                    <div class="panel-footer">
                        <span class="pull-left">SOLICITAR</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<?php
$setorNome = $this->session->userdata('setorNome');
$coordenacoes = $this->util->coordenacoes();
if (in_array($setorNome, $coordenacoes)) {
    ?>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" style="text-align: center;"> SERVIÇOS PARA COORDENADORES </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <?php if (in_array($setorNome, array('FINANCEIRO', 'FINANCEIRO_COORD'))) { ?>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12 text-right">
                                    <!--<div class="huge">26</div> -->
                                    <div>FINANC - ADIANTAMENTO SALÁRIO</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?php echo site_url('adm/rh/adiantamento/adiantamentorFinancList'); ?>">
                            <div class="panel-footer">
                                <span class="pull-left">DETALHES</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            <?php } ?>
            teste
            <?php echo $setorNome; ?>
            <?php if (in_array($setorNome, array('POSGRADUACAO', 'POSGRADUACAO_COORD'))) { ?>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                   <!-- <i class="fa fa-comments fa-5x"></i> -->
                                </div>
                                <div class="col-xs-12 text-right">
                                    <!--<div class="huge">26</div> -->
                                    <div> SOLICITAÇÃO DE DESCONTO - PÓS-GRADUAÇÃO</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?php echo site_url('adm/rh/descontoCurso/descFuncPosList'); ?>" class="">
                            <div class="panel-footer">
                                <span class="pull-left">DETALHES</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>      
                </div>
            <?php } ?>
            <?php
            if (in_array($setorNome, $coordenacoes)) {
                ?>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                   <!-- <i class="fa fa-comments fa-5x"></i> -->
                                </div>
                                <div class="col-xs-12 text-right">
                                    <!--<div class="huge">26</div> -->
                                    <div>JUSTIFICATIVA DE AUSÊNCIA</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?php echo site_url('adm/rh/ausencia/ausListCoord'); ?>">
                            <div class="panel-footer">
                                <span class="pull-left">DETALHES</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                   <!-- <i class="fa fa-comments fa-5x"></i> -->
                                </div>
                                <div class="col-xs-9 text-right">
                                    <!--<div class="huge">26</div> -->
                                    <div>AJUSTE DE PONTO</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?php echo site_url('adm/rh/AjustePonto/ajusCoordList'); ?>">
                            <div class="panel-footer">
                                <span class="pull-left">DETALHES</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>

<?php if (in_array($setorNome, array('RH_COORD'))) { ?>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" style="text-align: center;">SERVIÇOS PARA RH</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12 text-right">
                                <!--<div class="huge">26</div> -->
                                <div>ADIANTAMENTO SALÁRIO</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?php echo site_url('adm/rh/adiantamento/adiantamentoRh'); ?>">
                        <div class="panel-footer">
                            <span class="pull-left">DETALHES</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                               <!-- <i class="fa fa-comments fa-5x"></i> -->
                            </div>
                            <div class="col-xs-12 text-right">
                                <!--<div class="huge">26</div> -->
                                <div> SOLICITAÇÃO DE DESCONTO</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?php echo site_url('adm/rh/descontoCurso/descFuncRhList'); ?>" class="">
                        <div class="panel-footer">
                            <span class="pull-left">DETALHES</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                               <!-- <i class="fa fa-comments fa-5x"></i> -->
                            </div>
                            <div class="col-xs-12 text-right">
                                <!--<div class="huge">26</div> -->
                                <div>JUSTIFICATIVA DE AUSÊNCIA</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?php echo site_url('adm/rh/ausencia/ausListRh'); ?>">
                        <div class="panel-footer">
                            <span class="pull-left">DETALHES</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                               <!-- <i class="fa fa-comments fa-5x"></i> -->
                            </div>
                            <div class="col-xs-9 text-right">
                                <!--<div class="huge">26</div> -->
                                <div>AJUSTE DE PONTO</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?php echo site_url('adm/rh/ajustePonto/ajusRhList'); ?>">
                        <div class="panel-footer">
                            <span class="pull-left">DETALHES</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>


<?php } ?>
<?php if (in_array($setorNome, array('DIRECAO'))) { ?>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" style="text-align: center;">SERVIÇOS PARA GERÊNCIA</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                               <!-- <i class="fa fa-comments fa-5x"></i> -->
                            </div>
                            <div class="col-xs-12 text-right">
                                <!--<div class="huge">26</div> -->
                                <div>JUSTIFICATIVA DE AUSÊNCIA</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?php echo site_url('adm/rh/ausencia/ausListDirec'); ?>">
                        <div class="panel-footer">
                            <span class="pull-left">DETALHES</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<hr>



