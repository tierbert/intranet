<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            CAPACITAÇÃO
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px;">
    <?php foreach ($cursos->result() as $curso) { ?>
        <h3>
            <?php echo $curso->cur_curso . ' / ' . $curso->cur_setor . ' / ' . $curso->cur_horario; ?>
            <span class="pull-right">
                <strong>
                    Vagas:
                </strong>
                <?php echo $curso->cur_vagas; ?>
            </span>
        </h3>
        <?php
        $inscricoes = $this->capacitamodel->getInscCurso($curso->cur_id);
        ?>
        <table class = "table table-bordered table-hover table-striped">
            <tr>
                <th>#</th>
                <th>Colaborador</th>
                <th>Setor</th>
                <th>Local</th>
                <th>Profissional</th>
                <th></th>
            </tr>
            <?php
            $i = 0;
            foreach ($inscricoes->result() as $inscricao) {
                $i++;
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo mb_strtoupper($inscricao->usu_nome); ?></td>
                    <td><?php echo $inscricao->set_nome; ?></td>
                    <td><?php echo $inscricao->cur_sala; ?></td>
                    <td><?php echo $inscricao->cur_profissional; ?></td>
                    <td>
                        <a href="<?php echo site_url('adm/rh/capacitaRh/delete/' . $inscricao->age_id); ?>">
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                    </td>
                </tr>
            <?php } ?>
        </table>


        <hr>
    <?php } ?>
</div>