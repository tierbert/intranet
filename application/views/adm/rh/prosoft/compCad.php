<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            CADASTRAR COMPETÊNCIA
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('adm/rh/prosoft'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 300px;">

    <div class=form-horizontal>    
        <div class="error">
            <?php echo validation_errors(); ?>
        </div>
        <?php echo form_open() ?>

        <div class="form-group">
            <?php $this->util->input2('COMPETÊMCIA', 'com_competencia', '', 'competencia', 2); ?>

        </div>
        <div class=form-group>
            <div class="col-md-2">
                <label>&nbsp;</label>
                <input type=submit  value=Salvar class='btn btn-success form-control'>
            </div>

        </div>            
        <?php echo form_close() ?>
    </div>

    <?php if (isset($repetirComp)) { ?>
        <br>
        <h4 style="color: red">
            OS DADOS DA COMPETÊNCIA <?php echo $repetirComp->com_competencia; ?> SERÃO COPIADOS PARA ESTA COMPETÊNCIA.
        </h4>
    <?php } ?>
</div>
<?php $this->util->mascaras(); ?>


<script>
    $('.competencia').mask('99/9999');
</script>
<style>
    .error p{
        color: white;
        background-color: red;
        padding: 5px;
        margin-bottom: 5px;
    }
</style>