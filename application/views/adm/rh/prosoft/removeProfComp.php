<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            PROSOFT
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/rh/prosoft'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px; padding: 0">
    <div style="padding: 40px;">




        <h3> <?php echo 'Deletar' ?></h3>
        <div class='form'>
            <?php echo form_open($this->uri->uri_string(), 'post') ?>
            Tem certeza que quer excluir: <?php echo $object->cd_id ?>?
            <br>
            <?php echo form_submit('agree', 'Sim') ?>
            <?php echo form_submit('disagree', 'Não') ?>
            <?php echo form_close() ?>
            <p>
                <?php echo anchor($this->uri->segment(1), 'Voltar') ?>
            </p>
        </div>
