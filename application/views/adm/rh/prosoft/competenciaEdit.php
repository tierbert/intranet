<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            PROSOFT
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/rh/prosoft'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px; padding: 0">
    <div style="padding: 40px;">


        <div class="row">

            <div class="col-md-7">

                <table class="table table-bordered table-condensed">
                    <tr>
                        <td>
                            TOTAL: <?php echo $dadosCompetencia->num_rows(); ?>
                        </td>
                        <td>
                            INTEGRAL: 
                            <?php if (isset($totais['INTEGRAL'])) echo $totais['INTEGRAL']; ?>
                        </td>
                        <td>
                            P.PROPORCIONAL: 
                            <?php if (isset($totais['PARCIAL_PROPORCIONAL'])) echo $totais['PARCIAL_PROPORCIONAL']; ?>
                        </td>
                        <td>
                            P.REGULAR: 
                            <?php if (isset($totais['PARCIAL_REGULAR'])) echo $totais['PARCIAL_REGULAR']; ?>
                        </td>
                        <td>
                            HORISTA: 
                            <?php if (isset($totais['HORISTA'])) echo $totais['HORISTA']; ?>
                        </td>
                    </tr>

                </table>

            </div>
            <div class="col-md-4 pull-right">

                <a href="<?php echo site_url('adm/rh/prosoft/compInsere/' . $idCompetencia); ?>" class="btn btn-primary" title="INSERIR DOCENTE">
                    <span class="glyphicon glyphicon-education"></span>
                    <span class="glyphicon glyphicon-plus"></span>
                </a>

                <a href="<?php echo site_url('adm/rh/prosoft/importar/' . $idCompetencia); ?>" class="btn btn-primary" title="IMPORTAR">
                    &nbsp;&nbsp;
                    <span class="glyphicon glyphicon-import"></span>
                    &nbsp;&nbsp;
                </a>

                <a href="<?php echo site_url('adm/rh/prosoft/imprimir/' . $idCompetencia); ?>" class="btn btn-primary" title="IMPRIMIR">
                    &nbsp;&nbsp;
                    <span class="glyphicon glyphicon-print"></span> 
                    &nbsp;&nbsp;
                </a>
                <a href="<?php echo site_url('adm/rh/prosoft/exportar/' . $idCompetencia); ?>" class="btn btn-primary" title="EXPORTAR">
                    &nbsp;&nbsp;
                    <span class="glyphicon glyphicon-export"></span> EXPORTAR
                    &nbsp;&nbsp;
                </a>
            </div>
        </div>

        <hr>

        <table class="table table-bordered table-condensed">
            <td class="text-center">
                COMPETÊNCIA <br>
                <span style="font-size: 25px; font-weight: bold">
                    <?php echo $dadosCompetencia->row('com_competencia'); ?>
                </span>
            </td>

            <?php
            foreach ($baseCalculo as $k => $bc) {
                $modalidade = mb_strtoupper(str_replace('_', ' ', $k));
                $valorModalidade = $this->util->moeda2($bc);
                $cor = $modalidade == 'GRADUADO INTEGRAL' ? 'success' : ($modalidade == 'GRADUADO HORA' ? 'success' : '');
                $cor = $modalidade == 'ESPECIALISTA INTEGRAL' ? 'info' : ($modalidade == 'ESPECIALISTA HORA' ? 'info' : $cor);
                $cor = $modalidade == 'MESTRE INTEGRAL' ? 'warning' : ($modalidade == 'MESTRE HORA' ? 'warning' : $cor);
                $cor = $modalidade == 'DOUTOR INTEGRAL' ? 'danger' : ($modalidade == 'DOUTOR HORA' ? 'danger' : $cor);
                ?>
                <td class="text-center <?php echo $cor; ?>">
                    <?php echo $modalidade . '<br> ' . $valorModalidade; ?>
                </td>
            <?php } ?>
        </table>

        <hr>



        <table class='table table-bordered table-striped table-condensed table-hover' id="tabela">
            <thead>
                <tr>
                    <th>COD</th>
                    <th>NOME</th>
                    <th>TITULAÇÃO</th>
                    <th>REGIME</th>
                    <th style="background: #eee; width: 5px;" ></th>
                    <th class="text-center" title="CH P. PROPORCIONAL">CH PP</th>
                    <th class="text-center" title="CH AULA REGIME INTEGRAL">CH ARI</th>
                    <th class="text-center" title="CH AULA REGIME PARCIAL">CH ARP</th>
                    <th class="text-center" title="CH AULA REGIME HORISTA">CH ARH</th>
                    <th style="background: #eee; width: 5px; max-width: 5px;"></th>
                    <th class="text-center" title="CH EXTRA SALA">CH ES</th>
                    <th class="text-center" title="CH TOTAL">CH T</th>
                    <th class="text-center" title="CH AULA REGIME INTEGRAL R$">CH ARI</th>
                    <th class="text-center" title="CH AULA REGIME PARCIAL R$">CH ARP</th>
                    <th title="CH AULA HORISTA R$">CH AH</th>
                    <th title="DSR AULA (1/6) DA H. AULA">DSR</th>
                    <th title="R$ ATIVIDADE EXTRA-CLASSE">AEC</th>
                    <th style="background: #eee"></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($dadosCompetencia->result() as $dado) {
                    $cor = '';
                    $cor = $dado->cd_titulacao == 'GRADUADO' ? 'success' : '';
                    $cor = $dado->cd_titulacao == 'ESPECIALISTA' ? 'info' : $cor;
                    $cor = $dado->cd_titulacao == 'MESTRE' ? 'warning' : $cor;
                    $cor = $dado->cd_titulacao == 'DOUTOR' ? 'danger' : $cor;
                    ?>
                    <tr class="">
                        <td><?php echo $dado->pro_registroRh ?> </td>
                        <td><?php echo substr($dado->pro_nome, 0, 25) ?> </td>
                        <td class="<?php echo $cor; ?>"><?php echo $dado->cd_titulacao ?> </td>
                        <td>
                            <?php echo str_replace('PARCIAL_', 'PARC. ', $dado->cd_regime) ?> 
                        </td>
                        <td style="background: #eee; width: 5px;"></th>
                        <td class="text-center"><?php echo $dado->cd_chProporcional; ?></td>
                        <td class="text-center"><?php echo $dado->cd_chAulaRegIntegral; ?></td>
                        <td class="text-center"><?php echo $dado->cd_chAulaRegParcial; ?></td>
                        <td class="text-center"><?php echo $dado->cd_chAulaRegHorista; ?></td>
                        <th style="background: #eee; width: 5px"></th>
                        <td class="text-center"><?php echo $dado->cd_chExtraSala; ?></td>
                        <td class="text-center"><?php echo $dado->cd_chTotal; ?></td>
                        <td><?php echo $dado->cd_chAulaRegIntegralTotal != 0 ? $this->util->moeda2($dado->cd_chAulaRegIntegralTotal) : ''; ?></td>
                        <td><?php echo $dado->cd__chAulaRegParcialTotal != 0 ? $this->util->moeda2($dado->cd__chAulaRegParcialTotal) : ''; ?></td>
                        <td><?php echo ($dado->cd__chAulaHorista != 0 ? $this->util->moeda2($dado->cd__chAulaHorista) : ''); ?></td>
                        <td><?php echo ($dado->cd__dsrAulaHorAula != 0 ? $this->util->moeda2($dado->cd__dsrAulaHorAula) : ''); ?></td>
                        <td><?php echo ($dado->cd__atividadeExtraClasse != 0 ? $this->util->moeda2($dado->cd__atividadeExtraClasse) : ''); ?></td>
                        <th style="background: #eee; width: 5px"></th>
                        <td>
                            <a href="<?php echo site_url('adm/rh/prosoft/calcularProf/' . $idCompetencia . '/' . $dado->cd_id . '/' . $dado->pro_id); ?>" class="btn btn-xs btn-info ">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                            <a href="<?php echo site_url('adm/rh/prosoft/removeProfComp/' . $idCompetencia . '/' . $dado->cd_id . '/' . $dado->pro_id); ?>" class="btn btn-xs btn-danger ">
                                <span class="glyphicon glyphicon-remove"></span>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/dataTables'); ?>/jquery.dataTables.js"></script>
<script>

    $('#tabela').DataTable(
            {
                paging: false,
                order: [[1, "asc"]]
            }
    );

</script>
