<div class="painel" style="min-height: 400px; padding: 0">
    <div style="padding: 40px;">
        <div class="row">
            <div class="col-md-7">
                <table class="table table-bordered table-condensed">
                    <tr>
                        <td>
                            TOTAL: <?php echo $dadosCompetencia->num_rows(); ?>
                        </td>
                        <td>
                            INTEGRAL: 
                            <?php if (isset($totais['INTEGRAL'])) echo $totais['INTEGRAL']; ?>
                        </td>
                        <td>
                            P.PROPORCIONAL: 
                            <?php if (isset($totais['PARCIAL_PROPORCIONAL'])) echo $totais['PARCIAL_PROPORCIONAL']; ?>
                        </td>
                        <td>
                            P.REGULAR: 
                            <?php if (isset($totais['PARCIAL_REGULAR'])) echo $totais['PARCIAL_REGULAR']; ?>
                        </td>
                        <td>
                            HORISTA: 
                            <?php if (isset($totais['HORISTA'])) echo $totais['HORISTA']; ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <hr>
        <table class="table table-bordered table-condensed" style="font-size: 10px;" border="1">
            <td>
                COMPETÊNCIA <br>
                <?php echo $dadosCompetencia->row('com_competencia'); ?>
            </td>
            <?php
            foreach ($baseCalculo as $k => $bc) {
                $modalidade = mb_strtoupper(str_replace('_', ' ', $k));
                $valorModalidade = $this->util->moeda2($bc);
                ?>
                <td>
                    <?php echo $modalidade . '<br> ' . $valorModalidade; ?>
                </td>
            <?php } ?>
        </table>
        <hr>
        <table class='table table-bordered table-striped table-condensed table-hover' id="tabela" style="font-size: 7px;" border="1">
            <thead>
                <tr>
                    <th>COD</th>
                    <th>NOME</th>
                    <th>TITULAÇÃO</th>
                    <th>REGIME</th>
                    <th class="text-center" title="CH P. PROPORCIONAL">CH PP</th>
                    <th class="text-center" title="CH AULA REGIME INTEGRAL">CH ARI</th>
                    <th class="text-center" title="CH AULA REGIME PARCIAL">CH ARP</th>
                    <th class="text-center" title="CH AULA REGIME HORISTA">CH ARH</th>
                    <th class="text-center" title="CH EXTRA SALA">CH ES</th>
                    <th class="text-center" title="CH TOTAL">CH T</th>
                    <th class="text-center" title="CH AULA REGIME INTEGRAL R$">CH ARI</th>
                    <th class="text-center" title="CH AULA REGIME PARCIAL R$">CH ARP</th>
                    <th title="CH AULA HORISTA R$">CH AH</th>
                    <th title="DSR AULA (1/6) DA H. AULA">DSR</th>
                    <th title="R$ ATIVIDADE EXTRA-CLASSE">AEC</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($dadosCompetencia->result() as $dado) {
                    ?>
                    <tr>
                        <td><?php echo $dado->pro_registroRh ?> </td>
                        <td><?php echo substr($dado->pro_nome, 0, 15) ?> </td>
                        <td><?php echo $dado->pro_titulacao ?> </td>
                        <td><?php echo str_replace('PARCIAL_PROPORCIONAL', 'P.PROPOR', $dado->pro_regime) ?> </td>
                        <td class="text-center"><?php echo $dado->cd_chProporcional; ?></td>
                        <td class="text-center"><?php echo $dado->cd_chAulaRegIntegral; ?></td>
                        <td class="text-center"><?php echo $dado->cd_chAulaRegParcial; ?></td>
                        <td class="text-center"><?php echo $dado->cd_chAulaRegHorista; ?></td>
                        <td class="text-center"><?php echo $dado->cd_chExtraSala; ?></td>
                        <td class="text-center"><?php echo $dado->cd_chTotal; ?></td>
                        <td><?php echo $dado->cd_chAulaRegIntegralTotal != 0 ? $this->util->moeda2($dado->cd_chAulaRegIntegralTotal) : ''; ?></td>
                        <td><?php echo $dado->cd__chAulaRegParcialTotal != 0 ? $this->util->moeda2($dado->cd__chAulaRegParcialTotal) : ''; ?></td>
                        <td><?php echo ($dado->cd__chAulaHorista != 0 ? $this->util->moeda2($dado->cd__chAulaHorista) : ''); ?></td>
                        <td><?php echo ($dado->cd__dsrAulaHorAula != 0 ? $this->util->moeda2($dado->cd__dsrAulaHorAula) : ''); ?></td>
                        <td><?php echo ($dado->cd__atividadeExtraClasse != 0 ? $this->util->moeda2($dado->cd__atividadeExtraClasse) : ''); ?></td>

                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/dataTables'); ?>/jquery.dataTables.js"></script>
<script>
    $('#tabela').DataTable({
        paging: false,
        order: [[1, "asc"]]
    });
</script>
