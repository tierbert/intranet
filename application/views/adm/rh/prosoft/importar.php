<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            PROSOFT / IMPORTAR
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/rh/prosoft'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px; padding: 40PX;">
    <div class="row">
        <?php echo form_open_multipart(); ?>
        <div class="form-group">
            <div class='col-md-4'>
                <label>ENVIAR ARQUIVO</label>
                <input class="form-control" type="file" name="xml" required="true">
                <input type="hidden" name="teste" value="1">
            </div>
            <div class='col-md-2'>
                <label>&nbsp;</label>
                <input type="submit" class="form-control btn btn-primary">
            </div>

        </div>
        <?php echo form_close(); ?>
    </div>
</div>