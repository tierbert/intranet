<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            PROSOFT / CALCULAR
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <?php if (isset($professor)) { ?>

                <a href="<?php echo site_url('adm/rh/prosoft/competenciaEdit/' . $idCompetencia); ?>" class=" btn btn-primary">
                    Voltar
                </a>
            <?php } else { ?>
                <a href="<?php echo site_url('adm/rh/prosoft'); ?>" class=" btn btn-primary">
                    Voltar
                </a>
            <?php } ?>
        </div>
    </div>
</div>

<?php echo form_open(); ?>

<div class="painel" style="min-height: 400px; padding: 40PX;">
    <div class="row">
        <div class="table-responsive">


            <table class="table table-bordered table-condensed">
                <?php
                foreach ($baseCalculo as $k => $bc) {
                    $modalidade = mb_strtoupper(str_replace('_', ' ', $k));
                    $valorModalidade = $this->util->moeda2($bc);
                    ?>
                    <td>
                        <?php echo $modalidade . '<br> ' . $valorModalidade; ?>
                    </td>
                <?php } ?>
            </table>

            <hr>


            <table class="table table-bordered table-hover table-striped" >
                <tr>
                    <th>COD </th>
                    <th colspan="4">NOME</th>
                    <th colspan="4">TITULAÇÃO</th>
                    <th colspan="4">REGIME</th>
                </tr>
                <tr>
                    <td>
                        <?php
                        if (isset($professor)) {
                            echo $professor->pro_registroRh;
                        }
                        ?>
                    </td>
                    <td colspan="4">

                        <?php if (isset($funcao) && $funcao == 'inserir') { ?>
                            <input type="hidden" value="<?php echo $idCd; ?>" name="cd_idCompetencia">
                        <?php } ?>

                        <?php
                        if (isset($funcao) && $funcao == 'inserir') {
                            $this->util->select2('SELECIONE', 'cd_idProfessor', $professores, '', ' select2', 12);
                        }

                        if (isset($professor)) {
                            echo $professor->pro_nome;
                        }
                        $titulacao = array(
                            '' => '',
                            "GRADUADO" => 'GRADUADO',
                            "ESPECIALISTA" => 'ESPECIALISTA',
                            "MESTRE" => 'MESTRE',
                            "DOUTOR" => 'DOUTOR',
                            "SUP_ESTAGIO" => 'SUP_ESTAGIO',
                            "MESTRADO" => 'MESTRADO',
                        );

                        $regimes = array(
                            '' => '',
                            "INTEGRAL" => 'INTEGRAL',
                            "PARCIAL_PROPORCIONAL" => 'PARCIAL_PROPORCIONAL',
                            "PARCIAL_REGULAR" => 'PARCIAL_REGULAR',
                            "HORISTA" => 'HORISTA'
                        );
                        ?>

                    </td>
                    <td colspan="4">
                        <?php if (isset($professor)) { ?>
                            <?php $this->util->select2('titulação', 'cd_titulacao', $titulacao, $compDados->cd_titulacao, '', 12, 'titulacao'); ?>
                        <?php } elseif (isset($professores)) { ?>
                            <?php $this->util->select2('titulação', 'cd_titulacao', $titulacao, '', '', 12, 'titulacao'); ?>
                        <?php } else { ?>
                            <select class="form-control" id="titulacao">
                                <option value=""></option>
                                <option value="GRADUADO">GRADUADO</option>
                                <option value="ESPECIALISTA">ESPECIALISTA</option>
                                <option value="MESTRE">MESTRE</option>
                                <option value="DOUTOR">DOUTOR</option>
                                <option value="SUP_ESTAGIO">SUP_ESTAGIO</option>
                                <option value="MESTRADO">MESTRADO</option>
                            </select>
                        <?php } ?>
                    </td>
                    <td colspan="4">
                        <?php if (isset($professor)) { ?>
                            <?php $this->util->select2('regime', 'cd_regime', $regimes, $compDados->cd_regime, '', 12, 'regime'); ?>
                        <?php } elseif (isset($professores)) { ?>
                            <?php $this->util->select2('regime', 'cd_regime', $regimes, '', '', 12, 'regime'); ?>
                        <?php } else { ?>
                            <select class="form-control" id="regime">
                                <option value=""></option>
                                <option value="INTEGRAL">INTEGRAL</option>
                                <option value="PARCIAL_PROPORCIONAL">PARCIAL_PROPORCIONAL</option>
                                <option value="PARCIAL_REGULAR">PARCIAL_REGULAR</option>
                                <option value="HORISTA">HORISTA</option>
                            </select>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <th>CH</th>
                    <th>CH P. PROPORC.</th>
                    <th>CH AULA REGIME INTEGRAL
                    </th>
                    <th>CH AULA REGIME PARCIAL
                    </th>
                    <th>CH AULA REGIME HORISTA
                    </th>
                    <th>CH EXTRA SALA
                    </th>
                    <th>CH TOTAL
                    </th>
                    <th>CH AULA<br> REGIME <br>INTEGRAL R$
                    </th>
                    <th>CH AULA<br> REGIME <br>PARCIAL R$
                    </th>
                    <th>CH AULA HORISTA R$
                    </th>
                    <th>DSR AULA (1/6) DA H. AULA
                    </th>
                    <th>R$ ATIVIDADE EXTRA-CLASSE
                    </th>
                </tr>

                <tr>
                    <td>
                        <input type="text"  id="ch" class="form-control" disabled="true" style="width: 50px;" >
                    </td>
                    <td>
                        <input type="text" name="cd_chProporcional" id="fol_chProporcional" value="<?php echo isset($compDados) ? $compDados->cd_chProporcional : ''; ?>" class="form-control" disabled="true" required="true" >
                    </td>
                    <td>
                        <input type="text" name="cd_chAulaRegIntegral" id="cd_chAulaRegIntegral" value="<?php echo isset($compDados) ? $compDados->cd_chAulaRegIntegral : ''; ?>" class="form-control" disabled="true" required="true">
                    </td>
                    <td>
                        <input type="text" name="cd_chAulaRegParcial" id="fol_chAulaRegParcial" value="<?php echo isset($compDados) ? $compDados->cd_chAulaRegParcial : ''; ?>" class="form-control" disabled="true" required="true">
                    </td>
                    <td>
                        <input type="text" name="cd_chAulaRegHorista" id="fol_chAulaRegHorista" value="<?php echo isset($compDados) ? $compDados->cd_chAulaRegHorista : ''; ?>" class="form-control" disabled="true" required="true">
                    </td>
                    <td>
                        <input type="text"  id="fol_chExtraSala" value="<?php echo isset($compDados) ? $compDados->cd_chExtraSala : ''; ?>" class="form-control" disabled="true">
                    </td>
                    <td>
                        <input type="text"  id="fol_chTotal" value="<?php echo isset($compDados) ? $compDados->cd_chTotal : ''; ?>" class="form-control" disabled="true">
                    </td>
                    <td>
                        <input type="text"  id="fol_chAulaRegIntegralTotal" value="<?php echo isset($compDados) ? $compDados->cd_chAulaRegIntegralTotal : ''; ?>" class="form-control" disabled="true">
                    </td>
                    <td>
                        <input type="text"  id="fol__chAulaRegParcialTotal" value="<?php echo isset($compDados) ? $compDados->cd__chAulaRegParcialTotal : ''; ?>" class="form-control" disabled="true">
                    </td>
                    <td>
                        <input type="text"  id="fol__chAulaHorista"  value="<?php echo isset($compDados) ? $compDados->cd__chAulaHorista : ''; ?>" class="form-control" disabled="true">
                    </td>
                    <td>
                        <input type="text"  id="fol__dsrAulaHorAula" value="<?php echo isset($compDados) ? $compDados->cd__dsrAulaHorAula : ''; ?>" class="form-control" disabled="true">
                    </td>
                    <td>
                        <input type="text"  id="fol__atividadeExtraClasse" value="<?php echo isset($compDados) ? $compDados->cd__atividadeExtraClasse : ''; ?>" class="form-control" disabled="true">
                    </td>
                </tr>
                <tr class="hidden">
                    <td>
                        <input id='teste' type="hidden  " class="form-control">
                    </td>
                </tr>

                <tr>
                    <td colspan="5">
                        <div class="bgAviso hidden" style="background:#d9534f; color: white; width: 100%; height: 120px; padding: 9px; border-radius: 10px; -moz-border-radius: 10px; -webkit-border-radius: 10px; font-size: 20px;">
                            AVISO
                            <hr style="margin: 0; border-color: white;" >
                            <span class="aviso" style="font-size: 15px;"></span>
                        </div>


                    </td>
                    <td colspan="7">

                        <div class="form-group divJust hidden">
                            <label>JUSTIFICATIVA</label>
                            <textarea name="cd_justificativa" disabled class="form-control justif" style="height: 99px;"></textarea>
                        </div>

                    </td>
                </tr>

            </table>
        </div>
        <?php if (isset($professor) || isset($professores)) { ?>

            <button class="btn btn-primary salvar">
                SALVAR
            </button>

        <?php } ?>

        <?php echo form_close(); ?>
    </div>
</div>



<?php $this->util->mascaras() ?>

<script>


<?php if (isset($compDados)) { ?>

        var regimeInicial = '<?php echo $compDados->cd_regime; ?>';


        if (regimeInicial == 'INTEGRAL') {
            $('#ch').val('44');
            $('#fol_chTotal').val('44');
            $('#cd_chAulaRegIntegral').removeAttr('disabled');
            $('#cd_chAulaRegIntegral').focus();
        }

        if (regimeInicial == 'PARCIAL_PROPORCIONAL') {
            $('#fol_chProporcional').removeAttr('disabled');
            $('#fol_chAulaRegParcial').removeAttr('disabled');

            $('#fol_chProporcional').focus();
        }

        if (regimeInicial == 'PARCIAL_REGULAR') {
            $('#fol_chAulaRegParcial').removeAttr('disabled');
            $('#fol_chAulaRegParcial').focus();
        }

        if (regimeInicial == 'HORISTA') {
            $('#fol_chAulaRegHorista').removeAttr('disabled');
            $('#fol_chAulaRegHorista').focus();
        }

<?php } ?>
    var graduadoValorIntegral = <?php echo $baseCalculo['graduado_integral']; ?>;
    var graduadoValorHora = <?php echo $baseCalculo['graduado_hora']; ?>;

    var especialistaValorIntegral = <?php echo $baseCalculo['especialista_integral']; ?>;
    var especialistaValorHora = <?php echo $baseCalculo['especialista_hora']; ?>;

    var mestreValorIntegral = <?php echo $baseCalculo['mestre_integral']; ?>;
    var mestreValorHora = <?php echo $baseCalculo['mestre_hora']; ?>;

    var doutorValorIntegral = <?php echo $baseCalculo['doutor_integral']; ?>;
    var doutorValorHora = <?php echo $baseCalculo['doutor_hora']; ?>;

    var mestradoValor = <?php echo $baseCalculo['mestrado']; ?>;
    var sup_estagioValor = <?php echo $baseCalculo['sup_estagio']; ?>;

    var hora = '44';

    $('#ch').mask('99', {completed: function () {

            var regime = $('#regime').val();
            if (regime == '') {
                alert('INFORMA O REGIME');
                $('#ch').val('');
                return false;
            }
            //alert("Você digitou o seguinte:" + this.val());
            //teste(this.val());
        }
    });

    $('#regime').on('change', function () {
        //alert($(this).find(":selected").val());
        $('input').removeAttr('disabled');
        $('input').attr('disabled', 'true');
        $('input').val("");
        $('.salvar').val("salvar");

        $('.bgAviso').addClass('hidden');
        $('.divJust').addClass('hidden');
        $('.justif').prop('required', false);

        var regime;
        if (getTitulacao()) {
            var titulacao = getTitulacao();
            regime = $('#regime').val();
        } else {
            $("#regime option[value='']").attr('selected', true)
            return false;
        }
        console.log('Titulacao: ' + titulacao + ' Regime: ' + regime);

        if (regime === 'INTEGRAL') {
            $('#ch').val('44');
            $('#fol_chTotal').val('44');
            $('#cd_chAulaRegIntegral').removeAttr('disabled');
            $('#cd_chAulaRegIntegral').focus();


        }

        if (regime === 'PARCIAL_PROPORCIONAL') {
            $('#fol_chProporcional').removeAttr('disabled');
            $('#fol_chAulaRegParcial').removeAttr('disabled');

            $('#fol_chProporcional').focus();
        }

        if (regime === 'PARCIAL_REGULAR') {
            $('#fol_chAulaRegParcial').removeAttr('disabled');
            $('#fol_chAulaRegParcial').focus();
        }

        if (regime === 'HORISTA') {
            $('#fol_chAulaRegHorista').removeAttr('disabled');
            $('#fol_chAulaRegHorista').focus();
        }

    });




    $('#cd_chAulaRegIntegral').mask('99', {completed: function () {
            if (getTitulacao()) {
                var titulacao = getTitulacao();
                var regime = $('#regime').val();
            } else {
                $("#regime option[value='']").attr('selected', true)
                return false;
            }

            var fol_chAulaRegHorista = $('#cd_chAulaRegIntegral').val();

            if (fol_chAulaRegHorista == '00') {
                var fol__dsrAulaHorAula = (getValor(titulacao) / 6).toFixed(2);
                $('#fol__dsrAulaHorAula').val(fol__dsrAulaHorAula);
                var fol__chAulaHorista = (getValor(titulacao) - fol__dsrAulaHorAula).toFixed(2);
                $('#fol_chAulaRegIntegralTotal').val(fol__chAulaHorista);
            } else {
                $('#fol_chAulaRegIntegralTotal').val(getValor(titulacao));
                var fol__chAulaHorista = getValorHora(titulacao) * fol_chAulaRegHorista * 4.5;
                $('#fol__chAulaHorista').val(fol__chAulaHorista.toFixed(2));
                var fol__dsrAulaHorAula = (fol__chAulaHorista / 6).toFixed(2);
                $('#fol__dsrAulaHorAula').val(fol__dsrAulaHorAula);
                var extc = (getValor(titulacao) - fol__chAulaHorista - fol__dsrAulaHorAula).toFixed(2);
                $('#fol__atividadeExtraClasse').val(extc);
            }
        }
    });




    $('#fol_chProporcional').mask('99', {completed: function () {
            $('#fol_chAulaRegParcial').focus();
        }
    });

    $('#fol_chAulaRegParcial').mask('99', {completed: function () {
            if (getTitulacao()) {
                var titulacao = getTitulacao();
                var regime = $('#regime').val();
            } else {
                $("#regime option[value='']").attr('selected', true)
                return false;
            }
            log(regime);
            if (regime == 'PARCIAL_REGULAR') {
                //var fol_chProporcional = $('#fol_chProporcional').val();


                var fol_chAulaRegParcial = $('#fol_chAulaRegParcial').val();

                var fol_chTotal = (fol_chAulaRegParcial / 0.75).toFixed(0);
                $('#fol_chTotal').val(fol_chTotal);


                var fol_chExtraSala = fol_chTotal - fol_chAulaRegParcial;
                $('#fol_chExtraSala').val(fol_chExtraSala);



                var fol__chAulaRegParcialTotal = (getValorHora(titulacao) * fol_chAulaRegParcial * 4.5).toFixed(2);

                $('#fol__chAulaRegParcialTotal').val(fol__chAulaRegParcialTotal);

                var fol__dsrAulaHorAula = (fol__chAulaRegParcialTotal / 6).toFixed(2);
                $('#fol__dsrAulaHorAula').val(fol__dsrAulaHorAula);

                //var proporcao = ((fol_chProporcional * 100) / 44).toFixed(2);
                //log('prop: ' + proporcao);


                //var fol__atividadeExtraClasse = ((getValor(titulacao) * proporcao) / 100).toFixed(2) - fol__chAulaRegParcialTotal - fol__dsrAulaHorAula;
                var fol__atividadeExtraClasse = (((parseFloat(fol__chAulaRegParcialTotal) + parseFloat(fol__dsrAulaHorAula)) * 11) / 100).toFixed(2);
                //log('valorProp: ' + fol__atividadeExtraClasse);
                $('#fol__atividadeExtraClasse').val(fol__atividadeExtraClasse);
            }


            if (regime == 'PARCIAL_PROPORCIONAL') {

                var fol_chProporcional = $('#fol_chProporcional').val();
                var fol_chAulaRegParcial = $('#fol_chAulaRegParcial').val();
                var fol_chTotal = (fol_chAulaRegParcial / 0.75).toFixed(0);


                var proporcao = ((fol_chProporcional * 100) / 44).toFixed(2);

                var fol__chAulaRegParcialTotal = ((getValor(titulacao) * proporcao) / 100).toFixed(2);
                $('#fol__chAulaRegParcialTotal').val(fol__chAulaRegParcialTotal);


                //var fol_chAulaRegHorista = $('#cd_chAulaRegParcial').val();
                var fol__chAulaHorista = getValorHora(titulacao) * fol_chAulaRegParcial * 4.5;

                $('#fol__chAulaHorista').val(fol__chAulaHorista.toFixed(2));

                var fol__dsrAulaHorAula = (fol__chAulaHorista / 6).toFixed(2);
                $('#fol__dsrAulaHorAula').val(fol__dsrAulaHorAula);

                var fol__atividadeExtraClasse = (fol__chAulaRegParcialTotal - fol__chAulaHorista - fol__dsrAulaHorAula).toFixed(2);

                $('#fol__atividadeExtraClasse').val(fol__atividadeExtraClasse);


                /*
                 
                 $('#fol_chTotal').val(fol_chTotal);
                 $('#fol_chExtraSala').val(fol_chTotal - fol_chAulaRegParcial);
                 
                 var proporcao = ((fol_chAulaRegParcial * 100) / 44).toFixed(2);
                 
                 var fol__dsrAulaHorAula = (((getValor(titulacao) * proporcao) / 100).toFixed(2) / 6).toFixed(2);
                 $('#fol__dsrAulaHorAula').val(fol__dsrAulaHorAula);
                 
                 
                 
                 
                 var fol__atividadeExtraClasse = (((getValor(titulacao) * proporcao2) / 100).toFixed(2) - fol__dsrAulaHorAula - fol__chAulaRegParcialTotal).toFixed(2);
                 log('valorProp: ' + fol__atividadeExtraClasse);
                 $('#fol__atividadeExtraClasse').val(fol__atividadeExtraClasse);
                 */
            }


            if (fol_chAulaRegParcial < 9 || fol_chAulaRegParcial > 29) {
                $('.bgAviso').removeClass('hidden');
                $('.divJust').removeClass('hidden');
                $('.justif').removeAttr('disabled');
                $('.aviso').html('NECESSÁRIO INFORMAR JUSTIFICATIVA PARA CH MENOR QUE 9 HORAS OU CH MAIOR QUE 22 HORAS.');
                $('.justif').prop('required', true);
            } else {
                $('.bgAviso').addClass('hidden');
                $('.divJust').addClass('hidden');
                $('.justif').prop('required', false);
            }

        }
    });

    $('#fol_chAulaRegHorista').mask('99', {completed: function () {
            if (getTitulacao()) {
                var titulacao = getTitulacao();
                regime = $('#regime').val();
            } else {
                $("#regime option[value='']").attr('selected', true)
                return false;
            }

            if (titulacao == 'SUP_ESTAGIO') {

                var fol_chAulaRegHorista = $('#fol_chAulaRegHorista').val();
                var fol__chAulaHorista = (getValorHora(titulacao) * fol_chAulaRegHorista * 4).toFixed(2);

                var fol__dsrAulaHorAula = (fol__chAulaHorista / 6).toFixed(2);
                $('#fol__dsrAulaHorAula').val(fol__dsrAulaHorAula);

                var fol__chAulaHorista = (fol__chAulaHorista - fol__dsrAulaHorAula).toFixed(2);
                $('#fol__chAulaHorista').val(fol__chAulaHorista);

            } else {

                var fol_chAulaRegHorista = $('#fol_chAulaRegHorista').val();
                var fol__chAulaHorista = getValorHora(titulacao) * fol_chAulaRegHorista * 4.5;
                $('#fol__chAulaHorista').val(fol__chAulaHorista.toFixed(2));

                var fol__dsrAulaHorAula = (fol__chAulaHorista / 6).toFixed(2);
                $('#fol__dsrAulaHorAula').val(fol__dsrAulaHorAula);

            }
            //log(fol__chAulaHorista + ' - ' + fol__dsrAulaHorAula);
        }
    });



    function getTitulacao() {
        var titulacao = $('#titulacao').val();
        if (titulacao == '') {
            alert('INFORMA A TITULAÇÃO');
            //$('#ch').val('');
            return false;
        } else {
            //return $('#ch').val();
            return titulacao;
        }
    }

    function getValorHora(titulacao) {
        if (titulacao == 'GRADUADO') {
            return graduadoValorHora;
        } else if (titulacao == 'ESPECIALISTA') {
            return especialistaValorHora;
        } else if (titulacao == 'MESTRE') {
            return mestreValorHora;
        } else if (titulacao == 'DOUTOR') {
            return doutorValorHora;
        } else if (titulacao == 'SUP_ESTAGIO') {
            return sup_estagioValor;
        }
    }

    function getValor(titulacao) {
        if (titulacao == 'GRADUADO') {
            return graduadoValorIntegral;
        } else if (titulacao == 'ESPECIALISTA') {
            return especialistaValorIntegral;
        } else if (titulacao == 'MESTRE') {
            return mestreValorIntegral;
        } else if (titulacao == 'DOUTOR') {
            return doutorValorIntegral;
        } else if (titulacao == 'MESTRADO') {
            return mestradoValor;
        }
    }


    function log(texto) {
        console.log(texto);
    }

</script>
