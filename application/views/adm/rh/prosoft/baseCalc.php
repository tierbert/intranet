<div class="box"  style="">
    <?php //$this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            PROSOFT
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/rh/prosoft'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px; padding: 0">
    <div class="row" style="padding: 40px;">




        <div class="col-md-6">
            <table class='table table-bordered table-striped table-condensed table-hover'>
                <thead>
                    <tr>
                        <th>TITULAÇÃO</th>
                        <th>V. INTEGRAL</th>
                        <th>V. HORA</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($baseCalculo->result() as $dado) {
                        ?>
                        <tr>
                            <td><?php echo $dado->bc_titulacao ?> </td>
                            <td><?php echo $this->util->moeda2($dado->bc_valorRegIntegral) ?> </td>
                            <td><?php echo $this->util->moeda2($dado->bc_valorHoraAula) ?> </td>
                            <td>
                                <a href="<?php echo site_url('adm/rh/prosoft/baseCalcEdit/' . $dado->bc_id); ?>" class="btn btn-xs btn-primary">
                                    ALTERAR
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        


    </div>
</div>