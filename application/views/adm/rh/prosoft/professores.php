<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            PROSOFT / CADASTRO DE PROFESSORES
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/rh/prosoft'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px; padding: 0">
    <div class="row" style="padding: 40px;">

        <table class='table table-bordered table-striped table-condensed table-hover' id="tabela">
            <thead>
                <tr>
                    <th>COD</th>
                    <th>NOME</th>
                    <th>TITULAÇÃO</th>
                    <th>REGIME</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($professores->result() as $dado) {

                    $aula = $this->prosoftmodel->getAulasbyCod($dado->pro_idAula);
                    $divisao = $this->prosoftmodel->getDivisaobyCod($dado->pro_idDivisao);
                    ?>
                    <tr>
                        <td><?php echo $dado->pro_registroRh ?> </td>
                        <td><?php echo $dado->pro_nome ?> </td>
                        <td><?php echo $dado->pro_titulacao ?> </td>
                        <td><?php echo $dado->pro_regime ?> </td>
                        <td>
                            <a href="<?php echo site_url('adm/rh/prosoft/profEditar/' . $dado->pro_id); ?>" class="btn btn-xs btn-info ">
                                ALTERAR
                            </a>

                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/js/dataTables'); ?>/jquery.dataTables.js"></script>
<script>

    $('#tabela').DataTable(
            {
                paging: false,
                order: [[1, "asc"]]
            }
    );

</script>