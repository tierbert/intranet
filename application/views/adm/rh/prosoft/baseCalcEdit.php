<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            ALTERAR BASE DE CÁLCULO
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('adm/rh/prosoft/baseCalc'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 300px;">

    <div class=form-horizontal>    
        <div class="error">
            <?php echo validation_errors(); ?>
        </div>
        <?php echo form_open() ?>

     
        <?php //print_r($baseCalculo) ?>

        <table class="table table-bordered table-striped table-condensed table-hover" style="width: 50%">
            <thead>
                <tr>
                    <th>TITULAÇÃO</th>
                    <th>V. INTEGRAL</th>
                    <th>V. HORA</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>GRADUADO </td>
                    <td>
                        <input value="<?php echo $baseCalculo['graduado_integral']; ?>" name="graduado_integral" type="text" class="form-control">
                    </td>
                    <td>
                        <input value="<?php echo $baseCalculo['graduado_hora']; ?>" name="graduado_hora" type="text" class="form-control">
                    </td>
                </tr>
                <tr>
                    <td>ESPECIALISTA </td>
                    <td>
                        <input value="<?php echo $baseCalculo['especialista_integral']; ?>" name="especialista_integral" type="text" class="form-control">
                    </td>
                    <td><input value="<?php echo $baseCalculo['especialista_hora']; ?>" name="especialista_hora" type="text" class="form-control">
                    </td>

                </tr>
                <tr>
                    <td>MESTRE </td>
                    <td><input value="<?php echo $baseCalculo['mestre_integral']; ?>" name="mestre_integral" type="text" class="form-control"> </td>
                    <td><input value="<?php echo $baseCalculo['mestre_hora']; ?>" name="mestre_hora" type="text" class="form-control"></td>
                </tr>
                <tr>
                    <td>DOUTOR </td>
                    <td><input value="<?php echo $baseCalculo['doutor_integral']; ?>" name="doutor_integral" type="text" class="form-control"> </td>
                    <td><input value="<?php echo $baseCalculo['doutor_hora']; ?>" name="doutor_hora" type="text" class="form-control"> </td>
                </tr>
            </tbody>
        </table>

        <div class=form-group>
            <div class="col-md-2">
                <label>&nbsp;</label>
                <input type=submit  value=Salvar class='btn btn-success form-control'>
            </div>

        </div>   

        <?php echo form_close() ?>
    </div>
</div>
<?php $this->util->mascaras(); ?>


<script>
    $('.competencia').mask('99/9999');
</script>
<style>
    .error p{
        color: white;
        background-color: red;
        padding: 5px;
        margin-bottom: 5px;
    }
</style>