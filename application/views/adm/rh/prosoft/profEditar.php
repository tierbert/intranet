<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            PROSOFT
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/rh/prosoft/professores'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px; padding: 0">
    <div class="row" style="padding: 40px;">
        <div class=form-horizontal>    
            <div>
                <?php echo validation_errors(); ?>
            </div>

            <?php echo form_open() ?>
            <div class="form-group">
                <?php $this->util->input2('Nome', 'pro_nome', $object->pro_nome); ?>
                <?php
                $status = array(
                    '' => '',
                    'ATIVO' => 'ATIVO',
                    'INATIVO' => 'INATIVO',
                );
                $this->util->select2('STATUS', 'pro_status', $status, $object->pro_status);
                ?>
            </div>
            <hr>


            <?php
            $titulacao = array(
                '' => '',
                "GRADUADO" => 'GRADUADO',
                "ESPECIALISTA" => 'ESPECIALISTA',
                "MESTRE" => 'MESTRE',
                "DOUTOR" => 'DOUTOR'
            );

            $regime = array(
                '' => '',
                "INTEGRAL" => 'INTEGRAL',
                "PARCIAL_PROPORCIONAL" => 'PARCIAL_PROPORCIONAL',
                "PARCIAL_REGULAR" => 'PARCIAL_REGULAR',
                "HORISTA" => 'HORISTA'
            );
            ?>

            <div class="form-group">
                <?php $this->util->select2('titulação', 'pro_titulacao', $titulacao, $object->pro_titulacao); ?>
                <?php $this->util->select2('regime', 'pro_regime', $regime, $object->pro_regime); ?>
            </div>

            <br>
            <div class="form-group">
                <?php $this->util->select2('AULAS', 'pro_idAula', $aulas, $object->pro_idAula); ?>
                <?php $this->util->select2('DIVISÃO', 'pro_idDivisao', $divisoes, $object->pro_idDivisao); ?>
            </div>

            <div class=form-group>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <input type=submit  value=Salvar class='btn btn-success form-control'>
                </div>

            </div>            

            <?php echo form_close() ?>
        </div>
