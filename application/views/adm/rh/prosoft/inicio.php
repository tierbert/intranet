<div class="box"  style="">
    <?php //$this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            PROSOFT
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px; padding: 0">
    <div class="row" style="padding: 40px;">
        <div class="col-md-6">
            <table class='table table-bordered table-striped table-condensed table-hover'>
                <thead>
                    <tr>
                        <th>COMPETÊNCIA</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($competencias->result() as $dado) {
                        ?>
                        <tr>
                            <td><?php echo $dado->com_competencia ?> </td>

                            <td>
                                <a href="<?php echo site_url('adm/rh/prosoft/competenciaEdit/' . $dado->com_id); ?>" class="btn btn-xs btn-primary">
                                    CONFERIR
                                </a>
                                <a href="<?php echo site_url('adm/rh/prosoft/compCad/' . $dado->com_id); ?>" class="btn btn-xs btn-danger" title="REPETIR">
                                    <span class="glyphicon glyphicon-repeat"></span>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-6">

            <div class="row">

                <a href="<?php echo site_url('adm/rh/prosoft/compCad'); ?>">
                    <div class="col-md-3 text-center">
                        <span style="font-size: 40px;" class="glyphicon glyphicon-plus"></span>
                        <br>
                        NOVA <br>COMPETÊNCIA
                    </div>
                </a>

                <a href="<?php echo site_url('adm/rh/prosoft/professores'); ?>">
                    <div class="col-md-3 text-center">
                        <span style="font-size: 40px;" class="glyphicon glyphicon-blackboard"></span>
                        <br>
                        CADASTRO DE 
                        PROFESSORES
                    </div>
                </a>

                <a href="<?php echo site_url('adm/rh/prosoft/baseCalc'); ?>">
                    <div class="col-md-3 text-center">
                        <span style="font-size: 40px;" class="glyphicon glyphicon-alert"></span>
                        <br>
                        BASE DE CÁLCULO
                    </div>
                </a>

                <a href="<?php echo site_url('adm/rh/prosoft/calcular/'); ?>">
                    <div class="col-md-2 text-center">
                        <span style="font-size: 40px;" class="glyphicon glyphicon-phone"></span>
                        <br>
                        CALCULADORA
                    </div>
                </a>
            </div>
            <hr style="border: 1px solid #337ab7;">
            <div class="row">
            </div>
        </div>
    </div>
</div>
<?php
if ($cadastrado == 1) {
    $this->util->mensagem('COMPETENCIA CADASTRADA');
}
?>