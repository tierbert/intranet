
<h2>Editando Cadastro</h2>
<hr>
<div class=form-horizontal>    
	<div>
		<?php echo validation_errors(); ?>
	</div>

	<?php echo form_open() ?>
	<?php $this->util->input('ap_idUsuario', 'ap_idUsuario', $object->ap_idUsuario); ?>
	<?php $this->util->input('ap_idProfessor', 'ap_idProfessor', $object->ap_idProfessor); ?>
	<?php $this->util->input('ap_dataAjuste', 'ap_dataAjuste', $object->ap_dataAjuste); ?>
	<?php $this->util->input('ap_correcao', 'ap_correcao', $object->ap_correcao); ?>
	<?php $this->util->input('ap_justificativa', 'ap_justificativa', $object->ap_justificativa); ?>
	<?php $this->util->input('ap_coordParecer', 'ap_coordParecer', $object->ap_coordParecer); ?>
	<?php $this->util->input('ap_coordObservacoes', 'ap_coordObservacoes', $object->ap_coordObservacoes); ?>
	<?php $this->util->input('ap_rhParecer', 'ap_rhParecer', $object->ap_rhParecer); ?>
	
	
	<div class=form-group>
		<input type=submit  value=Salvar class='btn btn-primary'>
	</div>            
	<?php echo form_close() ?>
	<div>
