<br><br>
<div class="container">
	<div class="row">
		<nav class="navbar ">

			<!--<a class='btn btn-primary2 pull-right' href='<?php echo site_url('adm/rh/AjustePonto') ?>'>VOLTAR</a> --> 
			<br><h4 style="text-align: center;">NOVO AJUSTE DE PONTO</h4>  
			<!-- COLOCAR BOTÃO E TEXTO -->       

			<!-- Barra Menu (não coloquei nada.. dexei somente pra centralizar o conteudo da principal-->   
		</nav>

		<div class="container-fluid text-center">    
			<div class="row content">
				<div class="col-sm-1 sidenav">   
					<!-- Barra Lateral (não coloquei nada.. dexei somente pra centralizar o conteudo da principal-->   
				</div>
				<div class="col-sm-10 form-horizontal">
					<div>
						<?php echo validation_errors(); ?>
					</div>
					<?php echo form_open() ?>
					<?php $this->util->input('DATA AJUSTE', 'ap_dataAjuste', $object->ap_dataAjuste, 'data popupDatepicker', 2,'','required'); ?>
					<?php $this->util->input('CORREÇÃO', 'ap_correcao', $object->ap_correcao, '', 6,'','required'); ?>
					<div class="form-group">
						<label for="textArea" class="col-lg-2 control-label required">JUSTIFICATIVA:</label>
						<div class="col-lg-6">
							<textarea class="form-control" rows="3" id="textArea" name="ap_justificativa"></textarea>
							<span class="help-block"></span>
						</div>
					</div>
					<div class=form-group>
						<input type=submit  value=Salvar class='btn btn-success pull-right'>
					</div>            
					<?php echo form_close() ?>
				</div>
				<?php echo $this->util->mascaras(); ?>     
				<footer class="site-footer">
					<br>
				</footer>
				<div class="col-sm-1 sidenav">
					<!-- Barra Lateral (não coloquei nada.. dexei somente pra centralizar o conteudo da principal-->
				</div>
			</div>
		</div>
	</div>
</div>
<link href="<?php echo base_url('assets'); ?>/js/datepick5/css/redmond.datepick.css" rel="stylesheet">
<script src="<?php echo base_url('assets'); ?>/js/datepick5/js/jquery.plugin.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/datepick5/js/jquery.datepick.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/datepick5/js/jquery.datepick-pt.js"></script>
<script>
    $(function () {
        $('.popupDatepicker').datepick();/* calendario data*/
    });
</script>