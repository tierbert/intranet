<br><br>
<div class="container">
    <div class="row">
        <nav class="navbar ">

                        <!--<a class='btn btn-primary2 pull-right' href='<?php echo site_url('adm/rh/AjustePonto') ?>'>VOLTAR</a> --> 
            <h4 style="text-align: center;">AJUSTE DE PONTO</h4> 
            <!-- COLOCAR BOTÃO E TEXTO -->       

            <!-- Barra Menu (não coloquei nada.. dexei somente pra centralizar o conteudo da principal-->   
        </nav>

        <div class="container-fluid ">    
            <div class="row content">

                <div class="form-horizontal">
                    <div>
                        <?php echo validation_errors(); ?>
                    </div>
                    <div class="form-group">
                        <?php $this->util->input2('DATA AJUSTE', 'ap_dataAjuste', $object->ap_dataAjuste, 'data', 2, '', 'disabled'); ?>
                        <?php $this->util->input2('CORREÇÃO', 'ap_correcao', $object->ap_correcao, '', 4, '', 'disabled'); ?>
                        <div class="col-md-6">
                            <label for="textArea" class="">JUSTIFICATIVA:</label>
                            <textarea class="form-control" rows="2" id="textArea" name="ap_justificativa" disabled=""><?php echo $object->ap_justificativa; ?></textarea>
                            <span class="help-block"></span>
                        </div>
                    </div>

                </div>
                <br>
                <h4 style="text-align: center;">PARECER COORDENADOR</h4>
                <?php echo form_open() ?>
                <div class="form-horizontal">

                    <div class="form-group">
                        <?php $this->util->select2('PARECER', 'ap_coordParecer', array('' => '', 'DEFERIDO' => 'DEFERIDO', 'INDEFERIDO' => 'INDEFERIDO'), $object->ap_coordParecer, '', 3); ?>
                        <div class="col-lg-6">
                            <label for="" class="">JUSTIFICATIVA</label>
                            <textarea class="form-control justificativa" name="ap_coordObservacoes" required><?php echo $object->ap_coordObservacoes; ?></textarea>
                        </div>
                    </div>
                    <div class=form-group>
                        <input type=submit  value=Salvar class='btn btn-success pull-right'>
                    </div>  
                </div>
                <?php echo form_close() ?>
            </div>
            <?php echo $this->util->mascaras(); ?>     
            <footer class="site-footer">
                <br>
            </footer>
            <div class="col-sm-1 sidenav">
                <!-- Barra Lateral (não coloquei nada.. dexei somente pra centralizar o conteudo da principal-->
            </div>
        </div>
    </div>
</div>
