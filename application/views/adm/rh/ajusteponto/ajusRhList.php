<br><br>
<nav class="navbar">
  <div class="container-fluid">    
    <h4 style="text-align: center;">AJUSTE DE PONTO</h4>       
  </div>
</nav>

<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-12 text-left">
      <div class="checkout-wrap">
        <ul class="checkout-bar">

              <li class="visited first"> <a href="#">SOLICITAÇÃO</a> </li>

              <li class="previous visited">MENU</li>

              <li class="active">MENU</li>    

              <li class="Complete">MENU</li> 

        </ul>
      </div>
    </div>
    <div class="col-sm-12 text-left">
      <br><br><br><br><br>

      <?php if ($dados->num_rows() > 0) { ?>

          <div class="col-sm-3"><b>DATA AJUSTE</div>
            <div class="col-sm-3">CORREÇÃO</div>
            <div class="col-sm-3">JUSTIFICATIVA</div>
            <div class="col-sm-3">OPÇÕES</b></div><br><br>

        <?php foreach ($dados->result() as $dado) {
          ?>
              <div class="col-sm-3"><?php echo $this->util->data2($dado->ap_dataAjuste) ?> </div>
              <div class="col-sm-3" style="text-transform: uppercase;"><?php echo $dado->ap_correcao ?> </div>
              <div class="col-sm-3" style="text-transform: uppercase;"><?php echo $dado->ap_justificativa ?> </div>

          <div class="col-sm-3"><a class='btn btn7 btn-danger' href="<?php echo site_url( 'adm/rh/ajustePonto/ajusRhEdit/' . $dado->ap_id) ?>">
                  PARECER
                </a>  </div> <br><br>
            <?php } ?>
          </div>
          <?php } else { ?>
          <h4 style="text-align: center;">NENHUM REGISTRO ENCONTRADO</h4>
          <?php } ?>
        </div>
      </div>
      <footer class="container-fluid text-center">  
        <a class='btn btn-primary2 pull-left' href='<?php echo site_url('adm/rh/Iniciorh') ?>'>VOLTAR</a>
      </footer>
      <hr>
    </div>
  </div>