<br><br>
<nav class="navbar">
    <div class="container-fluid">          
        <?php echo validation_errors(); ?>
        <h4 style="text-align: center;">MINHAS SOLICITAÇÕES</h4>
    </div>
</nav>

<div class="container-fluid text-center">    
    <div class="row content">
        <div class="col-sm-12 text-left">
            <h3 style="text-align: center;">ACOMPANHE AQUI SEU PROTOCOLO</h3>
            <div class="checkout-wrap">
                <ul class="checkout-bar">

                    <li class="visited first"> <a href="#">SOLICITAÇÃO</a> </li>

                    <li class="active"> <a href="#">COORDENADOR</a></li>

                    <li class="">RECURSOS HUMANOS</li>

                    <li class="">CONCLUIR</li>    
                </ul>
            </div>
        </div>

        <div class="col-sm-12 text-left">
            <br> <br> <br> <br> <br>

            <div class="col-sm-7">
                <img class="img-responsive" src="../../../../../assets/tema/images/rh/coord.jpg" alt=""/>          
            </div>

            <div class="col-sm-5">

                <h4>PARECER COORDENADOR</h4> <br>
                <?php if (count($object) != 0) { ?>           
                    <?php // echo $object->usu_nome?>

                    <b> STATUS DA SOLICITAÇÃO:</b><br>
                    <?php
                    if ($object->ap_coordParecer == null) {
                        echo "AGUARDANDO PARECER<br>";
                    } else {
                        ?>
        <?php echo $object->ap_coordParecer ?><br>

                        <b> MOTIVO: <br></b>
                        <p><?php echo $object->ap_coordObservacoes ?> </p> 
                    <?php } ?>


                <?php } ?>
                <?php ?>
<?php //} 
?>          
            </div>
        </div>   


        <footer class="container-fluid text-center">
            
            <a class='btn btn-primary2 pull-left' href='<?php echo site_url('adm/rh/AjustePonto/') ?>'>VOLTAR</a>
            <?php if ($object->ap_coordParecer == 'DEFERIDO') { ?>
                <a class='btn btn-info pull-right' href='<?php echo site_url('adm/rh/AjustePonto/ajusteParecRh/' . $object->ap_id) ?>'>PRÓXIMO</a>   
<?php } ?>
        </footer>
        <hr>
    </div>
</div>