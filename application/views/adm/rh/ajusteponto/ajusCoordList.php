<br><br>
<nav class="navbar">
    <div class="container-fluid">    
        <h4 style="text-align: center;">AJUSTE DE PONTO</h4>             
    </div>
</nav>

<div class="col-sm-12 text-left">
    <br><br>

    <?php if ($dados->num_rows() > 0) { ?>

        <div class="col-sm-2"><b>DATA AJUSTE</div>
        <div class="col-sm-3">CORREÇÃO</div>
        <div class="col-sm-3">JUSTIFICATIVA</div>
        <div class="col-sm-3">NOME</div>
        <div class="col-sm-3">SETOR</div>
        <div class="col-sm-3">OPÇÕES</b></div>
        <br><br>

        <?php foreach ($dados->result() as $dado) {
            ?>
            <div class="col-sm-2"><?php echo $this->util->data2($dado->ap_dataAjuste) ?> </div>
            <div class="col-sm-3" style="text-transform:uppercase"><?php echo $dado->ap_correcao ?> </div>
            <div class="col-sm-3" style="text-transform:uppercase"><?php echo $dado->ap_justificativa ?> </div>
            <div class="col-sm-3" style="text-transform:uppercase"><?php echo $dado->usu_nome ?> </div>
            <div class="col-sm-3" style="text-transform:uppercase"><?php echo $dado->set_nome ?> </div>
            <div class="col-sm-3">
                <a class='btn btn7 btn-danger' href="<?php echo site_url('adm/rh/ajustePonto/ajusCoordEdit/' . $dado->ap_id) ?>">
                    PARECER
                </a> 
            </div>
            <br><br>
        <?php } ?>
    </div>
<?php } else { ?>
    <h4 style="text-align: center;">NENHUM REGISTRO ENCONTRADO</h4>
<?php } ?>
</div>
</div>
<footer class="container-fluid text-center">  
    <a class='btn btn-primary2 pull-left' href='<?php echo site_url('adm/rh/Iniciorh') ?>'>VOLTAR</a>
</footer>
<hr>