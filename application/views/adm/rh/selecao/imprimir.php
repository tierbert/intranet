<html>
    <head>
        <title>Comprovante De Solicitação</title>
        <meta content="">
        <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css/bootstrap.min337.css">
        <link href="<?php echo base_url('assets/tema/'); ?>/css/style.css" rel="stylesheet"> 
        <style>
            .table1 { 
                border-left: 1px solid #000;
                border-top: 1px solid #000;
                border-right: 1px solid #000;
                border-bottom: 1px solid #000;
            }
            .negrito{
                font-weight: bold;
            }

            label{
                font-weight: bold;
            }

            .texto{
                font-weight: normal;
            }

        </style>
    </head>
    <body style="text-transform: uppercase">

        <table class="table1" style="margin-top: 70px; width: 100%">
            <tr>
                <td>
                    <img style="width: 10%; padding: 10px;" src="<?php echo base_url('assets/tema/images/logo.jpg'); ?>" >

                </td>
                <td class="text-center" style="font-weight: bold; font-size: 10px;">

                    CESG - CENTRO DE EDUCAÇÃO SUPERIOR DE <br>
                    GUANAMBI S/C FACULDADE GUANAMBI
                    <br>
                    CNPJ: 04.097.860/0001-46

                    <br><br>

                    <?php
                    date_default_timezone_set('America/Bahia');
                    $data = Date('d/m/Y h:i:s');
                    ?>

                    EMITIDO EM: <?php echo $data; ?>
                </td>
                <td style="width: 180px;">
                    &nbsp;</td>
            </tr>
        </table>










        <div class="painel" style="min-height: 400px;">
            <div class="row">
                <div class="form-horizonta">
                    <br>
                    <h3 class="text-center">SELEÇÃO DE DOCENTES</h3>
                    <hr>
                    <h4>DADOS PESSOAIS</h4>
                    <br>


                    <div class="form-group">
                        <div class="col-xs-2 titulo" style="font-weight: bold;">
                            NOME:
                        </div>
                        <div class="col-md-5">
                            <?php echo mb_strtoupper($dados->sp_nome); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-2 titulo" style="font-weight: bold; ">
                            email:
                        </div>
                        <div class="col-md-5">
                            <?php echo mb_strtoupper($dados->sp_email); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-2 titulo" style="font-weight: bold; ">
                            lograd.:
                        </div>
                        <div class="col-xs-3">
                            <?php echo mb_strtoupper($dados->sp_logradouro); ?>
                        </div>
                        <div class="col-xs-2 titulo" style="font-weight: bold; ">
                            numero.:
                        </div>
                        <div class="col-xd-2">
                            <?php echo mb_strtoupper($dados->sp_numero); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2 titulo" style="font-weight: bold; ">
                            bairro:
                        </div>
                        <div class="col-xs-3">
                            <?php echo mb_strtoupper($dados->sp_bairro); ?>
                        </div>
                        <div class="col-xs-2 titulo" style="font-weight: bold; ">
                            complem.:
                        </div>
                        <div class="col-xs-2">
                            <?php echo mb_strtoupper($dados->sp_complemento); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2 titulo" style="font-weight: bold; ">
                            cidade:
                        </div>
                        <div class="col-xs-3">
                            <?php echo mb_strtoupper($dados->sp_cidade); ?>
                        </div>
                        <div class="col-xs-2 titulo" style="font-weight: bold; ">
                            ESTADO.:
                        </div>
                        <div class="col-xs-2">
                            <?php echo mb_strtoupper($dados->sp_estado); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-2 titulo" style="font-weight: bold; ">
                            telefone:
                        </div>
                        <div class="col-xs-3">
                            <?php echo mb_strtoupper($dados->sp_telefone); ?>

                        </div>
                    </div>


                    <hr>
                    <h4>FORMAÇÃO</h4>
                    <hr>
                    <h5><i>GRADUAÇÃO</i></h5>
                    <hr>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label>GRADUAÇÃO EM: <?php echo mb_strtoupper($dados->sp_graduacaoCurso); ?></label>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-5">
                            <label>InÍcio EM: <?php echo $this->util->data2($dados->sp_graduacaoInicio); ?></label>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-5">
                            <label>CONCLUÍDA EM: <?php echo $this->util->data2($dados->sp_graduacaoFim); ?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Instituicao DE ENSINO: <?php echo mb_strtoupper($dados->sp_graduacaoInstituicao); ?></label>
                        </div>
                    </div>
                    <hr>
                    <h5><i>ESPECIALIZAÇÃO</i></h5>
                    <hr>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label>ESPECIALIZAÇÃO EM:</label>
                            <?php echo mb_strtoupper($dados->sp_especializacaoCurso); ?>

                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-5">
                            <label>InÍcio EM: <?php echo $this->util->data2($dados->sp_especializacaoInicio); ?></label>


                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-5 ">
                            <label>CONCLUÍDA EM: <?php echo $this->util->data2($dados->sp_especializacaoFim); ?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>TEMA: <?php echo mb_strtoupper($dados->sp_especializacaoTema); ?></label> 
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Instituicao DE ENSINO: <?php echo mb_strtoupper($dados->sp_especializacaoInstituicao); ?></label>
                        </div>
                    </div>
                    <hr>
                    <h5><i>MESTRADO</i></h5>
                    <hr>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label>MESTRADO EM: <?php echo mb_strtoupper($dados->sp_mestradoCurso); ?></label>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-5">
                            <label>InÍcio EM: <?php echo $this->util->data2($dados->sp_mestradoInicio); ?></label>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-5">
                            <label>CONCLUÍDO EM: <?php echo $this->util->data2($dados->sp_mestradoFim); ?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>TEMA: </label>
                            <br>
                            <?php echo mb_strtoupper($dados->sp_mestratoTema); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Instituicao DE ENSINO: </label>
                            <br>
                            <?php echo mb_strtoupper($dados->sp_mestradoInstituicao); ?>
                        </div>
                    </div>
                    <hr>
                    <h5><i>DOUTORADO</i></h5>
                    <hr>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label>DOUTORADO EM: </label>
                            <br>
                            <?php echo mb_strtoupper($dados->sp_doutoradoCurso); ?>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-5">
                            <label>InÍcio EM: </label>
                            <?php echo $this->util->data2($dados->sp_doutoradoInicio); ?>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-5">
                            <label>CONCLUÍDO EM: </label>
                            <?php echo $this->util->data2($dados->sp_doutoradoFim); ?>
                        </div>
                        <div class="col-md-12">
                            <label>TEMA: </label>
                            <?php echo mb_strtoupper($dados->sp_doutoradoTema); ?>
                        </div>
                        <br>
                        <div class="col-md-12">
                            <label>Instituicao DE ENSINO: </label>

                            <?php echo mb_strtoupper($dados->sp_doutoradoInstituicao); ?>
                        </div>
                    </div>

                    <hr>
                    <h5><i>RESUMO DA FORMAÇÃO</i></h5>
                    <br>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Resumo de formação: </label>
                            <br>
                            <?php echo mb_strtoupper($dados->sp_resumoFormacao); ?>
                        </div>
                    </div>

                    <hr>
                    <h5><i>Habilidades e competências – disciplinas </i></h5>
                    <br>

                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Disciplinas:</label>
                            <br>
                            <?php echo mb_strtoupper($dados->sp_habilidadeDisciplinas); ?>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>DISPONIBILIDADE:</label>
                            <br>
                            <?php echo mb_strtoupper($dados->sp_habilidadeDisponibilidade); ?>
                        </div>
                    </div>

                    <hr>
                    <h5><i>Experiência </i></h5>
                    <br>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Experiência:</label>
                            <br>
                            <?php echo mb_strtoupper($dados->sp_habilidadeExperiencias); ?>
                        </div>
                    </div>

                    <hr>
                    <h5><i>PRODUÇÃO ACADÊMICA (quando aplicável) </i></h5>
                    <hr>

                    <div class="form-group" style="margin-top: 15px;">
                        <div class="col-md-12">
                            <label>ARTIGOS PUBLICADOS EM REVISTAS INDEXADAS:</label>
                            <br>
                            <?php echo mb_strtoupper($dados->sp_producaoArtigos); ?>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 15px;">
                        <div class="col-md-12">
                            <label>CAPÍTULOS DE LIVROS:</label>
                            <br>
                            <?php echo mb_strtoupper($dados->sp_producaoCapitulosLivros); ?>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 15px;">
                        <div class="col-md-12">
                            <label>LIVROS: </label>
                            <textarea style="height: 100px;" class="form-control" disabled name="sp_producaoLiros" placeholder="[Título do livro, ano, número e páginas do livro]"></textarea>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 15px;">
                        <div class="col-md-12">
                            <label>TEXTOS COMPLETOS PUBLICADOS EM ANAIS DE EVENTOS: </label>
                            <br>
                            <?php echo mb_strtoupper($dados->sp_producaoTextoAnais); ?>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 15px;">
                        <div class="col-md-12">
                            <label>RESUMOS PUBLICADOS EM ANAIS DE EVENTOS INTERNACIONAIS:</label>
                            <br>
                            <?php echo mb_strtoupper($dados->sp_producaoResumoAnais); ?>
                        </div>
                    </div>


                    <div class="form-group" style="margin-top: 15px;">
                        <div class="col-md-12">
                            <label>DEMAIS PRODUÇÕES:</label>
                            <br>
                            <?php echo mb_strtoupper($dados->sp_producaoDemaisProducoes); ?>
                        </div>
                    </div>
                </div>
                <?php $this->util->mascaras() ?>
            </div>
        </div>
    </body>
</html>