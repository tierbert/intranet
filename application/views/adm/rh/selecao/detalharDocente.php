<div class="box hidden-print"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            SELEÇÃO RH - DOCENTE
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/rh/selecao'); ?>" class=" btn btn-primary hidden-print">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class="row">





        <div class="form-horizontal">
            <h3 class="text-center">SELEÇÃO DOCENTES</h3>
            <hr>
            <h4>DADOS PESSOAIS</h4>
            <br>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label>nome</label>
                    <input type="text" value="<?php echo mb_strtoupper($dados->sp_nome); ?>" disabled class="form-control" name="sp_nome" required="true">
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label>email</label>
                    <input value="<?php echo mb_strtoupper($dados->sp_email); ?>" type="email" disabled class="form-control" name="sp_email" required="true">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <label>telefone</label>
                    <input value="<?php echo mb_strtoupper($dados->sp_telefone); ?>"  type="text" disabled class="form-control telefone" name="sp_telefone" required="true">
                </div>
                <div class="col-md-5 col-sm-5 col-xs-6">
                    <label>logradouro</label>
                    <input value="<?php echo mb_strtoupper($dados->sp_logradouro); ?>" type="text" disabled class="form-control" name="sp_logradouro" required="true">
                </div>

                <div class="col-md-2 col-sm-2 col-xs-6">
                    <label>numero</label>
                    <input value="<?php echo mb_strtoupper($dados->sp_numero); ?>" type="text" disabled class="form-control" name="sp_numero" required="true">
                </div>
                <div class="col-md-2 col-sm-2 col-xs-6">
                    <label>bairro</label>
                    <input value="<?php echo mb_strtoupper($dados->sp_bairro); ?>" type="text" disabled class="form-control" name="sp_bairro" required="true">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label>complemento</label>
                    <input value="<?php echo mb_strtoupper($dados->sp_complemento); ?>" type="text" disabled class="form-control" name="sp_complemento">
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6">
                    <label>cidade</label>
                    <input type="text" value="<?php echo mb_strtoupper($dados->sp_cidade); ?>" disabled class="form-control" name="sp_cidade" required="true">
                </div>
                <div class="col-md-2 col-sm-2 col-xs-6">
                    <label>ESTADO</label>
                    <input type="text" value="<?php echo mb_strtoupper($dados->sp_estado); ?>" disabled class="form-control" name="sp_estado" required="true">
                </div>
            </div>
            <hr>
            <h4>FORMAÇÃO</h4>
            <br>
            <h5><i>GRADUAÇÃO</i></h5>
            <br>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label>GRADUAÇÃO EM:</label>
                    <input value="<?php echo mb_strtoupper($dados->sp_graduacaoCurso); ?>" disabled type="text" class="form-control" name="sp_graduacaoCurso" required="true">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <label>InÍcio EM</label>
                    <input type="text" value="<?php echo $this->util->data2($dados->sp_graduacaoInicio); ?>"  disabled class="form-control data" name="sp_graduacaoInicio" required="true">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <label>CONCLUÍDA EM</label>
                    <input type="text" value="<?php echo $this->util->data2($dados->sp_graduacaoFim); ?>" disabled class="form-control data" name="sp_graduacaoFim" required="true">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label>Instituicao DE ENSINO</label>
                    <input type="text" value="<?php echo mb_strtoupper($dados->sp_graduacaoInstituicao); ?>" disabled class="form-control" disabled name="sp_graduacaoInstituicao" required="true">
                </div>
            </div>
            <br>
            <h5><i>ESPECIALIZAÇÃO</i></h5>
            <br>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label>ESPECIALIZAÇÃO EM</label>
                    <input type="text" value="<?php echo mb_strtoupper($dados->sp_especializacaoCurso); ?>" disabled class="form-control" name="sp_especializacaoCurso">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <label>InÍcio EM</label>
                    <input type="text" value="<?php echo $this->util->data2($dados->sp_especializacaoInicio); ?>" disabled class="form-control data" name="sp_especializacaoInicio">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6 ">
                    <label>CONCLUÍDA EM</label>
                    <input type="text" value="<?php echo $this->util->data2($dados->sp_especializacaoFim); ?>" disabled class="form-control data" name="sp_especializacaoFim">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label>TEMA</label> 
                    <input type="text" class="form-control"  value="<?php echo mb_strtoupper($dados->sp_especializacaoTema); ?>" disabled name="sp_especializacaoTema">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label>Instituicao DE ENSINO</label>
                    <input type="text" value="<?php echo mb_strtoupper($dados->sp_especializacaoInstituicao); ?>" disabled class="form-control" name="sp_especializacaoInstituicao">
                </div>
            </div>
            <br>
            <h5><i>MESTRADO</i></h5>
            <br>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label>MESTRADO EM</label>
                    <input type="text" class="form-control" name="sp_mestradoCurso" disabled value="<?php echo mb_strtoupper($dados->sp_mestradoCurso); ?>" >
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <label>InÍcio EM</label>
                    <input type="text" class="form-control data" name="sp_mestradoInicio" disabled value="<?php echo $this->util->data2($dados->sp_mestradoInicio); ?>" >
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <label>CONCLUÍDO EM</label>
                    <input type="text" class="form-control data" name="sp_mestradoFim" disabled value="<?php echo $this->util->data2($dados->sp_mestradoFim); ?>" >
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label>TEMA</label>
                    <input type="text" class="form-control" name="sp_mestratoTema" disabled value="<?php echo mb_strtoupper($dados->sp_mestratoTema); ?>" >
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label>Instituicao DE ENSINO</label>
                    <input type="text" class="form-control" name="sp_mestradoInstituicao" disabled value="<?php echo mb_strtoupper($dados->sp_mestradoInstituicao); ?>" >
                </div>
            </div>
            <br>
            <h5><i>DOUTORADO</i></h5>
            <br>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label>DOUTORADO EM</label>
                    <input type="text" class="form-control" name="sp_doutoradoCurso" disabled value="<?php echo mb_strtoupper($dados->sp_doutoradoCurso); ?>" >
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <label>InÍcio EM</label>
                    <input type="text" class="form-control data" name="sp_doutoradoInicio" disabled value="<?php echo $this->util->data2($dados->sp_doutoradoInicio); ?>">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <label>CONCLUÍDO EM</label>
                    <input type="text" class="form-control data" name="sp_doutoradoFim" disabled value="<?php echo $this->util->data2($dados->sp_doutoradoFim); ?>">
                </div>
                <div class="col-md-12">
                    <label>TEMA</label>
                    <input type="text" class="form-control" name="sp_doutoradoTema" disabled value="<?php echo mb_strtoupper($dados->sp_doutoradoTema); ?>" >
                </div>
                <div class="col-md-12">
                    <label>Instituicao DE ENSINO</label>
                    <input type="text" class="form-control" name="sp_doutoradoInstituicao" disabled value="<?php echo mb_strtoupper($dados->sp_doutoradoInstituicao); ?>" >
                </div>
            </div>

            <br>
            <h5><i>RESUMO DA FORMAÇÃO</i></h5>
            <br>
            <div class="form-group">
                <div class="col-md-12">
                    <label>Resumo de formação</label>
                    <div style="height: 100px;" class="form-control" disabled name="sp_resumoFormacao" ><?php echo mb_strtoupper($dados->sp_resumoFormacao); ?></div>
                </div>
            </div>

            <br>
            <h5><i>Habilidades e competências – disciplinas </i></h5>
            <br>

            <div class="form-group">
                <div class="col-md-12">
                    <label>Disciplinas</label>
                    <div style="height: 100px;" class="form-control" disabled name="sp_habilidadeDisciplinas"><?php echo mb_strtoupper($dados->sp_habilidadeDisciplinas); ?></div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <label>DISPONIBILIDADE</label>
                    <div style="height: 100px;" class="form-control" disabled name="sp_habilidadeDisponibilidade" ><?php echo mb_strtoupper($dados->sp_habilidadeDisponibilidade); ?></div>
                </div>
            </div>

            <br>
            <h5><i>Experiência </i></h5>
            <br>
            <div class="form-group">
                <div class="col-md-12">
                    <label>Experiência</label>
                    <div style="height: 100px;" class="form-control" disabled name="sp_habilidadeExperiencias" ><?php echo mb_strtoupper($dados->sp_habilidadeExperiencias); ?></div>
                </div>
            </div>

            <br>
            <h5><i>PRODUÇÃO ACADÊMICA (quando aplicável) </i></h5>
            <br>

            <div class="form-group" style="margin-top: 15px;">
                <div class="col-md-12">
                    <label>ARTIGOS PUBLICADOS EM REVISTAS INDEXADAS</label>
                    <div style="height: 100px;" class="form-control" disabled name="sp_producaoArtigos"><?php echo mb_strtoupper($dados->sp_producaoArtigos); ?></div>
                </div>
            </div>
            <div class="form-group" style="margin-top: 15px;">
                <div class="col-md-12">
                    <label>CAPÍTULOS DE LIVROS</label>
                    <div style="height: 100px;" class="form-control" disabled name="sp_producaoCapitulosLivros"><?php echo mb_strtoupper($dados->sp_producaoCapitulosLivros); ?></div>
                </div>
            </div>
            <div class="form-group" style="margin-top: 15px;">
                <div class="col-md-12">
                    <label>LIVROS</label>
                    <div style="height: 100px;" class="form-control" disabled name="sp_producaoLiros" placeholder="[Título do livro, ano, número e páginas do livro]"></div>
                </div>
            </div>
            <div class="form-group" style="margin-top: 15px;">
                <div class="col-md-12">
                    <label>TEXTOS COMPLETOS PUBLICADOS EM ANAIS DE EVENTOS</label>
                    <div style="height: 100px;" class="form-control" disabled name="sp_producaoTextoAnais"><?php echo mb_strtoupper($dados->sp_producaoTextoAnais); ?></div>
                </div>
            </div>
            <div class="form-group" style="margin-top: 15px;">
                <div class="col-md-12">
                    <label>RESUMOS PUBLICADOS EM ANAIS DE EVENTOS INTERNACIONAIS</label>
                    <div style="height: 100px;" class="form-control" disabled name="sp_producaoResumoAnais"><?php echo mb_strtoupper($dados->sp_producaoResumoAnais); ?></div>
                </div>
            </div>


            <div class="form-group" style="margin-top: 15px;">
                <div class="col-md-12">
                    <label>DEMAIS PRODUÇÕES</label>
                    <div style="height: 100px;" class="form-control" disabled name="sp_producaoDemaisProducoes"><?php echo mb_strtoupper($dados->sp_producaoDemaisProducoes); ?></div>
                </div>
            </div>


        </div>
        <?php $this->util->mascaras() ?>

        <br>
        <a href="<?php echo site_url('adm/rh/selecao'); ?>" class=" btn btn-primary pull-right">
            Voltar
        </a>





    </div>
</div>