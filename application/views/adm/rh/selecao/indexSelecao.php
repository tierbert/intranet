<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            SELEÇÃO RH DOCENTE / ADMINISTRATIVO
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class="row">


        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">DOCENTES</a></li>
            <li class="disabled"><a href="#profile" class="disabled">ADMINISTRATIVO</a></li>
            <img src="../../../../../../../../tisuport/Pictures/Deepin Screenshot_20171127103204.png" alt=""/>
            <li class="disabled"><a href="#messages">ARQUIVADOS</a></li>
        </ul>

        <hr style="margin: 0;margin-bottom: 10px;">
    </div>

    <div class="row">
        <?php if (count($dados) != 0) { ?>
            <table class='table table-bordered table-striped table-condensed table-hover'>
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>CIDADE</th>
                        <th>TELEFONE</th>
                        <th>EMAIL</th>
                        <th>DATA CADASTRO</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dados->result() as $dado) {
                        ?>
                        <tr>
                            <td><?php echo mb_strtoupper($dado->sp_nome) ?> </td>
                            <td><?php echo mb_strtoupper($dado->sp_cidade . ' - ' . $dado->sp_estado) ?> </td>
                            <td><?php echo $dado->sp_telefone ?> </td>
                            <td><?php echo $dado->sp_email ?> </td>
                            <td><?php echo $this->util->data3($dado->sp_dataCriacao) ?> </td>
                            <td>
                                <a href="<?php echo site_url('adm/rh/selecao/detalharDocente/' . $dado->sp_id); ?>" class="btn btn-xs btn-info ">
                                    DETALHAR
                                </a>
                                <a href="<?php echo site_url('adm/rh/selecao/imprimir/' . $dado->sp_id); ?>" class="btn btn-xs btn-warning ">
                                    IMPRIMIR
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } else { ?>
            <h4>Nenhum registro encontrado</h4>
        <?php } ?>
    </div>
</div>