
<br><br><br>
<div class="container-fluid text-center">    
    <div class="row content">
        <div class="col-sm-12 text-left">
            <?php echo validation_errors(); ?>
            <h3 style="text-align: center;">HISTÓRICO DO PROTOCOLO</h3>
            <br>
            <div class="checkout-wrap">
                <ul class="checkout-bar">
                    <li class="visited first"> <a href="#">SOLICITAÇÃO</a> </li>

                    <li class="active visited"> <a href="#">COORDENAÇÃO</a></li>

                    <li class="active visited">RECURSOS HUMANOS</li>

                     <li class="active visited">GERÊNCIA</li>

                    <li class="active ">CONCLUIR</li>    

               

                </ul>
            </div>
        </div>
    </div>
</div>
<br><br><br>

<div class="container">
    <div class="row">

        <div class="col-sm-4">

            <br><br>
            <div class="alert alert-dismissible alert-success">
                <h5> PARECER COORDENADOR: </h5>
                <?php if (count($object) != 0) { ?>       
                    <?php
                    if ($object->aus_coordParecer == null) {
                        echo "AGUARDANDO PARECER<br>";
                    } else {
                        ?>
                        <?php echo $object->aus_coordParecer ?> <br>
                        <h5> OBS:</h5>
                        <?php echo $object->aus_coordObservacoes ?>
                    <?php } ?>
                    <?php ?>
                <?php }
                ?>
            </div>
        </div>

        <div class="col-sm-4">

            <br><br>
            <div class="alert alert-dismissible alert-info">
                <h5> PARECER RH: </h5>
                <?php if (count($object) != 0) { ?>       
                    <?php
                    if ($object->aus_rhParecer == null) {
                        echo "AGUARDANDO PARECER<br>";
                    } else {
                        ?>
                        <?php echo $object->aus_rhParecer ?> <br>


                    <?php } ?>
                    <?php ?>
                <?php }
                ?>
            </div>
        </div>
        <br><br>
        <div class="col-sm-4"> 
        	    <div class="alert alert-dismissible alert-danger">
                <h5> PARECER GERÊNCIA: </h5>
                <?php if (count($object) != 0) { ?>       
                    <?php
                    if ($object->aus_gerParecer == null) {
                        echo "AGUARDANDO PARECER<br>";
                    } else {
                        ?>
                        <?php echo $object->aus_gerObservacoes ?> <br>


                    <?php } ?>
                    <?php ?>
                <?php }
                ?>
            </div>

        </div> 

    </div>
</div>
<footer class="container-fluid text-center">
    <a class='btn btn-primary2 pull-left' href='<?php echo site_url('adm/rh/ausencia/') ?>'>VOLTAR</a>       
    <hr>       
</footer>

