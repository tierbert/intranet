<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            NOVA JUSTIFICATIVA DE AUSÊNCIA
        </div>
        <div class="col-md-6 col-xs-6 text-right">
           <!-- <a class='btn btn-warning' href='<?php echo site_url('adm/rh/adiantamento/adiantamentoFuncNovo') ?>'>SOLICITAR ADIANTAMENTO</a> 
            <a href="<?php echo site_url('adm/rh/Iniciorh'); ?>" class=" btn btn-primary">
                Voltar
            </a> -->
        </div>
    </div>
</div>
<div class="painel" style="min-height: 440px;">
    <div class=form-horizontal> 
        <?php echo form_open(); ?>
        <div>
            <?php echo validation_errors(); ?>
        </div>
        <?php echo form_open() ?>
        <div class="form-group">
            <?php $this->util->input2('DATA INICIAL', 'aus_dataInicial', $object->aus_dataInicial, 'data dataInicial inicial popupDatepicker', 2, '', 'required'); ?>
            <?php $this->util->input2('DATA FINAL', 'aus_dataFinal', $object->aus_dataFinal, 'dataFinal ', 2, '', 'required'); ?>
            <div class="col-md-6 ">
                <label for="" class="">&nbsp;</label>
                <div class="totalDias">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6">
               <br> <label for="" class="">MOTIVO</label>
                <select class="form-control motivo" name="aus_motivo" required="true">
                    <option value="">SELECIONE O MOTIVO</option>
                    <option value="0">COMPENSAÇÃO DE HORAS ACUMALADAS</option>
                    <option value="1">CONSULTA/EXAME MÉDICO</option>
                    <option value="2">ACOMPANHAMENTO DE FILHO MENOR OU CÔNJUGE EM PROCEDIMENTO MÉDICO/CIRÚRGICO</option>
                    <option value="3">OUTROS</option>
                </select>
            </div>
            <div class="col-md-6 aviso hidden alert alert-dismissible alert-info">
                <label for="" class="">AVISO</label>
                <div class="conteudo">
                </div>
            </div>

            <div class="col-lg-6">
               <br> <label for="" class="">JUSTIFICATIVA</label>
                <textarea class="form-control justificativa" name="aus_justificativa"></textarea>
            </div>
        </div>

        <div class="col-md-4">
            &nbsp;
            <div class="form-group">
                <input type=submit  value=Salvar class='btn btn-success'>
            </div>
            <?php echo form_close() ?>  
        </div>           
    </div>
    <?php echo form_close() ?>
</div>
<?php echo $this->util->mascaras(); ?>



<script>
    $(function () {
        $('.motivo').click(function () {
            var motivo = $('.motivo').val();
            if (motivo == 0) {
                $('.aviso').addClass('hidden');
                $(".justificativa").prop('required', true);
            }
            if (motivo == 1) {
                $('.aviso').removeClass('hidden');
                $('.conteudo').html('VOCÊ INFORMOU QUE ESTARÁ SOB PROCEDIMENTO MÉDICO. SERÁ NECESSÁRIO APRESENTAR O REFERIDO '
                        + 'ATESTADO JUNTO AO SETOR DE RECURSOS HUMANOS DA FACULDADE GUANAMBI.');
                $(".justificativa").prop('required', false);
            }
            if (motivo == 2) {
                $('.aviso').removeClass('hidden');
                $('.conteudo').html('VOCÊ INFORMOU QUE ESTARÁ ACOMPANHANDO FILHO MENOR OU CÔNJUGE EM PROCEDIMENTO MÉDICO. '
                        + 'SERÁ NECESSÁRIO APRESENTAR O REFERIDO ATESTADO JUNTO AO SETOR DE RECURSOS HUMANOS DA FACULDADE GUANAMBI');
                $(".justificativa").prop('required', false);
            }
            if (motivo == 3) {
                $('.aviso').addClass('hidden');
                $(".justificativa").prop('required', true);
            }
        });
    });</script>

<script src="<?php echo base_url('assets'); ?>/js/moment.js"></script>
<link href="<?php echo base_url('assets'); ?>/js/datepick5/css/redmond.datepick.css" rel="stylesheet">
<script src="<?php echo base_url('assets'); ?>/js/datepick5/js/jquery.plugin.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/datepick5/js/jquery.datepick.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/datepick5/js/jquery.datepick-pt.js"></script>
<script>
    $(function () {

        $('.popupDatepicker').datepick(); /* calendario data*/
        $('.dataFinal').datepick({
            onSelect: function (dates) {
                //alert('The chosen date(s): ' + dates);
                var a = moment($('.dataInicial').val(), 'D/M/YYYY');
                var b = moment($('.dataFinal').val(), 'D/M/YYYY');
                var diffDays = b.diff(a, 'days');
                //alert(diffDays);

                $('.totalDias').html('(Dias: ' + (diffDays + 1) + ')')

                if (a > b) {
                    alert('Você escolheu data final anterior a inicial. Favor corrigir!');
                }

            },
            showTrigger: '#calImg'});
    });
</script>