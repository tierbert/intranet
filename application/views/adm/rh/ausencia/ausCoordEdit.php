<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            ANÁLISE JUSTIFICATIVA DE AUSÊNCIA
        </div>
        <div class="col-md-6 col-xs-6 text-right">
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal> 
        <?php echo form_open(); ?>
        <fieldset>
            <div>
                <?php echo validation_errors(); ?>
            </div>
            <?php echo form_open() ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">

                        <?php $this->util->input2('DATA INICIAL', '', $object->aus_dataInicial, 'data', 2, '', 'required disabled'); ?>
                        <?php $this->util->input2('DATA FINAL', '', $object->aus_dataFinal, 'data', 2, '', 'required disabled'); ?>

                        <?php
                        $motivos = array(
                            "0" => 'COMPENSAÇÃO DE HORAS ACUMALADAS',
                            "1" => 'CONSULTA/EXAME MÉDICO',
                            "2" => 'ACOMPANHAMENTO DE FILHO MENOR OU CÔNJUGE EM PROCEDIMENTO MÉDICO/CIRÚRGICO',
                            "3" => 'OUTROS'
                        );
                        ?>

                        <div class="col-md-4">
                            <label for="" class="">MOTIVO</label>
                            <select class="form-control motivo" name="" required="true" disabled>
                                <option value=""><?php echo $motivos[$object->aus_motivo]; ?></option>
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <label for="" class="">JUSTIFICATIVA</label>
                            <textarea class="form-control justificativa" disabled><?php echo $object->aus_justificativa; ?></textarea>
                        </div>
                    </div>

                    <?php echo form_open() ?>
                    <br>
                    <h4 style="text-align: center;">PARECER COORDENADOR</h4> <br>
                    <div class="form-group">
                        <?php $this->util->select2('PARECER', 'aus_coordParecer', array('' => '', 'DEFERIDO' => 'DEFERIDO', 'INDEFERIDO' => 'INDEFERIDO'), $object->aus_coordParecer, '', 3); ?>
                        <div class="col-lg-6">
                            <label for="" class="">JUSTIFICATIVA</label>
                            <textarea class="form-control justificativa" name="aus_coordObservacoes" required><?php echo $object->aus_coordObservacoes; ?></textarea>
                        </div>
                    </div>
                    <div class="col-md-4">
                        &nbsp;
                        <div class="form-group">
                            <button type="submit" class="btn btn-success" value="Salvar">Salvar</button>
                        </div>
                    </div>
                    <?php echo form_close() ?>
                </div>
                <?php echo $this->util->mascaras(); ?>
            </div>
        </fieldset>
    </div>
</div>
