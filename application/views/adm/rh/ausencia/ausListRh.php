<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            JUSTIFICATIVAS DO AUSÊNCIA
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('adm/rh/Iniciorh'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal> 
        <?php if ($dados->num_rows() > 0) { ?>

            <div class="col-sm-3"><b> NOME</div>
            <div class="col-sm-3">DATA INICIAL</div>
            <div class="col-sm-3">DATA FINAL</div>
            <div class="col-sm-3">OPÇÕES</b></div>
            <br><hr>

            <?php foreach ($dados->result() as $dado) {
                ?>
                <table class='table table-bordered table-striped table-condensed table-hover table-responsive tabela-aredondada'> 
                    <div class="col-sm-3"><?php echo $dado->usu_nome ?></div>
                    <div class="col-sm-3"><?php echo $this->util->data2($dado->aus_dataInicial) ?> </div>
                    <div class="col-sm-3"><?php echo $this->util->data2($dado->aus_dataFinal) ?> </div>

                    <div class="col-sm-3"><a href="<?php echo site_url('adm/rh/ausencia/ausRhEdit/' . $dado->aus_id) ?>" class='btn btn7 btn-danger' >
                            PARECER
                        </a>  </div> <br><br>
                </table>
            <?php } ?>
        </div>
    <?php } else { ?>
        <h4 style="text-align: center;">NENHUM REGISTRO ENCONTRADO</h4>
    <?php } ?>
</div>
