<br><br>
<div class="container">
    <div class="row">
        <nav class="navbar ">

        <!--<a class='btn btn-primary2 pull-right' href='<?php echo site_url('adm/rh/ausencia') ?>'>VOLTAR</a>  -->  
            <h4 style="text-align: center;">JUSTIFICATIVA DE AUSÊNCIA</h4>  
            <!-- COLOCAR BOTÃO E TEXTO -->       

            <!-- Barra Menu (não coloquei nada.. dexei somente pra centralizar o conteudo da principal-->   
        </nav>

        <div class="container-fluid ">    
            <div class="row content">

                <div class="col-sm-12 ">
                    <div>
                        <?php echo validation_errors(); ?>
                    </div>

                    <div class="form-horizontal">
                        <div class="form-group">
                            <?php $this->util->input2('DATA INICIAL', '', $object->aus_dataInicial, 'data', 2, '', 'required disabled'); ?>
                            <?php $this->util->input2('DATA FINAL', '', $object->aus_dataFinal, 'data', 2, '', 'required disabled'); ?>

                            <?php
                            $motivos = array(
                                "0" => 'COMPENSAÇÃO DE HORAS ACUMALADAS',
                                "1" => 'CONSULTA/EXAME MÉDICO',
                                "2" => 'ACOMPANHAMENTO DE FILHO MENOR OU CÔNJUGE EM PROCEDIMENTO MÉDICO/CIRÚRGICO',
                                "3" => 'OUTROS'
                            );
                            ?>

                            <div class="col-md-4">
                                <label for="" class="">MOTIVO</label>
                                <select class="form-control motivo" name="" required="true" disabled>
                                    <option value=""><?php echo $motivos[$object->aus_motivo]; ?></option>
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label for="" class="">JUSTIFICATIVA</label>
                                <textarea class="form-control justificativa" disabled><?php echo $object->aus_justificativa; ?></textarea>
                            </div>
                        </div>
                        <hr>
                        <h5 style="text-align: center;"> PARECER COORDENADOR DO SETOR</h5>
                        <div class="form-group">
                            <?php $this->util->select2('PARECER', '', array('' => '', 'DEFERIDO' => 'DEFERIDO', 'INDEFERIDO' => 'INDEFERIDO'), $object->aus_coordParecer, '', 3, '', 'disabled'); ?>
                            <div class="col-lg-6">
                                <label for="" class="">JUSTIFICATIVA</label>
                                <textarea class="form-control justificativa" disabled name=""><?php echo $object->aus_coordObservacoes; ?></textarea>
                            </div>
                        </div>
                        <hr>
                        <h5 style="text-align: center;"> PARECER RECURSOS HUMANOS</h5>
                        <div class="form-group">
                            <?php $this->util->select2('PARECER', '', array('' => '', 'DEFERIDO' => 'DEFERIDO', 'INDEFERIDO' => 'INDEFERIDO'), $object->aus_rhParecer, '', 3, '', 'disabled'); ?>
                            <div class="col-lg-6">
                                <label for="" class="">JUSTIFICATIVA</label>
                                <textarea class="form-control justificativa" name="" disabled><?php echo $object->aus_rhObservacao; ?></textarea>
                            </div>
                        </div>
                        <hr>
                        <?php echo form_open() ?>
                        <h5 style="text-align: center;"> PARECER GERÊNCIA</h5>
                        <div class="form-group">
                            <?php $this->util->select2('PARECER', 'aus_gerParecer', array('' => '', 'DEFERIDO' => 'DEFERIDO', 'INDEFERIDO' => 'INDEFERIDO'), $object->aus_gerParecer, '', 3); ?>
                            <div class="col-lg-6">
                                <label for="" class="">JUSTIFICATIVA</label>
                                <textarea class="form-control justificativa" name="aus_gerObservacoes" required><?php echo $object->aus_gerObservacoes; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class=form-group>
                                <input type=submit  value=Salvar class='btn btn-success pull-right'>
                            </div>  
                        </div>
                        <?php echo form_close() ?>
                    </div>
                </div>
                <?php echo $this->util->mascaras(); ?>

                <footer class="site-footer">
                    <!-- COLOCAR BOTÃO VOLTAR--> 
                    <br>
                </footer>
                <div class="col-sm-1 sidenav">
                    <!-- Barra Lateral (não coloquei nada.. dexei somente pra centralizar o conteudo da principal-->
                </div>
            </div>
        </div>
    </div>
</div>
</div>

