<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            ACOMPANHE AQUI SEU PROTOCOLO
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a class='btn btn-warning' href='<?php echo site_url('adm/rh/ausencia/criar') ?>'> NOVA JUSTIFICATIVA</a> 
            <a href="<?php echo site_url('adm/rh/Iniciorh'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal>       
        <?php if ($dados->num_rows() > 0) { ?>

            <div class="col-sm-2"><b>DATA INICIAL</div>
            <div class="col-sm-2">DATA FINAL</div>
            <div class="col-sm-5">JUSTIFICATIVA</div>
            <!--  <div class="col-sm-2">PARECER COORD</div>
                    <div class="col-sm-2">PARECER FINAL</div> -->
            <div class="col-sm-3">OPÇÕES</b> </div> 
    <!-- <th>CURSO</th>
    <th>SEMESTRE</th>
    <th>PARECER</th> 
    <th>PERCENTUAL</th> -->
            <br><hr>

            <?php foreach ($dados->result() as $dado) { ?>
                <table class='table table-bordered table-striped table-condensed table-hover table-responsive tabela-aredondada'>
                    <div class="col-sm-2"><?php echo $this->util->data2($dado->aus_dataInicial) ?> </div>
                    <div class="col-sm-2"><?php echo $this->util->data2($dado->aus_dataFinal) ?> </div>
                    <div class="col-sm-5" style="text-transform: uppercase;"><?php echo $dado->aus_justificativa ?> </div>
                    <!--<div class="col-sm-2"><?php echo $this->util->data($dado->aus_coordParecer) ?> </div>
                      <div class="col-sm-2"><?php echo $this->util->data($dado->aus_gerParecer) ?> </div> -->
                      <!--<td><?php echo $dado->dc_curso ?> </td>
                          <td><?php echo $dado->dc_semestre ?> </td>
                          <td><?php echo $dado->dc_rhParecer ?> </td> 
                          <td><?php echo $dado->dc_gerPercentual ?> </td> -->

                    <div class="col-sm-3"> <a class='btn btn7 btn-danger' href="<?php echo site_url('adm/rh/ausencia/ausenciaParecCoord/' . $dado->aus_id) ?>">INFO</a> </div>      

                    <br><br>
                </table>
            <?php } ?>
            <!-- <?php echo $this->util->data2($dado->adi_financDataAgendado) ?> -->

        <?php } else { ?>
            <h4 style="text-align: center;">NENHUM REGISTRO ENCONTRADO</h4>
        <?php } ?>

    </div>   
</div>

