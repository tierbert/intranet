<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            ANÁLISE DE ADIANTAMENTO
        </div>
        <div class="col-md-6 col-xs-6 text-right">
        </div>
    </div>
</div>


<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal> 
        <?php echo validation_errors(); ?>
        <h4> <b> FUNCIONÁRIO: </b> <?php echo $object->usu_nome ?> </h4> <br>
        <?php echo form_open() ?>        
        <div class="form-group">
            <?php //$this->util->input('adi_idUsuario', '', $object->adi_idUsuario); ?>
            <?php $this->util->input2('DATA', '', $this->util->data2($object->adi_data), '', '4', '', 'disabled'); ?>
            <?php $this->util->input2('VALOR', '', "R$" . number_format($object->adi_valor, 2, ",", '.'), '', '4', '', 'disabled'); ?>
            <?php $this->util->input2('OBSERVAÇÃO', '', $object->adi_obs, '', '4', '', 'disabled'); ?>
        </div>
        <hr>
        <div class="form-group">
            <?php $this->util->select2('PARECER', 'adi_rhParecer', array('' => '', 'DEFERIDO' => 'DEFERIDO', 'INDEFERIDO' => 'INDEFERIDO'), $object->adi_rhParecer, 'parecer', 3, '', 'required'); ?>
            <?php $this->util->input2('DATA PARECER', 'adi_rhDataParecer', date('d-m-Y'), 'data popupDatepicker', 2, '', 'required'); ?>
            <?php $this->util->input2('OBSERVAÇÃO', 'adi_rhObservacao', $object->adi_rhObservacao, '', 7, '', 'required'); ?>
        </div>
        <hr>
        <div class="form-group hidden exibe">
            <?php $this->util->select2('FORMA PAGAMENTO', 'adi_rhIntegralParcela', array('' => '', 'INTEGRAL' => 'INTEGRAL', 'PARCIAL' => 'PARCIAL'), $object->adi_rhIntegralParcela, '', 3, '', ''); ?>
            <?php $this->util->input2('QTD PARCELAS', 'adi_quantParcelas', $object->adi_quantParcelas, '', 2); ?>
            <?php $this->util->input2('MÊS RECEBIMENTO', 'adi_mesParcela', $object->adi_mesParcela, 'mesParec', 3, '', ''); ?>

        </div>
       
        <div class=form-group>
            <input type=submit  value=Salvar class='btn btn-success pull-right'>
        </div>            
        <?php echo form_close() ?>
    </div>

</div>

<?php echo $this->util->mascaras(); ?>
<script>
    $(function () {
        $('.mesParec').mask('99/9999');
    })
</script>

<link href="<?php echo base_url('assets'); ?>/js/datepick5/css/redmond.datepick.css" rel="stylesheet">
<script src="<?php echo base_url('assets'); ?>/js/datepick5/js/jquery.plugin.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/datepick5/js/jquery.datepick.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/datepick5/js/jquery.datepick-pt.js"></script>
<script>
    /* FUNÇÃO PARA MOSNTRAR O RESTO DO FORMULARIO CASO SEJA DEFERIDO - ATENTAR PARA OS NOMES DAS CLASSES NO FORMULÁRIO*/
    $(function () {
        $('.parecer').click(function () {
            if ($('.parecer').val() == 'DEFERIDO') {
                $('.exibe').removeClass('hidden');
            } else {
                $('.exibe').addClass('hidden');
            }

        });

        $('.popupDatepicker').datepick();/* calendario data*/

        $('.valor').maskMoney({
            //prefix: 'R$ ',
            allowNegative: true,
            thousands: '.',
            decimal: ',',
            affixesStay: false,
            allowZero: true
        });
    });
</script>