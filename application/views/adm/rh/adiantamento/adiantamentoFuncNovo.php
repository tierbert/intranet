<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            SOLICITAR ADIANTAMENTO
        </div>
        <div class="col-md-6 col-xs-6 text-right">
           <!-- <a class='btn btn-warning' href='<?php echo site_url('adm/rh/adiantamento/adiantamentoFuncNovo') ?>'>SOLICITAR ADIANTAMENTO</a> 
            <a href="<?php echo site_url('adm/rh/Iniciorh'); ?>" class=" btn btn-primary">
                Voltar
            </a> -->
        </div>
    </div>
</div>


<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal> 
        <?php echo form_open(); ?>

        <div>
            <?php echo validation_errors(); ?>
        </div>
        <?php echo form_open() ?>

        <div class="form-group">
            <?php $this->util->input2('DATA', 'adi_data', date('d/m/Y'), 'data popupDatepicker', 2); ?>       

            <?php $this->util->input2('VALOR', 'adi_valor', $object->adi_valor, 'valor', 2, '','required'); ?>

        </div>
        <br>
        <div class="form-group">
            <?php $this->util->input2('OBSERVAÇÃO', 'adi_obs', $object->adi_obs, '', 8, '', 'required'); ?>
        </div>

        <div class="col-md-4">
            &nbsp;
            <div class="form-group">
                <input type=submit  value=Salvar class='btn btn-success'>
            </div>
            <?php echo form_close() ?>  
        </div>
    </div>
</div>

<?php echo $this->util->mascaras(); ?>
<script src="<?php echo base_url('assets/js/jquery.maskMoney.js'); ?>"></script>

<link href="<?php echo base_url('assets'); ?>/js/datepick5/css/redmond.datepick.css" rel="stylesheet">
<script src="<?php echo base_url('assets'); ?>/js/datepick5/js/jquery.plugin.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/datepick5/js/jquery.datepick.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/datepick5/js/jquery.datepick-pt.js"></script>
<script>
    $(function () {
        $('.popupDatepicker').datepick();/* calendario data*/

        $('.valor').maskMoney({
            //prefix: 'R$ ',
            allowNegative: true,
            thousands: '.',
            decimal: ',',
            affixesStay: false,
            allowZero: true
        });

    });
</script>