<h3 class='pull-left'><?php echo ucfirst($titulo); ?> - Listando: </h3>
<a class='btn btn-primary pull-right' href='<?php echo site_url($this->uri->segment(1) . '/criar/') ?>'>Nova <?php echo ucfirst(substr($titulo, 0, -2)) ?></a>
<?php if (count($dados) != 0) { ?>
    <table class='table table-bordered table-striped table-condensed table-hover'>
        <thead>
            <tr>
                <th>adi_idUsuario</th>
                <th>adi_data</th>
                <th>adi_valor</th>
                <th>adi_obs</th>
                <th>adi_rhParecer</th>
                <th>adi_rhDataParecer</th>
                <th>adi_rhIntegralParcela</th>
                <th>adi_quantParcelas</th>
                <th>adi_mesParcela</th>
                <th>adi_financParecer</th>
                <th>adi_financDataAgendado</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dados as $dado) {
                ?><tr><td><?php echo $dado->adi_idUsuario ?> </td>
                    <td><?php echo $dado->adi_data ?> </td>
                    <td><?php echo $dado->adi_valor ?> </td>
                    <td><?php echo $dado->adi_obs ?> </td>
                    <td><?php echo $dado->adi_rhParecer ?> </td>
                    <td><?php echo $dado->adi_rhDataParecer ?> </td>
                    <td><?php echo $dado->adi_rhIntegralParcela ?> </td>
                    <td><?php echo $dado->adi_quantParcelas ?> </td>
                    <td><?php echo $dado->adi_mesParcela ?> </td>
                    <td><?php echo $dado->adi_financParecer ?> </td>
                    <td><?php echo $dado->adi_financDataAgendado ?> </td>
                    <td><a class='btn btn-sm btn-primary' href="<?php echo site_url($this->uri->segment(1) . '/editar/' . $dado->adi_id)
                ?>">Editar</a>
                        <a class='btn btn-sm btn-danger' href="<?php echo site_url($this->uri->segment(1) . '/delete/' . $dado->adi_id) ?>">Deletar</a>
                    </td>

                </tr>
            <?php } ?>

        </tbody>
    </table>
    <ul class='pagination pagination-small'>
        <li>
            <?php echo $paginacao; ?>
        </li>
    </ul>
<?php } else { ?>
    <h4>Nenhum registro encontrado</h4>
<?php } ?>

