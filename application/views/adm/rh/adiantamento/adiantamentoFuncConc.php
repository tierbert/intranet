<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            HISTÓRICO DO PROTOCOLO
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a  href='<?php echo site_url('adm/rh/adiantamento/adiantamentoFunc') ?>' class=" btn btn-danger">
                Início
            </a> 
        </div>
    </div>
</div>
<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal> 
        <div class="col-sm-4">          
            <div class="alert alert-dismissible alert-success">
                  <b>  <h5> PARECER RH: </h5></b> 
                <?php if (count($object) != 0) { ?>       
                    <?php
                    if ($object->adi_rhParecer == null) {
                        echo "AGUARDANDO PARECER<br>";
                    } else {
                        ?>
                        <?php echo $object->adi_rhParecer ?> <br>
                        <h5> OBS:</h5>
                        <?php echo $object->adi_rhObservacao ?>
                    <?php } ?>
                    <?php ?>
                <?php }
                ?>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="alert alert-dismissible alert-info">
                <b>  <h5> PARECER FINANCEIRO: </h5></b> 
                <?php if (count($object) != 0) { ?>       
                    <?php
                    if ($object->adi_rhParecer == null) {
                        echo "AGUARDANDO PARECER<br>";
                    } else {
                        ?>
                        <?php echo $object->adi_financParecer ?> <br>
                        <h5> OBS:</h5>
                        <?php echo $object->adi_financObs ?>
                    <?php } ?>
                    <?php ?>
                <?php }
                ?>
            </div>
        </div>

        <div class="col-sm-4"> 
            <div class="alert alert-dismissible alert-danger">
                <b>   <h5> DATA PARA RECEBIMENTO:</h5></b> 
                <?php echo $this->util->data2($object->adi_financDataAgendado) ?>
            </div>
        </div> 

    </div>
</div>


