<br><br>
<div class="container">
    <div class="row">
        <nav class="navbar ">
            <h4 style="text-align: center;">MINHAS SOLICITAÇÕES</h4>
            <!-- <a class='btn btn-warning pull-left' href='<?php echo site_url('adm/rh/adiantamento/adiantamentoFuncNovo') ?>'>SOLICITAR ADIANTAMENTO</a> -->

            <!-- Barra Menu (não coloquei nada.. dexei somente pra centralizar o conteudo da principal-->   
        </nav>   

        <!--  <link href='https://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700' rel='stylesheet' type='text/css'>-->

        <h1>ACOMPANHE AQUI SEU PROTOCOLO</h1>

        <div class="checkout-wrap">
            <ul class="checkout-bar">

                <li class="active">
                    <a href="#">Solicitação</a>
                </li>

                <li class="next">Parecer do RH</li>

                <li class="next">Parecer do Financeiro</li>


                <li class="">Concluído</li>

            </ul>
        </div>

        <br> <br> <br> <br> <br>


        <div class="row">
            <div class="col-sm-4">.col-sm-4</div>
            <div class="col-sm-4">.col-sm-4</div>
            <div class="col-sm-4">.col-sm-4</div>
        </div>



        <div class="container-fluid text-center">    
            <div class="row content">
                <div class="col-sm-1 sidenav">   
                    <!-- Barra Lateral (não coloquei nada.. dexei somente pra centralizar o conteudo da principal-->   
                </div>
                <div class="col-sm-10 text-left">
                  <!--<a class='btn btn-primary2 pull-right' href='<?php echo site_url('adm/rh/Iniciorh') ?>'>VOLTAR</a> -->
                    <a class='btn btn-warning pull-left' href='<?php echo site_url('adm/rh/adiantamento/adiantamentoFuncNovo') ?>'>SOLICITAR ADIANTAMENTO</a> 
                    <br>  <br>

                    <?php if (count($dados) != 0) { ?>

                        <table class='table table-bordered table-striped table-condensed table-hover table-responsive'>
                            <thead>
                                <tr>
                                    <th>DATA DA SOLICITAÇÃO</th>
                                    <th>VALOR</th>
                                    <th>MOTIVO</th>
                                    <!--<th>DATA PARA RETIRADA</th>-->
                                    <!--<th>OPÇÕES</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($dados as $dado) {
                                    ?>
                                    <tr>
                                        <td><?php echo $this->util->data2($dado->adi_data) ?> </td>
                                        <td>R$ <?php echo number_format($dado->adi_valor, 2, ",", '.'); ?> </td>
                                        <td><?php echo $dado->adi_obs ?> </td>
                                        <!--<td><?php echo $this->util->data2($dado->adi_financDataAgendado) ?> </td> -->
                                        <td>
                                            <a class='btn btn-sm btn-danger' data-toggle="modal" href="#<?php echo $dado->adi_id ?>">
                                                DETALHES
                                            </a>
                                            <!-- MOLDAL   -->
                                            <div class="modal" id="<?php echo $dado->adi_id ?>">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">     
                                                        <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>       -->   
                                                        <div class="modal-body">

                                                            <h4 style="text-align: center;">PARECER RH</h4>
                                                            <div style="background: #F3FAEA">
                                                                <b> STATUS DA SOLICITAÇÃO:</b><br>
                                                                <?php
                                                                if ($dado->adi_rhParecer == null) {
                                                                    echo "AGUARDANDO PARECER<br>";
                                                                } else {
                                                                    ?>
                                                                    <?php echo $dado->adi_rhParecer ?><br>
                                                                <?php } ?>
                                                                <b> MOTIVO: <br></b>
                                                                <p><?php echo $dado->adi_rhObservacao ?> </p>                                                    
                                                            </div>
                                                            <br>
                                                            <h4 style="text-align: center;">PARECER FINANCEIRO</h4>
                                                            <div style="background: #EFF0F9"> 
                                                                <b> STATUS DA SOLICITAÇÃO:</b><br>
                                                                <?php
                                                                if ($dado->adi_financParecer == null) {
                                                                    echo "AGUARDANDO PARECER <br>";
                                                                } else {
                                                                    ?>
                                                                    <?php echo $dado->adi_financParecer ?><br>
                                                                <?php } ?>
                                                                <b> DATA PARA RETIRADA: <br></b>
                                                                <?php echo $this->util->data2($dado->adi_financDataAgendado) ?> <br>

                                                                <b> MOTIVO: <br></b>
                                                                <p><?php echo $dado->adi_financObs ?><br> </p>
                                                            </div>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>       
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--END MODAL -->

                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                    <?php } else { ?>
                        <h4>Nenhum registro encontrado</h4>
                    <?php } ?>

                    <footer class="site-footer">
                        <a class='btn btn-primary2 pull-left' href='<?php echo site_url('adm/rh/Iniciorh') ?>'>VOLTAR</a>
                        <br><br><br>
                    </footer>
                    <div class="col-sm-1 sidenav">

                        <!-- Barra Lateral (não coloquei nada.. dexei somente pra centralizar o conteudo da principal-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>














