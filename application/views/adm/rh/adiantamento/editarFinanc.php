<div class="box"  style="">
    <?php $this->load->view('botoes') ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            DETALHES DA SOLICITAÇÃO DE ADIANTAMENTO
        </div>
        <div class="col-md-6 col-xs-6 text-right">

        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class=form-horizontal> 

        <?php echo form_open(); ?>
        <fieldset>
            <div>
                <?php echo validation_errors(); ?>
            </div>

            <?php echo form_open() ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <h4 style="text-align: center;"> DETALHES SOLICITAÇÃO </h4> <hr>
                        <?php //$this->util->input('adi_idUsuario', '', $object->adi_idUsuario); ?>
                        <?php $this->util->input2('DATA', '', $object->adi_data, '', '4', '', 'disabled'); ?>
                        <?php $this->util->input2('VALOR', '', $object->adi_valor, '', '4', '', 'disabled'); ?>
                        <?php $this->util->input2('OBSERVAÇÃO', '', $object->adi_obs, '', '4', '', 'disabled'); ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <br><br>
                    <h4 style="text-align: center;"> PARECER RH </h4> <hr>
                    <div class="form-group">
                        <?php $this->util->select2('PARECER', '', array('' => '', 'DEFERIDO' => 'DEFERIDO', 'INDEFERIDO' => 'INDEFERIDO'), $object->adi_rhParecer, '', 3, '', 'disabled'); ?>
                        <?php $this->util->input2('DATA PARECER', '', $this->util->data($object->adi_rhDataParecer), 'data', 2, '', 'disabled'); ?>
                        <?php $this->util->input2('OBSERVAÇÃO', '', $object->adi_rhObservacao, '', 7, '', 'disabled'); ?>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <?php $this->util->select2('FORMA PAGAMENTO', '', array('' => '', 'INTEGRAL' => 'INTEGRAL', 'PARCIAL' => 'PARCIAL'), $object->adi_rhIntegralParcela, '', 3, '', 'disabled'); ?>
                        <?php $this->util->input2('QUANT PARCELAS', '', $object->adi_quantParcelas, '', 3, '', 'disabled'); ?>
                        <?php $this->util->input2('MÊS RECEBIMENTO', '', $object->adi_mesParcela, 'mesParec', 3, '', 'disabled'); ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <br><br>
                    <h4 style="text-align: center;"> PARECER FINANCEIRO </h4> <hr>
                    <div class="form-group">
                        <?php $this->util->select2('PARECER', 'adi_financParecer', array('' => '', 'DEFERIDO' => 'DEFERIDO', 'INDEFERIDO' => 'INDEFERIDO'), $object->adi_financParecer, 'parecer', 4); ?>
                    </div>
                    <br>
                    <div class="form-group hidden exibe">
                        <?php $this->util->input2('DATA RECEBIMENTO', 'adi_financDataAgendado', $this->util->data($object->adi_financDataAgendado), 'data popupDatepicker', 4); ?> 



                        <?php $this->util->input2('OBSERVAÇÃO', 'adi_obs', $object->adi_financObs, '', 8, ''); ?> <br>
                        <!-- <b> OBSERVAÇÃO </b><br>
                         <div class="col-sm-6">
                             <textarea class="form-control" rows="2" name="adi_financObs" ></textarea>
                         </div> -->
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                &nbsp;
                <div class="form-group">
                    <button type="submit" class="btn btn-success" value="Salvar">Salvar</button>
                </div>
            </div>
            <?php echo form_close() ?>
    </div>
    <?php echo $this->util->mascaras(); ?>
</div>

<script>
    $(function () {
        $('.mesParec').mask('99/9999');
    })
</script>

<link href="<?php echo base_url('assets'); ?>/js/datepick5/css/redmond.datepick.css" rel="stylesheet">
<script src="<?php echo base_url('assets'); ?>/js/datepick5/js/jquery.plugin.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/datepick5/js/jquery.datepick.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/datepick5/js/jquery.datepick-pt.js"></script>
<script>
    $(function () {
        /* FUNÇÃO PARA MOSNTRAR O RESTO DO FORMULARIO CASO SEJA DEFERIDO - ATENTAR PARA OS NOMES DAS CLASSES NO FORMULÁRIO*/
        $('.parecer').click(function () {
            if ($('.parecer').val() == 'DEFERIDO') {
                $('.exibe').removeClass('hidden');
            } else {
                $('.exibe').addClass('hidden');
            }

        });


        $('.popupDatepicker').datepick();/* calendario data*/

        $('.valor').maskMoney({
            //prefix: 'R$ ',
            allowNegative: true,
            thousands: '.',
            decimal: ',',
            affixesStay: false,
            allowZero: true
        });

    });
</script>