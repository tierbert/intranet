<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            <?php echo ucfirst($titulo); ?> 
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('adm/programas/posList'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <section style="padding: 0em 3em 3em 3em;">
        <div class="container" style="background:#fff;padding: 40px;box-shadow: 0px 0px 20px #ddd;">
            <div class="row">
                <div class="col-md-12" style="text-align: center;">
                    <h2>
                        <span style="font-size: 1.2em;font-weight: bold;">Pós UniFG</span> <br/>
                        <span style="font-size: 1em;">Formulário de pré-matrícula</span>
                    </h2>
                    <hr style="margin:50px 40px 40px 0px">
                </div>
            </div>
            <style>
                .form-control{
                    background: #eee;
                    text-transform: uppercase
                }
            </style>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Nome (obrigatório)</label>
                        <span class="">
                            <input value="<?php echo $dado->pc_nomeCurso ?>" required="true" type="text" name="pos_nome" class="form-control" aria-required="true" aria-invalid="false" />
                        </span> 
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Nome (obrigatório)</label>
                        <span class="">
                            <input value="<?php echo $dado->pos_nome ?>" required="true" type="text" name="pos_nome" class="form-control" aria-required="true" aria-invalid="false" />
                        </span> 
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>
                            Data de Nascimento
                        </label>
                        <input value="<?php echo $this->util->data($dado->pos_dataNascimento) ?>" required="true" type="text" name="pos_dataNascimento" value="" class="form-control data" aria-required="true" aria-invalid="false" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>
                            Nacionalidade (obrigatório)
                        </label>
                        <input value="<?php echo $dado->pos_nacionalidade ?>" required="true" type="text" name="pos_nacionalidade" value="" size="40" class="form-control" aria-required="true" aria-invalid="false" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>
                            Naturalidade (obrigatório)
                        </label>
                        <input value="<?php echo $dado->pos_naturalidade ?>" required="true" type="text" name="pos_naturalidade" value="" size="40" class="   form-control" aria-required="true" aria-invalid="false" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>CPF (obrigatório)</label>
                        <input value="<?php echo $dado->pos_cpf ?>" required="true" type="text" name="pos_cpf" value="" size="40" class="cpf  form-control" aria-required="true" aria-invalid="false" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label >RG (obrigatório)</label>
                        <input value="<?php echo $dado->pos_rg ?>" required="true" type="text" name="pos_rg" value="" size="40" class="   form-control" aria-required="true" aria-invalid="false" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label >Órgão Emissor (obrigatório)</label>
                        <input value="<?php echo $dado->pos_orgaoEmissor ?>" required="true" type="text" name="pos_orgaoEmissor" value="" size="40" class="   form-control" aria-required="true" aria-invalid="false" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Estado Civil (obrigatório)</label>
                        <input value="<?php echo $dado->pos_estadoSivio ?>" required="true" type="text" name="pos_orgaoEmissor" value="" size="40" class="   form-control" aria-required="true" aria-invalid="false" />

                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Empresa onde Trabalha</label>
                        <input value="<?php echo $dado->pos_empTrabalho ?>" required="true" type="text" name="pos_empTrabalho" value="" size="40" class="  form-control" aria-invalid="false" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Telefone</label>
                        <input value="<?php echo $dado->pos_telefone ?>" required="true" type="tel" name="pos_telefone" value="" size="40" class="form-control telefone" aria-invalid="false" />
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Celular (obrigatório)</label>
                        <input value="<?php echo $dado->pos_celular ?>" required="true" type="tel" name="pos_celular" value="" size="40" class=" form-control telefone" aria-required="true" aria-invalid="false" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Pai (obrigatório)</label>
                        <input value="<?php echo $dado->pos_pai ?>" required="true" type="text" name="pos_pai" value="" size="40" class="   form-control" aria-required="true" aria-invalid="false" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Mãe (obrigatório)</label>
                        <input value="<?php echo $dado->pos_mae ?>" required="true" type="text" name="pos_mae" value="" size="40" class="   form-control" aria-required="true" aria-invalid="false" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Endereço (obrigatório)</label>
                        <input value="<?php echo $dado->pos_endereco ?>" required="true" type="text" name="pos_endereco" value="" size="40" class="   form-control" aria-required="true" aria-invalid="false" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>CEP (obrigatório)</label>
                        <input value="<?php echo $dado->pos_cep ?>" required="true" type="text" name="pos_cep" value="" size="40" class="cep form-control" aria-required="true" aria-invalid="false" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>E-mail (obrigatório)</label>
                        <input  value="<?php echo $dado->pos_email ?>" required="true" type="email" name="pos_email" value="" size="40" class="form-control" aria-required="true" aria-invalid="false" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>
                            Graduação (obrigatório)
                        </label>
                        <input value="<?php echo $dado->pos_graduacao ?>" required="true" type="text" name="pos_graduacao" value="" size="40" class="   form-control" aria-required="true" aria-invalid="false" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Instituição Superior onde estuda ou estudou (obrigatório)</label>
                        <input value="<?php echo $dado->pos_instituicao ?>" required="true" type="text" name="pos_instituicao" value="" size="40" class="   form-control" aria-required="true" aria-invalid="false" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Forma de Pagamento (obrigatório)</label> 
                        <input value="<?php echo $dado->pos_formaPagamento ?>" required="true" type="text" name="pos_instituicao" value="" size="40" class="   form-control" aria-required="true" aria-invalid="false" />

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>
                            Data de Cadastro
                        </label>
                        <input value="<?php echo $this->util->data3($dado->pos_dataCadastro) ?>" required="true" type="text" name="pos_dataNascimento" value="" class="form-control data" aria-required="true" aria-invalid="false" />
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>