<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            <?php echo ucfirst($titulo); ?> 
        </div>
        <div class="col-md-6 col-xs-6 text-right">

            <a href="<?php echo site_url('adm/painel'); ?>" class=" btn btn-primary">
                VOLTAR
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class="row">
        <div class="form-horizontal hidden-print" style="margin-bottom: 10px;">

            <?php if (count($dados) != 0) { ?>
                <table class='table table-bordered table-striped table-condensed table-hover'>
                    <thead>
                        <tr>
                            <th>NOME</th>
                            <th>DSICIPLINA</th>
                            <th>DATA CADASTRO</th>
                            <th>AÇÃO</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($dados->result() as $dado) {
                            ?>
                            <tr>
                                <td><?php echo mb_strtoupper($dado->pos_nome) ?> </td>
                                <td><?php echo $dado->pc_nomeCurso ?> </td>
                                <td><?php echo $this->util->data3($dado->pos_dataCadastro) ?> </td>
                                <td>
                                    <a class='btn btn-xs btn-primary' href="<?php echo site_url('adm/programas/posDetalhar/' . $dado->pos_id) ?>">
                                        DeTALHAR
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } else { ?>
                <h4>Nenhum registro encontrado</h4>
            <?php } ?>
        </div>
    </div>
</div>