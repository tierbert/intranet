<div class="row" style="padding: 10px;">
    <a href="<?php echo site_url('adm/painel'); ?>" class="btn btn-primary">Voltar</a>
</div>

<div class="row">
    <div class = "col-md-12 well" style="margin-bottom: 10px;">
        <div class="col-md-4 text-center">
            <div class = "col-md-12" style="padding: 10px;">
                <h3>
                    Painel Professor 
                </h3>
            </div>
            <div class="thumbnail">
                <?php
                $imgProf = base_url() . 'assets/tv/anuncios/prof.jpg';
                $checkImgProf = @getimagesize($imgProf);
                if ($checkImgProf) {
                    ?>
                    <img src="<?php echo $imgProf; ?>">
                <?php } else {
                    ?>
                    <img src="<?php echo base_url('assets'); ?>/tv/anuncios/padrao.jpg">
                <?php } ?>
                <div class="caption">
                    <hr>
                    <form method="post" action="" enctype="multipart/form-data" class="form-horizontal">
                        <?php if ($checkImgProf) { ?>
                            <div class="text-center">
                                <a href="<?php echo site_url('adm/programas/anunciosDel/prof'); ?>" class="btn btn-primary">
                                    Remover
                                </a>
                            </div>
                        <?php } else { ?>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="finalidade">Link</label>
                                    <input type="text" class="form-control" name="link" required="true">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="finalidade">Imagem</label>
                                    <input type="file" class="form-control" name="upload" required="true">

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>&nbsp;</label>
                                    <input type="submit" class="form-control btn btn-primary" value="Enviar Imagem">
                                </div>
                            </div>
                            <input type="hidden" name="corredor" value="prof">
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>


        <div class="col-md-4">
            <div class = "col-md-12 text-center" style="padding: 10px;">
                <h3>
                    Painel Funcionário 
                </h3>
            </div>
            <div class="thumbnail">
                <?php
                $imgFunc = base_url() . 'assets/tv/anuncios/func.jpg';
                $checkImgFunc = @getimagesize($imgFunc);
                if ($checkImgFunc) {
                    ?>
                    <img src="<?php echo $imgFunc; ?>">
                <?php } else { ?>
                    <img src="<?php echo base_url('assets'); ?>/tv/anuncios/padrao.jpg">
                <?php } ?>
                <div class="caption">
                    <hr>

                    <form method="post" action="" enctype="multipart/form-data" class="form-horizontal">
                        <?php if ($checkImgFunc) { ?>
                            <div class="text-center">
                                <a href="<?php echo site_url('adm/programas/anunciosDel/func'); ?>" class="btn btn-primary">
                                    Remover
                                </a>
                            </div>
                        <?php } else { ?>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="finalidade">Link</label>
                                    <input type="text" class="form-control" name="link" required="true">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="finalidade">Imagem</label>
                                    <input type="file" class="form-control" name="upload" required="true">

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>&nbsp;</label>
                                    <input type="submit" class="form-control btn btn-primary" value="Enviar Imagem">
                                </div>
                            </div>
                            <input type="hidden" name="corredor" value="func">
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>


        <div class="col-md-4 text-center">
            <div class = "col-md-12" style="padding: 10px;">
                <h3>
                    Painel Aluno
                </h3>
            </div>
            <div class="thumbnail">
                <?php
                $imgAluno = base_url() . 'assets/tv/anuncios/alun.jpg';
                $checkImgAlu = @getimagesize($imgAluno);
                if ($checkImgAlu) {
                    ?>
                    <img src="<?php echo $imgAluno; ?>">
                <?php } else { ?>
                    <img src="<?php echo base_url('assets'); ?>/tv/anuncios/padrao.jpg">
                <?php } ?>
                <div class="caption">
                    <hr>
                    <form method="post" action="" enctype="multipart/form-data" class="form-horizontal">
                        <?php if ($checkImgAlu) { ?>
                            <div class="text-center">
                                <a href="<?php echo site_url('adm/programas/anunciosDel/alun'); ?>" class="btn btn-primary">
                                    Remover
                                </a>
                            </div>
                        <?php } else { ?>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="finalidade">Link</label>
                                    <input type="text" class="form-control" name="link" required="true">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="finalidade">Imagem</label>
                                    <input type="file" class="form-control" name="upload" required="true">

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>&nbsp;</label>
                                    <input type="submit" class="form-control btn btn-primary" value="Enviar Imagem">
                                </div>
                            </div>
                            <input type="hidden" name="corredor" value="alun">
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>