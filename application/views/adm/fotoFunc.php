<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css" integrity="sha384-PDle/QlgIONtM1aqA2Qemk5gPOE7wFq8+Em+G/hmo5Iq0CCmYZLv3fVRDJ4MMwEA" crossorigin="anonymous">



    </head>
    <body>

        <?php
        if (isset($_GET['idPerfil'])) {

            $idPerfil = $_GET['idPerfil'];
            // $image_link = "";

            // $data = array(
            //     'token' => '5e0d3b28bd55aa5bbb1d35ff89a08b93',
            //     'format' => 'json',
            //     // 'tipo' => 'COMPLETA',
            //     'filtrarPor' => 'idFuncionario',
            //     'termo' => $idFuncionario
            // );
            // $URI = "http://guanambi.jacad.com.br:80/academico/api/v1/rh/funcionarios/";

            // $ch = curl_init($URI);
            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

            // curl_setopt($ch, CURLOPT_POST, true);
            // curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            // curl_setopt($ch, CURLOPT_URL, $URI);

            // $result = curl_exec($ch);
            // curl_close($ch);
            // $result = json_decode($result, true);

            $image_link = "http://guanambi.jacad.com.br/academico/images/perfil/" . $_GET['idPerfil'] . "/640";

            // foreach ($result as $key) {
            // 	echo $key['ra'] . " - " . $key['nome'] . " - " . $key['foto'] . " - " . $key['login'] . " - " . $key['senha'] . "<br/>";
            // }
        }
        ?>
        <div class="container">

            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <form class="form_aluno form-inline" style="padding: 20px 0px 0px 0px;">
                        <div class="form-group mx-sm-3 mb-2" style="margin-left: 0px !important;width: 80%">
                            <input type="text" class="form-control" style="width: 100%" id="idPerfil" name="idPerfil" placeholder="Informe o código do funcionário" autofocus="on" autocomplete="off">
                        </div>
                        <button type="submit" class="btn btn-primary mb-2">Consultar</button>
                    </form>
                </div>
                <div class="col-md-8 offset-md-2">
                    <span style="font-size: .7em">Códigos para teste: 4537, 11260, 20469, 11226, 7794</span>
                    <hr>
                    <?php
                        if (isset($_GET['idPerfil'])) {
                    ?>
                            <p style="text-align: center;">
                                <img src="<?php echo $image_link; ?>">	
                            </p>
                    <?php } ?>
                </div>
            </div>

        </div>
    </body>
</html>