<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            Avaliação Substitutiva
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('professor/painelprof'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">

    <div class="col-md-12 texto-logo " >
        <h5>Olá Professor: <?php echo $nomeProf; ?></h5>
    </div>
    <section id="inicio">




        <div class="row">

            <div class="col-md-12 text-center">
                <h2 class="section-heading">Avaliação Substitutiva</h2>
                <h3 class="section-subheading text-muted">2019.1</h3>
                <hr>
            </div>
            <div class="col-md-12 text-center">
                <div class="row">
                    <div class="col-md-12">
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="subheading">Instruções:</h4>
                            </div>
                            <div class="timeline-body" style="text-align: justify ; padding: 2em" >
                                <p class="text-topo"></p>
                                <p>
                                    Através do módulo “Avaliação Substitutiva”, “Lista de Assinatura”, o
                                    docente deverá imprimir a lista de presença a ser utilizada no momento
                                    da prova. Apenas os alunos que constarem na lista deverão receber a
                                    prova. Em caso de divergência, verificar junto ao coordenador do curso
                                    ou setor de Protocolo.
                                </p>
                                <p>
                                    É necessário entregar uma cópia da lista de presença (cada disciplina)
                                    assinada pelos alunos à Secretaria, em até 48hs úteis, para fins de
                                    arquivamento.
                                </p>
                                <p>
                                    Caso o aluno compareça no dia da avaliação, mas o seu nome não
                                    conste na lista, o discente deverá ser encaminhado para o setor
                                    financeiro que verificará o comprovante de pagamento.
                                </p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> 
    <section id="menu" style="background: #ededed">
    </section>
    <hr>
    <div class="row">
        <div class="col-lg-12 text-center">
            <h3 class="section-subheading text-muted">Lista de Assinatura</h3>
        </div>
    </div>
    <?php if ($disciplinas) { ?>
        <table class="table  table-hover table-striped table-condensed">
            <thead>
                <tr>
                    <th>Disciplina</th>
                    <th>Professor</th>
                    <th>Semestre/Turno</th>
                    <th>Turma</th>
                    <th class="text-center">Total de <br>Solicitações pagas</th>
                    <th></th>
                </tr>
            </thead>
            <?php foreach ($disciplinas->result() as $disciplina) { ?>
                <tr>
                    <td><?php echo $disciplina->hor_disciplina; ?></td>
                    <td><?php echo $disciplina->hor_prof; ?></td>
                    <td><?php echo $disciplina->hor_turno; ?></td>
                    <td><?php echo $disciplina->hor_turma; ?></td>
                    <td class="text-center">
                        <?php
                        $totais = 0;
                        $dados = $this->solicitacoesmodel->totaisDisciplina($disciplina->hor_id, '2019-1');

                        if (isset($dados['pago'])) {
                            $totais += $dados['pago'];
                        }
                        if (isset($dados['baixado'])) {
                            $totais += $dados['baixado'];
                        }
                        echo $totais
                        ?>
                    </td>
                    <td>
                        <?php if ($totais != 0) { ?>
                            <a href="<?php echo site_url('adm/solicitacoesadm/relatorioAssinatPdf/' . $disciplina->hor_id); ?>" target="_blanck" class="btn btn-info btn-xs">
                                Imprimir
                            </a>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
    <?php } ?>
</div>