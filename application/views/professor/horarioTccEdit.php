<div class="painel" style="min-height: 400px;">
    <div class="row">
        <div class="col-md-6 ">
            <h2>NOVO HORÁRIO DE ATENDIMENTO</h2>
        </div>
        <div class="col-md-6 text-right">
            <a class='btn btn-info' href='<?php echo site_url('professor/painelprof/horarioTccNovo') ?>'>
                NOVO HORARIO
            </a>    
            <a href="<?php echo site_url('professor/painelprof/agendados'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
    <legend style="padding-top: 5px; padding-bottom: 5px;"></legend>

    <div class="container">


        <div class=form-horizontal>    
            <div>
                <?php echo validation_errors(); ?>
            </div>
            <?php echo form_open() ?>
            <div class=form-group>
                <?php //$this->util->input('tch_idProfessor', 'tch_idProfessor', $idProf); ?>
                <?php $this->util->input2('DATA', 'tch_data', $this->util->data($object->tch_data), 'data'); ?>
                <?php $this->util->input2('VAGAS', 'tch_vagas', $object->tch_vagas); ?>
                <?php $this->util->input2('HORARIO', 'tch_horario', $object->tch_horario); ?>
            </div>
            <div class=form-group>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <input type=submit  value=Salvar class='btn btn-primary form-control'>
                </div>

            </div>            
            <?php echo form_close() ?>
        </div>
        <?php $this->util->mascaras() ?>
    </div>
</div>