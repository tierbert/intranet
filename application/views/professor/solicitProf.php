<link href="<?php echo base_url(); ?>assets/css/css/metisMenu.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/css/sb-admin-2.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/css/css/morris.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/css/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<div>
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Solicitações</h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->


    <div class="row">

        <h4>Professor</h4>

        <div class="col-lg-3 col-md-6">
            <a href="<?php echo site_url('professor/painelprof/listaAssProf'); ?>">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="glyphicon glyphicon-print fa-3x" ></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"></div>
                                <div>Lista de Assinatura</div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <span class="pull-left">Consulte</span>
                        <span class="pull-right">
                            <i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>

                </div>
            </a>
        </div>



    </div>






</div>