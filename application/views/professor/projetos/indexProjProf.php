<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
           <!-- <?php echo ucfirst($titulo); ?> --> MINHAS SOLICITAÇÕES DE COMPRAS
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a   class='btn btn-info' href='<?php echo site_url('professor/projetosprof/criar/') ?>'>
                SOLICITAR COMPRA
            </a>
            <a href="<?php echo site_url('professor/painelprof/'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class="row">
        <div class="col-lg-12">
            <?php if ($dados->num_rows() != 0) { ?>
                <table class='table table-bordered table-striped table-condensed table-hover'>
                    <thead>
                        <tr>
                           <!-- <th>TÍTULO</th>--> 
                            <th>REQUISIÇÃO Nº</th>
                            <th>COMPRA</th>
                            <th>DATA</th>
                            <th>CUSTO</th>
                            <th>AÇÃO</th> 
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($dados->result() as $dado) {
                            ?>
                            <tr class="<?php //echo $cor;  ?>">
                                <td><?php echo $dado->pro_id ?> </td>
                                <td><?php echo $dado->pro_titulo ?> </td>                            
                                <td><?php echo $this->util->data2($dado->pro_dataApresentacao)   ?> </td>
                                <td> R$ <?php echo $dado->pro_custo ?> </td>
                               	<td> <a class='btn btn-xs btn-warning' href="<?php echo site_url('adm/ProjetosAdm/nupexPendentesAval/' . $dado->pro_id) ?>">
                                    DETALHES
                                </a> </td>
                               <!-- <td><?php echo $dado->pro_status ?> </td> -->
                               
                            </tr>
                        <?php } ?>

                    </tbody>
                </table>
                <ul class='pagination pagination-small'>
                    <li>
                        <?php //echo $paginacao;   ?>
                    </li>
                </ul>
            <?php } else { ?>
                <h4>Nenhum registro encontrado</h4>
            <?php } ?>

        </div>
    </div>
</div>