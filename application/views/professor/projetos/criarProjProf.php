<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            <?php echo ucfirst($titulo); ?>
        </div>
        <div class="col-md-6 col-xs-6 text-right">
            <a href="<?php echo site_url('professor/projetosprof/'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class="row">
        <div class="col-md-6 ">
            <h3 style="text-align: center;">NOTAS FISCAL</h3>
        </div>

    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="form-horizontal">
                <?php echo form_open_multipart(); ?>
                <div class="form-group">
                    <div class="col-md-9">
                        <label>RAZÃO SOCIAL</label>
                        <input name="titulo" type="text" class="form-control" required="true">
                    </div>
                    <div class="col-md-3">
                        <label>NÚMERO DA NOTA FISCAL</label>
                        <input name="titulo" type="text" class="form-control" required="true">
                    </div>
                    <div class="col-md-2">
                        <label>VENCIMENTO</label>
                        <input name="data" type="text" class="form-control data" required="true">
                    </div>
                    <div class="col-md-2">
                        <label>COMPETÊNCIA</label>
                        <input name="custo" type="text" class="form-control data" required="true">
                    </div>
                    <div class="col-md-3">
                        <label>FORMA DE PAGAMENTO</label>
                        <select name="pro_tipo" class="form-control" required="true">
                            <option value=''>SELECIONE O TIPO</option>
                            <option value="PESQUISA_EXTENSAO">DEPÓSITO</option>
                            <option value="ATIVIDADE_COMPLEMENTAR">DINHEIRO</option>
                            <option value="ATIVIDADE_COMPLEMENTAR">BOLETO</option>
                            <option value="ATIVIDADE_COMPLEMENTAR">TRANFERÊNCIA BANCÁRIA</option>
                        </select>
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <div class="col-md-11">
                        <label>DESCRIÇAO</label>
                        <textarea name="observacoes" class="form-control" style="height: 100px;"></textarea>
                    </div>
                </div>
                <hr>
                <h3>
                    ENVIAR NOTA FISCAL 
                </h3>
                <br>
                <div class="form-group">
                    <div class="col-md-5">   
                        <label>NOTA FISCAL</label> 
                        <input name="anexos[]" type="file" class="form-control" required="true"></div>
                    <div class="col-md-3">   
                        <label>COMPETÊNCIA</label>  
                        <input name="obs[]" type="text" class="form-control data">
                    </div>
                    <br>
                </div>
                <div class="form-group">
                    <div class="col-md-5">   
                        <label>BOLETOS</label> 
                        <input name="anexos[]" type="file" class="form-control" required="true"></div>
                    <div class="col-md-3">   
                        <label>DATA DE VENCIMENTO</label>  
                        <input name="obs[]" type="text" class="form-control data">
                    </div>
                    <br>
                </div>
                <div class="divAnexos" style="padding-top: 15px;">

                </div>
                <br>
                <a class="btn btn-warning btn-xs" id="btAddAnexo">
                    ADICIONAR MAIS BOLETOS
                </a>
                <hr>
                <div class="form-group">
                    <div class="col-md-3">
                        <label>&nbsp;</label>
                        <input type="submit" class="form-control btn btn-success" value="ENVIAR PROJETO">
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->util->mascaras() ?>

<script>
    var id = 1;

    $('#btAddAnexo').click(function () {
        $('.divAnexos').append("<div class='form-group divAnexo_" + id + "'>" +
                "<div class='col-md-5'>" +
                "      <label>BOLETOS</label>" +
                "     <input name='anexos[]' type='file' class='form-control' required='true'>" +
                "</div>" +
                "<div class='col-md-3'>" +
                "      <label>DATA DE VENCIMENTO</label>" +
                "     <input name='obs[]' type='text' class='form-control data'>" +
                "</div>" +
                "<div class='col-md-2'>" +
                "      <label>&nbsp;</label><br>" +
                "<a onclick='removeAnexo(" + id + ")' class='' id='anexo" + id + "' ><span class='glyphicon glyphicon-trash'></span></a></div><br></div>" +
                "");
        id++;
    });

    function removeAnexo(idAnexo) {
        $('.divAnexo_' + idAnexo).remove();
    }

</script>
