<body>
<br>
<a href="<?php echo site_url('professor/painelprof'); ?>" class="btn btn-primary"> Voltar </a>
<div class="container">
    <div class="row">

    			<div class="col-lg-12 text-center">
                        <h3 class="section-heading">Os vídeos abaixo têm como objetivo instruir os docentes no lançamento de notas e faltas no novo Sistema Acadêmico JACAD.</h3>
                        <h3 class="section-subheading text-muted">Acesse abaixo.</h3>
                    </div>

   				<div class="col-lg-6">
				    <div class="timeline-heading" style="text-align: center">
				        <h4 class="subheading"> Lançamento de Notas.</h4>
				    </div>                
				
				    <div class="thumbnail" >
				        <div class="embed-responsive embed-responsive-16by9">
				            <video  controls class="embed-responsive-item" width="440" height="280" 
				                src="<?php  echo base_url('assets\videos\professor\pp_notas_fg.mp4'); ?>" frameborder="0" allowfullscreen>
				            </video> 
				        </div>
				    </div>
				</div>
			
			       
				<div class="col-lg-6">
				    <div class="timeline-heading" style="text-align: center">
				        <h4 class="subheading"> Lançamento de Faltas.</h4>
				    </div>
				        
				    <div class="thumbnail">
				        <div class="embed-responsive embed-responsive-16by9">
				        	<video controls class="embed-responsive-item" width="440" height="280" 
				                src="<?php  echo base_url('assets\videos\professor\pp_faltas_fg.mp4'); ?>" frameborder="0" allowfullscreen>
				            </video>
				        </div>
				    </div>
				</div>
	</div>
</div> 
</body>
    <div>
        <?php include("rodape.php") ?>
    </div>

