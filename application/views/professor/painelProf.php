
<div class="box"  style="">
    <div class="row">
        <div class="col-md-6 col-xs-6 box-titulo" style="">
            ACESSO INTERNO
        </div>
        <div class="col-md-6 col-xs-6 text-justify">
            <?php
            $nome = $this->session->userdata('usu_nome');
            ?>

            <h4> Olá <?php echo $nome; ?>,</h4> SEJA BEM VINDO À INTRANET. 
        </div>
    </div>
</div>

<div class="painel" style="min-height: 150px;">
    <div class="col-md-1 textoVertical" style="border-left: 4px solid; border-color: #337ab7; width: 50px">
        AGENDAS
    </div>
    <!--
    <div class="col-md-2 text-center">
        <a href="<?php echo site_url('professor/painelprof/agendados'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-calendar"></span>
            <br>
            AGENDAMENTO <br>
            PLANTÃO DE TCC
        </a>
    </div>

    <div class="col-md-2 text-center">
        <a href="<?php echo site_url('professor/forumAprendP'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-comment"></span>
            <br>
            FÓRUM DE ENSINAGEM
        </a>
    </div>
    -->
    <div class="col-md-2 text-center">
        <a href="<?php echo site_url('professor/horarioprof'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-blackboard"></span>
            <br>
            AGENDAR<br>SALA / LABORATÓRIO
        </a>
    </div>
</div>
<!--
<div class="painel" style="min-height: 150px;">
    <div class="col-md-1 textoVertical" style="border-left: 4px solid; border-color: #337ab7; width: 50px">
        OUTROS
    </div>
    <div class="col-md-2">
        <a href="<?php echo site_url('professor/painelprof/videoswa'); ?>">
            <img class="img-circle img-responsive" src="<?php echo base_url('assets/tema/'); ?>images/about/swa.png" style="width: 100px; margin-left: 20px;">
            <span class="" style="margin-left: 20px;">
                MÓDULO SWA

            </span>
        </a>
    </div>

    <div class="col-md-2 text-center">
        <a href="<?php echo site_url('professor/painelprof/listaAssProf'); ?>">
            <span style="font-size: 60px;" class="glyphicon glyphicon-bookmark"></span>
            <br>
            AVALIAÇÃO<br>SUBSTITUTIVA
        </a>
    </div>
    
    
       <div class="col-md-2 text-center">
           <a href="<?php echo base_url(''); ?>/assets/materialApoio/professores/E-book - slides para aulas proveitosas.pdf" target="_blanck">
            <span style="font-size: 60px;" class="glyphicon glyphicon-file"></span>
            <br>
            SLIDES PARA AULAS PROVEITOSAS
        </a>
    </div>
    
</div>
-->
<!--
<div class="row" style="margin-top: 10px;">

    <div class="col-md-3 ">                      
        <a style="text-decoration: none;" href="<?php echo site_url('professor/projetosprof'); ?>">
            <div class="service" >
                <figure>
                    <i class="glyphicon glyphicon-duplicate i2" aria-hidden="true"></i>
                    <div class="separator" ></div>
                    <figcaption>
                        <h5 style="text-align: center;">
                            Solicitação de Projetos
                        </h5>
                    </figcaption>
                </figure>                       
            </div>
        </a>
    </div>

</div>
-->
<div class="painel" style="min-height: 170px;">
    <div class="col-md-1 textoVertical" style="border-left: 4px solid; border-color: #337ab7; width: 50px">
        &nbsp; PERIÓDICOS 
    </div>
    <div class="col-md-2 text-center">
        <a href="<?php echo site_url('aluno/Indexaluno/mestrado'); ?>" target="_blanck">
            <span style="font-size: 60px;" class="glyphicon glyphicon-tag"></span>
            <br>
            BASE DE DADOS DA REVISTA DOS TRIBUNAIS ON-LINE
        </a>

    </div>


    <div class="col-md-2 text-center">
        <a href="https://proview.thomsonreuters.com/library.html?sponsor=GUANAMBI-1" target="_blanck">
            <span style="font-size: 60px;" class="glyphicon glyphicon-tag"></span>
            <br>
            BIBLIOTECA REVISTA DOS TRIBUNAIS
        </a>

    </div>


</div>

<?php
$imgProf = base_url() . 'assets/tv/anuncios/prof.jpg';
?>
<button type="button" class="btn btn-primary btn-lg btn-modal hidden" data-toggle="modal" data-target="#myModal">
    Launch demo modal
</button>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin: 50px; width: 92%; height: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Fechar</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <?php $link = $this->db->where('anu_perfil', 'prof')->get('anuncios')->row() ?>
                <a href="<?php echo $link->anu_link; ?>" target="_blanck">
                    <img src="<?php echo $imgProf; ?>" class="img-responsive">
                </a>
            </div>
        </div>
    </div>
</div>
<?php
if (@getimagesize($imgProf)) {
    ?>
    <script>
        $('.btn-modal').click();
    </script>
<?php } ?>
<style type="text/css" >
    .textoVertical {
        writing-mode:tb-rl;
        -webkit-transform:rotate(180deg);
        -moz-transform:rotate(270deg);
        -o-transform: rotate(270deg);
        font-weight: bold;
        color: #337ab7;
        font-size: 18px;
    }
</style>
