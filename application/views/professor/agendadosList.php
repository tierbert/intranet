<div class="painel" style="min-height: 400px;">
    <div class="row">
        <div class="col-md-6 ">
            <h2>ALUNOS AGENDADOS PARA O HORÁRIO</h2>
        </div>
        <div class="col-md-6 text-right">
            <a class='btn btn-info' href='<?php echo site_url('professor/painelprof/horarioTccNovo') ?>'>
                NOVO HORARIO
            </a>    
            <a href="<?php echo site_url('professor/painelprof/agendados'); ?>" class=" btn btn-primary">
                Voltar
            </a>
        </div>
    </div>
    <legend style="padding-top: 5px; padding-bottom: 5px;"></legend>

    <div class="container">


        <?php if (count($agendados) != 0) { ?>
            <table class='table table-bordered table-striped table-condensed table-hover'>
                <thead>
                    <tr>
                        <th>Data</th>
                        <th>Horário</th>
                        <th>Aluno</th> 
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($agendados->result() as $dado) {
                        ?><tr>
                            <td><?php echo $this->util->data($dado->tch_data) ?> </td>
                            <td><?php echo $dado->tch_horario ?> </td>
                            <td><?php echo $dado->alu_nome ?> </td> 
                            <td>
                            </td>

                        </tr>
                    <?php } ?>

                </tbody>
            </table>
            <ul class='pagination pagination-small'>
                <li>
                    <?php //echo $paginacao; ?>
                </li>
            </ul>
        <?php } else { ?>
            <h4>Nenhum registro encontrado</h4>
        <?php } ?>
    </div>
</div>

