<link href="<?php echo base_url('assets'); ?>/bootstrap337/css/bootstrap.css" rel="stylesheet"> 
<link href="<?php echo base_url('assets/tema/'); ?>fonts/font-awesome/css/font-awesome.css" rel="stylesheet"> 
<script type="text/javascript" src="<?php echo base_url('assets/tema/'); ?>plugins/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/bootstrap337/js/bootstrap.js"></script>
<div class="row" style="padding:  10px 0 0  20px ;">
    <div class="col-md-12">

    </div>
    <div class="row">
        <div class="col-lg-12 text-center">

            <h2 class="section-heading">Agendamento Sala de Aula</h2>
            <h3 class="section-subheading text-muted">Solicite abaixo.</h3>
            <!--<button class="btn btn-success btn-xs">Sala Disponível</button>
            <button class="btn btn-primary btn-xs">Agendamento Confirmado</button>
            <button class="btn btn-warning btn-xs">Aguardando Confirmação</button>
            <button class="btn btn-danger btn-xs">Sala Ocupada</button> -->

            <span class="label label-success">SALA DISPONÍVEL</span>
            <span class="label label-primary">AGENDAMENTO CONFIRMADO</span>
            <span class="label label-warning">AGUARDANDO CONFIRMAÇÃO</span>
            <span class="label label-danger">SALA OCUPADA</span>
        </div> 

    </div>

</div>
<div class="row" style="padding: 20px 20px 0 20px;">
    <div class="col-md-12">
        <?php
        /*
          if ($semana == '') {
          $day = date('w');
          } else {
          $day = $semana;
          }
          echo $day;

          $domingo = date('m-d-Y', strtotime('-' . $day . ' days'));
          $datas = array(
          '1' => date('d-m-Y', strtotime('+' . (1 - $day) . ' days')), //segunda
          '2' => date('d-m-Y', strtotime('+' . (2 - $day) . ' days')), // terca
          '3' => date('d-m-Y', strtotime('+' . (3 - $day) . ' days')), // quarta
          '4' => date('d-m-Y', strtotime('+' . (4 - $day) . ' days')), // quinta
          '5' => date('d-m-Y', strtotime('+' . (5 - $day) . ' days')), //sexta
          '6' => date('d-m-Y', strtotime('+' . (6 - $day) . ' days')), //sabado
          );
         * 
         */



        if ($semana == '') {
            $semana = date('W') - 1;
            $day = date('w');
            $domingo = date('m-d-Y', strtotime('-' . $day . ' days'));
            $datas = array(
                '1' => date('d-m-Y', strtotime('+' . (1 - $day) . ' days')), //segunda
                '2' => date('d-m-Y', strtotime('+' . (2 - $day) . ' days')), // terca
                '3' => date('d-m-Y', strtotime('+' . (3 - $day) . ' days')), // quarta
                '4' => date('d-m-Y', strtotime('+' . (4 - $day) . ' days')), // quinta
                '5' => date('d-m-Y', strtotime('+' . (5 - $day) . ' days')), //sexta
                '6' => date('d-m-Y', strtotime('+' . (6 - $day) . ' days')), //sabado
            );
        } else {
            $dias = ($semana * 7) - 1;
            $diaDomingo = date('Y-m-d', strtotime('2019-01-01' . " + $dias  days"));

            $day = date('w', strtotime($diaDomingo));
            $domingo = date('m-d-Y', strtotime('-' . $day . ' days'));
            $datas = array(
                '1' => date('d-m-Y', strtotime($diaDomingo . '+' . (1 - $day) . ' days')), //segunda
                '2' => date('d-m-Y', strtotime($diaDomingo . '+' . (2 - $day) . ' days')), // terca
                '3' => date('d-m-Y', strtotime($diaDomingo . '+' . (3 - $day) . ' days')), // quarta
                '4' => date('d-m-Y', strtotime($diaDomingo . '+' . (4 - $day) . ' days')), // quinta
                '5' => date('d-m-Y', strtotime($diaDomingo . '+' . (5 - $day) . ' days')), //sexta
                '6' => date('d-m-Y', strtotime($diaDomingo . '+' . (6 - $day) . ' days')), //sabado
            );
        }


        $volta = '';
        $avanca = '';
        $semanaAtual = date('W') - 1;

        if ($semana == $semanaAtual) {
            $volta = 'disabled';
        }

        /*
          elseif ($semana > $semanaAtual + 1) {
          //$avanca = 'disabled';
          }
          if ($semana > $semanaAtual + 2) {
          echo "<script>;window.location.href='" . site_url() . "professor/horarioprof/calendario/$tipo/'</script>";
          }
         * 
         */
          //new code

          $day = date('w');
        $domingo = date('m-d-Y', strtotime('-' . $day . ' days'));
        $days_to_exclude = array();
        if($tipo == 'sala'){
            $days_to_exclude = array(
                '22-08-2019', '23-08-2019', '08-08-2019', '09-08-2019', '15-08-2019', '16-08-2019', '01-08-2019', '02-08-2019',
                '05-09-2019', '06-09-2019', '12-09-2019', '13-09-2019', '19-09-2019', '20-09-2019', 
                '30-10-2019', '31-10-2019', '03-10-2019', '03-10-2019', 
                '21-11-2019', '22-11-2019', '28-11-2019', '29-11-2019', 
                '05-12-2019', '06-12-2019', '12-12-2019', '13-12-2019'
            );
        }
        $dias_da_semana = array(
            'DOMINGO',
            'SEGUNDA',
            'TERÇA',
            'QUARTA',
            'QUINTA',
            'SEXTA',
            'SÁBADO'
        );

        $datas = array(
            date('d-m-Y', strtotime('+' . (1 - $day) . ' days')), //segunda
            date('d-m-Y', strtotime('+' . (2 - $day) . ' days')), // terca
            date('d-m-Y', strtotime('+' . (3 - $day) . ' days')), // quarta
            date('d-m-Y', strtotime('+' . (4 - $day) . ' days')), // quinta
            date('d-m-Y', strtotime('+' . (5 - $day) . ' days')), //sexta
            date('d-m-Y', strtotime('+' . (6 - $day) . ' days')), //sabado
        );

        $valid_days = array_values(array_diff($datas, $days_to_exclude));

          //----end of new code---
        ?>
        <div class="box"  style="">
            <div class="row">
                <div class="col-md-6 box-titulo" style="">
                    <?php echo $semana; ?>
                </div>

                <div class="col-md-2">
                    <a href="<?php echo site_url('professor/horarioprof/calendario/' . $tipo . '/' . ($semana - 1)); ?>" class=" btn btn-primary <?php echo $volta; ?>">
                        <span class="glyphicon glyphicon-backward"></span>
                    </a>
                    <a href="<?php echo site_url('professor/horarioprof/calendario/' . $tipo); ?>" class=" btn btn-primary ">
                        <span class="glyphicon glyphicon-home"></span>
                    </a>
                    <a href="<?php echo site_url('professor/horarioprof/calendario/' . $tipo . '/' . ($semana + 1)); ?>" class=" btn btn-primary <?php echo $avanca; ?>">
                        <span class="glyphicon glyphicon-forward"></span>
                    </a>
                </div>
                <div class="col-md-4 ">

                    <a href="<?php echo site_url('professor/horarioprof'); ?>" class=" btn btn-primary pull-right">
                        VOLTAR
                    </a>
                </div>
            </div>
        </div>



        <table class="table table-striped table-bordered table-hover">
            <tr>
                <th></th>
                <?php for ($i = 0; $i < sizeof($valid_days) ; $i++) { ?>
                            <th><?php echo $dias_da_semana[date('w', strtotime($valid_days[$i]))] ?> - <?php echo $valid_days[$i]; ?></th>        

                  <?php } ?>
                <th></th>
            </tr>
            <?php
//echo $domingo . '- ' . $segunda;
            foreach ($turnos->result() as $turno) {
                ?>
                <tr>
                    <td>
                        <?php echo $turno->tur_turno . '<br>' . $turno->tur_horario; ?>
                    </td>
                    <?php
                    for ($i = 0; $i < sizeof($valid_days); $i++) {
                        ?>
                        <td>
                            <?php
                            $array_size = sizeof($valid_days);
                            $salas = $this->horariom->getSalas($turno->tur_id, $i, $tipo, 'ATIVA', $valid_days[0], $valid_days[$array_size-1]);
                            foreach ($salas->result() as $sala) {
                                $nomeSala = $sala->sal_abreviatura;

                                /* se reservada */
                                if ($sala->res_id != NULL) {
                                    if ($sala->res_status == 'AGUARDANDO') {
                                        ?>
                                        <a href="<?php echo site_url('professor/horarioprof/reserva/' . $turno->tur_id . '/' . $sala->sal_id . '/' . $i . '/' . $datas[$i] . '/' . $tipo); ?>" class="btn btn-warning btn-xs">
                                            <?php echo $nomeSala; ?>
                                        </a>
                                    <?php } else { ?>
                                        <a href="<?php echo site_url('professor/horarioprof/reserva/' . $turno->tur_id . '/' . $sala->sal_id . '/' . $i . '/' . $datas[$i] . '/' . $tipo); ?>" class="btn btn-primary btn-xs">
                                            <?php echo $nomeSala; ?>
                                        </a>
                                        <?php
                                    }
                                } elseif ($sala->hor == NULL) {
                                    /* se lvre */
                                    ?>
                                    <a href="<?php echo site_url('professor/horarioprof/reserva/' . $turno->tur_id . '/' . $sala->sal_id . '/' . $i . '/' . $datas[$i] . '/' . $tipo); ?>" class="btn btn-success btn-xs">
                                        <?php echo $nomeSala; ?>
                                    </a>

                                <?php } else {
                                    ?>
                                    <a class="btn btn-danger btn-xs" >  
                                        <?php echo $nomeSala; ?>
                                    </a>
                                    <?php
                                }
                            }
                            ?>
                        </td>
                    <?php } ?>
                    <td></td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>

<div id="conteudo" style="display: none">
    <div>This is your div content</div>
    <div class="sala-titulo">..</div>
</div>
<style>
    .btn-xs2{
        min-width: 0px; 
        min-height: 0px; 
        padding: 0 3px  0 3px;
        margin: 0 0 0 0;
    }
</style>

<script>
    $(function () {
        $('[rel=popover]').popover({
            placement: 'auto right',
            trigger: 'manual',
            html: true,
            trigger: ' hover | focus | manual',
            content: function () {
                $('.sala-titulo').html('tests');
                return $('#conteudo').html();
            }
        });

    })
</script>