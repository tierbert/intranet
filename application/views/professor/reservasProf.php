<div class="box"  style="">
    <div class="row">
        <div class="col-md-4 col-xs-6 box-titulo" style="">
            Minhas Reservas de sala
        </div>
        <div class="col-md-8 col-xs-6 text-right">
            <a class='btn btn-info' href='<?php echo site_url('professor/horarioprof/calendario/labsaude2') ?>'>
                RESERVAR<br> LAB. DE SAÚDE II
            </a>
            <a class='btn btn-info' href='<?php echo site_url('professor/horarioprof/calendario/lab') ?>'>
                RESERVAR <br>LAB. DE SAÚDE
            </a>

            <?php if ($tipo == 'professor') { ?>
                <a href="<?php echo site_url('professor/painelprof'); ?>" class=" btn btn-primary">
                    Voltar
                </a>
            <?php } elseif ($tipo == 'aluno') { ?>
                <a href="<?php echo site_url('aluno/indexaluno'); ?>" class=" btn btn-primary">
                    Voltar
                </a>
            <?php } ?>
        </div>
    </div>
</div>

<div class="painel" style="min-height: 400px;">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    Prezado professor, nesta área você pode consultar o status das suas reservas para sala de aula.
                    <br>
                    <b> Atente-se a legenda:</b> 
                    <span class="label label-success">SALA CONFIRMADA</span>
                    <span class="label label-danger">AGENDAMENTO CANCELADO</span>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <?php if (count($dados) != 0) { ?>
                <table class='table table-bordered table-striped table-condensed table-hover tabela-aredondada'>
                    <thead>
                        <tr>
                            <th>Data</th>
                            <th>Sala / LAB</th>
                            <th>TIPO</th>
                            <th>Turno</th>
                            <th>Status</th>
                            <?php if ($tipo == 'aluno') { ?>
                                <th>Estagiário</th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($dados->result() as $dado) {
                            $cor = '';
                            if ($dado->res_status == 'CONFIRMADA') {
                                $cor = 'success';
                            } elseif ($dado->res_status == 'CANCELADA') {
                                $cor = 'danger';
                            } else {
                                //$cor = 'warning';
                            }
                            ?>
                            <tr class="<?php echo $cor; ?>">
                                <td><?php echo $this->util->data($dado->res_data) ?> </td>
                                <td><?php echo $dado->sal_nome ?> </td>
                                <td><?php echo $dado->sal_tipo ?> </td>
                                <td><?php echo $dado->tur_horario ?> </td>
                                <td><?php echo $dado->res_status ?> </td>
                                <?php if ($tipo == 'aluno') { ?>
                                    <td><?php echo $dado->res_estagiario ?></td>
                                <?php } ?>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <ul class='pagination pagination-small'>
                    <li>
                        <?php //echo $paginacao;  ?>
                    </li>
                </ul>
            <?php } else { ?>
                <h4>Nenhum registro encontrado</h4>
            <?php } ?>

        </div>
    </div>
</div>