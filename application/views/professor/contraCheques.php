<div class="container">
    <div class="row">
        <h3>HOLLERITH</h3>
        <hr>
        <div class="row">
            <?php if (isset($dados) && count($dados) != 0) { ?>
            <table class='table table-bordered table-striped table-condensed table-hover' style="width: 40%; margin-left: 15px;">
                    <thead>
                        <tr>
                            <th>Registro RH</th>
                            <th>Mês</th>
                            <th>Ano</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($dados->result() as $professor) {
                            //$contracheque = $this->rh_contrachequemodel->getByFunCompetencia($professor->pro_registroRh, $mes, $ano);
                            ?><tr>
                                <td><?php echo $professor->rhc_registroRh ?> </td>
                                <td><?php echo $professor->rhc_mes ?> </td>
                                <td><?php echo $professor->rhc_ano ?> </td>
                                
                                <td>
                                    <?php if ($professor->rhc_arquivo != '') { ?>
                                        <?php
                                        $attr = array('width' => '1000', 'height' => '500', 'class' => 'btn btn-primary btn-xs', 'type' => "button");
                                        echo anchor_popup('adm/rh/contracheque/download/' . $professor->rhc_id, "<i class='glyphicon glyphicon-file'></i>", $attr)
                                        ?>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } else { ?>
                <h4>Nenhum registro encontrado</h4>
            <?php } ?>
        </div>


    </div>
</div>

<link href="<?php echo base_url('assets/'); ?>select2/css/select2.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('assets/'); ?>select2/js/select2.js"></script>
<script>

    $('.select2').select2();

</script>