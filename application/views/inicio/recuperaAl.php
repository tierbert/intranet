<img class="img-circle" id="img_logo" src="<?php echo base_url('assets/'); ?>img/logo.png">
<h1 style="color:#ffffff; text-shadow: 1px 1px 1px #000000">RECUPERAR SENHA</h1>
<div class="container" style="width: 80%">
    <div id="div-forms">
        <form id="login-form" action="" method="post">
            <div class="modal-body">
                
                <input name="email" class="form-control" type="text" placeholder="Digite seu número de matrícula" required>
                <br>
                <input type="submit" value="Enviar senha para meu e-mail" class="btn btn-primary pull-left">
                <div class="checkbox" style="text-align:left">
                    <label>
                        <a href="<?php echo site_url('inicio'); ?>">
                        Voltar
                    </a>
                    </label>
                </div>
            </div>
            <div class="modal-footer">
                <div>
                    
                </div>
            </div>
        </form>
    </div>
</div>


