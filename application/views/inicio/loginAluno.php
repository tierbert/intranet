<img class="img-circle" id="img_logo" src="<?php echo base_url('assets/'); ?>img/logo.png">
<h1 style="color:#ffffff; text-shadow: 1px 1px 1px #000000">LOGIN ALUNO </h1>
<div class="container" style="width: 80%">
    <div id="div-forms">
        <form id="login-form" action="" method="post">
            <div class="modal-body">
                <div id="div-login-msg">
                    <span id="text-login-msg"><b>Digite seu Login e senha.</b></span>
                </div>
                <input type="hidden" name="agenda" value="<?php echo $agenda; ?>">
                
                <input name="matricula" id="login_username" class="form-control" type="text" placeholder="Nº de Matricula" required>
                <input name="senha" id="login_password" class="form-control" type="password" placeholder="Senha" required>
                <div class="checkbox" style="text-align:left">
                    <label>
                        <a href="<?php echo site_url('inicio/recuperaAl'); ?>">
                            Esqueci Minha Senha
                        </a>
                    </label>
                </div>
            </div>
            <div class="modal-footer">
                <div>
                    <button type="submit" class="btn btn-skin scroll btn-lg btn-block">
                        Login
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>