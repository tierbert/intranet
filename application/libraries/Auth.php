<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth {

    private $ci;

    public function __construct() {
        $this->ci = &get_instance();
    }

    function verificaloginAdm() {
        $this->CI = & get_instance();

        $logado = $this->ci->session->userdata('loginPainelAdm');
        $idAluno = $this->ci->session->userdata('usu_id');
        $perfil = $this->ci->session->userdata('usu_perfil');

        if ($logado == 'loginPainelAdm') {
            return true;
        } else {
            //echo 'erro';
            $this->ci->session->sess_destroy();
            echo "<script>window.location.href='" . site_url() . "'</script>";
            exit();
        }
    }

    function verificaloginProf() {
        $this->CI = & get_instance();

        $logado = $this->ci->session->userdata('loginPainelProf');
        $idAluno = $this->ci->session->userdata('usu_id');
        $perfil = $this->ci->session->userdata('usu_perfil');

        if ($logado == 'loginPainelProf') {
            return true;
        } else {
            $this->ci->session->sess_destroy();
            echo "<script>window.location.href='" . site_url() . "'</script>";
            exit();
        }
    }

    function perfil() {
        $this->CI = & get_instance();
        $perfil = $this->ci->session->userdata('usu_perfil');
        return $perfil;
    }

    function liberado($liberado) {
        $this->CI = & get_instance();
        $perfil = $this->ci->session->userdata('usu_perfil');
        $liberados = explode('|', $liberado);


        $loginTipo = $this->ci->session->userdata('loginTipo');

        if (in_array($perfil, $liberados)) {
            return true;
        } else {
            if ($loginTipo == 'adm') {
                echo "<script>alert('Acesso nao permitido para este perfil');window.location.href='" . site_url('adm/painel') . "'</script>";
            } elseif ($loginTipo == 'professor') {
                echo "<script>alert('Acesso nao permitido para este perfil');window.location.href='" . site_url('professor/painel') . "'</script>";
            } else {
                
            }
        }
    }

}
