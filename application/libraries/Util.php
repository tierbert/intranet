<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Util {

    function __construct() {
        
    }

    public function horarios() {
        $arquivo = file_get_contents('./assets/sala/horarios.html');
        $dados = explode('<p>', $arquivo);
        return $dados;
    }

    public function hor_prof() {
        $arquivo = file_get_contents('./assets/sala/horarios.html');
        $dados = explode('<p>', $arquivo);
        $horarios = array('' => '');
        foreach ($dados as $key => $value) {
            if ($key == 0) {
                continue;
            }
            $pos = strpos($dados[$key], '<h1>');
            $pos2 = strpos($dados[$key], '</h1>');
            $horario = substr($dados[$key], $pos, $pos2);
            $horario = strip_tags($horario);
            $horarios[$key] = $horario;
        }

        asort($horarios);

        return $horarios;
    }

//Input tipo texto
    public function input($label = '', $name = '', $value = '', $class = '', $span = '4', $id = '', $extra = '', $smlabel = 2, $type = 'text') {

        if ($name != '') {
            $name = " name='$name'";
        }

        if ($value != '') {
            $value = " value='$value'";
        }
        if ($id != '') {
            $id = " id='$id'";
        }


        echo "
        <div class='form-group'>
            <label class='control-label col-sm-$smlabel' for='$name'>$label: </label>
            <div class='col-sm-$span'>
                <input $name $value $id  class='form-control $class' type='$type' $extra> 
            </div>
        </div>";
    }

    public function input2($label = '', $name = '', $value = '', $class = '', $span = '4', $id = '', $extra = '', $smlabel = 3, $tipo = 'text') {

        if ($name != '') {
            $name = " name='$name'";
        }

        if ($value != '') {
            $value = " value='$value'";
        }
        if ($id != '') {
            $id = " id='$id'";
        }

        echo "<div class='col-sm-$span'><label>$label</label><input $name $value $id  class='form-control $class' type='$tipo' $extra> </div>";
    }

    public function textArea($label = '', $name = '', $value = '', $rows = '7', $id = '', $class = '', $extra = '', $smlabel = 3) {
        if ($name != '') {
            $name = " name='$name'";
        }

        if ($id != '') {
            $id = " id='$id'";
        }
        echo "
        <div class='form-group'>
            <label class='control-label col-sm-3' for='$name'>$label: </label>
            <div class='col-sm-8'>
                    <textarea $name $id class='form-control $class' rows='$rows'>$value</textarea>
            </div>
        </div>";
    }

    public function textArea2($label = '', $name = '', $value = '', $rows = '7', $id = '', $class = '', $extra = "style='width:275px; height:100px;'", $smlabel = 3) {
        if ($name != '') {
            $name = " name='$name'";
        }

        if ($id != '') {
            $id = " id='$id'";
        }
        echo "
        <div class='form-group'>
            <div class='col-sm-8'>
                    <textarea $name $id class='form-control $class' $extra  rows='$rows'>$value</textarea>
            </div>
        </div>";
    }

    public function select($label = '', $name, $opcoes, $value, $class = '', $rows = '7', $id = '', $extra = '', $smlabel = 2) {
        ?>
        <div class = 'form-group'>
            <label class = 'control-label col-sm-<?php echo $smlabel; ?>'><?php echo $label ?>: </label>
            <div class = 'col-sm-<?php echo $rows; ?>'>
                <?php echo form_dropdown($name, $opcoes, $value, array('class' => 'form-control' . $class, 'id' => $id));
                ?>
            </div>
        </div> 
        <?php
    }

    public function select2($label = '', $name, $opcoes, $value, $class = '', $rows = '4', $id = '', $extra = '', $smlabel = 2) {
        ?>
        <div class='col-sm-<?php echo $rows; ?>'>
            <label ><?php echo $label ?>: </label>
            <div class = ''>
                <?php //echo form_dropdown($name, $opcoes, $value, array('class' => 'form-control ' . $class, 'id' => $id));
                ?>
                <?php echo form_dropdown($name, $opcoes, $value, "class='form-control $class' $id $extra"); ?>
            </div>
        </div> 
        <?php
    }

    public function selectClean($name, $opcoes, $value, $class = '', $rows = '7', $id = '') {
    
        echo form_dropdown($name, $opcoes, $value, array('class' => 'form-control' . $class, 'id' => $id)); ?>
        <?php
    }

    public function data($data) {
        $data = str_replace('/', '-', $data);
        $data = explode('-', $data);
        $data = array_reverse($data);
        $data = implode($data, '-');
        return $data;
    }

    public function data2($data) {
        $data = str_replace('/', '-', $data);
        $data = explode('-', $data);
        $data = array_reverse($data);
        $data = implode($data, '/');
        return $data;
    }

    public function data3($data) {
        $dh = explode(' ', $data);

        $data = str_replace('-', '/', $dh[0]);
        $data = explode('/', $data);
        $data = array_reverse($data);
        $data = implode($data, '/');
        return $data . ' - ' . $dh[1];
    }

    public function mascaras() {
        include 'util-mask.php';
    }

    public function miniatura($imagem) {
        $this->load->library('image_lib');
        $config['image_library'] = 'gd2';
        $config['source_image'] = '/home/www/html/codigniter/mark/assets/img/' . $imagem;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 75;
        $config['height'] = 50;

        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

    public function estados() {
        return array(
            "" => "Selecione!",
            "AC" => "AC",
            "AL" => "AL",
            "AM" => "AM",
            "AP" => "AP",
            "BA" => "BA",
            "CE" => "CE",
            "DF" => "DF",
            "ES" => "ES",
            "GO" => "GO",
            "MA" => "MA",
            "MT" => "MT",
            "MS" => "MS",
            "MG" => "MG",
            "PA" => "PA",
            "PB" => "PB",
            "PR" => "PR",
            "PE" => "PE",
            "PI" => "PI",
            "RJ" => "RJ",
            "RN" => "RN",
            "RO" => "RO",
            "RS" => "RS",
            "RR" => "RR",
            "SC" => "SC",
            "SE" => "SE",
            "SP" => "SP",
            "TO" => "TO"
        );
    }

    public function moeda($valor) {
        echo number_format($valor, 2, ".", "");
    }

    public function moeda2($valor) {
        return 'R$ ' . number_format($valor, 2, ".", ",");
    }

    public function coordenacoes() {
        return $coordenacoes = array(
            'APOIO_COORD',
            'BIBLIOTECA_COORD',
            'CONVENIOS_COORD',
            'COORD_CURSOS_COORD',
            'FINANCEIRO_COORD',
            'LABORATORIOS_COORD',
            'NUPEX_COORD',
            'PROTOCOLO_COORD',
            'RH_COORD',
            'POSGRADUACAO_COORD',
            'SECRETARIA_COORD',
            'SEGURANCA_COORD',
            'TI_COORD',
        );
    }

    public function diasSemana() {
        return array(
            '0' => 'DOMINGO',
            '1' => 'SEGUNDA',
            '2' => 'TERÇA',
            '3' => 'QUARTA',
            '4' => 'QUINTA',
            '5' => 'SEXTA',
            '6' => 'SÁBADO',
        );
    }

    public function mensagem($texto, $tipo = '', $tempo = '') {

        echo ""
        . "<script>"
        . "var mensagem = '$texto';"
        . "var tipo = '';"
        . "var tempo = '';"
        . ""
        . "if (tempo == '') {tempo = 2000;}"
        . ""
        . "if (tipo == '') {tipo = \"success\";}"
        . ""
        . "var cssMessage = 'display: block; position: fixed; top: 0; left: 40%; right: 40%; width: 20%; padding-top: 10px; z-index: 9999';"
        . "var cssInner = 'margin: 0 auto; box-shadow: 1px 1px 5px black;';"
        . ""
        . "var dialogo = '';"
        . "dialogo += '<div id=\"message\" style=\"' + cssMessage + '\">';"
        . "dialogo += '    <div class=\"alert alert-' + tipo + ' alert-dismissable\" style=\"' + cssInner + '\">';"
        . "dialogo += '    <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">×</a>';"
        . "dialogo += mensagem;"
        . "dialogo += '    </div>';"
        . "dialogo += '</div>';"
        . ""
        . "$('body') . append(dialogo);"
        . "$('#message') . hide();"
        . "$('#message') . fadeIn(200);"
        . ""
        . "setTimeout(function () {"
        . "$('#message') . fadeOut(300, function () {"
        . "$(this) . remove();"
        . "});"
        . "}, tempo);"
        . ""
        . "</script>";
    }

}
