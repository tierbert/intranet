<?php

/** CRUD Controller para agendamento__horario* */
class Matricula extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('agendamento__horariomodel', '', true);
        $this->load->model('Agendamento__agendadosmodel', 'agendadosModel', true);
        $this->load->model('cursosmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
    }

    /** Lista agendamento__horario * */
    public function index($inicio = 0) {


        $this->dados['pagina'] = 'adm/matricula/index';
        $this->dados['titulo'] = 'agendamento__horario';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function senhas($tipo) {
        $this->auth->liberado('TI|secretaria|coordenacao|financeiro');
        $this->dados['tipo'] = $tipo;
        $this->dados['pagina'] = 'adm/matricula/senhas';
        $this->dados['titulo'] = 'AGENDADOS';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function agendados($inicio = 0) {
        $maximo = 80;
        $config['base_url'] = site_url('/adm/agendamento/agend_horario/index');
        $config['total_rows'] = $this->agendadosModel->contaRegistros();
        $config['per_page'] = $maximo;
        $this->pagination->initialize($config);
        $this->dados['paginacao'] = $this->pagination->create_links();

        $this->dados['dados'] = $this->agendadosModel->all($inicio, $maximo, NULL);
        $this->dados['pagina'] = 'adm/matricula/agendados';
        $this->dados['titulo'] = 'AGENDADOS';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function horarios($inicio = 0) {
        $this->auth->liberado('TI|coordenacao|TCC');
        $maximo = 80;
        $config['base_url'] = site_url('/adm/agendamento/agend_horario/index');
        $config['total_rows'] = $this->agendamento__horariomodel->contaRegistros();
        $config['per_page'] = $maximo;
        $this->pagination->initialize($config);
        $this->dados['paginacao'] = $this->pagination->create_links();

        $this->dados['dados'] = $this->agendamento__horariomodel->all($inicio, $maximo);
        $this->dados['pagina'] = 'adm/matricula/horarios';
        $this->dados['titulo'] = 'agendamento__horario';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function horarioNovo() {

        $this->auth->liberado('TI|coordenacao|TCC');
        if ($_POST) {
            $this->agendamento__horariomodel->inserir($_POST);
            echo "<script>window.location.href='" . site_url('adm/matricula/horarios') . "'</script>";
        } else {
            $this->dados['cursos'] = $this->cursosmodel->getArray();
            $this->dados['object'] = $this->agendamento__horariomodel;
            $this->dados['pagina'] = 'adm/matricula/horarioNovo';
            $this->dados['titulo'] = 'Novo Horário ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function horarioEditar($id) {
        $this->auth->liberado('TI|coordenacao|TCC');
        $msg = null;
        if ($_POST) {
            $this->agendamento__horariomodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/adm/matricula/horarios/'</script>";
            return;
        }

        $this->dados['object'] = $this->agendamento__horariomodel->getById($id);
        $this->dados['pagina'] = 'adm/matricula/horarioEdit';
        $this->dados['titulo'] = 'Editar Horário ';
        $this->load->view($this->_tpl, $this->dados);
    }

}
