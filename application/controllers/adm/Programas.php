<?php

/** CRUD Controller para alunos* */
class Programas extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'directory'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();

        $this->load->model('programasmodel', '', true);
    }

    public function posList() {
        $this->auth->liberado('TI|POS-GRADUACAO');
        $this->dados['dados'] = $this->programasmodel->cadPosList();
        $this->dados['pagina'] = 'adm/programas/posList';
        $this->dados['titulo'] = 'PÓS-GRADUAÇAO';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function posDetalhar($id) {
        $this->auth->liberado('TI|POS-GRADUACAO');
        $this->dados['dado'] = $this->programasmodel->cadPosGetById($id);
        $this->dados['pagina'] = 'adm/programas/posDetalhar';
        $this->dados['titulo'] = 'PÓS-GRADUAÇAO';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function tvs() {
        $this->auth->liberado('TI|ASCOM');
        if ($this->input->post()) {
            $pasta = FCPATH . 'assets/tv/';
            $nome_temporario = $_FILES["upload"]["tmp_name"];
            $nome_real = $_FILES["upload"]["name"];
            copy($nome_temporario, $pasta . "$_POST[corredor].jpg");
            echo "<script>window.location.href = '" . site_url('adm/programas/tvs') . "'</script>";
        } else {
            $this->dados['pagina'] = 'adm/programas/tvs';
            $this->dados['titulo'] = 'PÓS-GRADUAÇAO';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function anuncios() {
        $this->auth->liberado('TI|ASCOM');
        if ($this->input->post()) {
            $pasta = FCPATH . 'assets/tv/anuncios/';
            $nome_temporario = $_FILES["upload"]["tmp_name"];
            $nome_real = $_FILES["upload"]["name"];


            //var_dump($_POST['corredor']);

            $this->db->where('anu_perfil', $_POST['corredor'])->set(array('anu_link' => $_POST['link']))->update('anuncios');

            //echo $nome_temporario, $pasta . "$_POST[corredor].jpg";
            copy($nome_temporario, $pasta . "$_POST[corredor].jpg");
            echo "<script>window.location.href = '" . site_url('adm/programas/anuncios') . "'</script>";
        } else {
            $this->dados['pagina'] = 'adm/programas/anuncios';
            $this->dados['titulo'] = 'PÓS-GRADUAÇAO';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function anunciosDel($tipo) {
        $this->auth->liberado('TI|ASCOM');

        $this->db->where('anu_perfil', $tipo)->set(array('anu_link' => ''))->update('anuncios');

        $pasta = FCPATH . 'assets/tv/anuncios/';
        unlink($pasta . $tipo . ".jpg");
        echo "<script>window.location.href = '" . site_url('adm/programas/anuncios') . "'</script>";
    }

}
