<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/** CRUD Controller para alunos* */
class Salas extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('salasmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth', 'form_validation'));
        $this->auth->verificaloginAdm();
    }

    /** Lista alunos * */
    public function index($inicio = 0) {
        $this->auth->liberado('TI|secretaria|coordenacao|financeiro');
        $maximo = 200;

        if ($this->input->post()) {
            $dados = $this->salasmodel->all($inicio, $maximo, $this->input->post());
            $this->dados['post'] = $this->input->post();
        } else {
            $dados = $this->salasmodel->all($inicio, $maximo, NULL);
            $this->dados['post'] = NULL;
        }


        $config['base_url'] = site_url('/salas/index');
        $config['total_rows'] = $dados->num_rows();
        $config['per_page'] = $maximo;
        $this->pagination->initialize($config);
        $this->dados['paginacao'] = $this->pagination->create_links();
        $this->dados['totalAlunos'] = $this->salasmodel->contaRegistros();
        $this->dados['dados'] = $dados;
        $this->dados['pagina'] = 'adm/salas/salas_list';
        $this->dados['titulo'] = 'SALAS / LABORATÓRIOS';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Cria novo(a)  * */
    public function criar() {
        $this->auth->liberado('TI|secretaria');

        if ($this->input->post()) {
            $this->salasmodel->inserir($this->input->post());
            echo "<script>window.location.href='" . site_url() . "/adm/salas/'</script>";
        } else {
            $this->dados['dados'] = $this->salasmodel;
            $this->dados['pagina'] = 'adm/salas/salas_novo';
            $this->dados['titulo'] = 'alunos ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    /**
     * Edit 
     * */
    public function editar($id) {
        $this->auth->liberado('TI|secretaria');
        if ($_POST) {
            $this->salasmodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/adm/salas/'</script>";
            return;
        }

        $this->dados['dados'] = $this->salasmodel->getById($id);
        $this->dados['pagina'] = 'adm/salas/salas_novo';
        $this->dados['titulo'] = 'alunos ';
        $this->load->view($this->_tpl, $this->dados);
    }

    /**
     * Delete
     * */
    public function delete($id) {
        $this->auth->liberado('TI');
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->salasmodel->delete($id);
            echo "<script>window.location.href = '" . site_url() . "/adm/salas/'</script>";
            return;
        } else {
            if ($_POST) {
                redirect('adm/alunos');
                return;
            }
            $object = $this->salasmodel->getById($id);
        }

        $this->dados['pagina'] = 'adm/salas/salas_del';
        $this->dados['titulo'] = 'alunos ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $id;
        $this->load->view($this->_tpl, $this->dados);
    }

}
