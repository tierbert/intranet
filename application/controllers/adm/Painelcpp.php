<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/** CRUD Controller para cppes* */
class Painelcpp extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('cpp__agendadosmodel', 'tcaa', true);
        $this->load->model('cpp__horariosmodel', 'cpph', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
    }

    public function index() {
        $this->auth->liberado('TI|cpp');
        $this->dados['horarios'] = $this->cpph->all();
        $this->dados['pagina'] = 'adm/cpp/agendados';
        $this->dados['titulo'] = 'agendamento__horario';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function horarioCppNovo() {
        $this->auth->liberado('TI|cpp');
        $idProf = $this->session->userdata('usu_id');
        $this->dados['idProf'] = $idProf;
        if ($_POST) {
            $this->cpph->inserir($_POST, $idProf);
            echo "<script>window.location.href='" . site_url('adm/painelcpp') . "'</script>";
        } else {
            $this->dados['object'] = $this->cpph;
            $this->dados['pagina'] = 'adm/cpp/horarioCppNovo';
            $this->dados['titulo'] = 'agendamento__horario';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function horarioCppEdit($id) {
        $this->auth->liberado('TI|cpp');
        $idProf = $this->session->userdata('usu_id');
        $this->dados['idProf'] = $idProf;
        if ($_POST) {
            $this->cpph->update($_POST, $id);
            echo "<script>window.location.href='" . site_url('adm/painelcpp') . "'</script>";
        } else {
            $this->dados['object'] = $this->cpph->getById($id);
            $this->dados['pagina'] = 'adm/cpp/horarioCppEdit';
            $this->dados['titulo'] = 'agendamento__horario';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function agendadosListCpp($idhorario) {
        $this->auth->liberado('TI|cpp');
        $idProf = $this->session->userdata('usu_id');
        $this->dados['agendados'] = $this->tcaa->getByhorario($idhorario);
        $this->dados['pagina'] = 'adm/cpp/agendadosList';
        $this->dados['titulo'] = 'agendamento__horario';
        $this->load->view($this->_tpl, $this->dados);
    }

}
