<?php

/** CRUD Controller para Agenda* */
class Agenda extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('modelagenda', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
    }

    public function index() {
        $this->dados['objects'] = $this->modelagenda->all();
        $this->dados['pagina'] = 'adm/agenda/list';
        $this->dados['titulo'] = 'RAMAIS ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function agendaAdm() {
        $this->auth->liberado('TI');
        $this->dados['objects'] = $this->modelagenda->all();
        $this->dados['pagina'] = 'adm/agenda/listAdm';
        $this->dados['titulo'] = 'RAMAIS ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function criar() {
        $this->auth->liberado('TI');
        $msg = null;
        if ($_POST) {
            $this->modelagenda->inserir($_POST);
            redirect('adm/agenda');
            return;
        }

        $this->dados['object'] = $this->modelagenda;
        $this->dados['pagina'] = 'adm/agenda/novo';
        $this->dados['titulo'] = 'RAMAIS ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function editar($id) {
        $this->auth->liberado('TI');
        $msg = null;
        if ($_POST) {
            $this->modelagenda->update($_POST, $id);
            redirect('adm/agenda');
            return;
        }

        $this->dados['object'] = $this->modelagenda->getById($id);
        $this->dados['pagina'] = 'adm/agenda/editar';
        $this->dados['titulo'] = 'agenda ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function delete($id) {
        $this->auth->liberado('TI');
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->modelagenda->delete($id);
            redirect('adm/agenda/agendaAdm');
            return;
        } else {
            if ($_POST) {
                redirect('adm/agenda/agendaAdm');
                return;
            }
            $object = $this->modelagenda->getById($id);
        }

        $this->dados['pagina'] = 'adm/agenda/delete';
        $this->dados['titulo'] = 'RAMAIS ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $id;
        $this->load->view($this->_tpl, $this->dados);
    }

}
