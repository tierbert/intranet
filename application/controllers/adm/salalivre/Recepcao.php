<?php

/** CRUD Controller para salas* */
class Recepcao extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('salalivre/frequenciamodel', '', true);
        $this->load->model('salalivre/kitsmodel', '', true);
        $this->load->model('professoresmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
    }

    /** Lista salas * */
    public function index() {
        if ($this->input->post()) {
            $reg = $this->input->post('registro');
            $senha = $this->input->post('senha');

            $this->db->where('pro_registroRh', $reg);
            $this->db->where('pro_senha', $senha);
            $login = $this->db->get('salalivre.professores');

            //echo $this->db->last_query();
            //echo $login->num_rows();

            if ($login->num_rows() > 0) {
                //$id = $usuario->usu_id;
                $this->session->set_userdata(
                        array(
                            'login' => 'logado',
                            'pro_id' => $login->row('pro_id'),
                            'pro_nome' => $login->row('pro_nome')
                ));
                echo "<script>window.location.href='" . site_url() . "/adm/salalivre/recepcao/inicio/'</script>";
            } else {
                echo "<script>alert('Dados Incorretos');window.location.href='" . site_url() . "/adm/salalivre/recepcao/'</script>";
            }
        } else {
            $this->dados['pagina'] = 'adm/salalivre/recepcao/index';
            $this->dados['titulo'] = 'salas';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function inicio() {

        $idProf = $this->session->userdata('pro_id');
        //$prof = $this->professoresmodel->getBy($idProf);
        $prof = $this->frequenciamodel->getBy($idProf, 'sala');
        $this->dados['prof'] = $prof;
        //$this->dados['freq'] = $freq;
        $this->dados['pagina'] = 'adm/salalivre/recepcao/inicio';
        $this->dados['titulo'] = 'salas';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function retiraKit() {
        if ($this->input->post()) {
            $this->frequenciamodel->inserir($this->input->post());
            echo "<script>alert('Kit retirado');window.location.href='" . site_url() . "/adm/salalivre/recepcao/inicio/'</script>";
        } else {
            $idProfessor = $this->session->userdata('pro_id');
            $idHorario = $this->db->get_where('salalivre.professores', array('pro_id' => $idProfessor));
            if ($idHorario->num_rows > 0) {
                $id = $idHorario->row('pro_idHorario');
                $dados = $this->util->horarios();
                $this->dados['dados'] = $dados[$id];
            }
            $this->dados['kits'] = $this->kitsmodel->getArray('SALA');
            $this->dados['pagina'] = '/adm/salalivre/recepcao/retiraKit';
            $this->dados['titulo'] = 'salas';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function retirarKit($idKit) {
        $this->frequenciamodel->retirarKit($idKit);
        echo "<script>window.location.href='" . site_url() . "/adm/salalivre/recepcao/inicio/'</script>";
    }

    public function devolverKit($idFreq) {
        $this->frequenciamodel->devolveKit($idFreq);
        echo "<script>alert('Kit devolvido');window.location.href='" . site_url() . "/adm/salalivre/recepcao/inicio/'</script>";
    }

    public function professores() {
        $prof = $this->professoresmodel->getArray2();
        $this->dados['prof'] = $prof;
        $this->dados['pagina'] = 'recepcao/professores';
        $this->dados['titulo'] = 'salas';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function kits() {
        $kits = $this->kitsmodel->pendentes();
        $this->dados['dados'] = $kits;
        $this->dados['pagina'] = 'adm/salalivre/recepcao/kits';
        $this->dados['titulo'] = 'salas';
        $this->load->view($this->_tpl, $this->dados);
    }

}
