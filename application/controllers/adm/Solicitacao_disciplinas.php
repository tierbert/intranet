<?php

/** CRUD Controller para solicitacao_disciplinas* */
class Solicitacao_disciplinas extends CI_Controller {

    private $_tpl = 'index.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('solicitacao_disciplinasmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
        $this->auth->liberado('TI');
    }

    /** Lista solicitacao_disciplinas * */
    public function index($inicio = 0) {
        $maximo = 10;
        $config['base_url'] = site_url('/solicitacao_disciplinas/index');
        $config['total_rows'] = $this->solicitacao_disciplinasmodel->contaRegistros();
        $config['per_page'] = $maximo;
        $this->pagination->initialize($config);
        $this->dados['paginacao'] = $this->pagination->create_links();

        $this->dados['dados'] = $this->solicitacao_disciplinasmodel->all($inicio, $maximo);
        $this->dados['pagina'] = 'solicitacao_disciplinas/solicitacao_disciplinas_list';
        $this->dados['titulo'] = 'solicitacao_disciplinas';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Cria novo(a)  * */
    public function criar() {
        if ($_POST) {
            $this->solicitacao_disciplinasmodel->inserir($_POST);
            echo "<script>window.location.href='" . site_url() . "/solicitacao_disciplinas/'</script>";
        } else {
            $this->dados['object'] = $this->solicitacao_disciplinasmodel;
            $this->dados['pagina'] = 'solicitacao_disciplinas/solicitacao_disciplinas_novo';
            $this->dados['titulo'] = 'solicitacao_disciplinas ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    /**
     * Edit 
     * */
    public function editar($id) {
        $msg = null;
        if ($_POST) {
            //$this->form_validation->set_rules('valid', '', 'required');
            // if ($this->form_validation->run() == TRUE) {
            //$object->save();
            $this->solicitacao_disciplinasmodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/solicitacao_disciplinas/'</script>";
            return;
//} else {
//$msg = 'Por favor preencha todos os dados';
// }
        }

        $this->dados['object'] = $this->solicitacao_disciplinasmodel->getById($id);
        $this->dados['pagina'] = 'solicitacao_disciplinas/solicitacao_disciplinas_edit';
        $this->dados['titulo'] = 'solicitacao_disciplinas ';
        $this->load->view($this->_tpl, $this->dados);
    }

    /**
     * Delete
     * */
    public function delete($id) {
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->solicitacao_disciplinasmodel->delete($id);
            echo "<script>window.location.href = '" . site_url() . "/solicitacao_disciplinas/'</script>";
            return;
        } else {
            if ($_POST) {
                redirect('solicitacao_disciplinas');
                return;
            }
            $object = $this->solicitacao_disciplinasmodel->getById($id);
        }

        $this->dados['pagina'] = 'solicitacao_disciplinas/solicitacao_disciplinas_del';
        $this->dados['titulo'] = 'solicitacao_disciplinas ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $id;
        $this->load->view($this->_tpl, $this->dados);
    }

}
