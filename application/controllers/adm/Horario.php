<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/** CRUD Controller para horario* */
class Horario extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('professoresmodel', '', true);
        $this->load->model('solicitacoesmodel', '', true);
        $this->load->model('horariomodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
    }

    /** Lista horario * */
    public function index($inicio = 0) {
        $this->auth->liberado('TI|secretaria|coordenacao');
        $maximo = 2000;
        $config['base_url'] = site_url('adm/horario/index');
        $config['total_rows'] = $this->horariomodel->contaRegistros();
        $config['per_page'] = $maximo;
        $this->pagination->initialize($config);
        $this->dados['paginacao'] = $this->pagination->create_links();
        $this->dados['totais'] = $this->solicitacoesmodel->totais();

        if ($this->input->post()) {
            $this->dados['dados'] = $this->horariomodel->all($inicio, $maximo, $this->input->post());
            $this->dados['post'] = $this->input->post();
        } else {
            $this->dados['dados'] = $this->horariomodel->all($inicio, $maximo, NULL);
        }

        $this->dados['pagina'] = 'horario/horario_list';
        $this->dados['titulo'] = 'horario';

        $this->load->view($this->_tpl, $this->dados);
    }

    /** Cria novo(a)  * */
    public function criar() {
        $this->auth->liberado('TI|secretaria');
        if ($_POST) {
            $this->horariomodel->inserir($_POST);
            echo "<script>window.location.href='" . site_url() . "/adm/horario/'</script>";
        } else {
            $this->dados['object'] = $this->horariomodel;
            $this->dados['pagina'] = 'horario/horario_novo';
            $this->dados['titulo'] = 'horario ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    /**
     * Edit 
     * */
    public function editar($id) {
        $this->auth->liberado('TI');
        $msg = null;
        if ($_POST) {
            $this->horariomodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/adm/horario/'</script>";
            return;
        }

        $this->dados['object'] = $this->horariomodel->getById($id);
        $this->dados['pagina'] = 'horario/horario_edit';
        $this->dados['titulo'] = 'horario ';
        $this->load->view($this->_tpl, $this->dados);
    }

    /**
     * Delete
     * */
    public function delete($id) {
        $this->auth->liberado('TI');
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->horariomodel->delete($id);
            echo "<script>window.location.href = '" . site_url() . "/adm/horario/'</script>";
            return;
        } else {
            if ($_POST) {
                redirect('adm/horario');
                return;
            }
            $object = $this->horariomodel->getById($id);
        }

        $this->dados['pagina'] = 'horario/horario_del';
        $this->dados['titulo'] = 'horario ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $id;
        $this->load->view($this->_tpl, $this->dados);
    }

    public function horProf($idHorario) {
        $this->auth->liberado('TI|coordenacao');
        if ($_POST) {
            $this->horariomodel->update($_POST, $idHorario);
            echo "<script>window.location.href = '" . site_url() . "/adm/horario/horProf/$idHorario'</script>";
            return;
        } else {
            $this->dados['professores'] = $this->professoresmodel->getArray();
            $this->dados['pagina'] = 'horario/horProf';
            $this->dados['titulo'] = 'horario ';
            $this->dados['object'] = $this->horariomodel->getById($idHorario);
            $this->dados['idHorario'] = $idHorario;
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function horStatus($idHorario) {
        $this->auth->liberado('TI|coordenacao');
        if ($_POST) {
            $this->horariomodel->update($_POST, $idHorario);
            echo "<script>window.location.href = '" . site_url() . "/adm/horario/'</script>";
            return;
        } else {
            $this->dados['professores'] = $this->professoresmodel->getArray();
            $this->dados['pagina'] = 'horario/horStatus';
            $this->dados['titulo'] = 'horario ';
            $this->dados['object'] = $this->horariomodel->getById($idHorario);
            $this->dados['idHorario'] = $idHorario;
            $this->load->view($this->_tpl, $this->dados);
        }
    }

}
