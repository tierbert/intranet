<?php

/** CRUD Controller para usuarios* */
class Usuarios extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('usuariosmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
        $this->auth->liberado('TI|RH|RH_COORD');
    }

    /** Lista usuarios * */
    public function index($inicio = 0) {
        $maximo = 100;
        $config['base_url'] = site_url('adm/usuarios/index');
        $config['total_rows'] = $this->usuariosmodel->contaRegistros();
        $config['per_page'] = $maximo;
        $this->pagination->initialize($config);
        $this->dados['paginacao'] = $this->pagination->create_links();

        if ($this->input->post()) {
            $this->dados['post'] = $this->input->post();
            $this->dados['dados'] = $this->usuariosmodel->all($inicio, $maximo, $this->input->post());
            $this->dados['pagina'] = 'adm/usuarios/usuarios_list';
            $this->dados['titulo'] = 'usuarios';
            $this->load->view($this->_tpl, $this->dados);
        } else {
            $this->dados['post'] = NULL;
            $this->dados['dados'] = $this->usuariosmodel->all($inicio, $maximo, NULL);
            $this->dados['pagina'] = 'adm/usuarios/usuarios_list';
            $this->dados['titulo'] = 'usuarios';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    /** Cria novo(a)  * */
    public function criar() {
        if ($this->input->post()) {
            $consulta = $this->usuariosmodel->getByRegRh($this->input->post('usu_login'));
            if ($consulta->num_rows() > 0) {
                echo "<script>alert('Nº DE REGISTRO DE RH JÁ CADASTRADO');window.location.href='" . site_url() . "/adm/usuarios/'</script>";
                exit();
            }
            $this->usuariosmodel->inserir($this->input->post());
            echo "<script>window.location.href='" . site_url() . "/adm/usuarios/'</script>";
        } else {

            $this->dados['setores'] = $this->usuariosmodel->getSetoresArray();
            $this->dados['object'] = $this->usuariosmodel;
            $this->dados['pagina'] = 'adm/usuarios/usuarios_novo';
            $this->dados['titulo'] = 'usuarios ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    /**
     * Edit 
     * */
    public function editar($id) {
        $msg = null;
        if ($_POST) {
            $this->usuariosmodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/adm/usuarios/'</script>";
            return;
        }
        $this->dados['setores'] = $this->usuariosmodel->getSetoresArray();
        $this->dados['object'] = $this->usuariosmodel->getById($id);
        $this->dados['pagina'] = 'adm/usuarios/usuarios_edit';
        $this->dados['titulo'] = 'usuarios ';
        $this->load->view($this->_tpl, $this->dados);
    }

    /**
     * Delete
     * */
    public function delete($id) {
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->usuariosmodel->delete($id);
            echo "<script>window.location.href = '" . site_url() . "/adm/usuarios/'</script>";
            return;
        } else {
            if ($_POST) {
                redirect('adm/usuarios');
                return;
            }
            $object = $this->usuariosmodel->getById($id);
        }

        $this->dados['pagina'] = 'adm/usuarios/usuarios_del';
        $this->dados['titulo'] = 'usuarios ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $id;
        $this->load->view($this->_tpl, $this->dados);
    }

}
