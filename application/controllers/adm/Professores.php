<?php

/** CRUD Controller para professores* */
class Professores extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('professoresmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth', 'form_validation'));
        $this->auth->verificaloginAdm();
        $this->auth->liberado('TI');
    }

    /** Lista professores * */
    public function index($inicio = 0) {
        $maximo = 20;
        $config['base_url'] = site_url('/adm/professores/index');
        $config['total_rows'] = $this->professoresmodel->contaRegistros();
        $config['per_page'] = $maximo;
        $this->pagination->initialize($config);
        $this->dados['paginacao'] = $this->pagination->create_links();

        if ($this->input->post()) {
            $this->dados['dados'] = $this->professoresmodel->all($inicio, $maximo, $this->input->post());
        } else {
            $this->dados['dados'] = $this->professoresmodel->all($inicio, $maximo, NULL);
        }


        $this->dados['pagina'] = 'adm/professores/professores_list';
        $this->dados['titulo'] = 'professores';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Cria novo(a)  * */
    public function criar() {
        $this->form_validation->set_rules('pro_registroRh', 'pro_registroRh', 'required|callback_is_db2_unique[' . $this->input->post('pro_registroRh') . ']');
        $this->form_validation->set_rules('pro_senha', 'SENHA', 'required|is_natural', array('is_natural' => 'A SENHA PARA A SALA LIVRE DEVE CONTER APENAS NÚMEROS'));
        if ($this->form_validation->run() == TRUE) {
            $this->professoresmodel->inserir($_POST);
            echo "<script>window.location.href='" . site_url() . "/adm/professores/'</script>";
        } else {
            //$this->dados['object'] = $this->professoresmodel;
            $this->dados['object'] = $this->professoresmodel;
            $this->dados['pagina'] = 'adm/professores/professores_novo';
            $this->dados['titulo'] = 'professores ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    function is_db2_unique($rh) {
        $dados = $this->db
                ->where('pro_registroRh', $rh)
                ->get("salalivre.professores");
        if ($dados->num_rows() == 0) {
            return true;
        } else {
            $this->form_validation->set_message('is_db2_unique', 'Professor Já Cadastrado');
            return false;
        }
    }

    public function editar($id) {
        $dados = $this->professoresmodel->getById($id);

        if ($this->input->post('pro_registroRh') != $dados->pro_registroRh) {
            $is_unique = '|callback_is_db2_unique[' . $this->input->post('pro_registroRh') . ']';
        } else {
            $is_unique = '';
        }

        $this->form_validation->set_rules('pro_registroRh', 'pro_registroRh', 'required' . $is_unique);
        $this->form_validation->set_rules('pro_senha', 'SENHA', 'required|is_natural', array('is_natural' => 'A SENHA PARA A SALA LIVRE DEVE CONTER APENAS NÚMEROS'));
        if ($this->form_validation->run() == TRUE) {
            //$object->save();
            $this->professoresmodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/adm/professores/'</script>";
            return;
        }

        $this->dados['object'] = $dados;
        $this->dados['pagina'] = 'adm/professores/professores_edit';
        $this->dados['titulo'] = 'professores ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function delete($id) {
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->professoresmodel->delete($id);
            echo "<script>window.location.href = '" . site_url() . "/adm/professores/'</script>";
            return;
        } else {
            if ($_POST) {
                redirect('adm/professores');
                return;
            }
            $object = $this->professoresmodel->getById($id);
        }

        $this->dados['pagina'] = 'adm/professores/professores_del';
        $this->dados['titulo'] = 'professores ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $id;
        $this->load->view($this->_tpl, $this->dados);
    }

    public function discProf($idProfessor) {
        
    }

}
