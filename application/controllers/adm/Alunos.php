<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/** CRUD Controller para alunos* */
class Alunos extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('solicitacoesmodel', '', true);
        $this->load->model('alunosmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth', 'form_validation'));
        $this->auth->verificaloginAdm();
    }

    /** Lista alunos * */
    public function index($inicio = 0) {
        $this->auth->liberado('TI|secretaria|coordenacao|financeiro');
        $maximo = 20;

        if ($this->input->post()) {
            $dados = $this->alunosmodel->all($inicio, $maximo, $this->input->post());
            $this->dados['post'] = $this->input->post();
        } else {
            $dados = $this->alunosmodel->all($inicio, $maximo, NULL);
            $this->dados['post'] = NULL;
        }


        $config['base_url'] = site_url('/alunos/index');
        $config['total_rows'] = $dados->num_rows();
        $config['per_page'] = $maximo;
        $this->pagination->initialize($config);
        $this->dados['paginacao'] = $this->pagination->create_links();
        $this->dados['totalAlunos'] = $this->alunosmodel->contaRegistros();
        $this->dados['dados'] = $dados;
        $this->dados['pagina'] = 'adm/alunos/alunos_list';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Cria novo(a)  * */
    public function criar() {
        $this->auth->liberado('TI|secretaria');

        $this->form_validation->set_rules('alu_matricula', 'Matricula', 'required|is_unique[alunos.alu_matricula]', array('is_unique' => 'NÚMERO DE MATRÍCULA JA CADASTRADO'));
        $this->form_validation->set_rules('alu_nome', '', 'required', array('required' => 'O CAMPO NOME É OBRIGATÓRIO'));
        $this->form_validation->set_rules('alu_senha', '', 'required', array('required' => 'O CAMPO SENHA É OBRIGATÓRIO'));
        if ($this->form_validation->run() == TRUE) {
            $this->alunosmodel->inserir($this->input->post());
            echo "<script>window.location.href='" . site_url() . "/adm/alunos/'</script>";
            return;
        } else {
            $this->dados['object'] = $this->alunosmodel;
            $this->dados['pagina'] = 'adm/alunos/alunos_novo';
            $this->dados['titulo'] = 'alunos ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function editar($id) {
        $this->auth->liberado('TI|secretaria');
        if ($_POST) {
            $this->alunosmodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/adm/alunos/'</script>";
            return;
        } else {
            $this->dados['object'] = $this->alunosmodel->getById($id);
            $this->dados['pagina'] = 'adm/alunos/alunos_edit';
            $this->dados['titulo'] = 'alunos ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function delete($id) {
        $this->auth->liberado('TI');
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->alunosmodel->delete($id);
            echo "<script>window.location.href = '" . site_url() . "/adm/alunos/'</script>";
            return;
        } else {
            if ($_POST) {
                redirect('adm/alunos');
                return;
            }
            $object = $this->alunosmodel->getById($id);
        }

        $this->dados['pagina'] = 'adm/alunos/alunos_del';
        $this->dados['titulo'] = 'alunos ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $id;
        $this->load->view($this->_tpl, $this->dados);
    }

}
