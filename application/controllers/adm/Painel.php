<?php

/** CRUD Controller para alunos* */
class Painel extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('usuariosmodel', '', true);
        $this->load->model('solicitacoesmodel', '', true);
        $this->load->model('alunosmodel', '', true);
        $this->load->helper(array('form', 'url', 'directory'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
    }

    public function index($inicio = 0) {
        $this->dados['totais'] = $this->solicitacoesmodel->totais();
        $this->dados['pagina'] = 'adm/painel/painel';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function solicitacoes() {
        $this->dados['totais'] = $this->solicitacoesmodel->totais();
        $this->dados['pagina'] = 'adm/painel/solicitacoes';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    function sair() {
        $this->session->sess_destroy();
        echo "<script>window.location.href='" . site_url() . "'</script>";
    }

    public function arquivos() {
        // $this->auth->liberado('TI|secretaria|coordenacao');
        $this->auth->liberado('TI|secretaria|coordenacao|recep_coord');
        $pastaLocal = BASEPATH . "../assets/arquivos/";
        $pastaRemota = base_url() . "/assets/arquivos/";
        if ($this->input->post()) {
            $tmpFilePath = $_FILES['arquivo']['tmp_name'];
            if ($tmpFilePath != "") {
                $nomeArquivo = $_FILES['arquivo']['name'];
                $newFilePath = $pastaLocal . $nomeArquivo;
                if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                    //echo 'ok';
                } else {
                    //echo 'erro';
                }
                echo "<script>window.location.href='" . site_url('adm/painel/arquivos') . "'</script>";
            }
        } else {
            $this->dados['pastaRemota'] = $pastaRemota;
            $this->dados['pastaLocal'] = $pastaLocal;
            $arquivos = directory_map($pastaLocal, 1);
            $this->dados['arquivos'] = $arquivos;
            $this->dados['pagina'] = 'adm/painel/arquivos';
            $this->dados['titulo'] = 'alunos';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function arquivosDel() {
        $this->auth->liberado('TI|secretaria|coordenacao');
        $arquivo = $this->input->post('arquivo');
        $pastaLocal = BASEPATH . "../assets/arquivos/";
        echo $pastaLocal . $arquivo;
        unlink($pastaLocal . $arquivo);
    }

    public function senha() {
        //$this->auth->liberado('TI|secretaria');
        if ($_POST) {
            $idusuario = $this->session->userdata('usu_id');
            $_POST['usu_trocaSenha'] = 1;
            $this->usuariosmodel->update($_POST, $idusuario);
            echo "<script>window.location.href='" . site_url('adm/painel') . "'</script>";
            return;
        }
        $this->dados['usu_id'] = $this->session->userdata('usu_id');
        $this->dados['pagina'] = 'adm/painel/senhaUsu';
        $this->dados['titulo'] = 'alunos ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function remessa() {
        //$this->auth->liberado('TI|secretaria');
        if ($_POST) {
            $nome = $_FILES['anexos']['tmp_name'];
            $fp = fopen($nome, 'r');
            $arquivo = (fread($fp, filesize($nome)));

            $this->dados['arquivo'] = $arquivo;
        } else {
            $this->dados['arquivo'] = NULL;
        }
        $this->dados['pagina'] = 'adm/painel/remessa';
        $this->dados['titulo'] = 'alunos ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function docsEstagio() {
        $this->auth->liberado('TI|coordenacao');
        $pastaLocal = FCPATH . "assets/estagio/";
        //$pastaRemota = base_url() . "/assets/arquivos/";
        if ($this->input->post()) {
            $tmpFilePath = $_FILES['arquivo']['tmp_name'];
            if ($tmpFilePath != "") {
                $nomeArquivo = $_FILES['arquivo']['name'];
                $nomeComposto = explode('_', $nomeArquivo);

                print_r($nomeComposto);

                if (count($nomeComposto) < 3) {
                    echo "<script>alert('Arquivo Fora do Padrão');window.location.href='" . site_url('adm/painel/docsEstagio') . "'</script>";
                    exit();
                }
                $newFilePath = $pastaLocal . strtoupper($nomeArquivo);
                if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                    //echo 'ok';
                } else {
                    //echo 'erro';
                }
                echo "<script>window.location.href='" . site_url('adm/painel/docsEstagio') . "'</script>";
            }
        } else {
            //$this->dados['pastaRemota'] = $pastaRemota;
            //$this->dados['pastaLocal'] = $pastaLocal;
            $arquivos = directory_map($pastaLocal, 1);
            $this->dados['arquivos'] = $arquivos;
            $this->dados['pagina'] = 'aluno/docsEstagio';
            $this->dados['titulo'] = 'alunos';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function docsEstagioDel() {
        $this->auth->liberado('TI|coordenacao');
        $arquivo = $this->input->post('arquivo');
        $pastaLocal = FCPATH . "assets/estagio/";
        //echo $pastaLocal . $arquivo;
        unlink($pastaLocal . ($arquivo));
        echo "<script>window.location.href='" . site_url('adm/painel/docsEstagio') . "'</script>";
    }

    public function foto() {
        $this->auth->liberado('TI|secretaria');
        $this->dados['pagina'] = 'adm/foto';
        $this->dados['titulo'] = 'alunos';
        $this->load->view('adm/foto', $this->dados);
    }

    public function fotoFunc() {
        $this->auth->liberado('TI|secretaria');
        $this->dados['pagina'] = 'adm/foto';
        $this->dados['titulo'] = 'alunos';
        $this->load->view('adm/fotoFunc', $this->dados);
    }

}
