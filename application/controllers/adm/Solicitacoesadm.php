<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/** CRUD Controller para solicitacoes* */
class Solicitacoesadm extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();
    private $semestre = '2019-1';

    public function __construct() {
        parent::__construct();
        $this->load->model('professoresmodel', '', true);
        $this->load->model('horariomodel', '', true);
        $this->load->model('solicitacao_disciplinasmodel', '', true);
        $this->load->model('solicitacoesmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
    }

    /** Lista solicitacoes * */
    public function index($inicio = 0) {
        $this->auth->verificaloginAdm();
        $this->auth->liberado('TI|secretaria|coordenacao|financeiro|CAA');
        $maximo = 40;
        if ($this->input->post()) {
            $solicitacoes = $this->solicitacoesmodel->all($inicio, $maximo, $this->input->post());
            $this->dados['dados'] = $solicitacoes;
            $this->dados['post'] = $this->input->post();
        } else {
            
        }

        $totais = $this->solicitacoesmodel->totais();
        $this->dados['totais'] = $totais;

        $total = array_sum($totais);

        $config['base_url'] = site_url('adm/solicitacoesadm/index');
        $config['total_rows'] = $total;
        $config['per_page'] = $maximo;
        $this->pagination->initialize($config);
        $this->dados['paginacao'] = $this->pagination->create_links();


        $this->dados['pagina'] = 'adm/solicitacoes/solicitacoes_list';
        $this->dados['titulo'] = 'solicitacoes';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function criar() {
        $this->auth->liberado('TI|secretaria|CAA');
        if ($_POST) {
            $this->solicitacoesmodel->inserir($_POST);
            echo "<script>window.location.href='" . site_url() . "/solicitacoes/'</script>";
        } else {
            $this->dados['object'] = $this->solicitacoesmodel;
            $this->dados['pagina'] = 'solicitacoes/solicitacoes_novo';
            $this->dados['titulo'] = 'solicitacoes ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function editar($id) {
        $this->auth->liberado('TI|financeiro|CAA');
        $msg = null;
        if ($_POST) {
            $this->solicitacoesmodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/adm/solicitacoesadm/'</script>";
            return;
        }

        $object = $this->solicitacoesmodel->getById($id);
        $this->dados['object'] = $object->row();
        $this->dados['pagina'] = 'adm/solicitacoes/solicitacoes_edit';
        $this->dados['titulo'] = 'solicitacoes ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function delete($id) {
        $this->auth->liberado('TI|CAA');
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->solicitacoesmodel->delete($id);
            echo "<script>window.location.href = '" . site_url() . "/solicitacoes/'</script>";
            return;
        } else {
            if ($_POST) {
                redirect('solicitacoes');
                return;
            }
            $object = $this->solicitacoesmodel->getById($id);
        }

        $this->dados['pagina'] = 'solicitacoes/solicitacoes_del';
        $this->dados['titulo'] = 'solicitacoes ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $id;
        $this->load->view($this->_tpl, $this->dados);
    }

    public function disciplinas($idSolicitacao) {
        $this->auth->liberado('TI|secretaria|coordenacao|financeiro|CAA');
        $object = $this->solicitacoesmodel->getById($idSolicitacao);
        $this->dados['object'] = $object->row();
        $this->dados['disciplinas'] = $this->solicitacao_disciplinasmodel->getBySolic($idSolicitacao);
        $this->dados['pagina'] = 'adm/solicitacoes/disciplinas';
        $this->dados['titulo'] = 'solicitacoes ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function relatorioStatus() {
        $this->auth->liberado('TI|secretaria|coordenacao|financeiro|CAA');

        if ($this->input->post()) {
            $disciplinas = $this->horariomodel->solicitacaoAssi($this->input->post(), $this->semestre);
            $this->dados['post'] = $this->input->post();
        } else {
            $disciplinas = NULL;
        }

        //$this->dados['totais'] = $this->solicitacoesmodel->totais();
        $this->dados['disciplinas'] = $disciplinas;
        $professores = $this->horariomodel->professores();
        $this->dados['professores'] = $professores;
        $this->dados['pagina'] = 'adm/solicitacoes/relatorioStatus';
        $this->dados['titulo'] = 'solicitacoes ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function relatorioStatusPdf($idDisciplina, $status) {
        $solicitacoes = $this->solicitacoesmodel->relatStatus($idDisciplina, $status, $this->semestre);
        $this->dados['solicitacoes'] = $solicitacoes;
        $this->dados['titulo'] = 'alunos';
        $mpdf = new mPDF();
        $html = $this->load->view('adm/solicitacoes/relatorioStatusPdf', $this->dados, TRUE);
        $mpdf->SetFooter('{PAGENO}');
        $mpdf->writeHTML($html);
        $mpdf->Output();
    }

    public function relatorioAssinat() {
        $this->auth->liberado('TI|secretaria|coordenacao|financeiro|CAA');

        if ($this->input->post()) {
            $disciplinas = $this->horariomodel->solicitacaoAssi($this->input->post(), $this->semestre);
            $this->dados['post'] = $this->input->post();
        } else {
            $disciplinas = NULL;
        }

        //$this->dados['totais'] = $this->solicitacoesmodel->totais();
        $this->dados['disciplinas'] = $disciplinas;
        $professores = $this->horariomodel->professores();
        $this->dados['professores'] = $professores;
        $this->dados['pagina'] = 'adm/solicitacoes/relatorioAssinat';
        $this->dados['titulo'] = 'solicitacoes ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function relatorioAssinatPdf($idDisciplina) {
        $this->auth->liberado('TI|secretaria|coordenacao|financeiro|professor|CAA');
        $solicitacoes = $this->solicitacoesmodel->relatPagos($idDisciplina);
        $this->dados['solicitacoes'] = $solicitacoes;
        $this->dados['titulo'] = 'alunos';
        $mpdf = new mPDF();
        $html = $this->load->view('adm/solicitacoes/relatorioAssinatPdf', $this->dados, TRUE);
        $mpdf->SetFooter('{PAGENO}');
        $mpdf->writeHTML($html);
        $mpdf->Output();
    }

    public function relatorioAssinatProf() {
        $this->auth->liberado('TI|secretaria|coordenacao|financeiro|CAA');
        $logado = $this->session->userdata('loginPainel');
        $idProf = $this->session->userdata('usu_id');
        $disciplinas = $this->horariomodel->getbyProf();
        $this->dados['totais'] = $this->solicitacoesmodel->totais();
        $this->dados['disciplinas'] = $disciplinas;
        $professores = $this->horariomodel->professores();
        $this->dados['professores'] = $professores;
        $this->dados['pagina'] = 'solicitacoes/relatAssinProf';
        $this->dados['titulo'] = 'solicitacoes ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function comprovante($idSolicitacao) {
        $confere = $this->solicitacoesmodel->getById($idSolicitacao);
        $nomeAluno = $confere->row('alu_nome');
        $matriculaAluno = $confere->row('alu_matricula');
        $idAluno = $confere->row('alu_id');

        if ($confere->num_rows() == 0) {
            // echo "<script>alert('Erro ao imprimir comprovante');window.location.href='" . site_url('aluno') . "'</script>";
        } else {
            $this->dados['nomeAluno'] = $nomeAluno;
            $this->dados['matriculaAluno'] = $matriculaAluno;
            $disciplinas = $this->solicitacao_disciplinasmodel->getBySolic($idSolicitacao);
            $this->dados['idSolicitacao'] = $idSolicitacao;
            $this->dados['disciplinas'] = $disciplinas;
            $this->dados['pagina'] = 'aluno/solicitacoes/comprovante';
            $this->dados['titulo'] = 'alunos';

            $mpdf = new mPDF();
            $html = $this->load->view('aluno/solicitacoes/comprovante', $this->dados, TRUE);
            $mpdf->SetFooter('{PAGENO}');
            $mpdf->writeHTML($html);
            $mpdf->Output();
        }
    }

    public function relatorios() {
        $this->auth->liberado('TI|secretaria|coordenacao|financeiro|CAA');
        $professores = $this->professoresmodel->getArray();
        $this->dados['professores'] = $professores;
        $this->dados['pagina'] = 'adm/solicitacoes/relatorios';
        $this->dados['titulo'] = 'solicitacoes ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function relProf() {
        $this->auth->liberado('TI|secretaria|coordenacao|financeiro|professor|CAA');

        $idProfessor = $this->input->post('prof');

        $professor = $this->professoresmodel->getById($idProfessor);
        $this->dados['professor'] = $professor;

        $disciplinas = $this->horariomodel->getByProf($idProfessor, $this->semestre);
        $this->dados['disciplinas'] = $disciplinas;
        $this->dados['titulo'] = 'alunos';
        $mpdf = new mPDF();
        $html = $this->load->view('adm/solicitacoes/relatProfPdf', $this->dados, TRUE);

        //echo $html;

        $mpdf->SetFooter('{PAGENO}');
        $mpdf->writeHTML($html);
        $mpdf->Output();
    }

    public function cargaHoraria() {
        $this->auth->liberado('TI|secretaria|coordenacao|financeiro|professor|CAA');

        $idProfessor = $this->input->post('prof');

        $professores = $this->professoresmodel->getArray();
        $this->dados['professores'] = $professores;

        $this->dados['titulo'] = 'alunos';
        $mpdf = new mPDF();
        $html = $this->load->view('adm/solicitacoes/cargaHoraria', $this->dados, TRUE);

        echo $html;
        //$mpdf->SetFooter('{PAGENO}');
        //$mpdf->writeHTML($html);
        //$mpdf->Output();
    }

}
