<?php

/** CRUD Controller para Agenda* */
class Esportelazer extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('esportmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
    }

    public function index() {
        $this->dados['horarios'] = $this->esportmodel->getHorariosReserva();
        $this->dados['pagina'] = 'adm/esportelazer/indexEsport';
        $this->dados['titulo'] = 'agenda ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function horarios() {
        $this->dados['horarios'] = $this->esportmodel->getHorarios();
        $this->dados['pagina'] = 'adm/esportelazer/horariosEsport';
        $this->dados['titulo'] = 'agenda ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function horarioCad() {
        if ($_POST) {
            $this->esportmodel->cadHorario($_POST);
            echo "<script>window.location.href='" . site_url() . "/adm/esportelazer/horarios'</script>";
        } else {
            $this->dados['dados'] = $this->esportmodel;
            $this->dados['pagina'] = 'adm/esportelazer/horarioCadEsport';
            $this->dados['titulo'] = 'agenda ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function horarioEdit($idHorario) {
        if ($_POST) {
            $this->esportmodel->horarioEdit($this->input->post(), $idHorario);
            echo "<script>window.location.href='" . site_url() . "/adm/esportelazer/horarios'</script>";
        } else {
            $this->dados['dados'] = $this->esportmodel->getHorarioById($idHorario);
            $this->dados['pagina'] = 'adm/esportelazer/horarioEditEsport';
            $this->dados['titulo'] = 'agenda ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function lancar() {
        if ($this->input->post()) {
            $controle = $this->input->post('control');
            if ($controle == 'gerar') {
                $periodo = $this->input->post('periodo');
                $inicio = $this->util->data(substr($periodo, 0, 10));
                $fim = $this->util->data(substr($periodo, 13, 20));

                $inicioDate = new DateTime($inicio);
                $fimDate = new DateTime($fim);
                $intervalo = $fimDate->diff($inicioDate);
                $diasIntervalo = $intervalo->days;

                for ($i = 0; $i <= $diasIntervalo; $i++) {
                    $inserirData = date('Y-m-d', strtotime("+$i days", strtotime($inicio)));
                    $diaSemana = date('w', strtotime($inserirData));

                    $horarios = $this->esportmodel->getHorariosByDia($diaSemana);
                    if ($horarios->num_rows() > 0) {
                        foreach ($horarios->result() as $horario) {
                            $horariosDia[] = array(
                                'idHorario' => $horario->eho_id,
                                'data' => $inserirData,
                                'diaSemana' => $diaSemana,
                                'horario' => $horario->eho_horario,
                                'modalidade' => $horario->eho_modalidade,
                            );
                        }
                    }
                }
                $this->dados['periodo'] = $periodo;
                $this->dados['horariosDia'] = $horariosDia;
                $this->dados['dados'] = $this->esportmodel;
                $this->dados['pagina'] = 'adm/esportelazer/lancarEsport';
                $this->dados['titulo'] = 'agenda ';
                $this->load->view($this->_tpl, $this->dados);
            } elseif ($controle == 'salvar') {
                $dados = $this->input->post();
                $this->esportmodel->inserirHorarios($dados);
                echo "<script>window.location.href='" . site_url() . "/adm/esportelazer'</script>";
            }
        } else {
            $this->dados['dados'] = $this->esportmodel;
            $this->dados['pagina'] = 'adm/esportelazer/lancarEsport';
            $this->dados['titulo'] = 'agenda ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function delete($id) {
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->esportmodel->delete($id);
            redirect('agenda');
            return;
        } else {
            if ($_POST) {
                redirect('agenda');
                return;
            }
            $object = $this->esportmodel->getById($id);
        }

        $this->dados['pagina'] = 'agenda/delete';
        $this->dados['titulo'] = 'agenda ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $id;
        $this->load->view($this->_tpl, $this->dados);
    }

    public function confirmar($idReserva) {
        $this->esportmodel->confirmarAdm($idReserva);
        echo "<script>window.location.href='" . site_url() . "/adm/esportelazer'</script>";
    }

}
