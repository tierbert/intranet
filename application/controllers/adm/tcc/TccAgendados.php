<?php

/** CRUD Controller para tcc__agendados* */
class TccAgendados extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('tcc__agendadosmodel', 'tcaa', true);
        $this->load->model('tcc__horariosmodel', 'tcch', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
        $this->auth->liberado('TI|COORDENACAO');
    }

    public function index($semana = '') {

        $this->dados['semana'] = $semana;
        $this->dados['turnos'] = $this->tcch->getTurnos();
        $this->dados['pagina'] = 'adm/tcc/agendadosAdm';
        $this->dados['titulo'] = 'tcc__agendados';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function horCad($idTurno, $dia) {
        if ($this->input->post()) {
            $this->tcch->horCad($this->input->post());
            echo "<script>window.location.href='" . site_url('adm/tcc/tccAgendados') . "'</script>";
        } else {
            $this->dados['idTurno'] = $idTurno;
            $this->dados['dia'] = $dia;
            $this->dados['professores'] = $this->tcch->getProfArray();
            $this->dados['pagina'] = 'adm/tcc/horCad';
            $this->dados['titulo'] = 'alunos ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function horDel($idHorario) {
        $response = NULL;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->tcch->horDel($idHorario);
            echo "<script>window.location.href = '" . site_url('adm/tcc/tccAgendados') . "'</script>";
            return;
        } else {
            if ($_POST) {
                redirect('adm/tcc/tccAgendados');
                return;
            }
            $object = $this->tcch->getById($idHorario);
        }

        $this->dados['pagina'] = 'adm/tcc/horDel';
        $this->dados['titulo'] = 'alunos ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['idHorario'] = $idHorario;
        $this->load->view($this->_tpl, $this->dados);
    }

    public function reservDel($idHorario) {
        $response = NULL;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->tcch->reservDel($idHorario);
            echo "<script>window.location.href = '" . site_url('adm/tcc/tccAgendados') . "'</script>";
            return;
        } else {
            if ($_POST) {
                redirect('adm/tcc/tccAgendados');
                return;
            }
            $object = $this->tcch->getAgendById($idHorario);
        }

        $this->dados['pagina'] = 'adm/tcc/reservDel';
        $this->dados['titulo'] = 'alunos ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['idHorario'] = $idHorario;
        $this->load->view($this->_tpl, $this->dados);
    }

}
