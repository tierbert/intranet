<?php

/** CRUD Controller para estag__locais* */
class EstagioPsicAdm extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('estag__locaismodel', '', true);
        $this->load->model('estag__agendadosmodel', '', true);
        $this->load->model('estag__vagasmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
        $this->auth->liberado('COORDENACAO|TI');
        //$this->auth->check_logged($this->router->class, $this->router->method);
    }

    /** Lista estag__locais * */
    public function index($tipoEstagio) {
        /*
          if (trim($curso) != 'PSICOLOGIA') {
          echo "<script>alert('SOMENTE PARA ALUNOS DO CURSO DE PSICOLOGIA');window.location.href='" . site_url() . "/aluno/indexaluno/'</script>";
          exit();
          }
         * 
         */
        $tipo = 1;

        if ($tipoEstagio == 1) {
            $this->dados['tipo'] = 'campo';
            $this->dados['tipoExt'] = 'Campo';
        } elseif ($tipoEstagio == 2) {
            $this->dados['tipo'] = 'orientacao';
            $this->dados['tipoExt'] = 'Orientação Acadêmica';
        }
        $this->dados['$tipoEstagio'] = $tipoEstagio;
        $this->dados['locais'] = $this->estag__locaismodel->getLocais();
        $this->dados['pagina'] = 'adm/estagPsic/estagPsicIndexAdm';
        $this->dados['titulo'] = 'ESTÁGIO PSICOLOGIA';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function aviso() {
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $curso = $this->session->userdata('curso');

        if (trim($curso) != 'PSICOLOGIA') {
            echo "<script>alert('SOMENTE PARA ALUNOS DO CURSO DE PSICOLOGIA');window.location.href='" . site_url() . "/aluno/indexaluno/'</script>";
            exit();
        }

        $this->dados['pagina'] = 'aluno/estagPsic/estagPsicAviso';
        $this->dados['titulo'] = 'estag__locais';
        $this->load->view('aluno/solicitacoes/indexSolicitacao', $this->dados);
    }

    public function seleciona($tipo, $tipoEstagio) {
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $this->dados['idAluno'] = $idAluno;
        $this->dados['nomeAluno'] = $nomeAluno;
        $this->dados['matriculaAluno'] = $matriculaAluno;
        if ($tipo == 1) {
            $this->dados['tipo'] = 'campo';
            $this->dados['tipoExt'] = 'Campo';
        } elseif ($tipo == 2) {
            $this->dados['tipo'] = 'orientacao';
            $this->dados['tipoExt'] = 'Orientação Acadêmica';
        }
        $this->dados['$tipoEstagio'] = $tipoEstagio;
        $this->dados['locais'] = $this->estag__locaismodel->getLocais($tipoEstagio);
        $this->dados['pagina'] = 'aluno/estagPsic/estagPsicSeleciona';
        $this->dados['titulo'] = 'estag__locais';
        $this->load->view('aluno/solicitacoes/indexSolicitacao', $this->dados);
    }

    public function selecionar($idVaga) {

        $idAluno = $this->session->userdata('alu_id');

        $tipo = $this->estag__vagasmodel->getById($idVaga);

        $vaga = $this->estag__agendadosmodel->getByAlunoTipo($idAluno, $tipo->ev_tipo);

        $agendados = $this->estag__agendadosmodel->agendadosVaga($idVaga, $tipo->ev_tipo);

        if (($tipo->ev_vagas - $agendados->num_rows()) == 0) {
            echo "alert('VAGAS ESGOTADAS');<script>window.location.href='" . site_url() . "/aluno/EstagioPsic/'</script>";
            exit();
        }

        echo $vaga->num_rows();
        if ($vaga->num_rows() > 0) {
            echo "<script>alert('RESERVA DE VAGA JÁ REALIZADA');window.location.href='" . site_url() . "/aluno/EstagioPsic/'</script>";
            exit();
        } else {

            $dadosVaga = array(
                'ea_idAluno' => $idAluno,
                'ea_idVaga' => $idVaga,
                'ea_status' => 'reservado',
            );
            $this->estag__agendadosmodel->inserir($dadosVaga);
            echo "<script>alert('VAGA RESERVADA');window.location.href='" . site_url() . "/aluno/EstagioPsic/'</script>";
        }
    }

    public function delete($idAgenda) {
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->estag__agendadosmodel->delete($idAgenda);
            echo "<script>window.location.href = '" . site_url('adm/EstagioPsicAdm/index/1') . "'</script>";
            return;
        } else {
            if ($_POST) {
                echo "<script>window.location.href = '" . site_url('adm/EstagioPsicAdm/index/1') . "'</script>";
                return;
            }
            $object = $this->estag__agendadosmodel->getById($idAgenda);
        }

        $this->dados['pagina'] = 'adm/estagPsic/delete';
        $this->dados['titulo'] = 'estag__locais ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $idAgenda;
        $this->load->view($this->_tpl, $this->dados);
    }

    public function cadVaga($idLocal) {
        if ($this->input->post()) {
            $this->estag__vagasmodel->inserir($this->input->post());
            echo "<script>alert('CADASTRADO COM SUCESSO');window.location.href = '" . site_url('adm/EstagioPsicAdm/index/1') . "'</script>";
        } else {
            //$this->dados['dados'] = $this->estag__vagasmodel->getByLocal($idLocal);
            $this->dados['dados'] = $this->estag__vagasmodel;
            $this->dados['idLocal'] = $idLocal;
            $this->dados['pagina'] = 'adm/estagPsic/cadVaga';
            $this->dados['titulo'] = 'ESTÁGIO PSICOLOGIA / VAGAS ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function editVaga($idLocal, $idVagas) {
        if ($this->input->post()) {
            $this->estag__vagasmodel->update($this->input->post(), $idVagas);
            echo "<script>alert('CADASTRADO COM SUCESSO');window.location.href = '" . site_url('adm/EstagioPsicAdm/index/1') . "'</script>";
        } else {
            //$this->dados['dados'] = $this->estag__vagasmodel->getByLocal($idLocal);
            $this->dados['dados'] = $this->estag__vagasmodel->getById($idVagas);
            $this->dados['idLocal'] = $idLocal;
            $this->dados['pagina'] = 'adm/estagPsic/cadVaga';
            $this->dados['titulo'] = 'ESTÁGIO PSICOLOGIA / VAGAS ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

}
