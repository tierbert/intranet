<?php

/** CRUD Controller para alunos* */
class ProjetosAdm extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();
    private $perfil;

    public function __construct() {
        parent::__construct();
        $this->load->model('projetosmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
        $perfil = $this->session->userdata('usu_perfil');
        $this->perfil = $perfil;
    }

    public function index() {
        if ($this->perfil == 'COORD.NUPEX' || $this->perfil == 'TI') {
            echo "<script>window.location.href = '" . site_url('adm/ProjetosAdm/nupexPendentes') . "'</script>";
        } elseif ($this->perfil == 'PROGPEX') {
            echo "<script>window.location.href = '" . site_url('adm/ProjetosAdm/academicaPendentes') . "'</script>";
        } elseif ($this->perfil == 'COORD.FINANCEIRO') {
            echo "<script>window.location.href = '" . site_url('adm/ProjetosAdm/FinanceiroPendentes') . "'</script>";
        } else {
            echo "<script>alert('ACESSO RESTRITO');window.location.href = '" . site_url('/adm/painel') . "'</script>";
            exit();
        }

        $this->dados['pagina'] = 'adm/projetos/indexProjetos';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function nupexPendentes() {
        $this->checaPerfil('COORD.NUPEX');
        $projetos = $this->projetosmodel->getProjetos('nupex', '');
        $this->dados['projetos'] = $projetos;
        $this->dados['pagina'] = 'adm/projetos/nupexPendentes';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function nupexCipe() {
        $this->checaPerfil('COORD.NUPEX');
        $projetos = $this->projetosmodel->getProjetos('cipe', '');
        $this->dados['projetos'] = $projetos;
        $this->dados['pagina'] = 'adm/projetos/nupexCipe';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function nupexPendentesAval($idProjeto) {
        $this->checaPerfil('COORD.NUPEX');
        if ($this->input->post()) {
            $idSetor = 18; /* coord.nupex */
            $parecer = $this->projetosmodel->getParecerBySetor($idProjeto, $idSetor);
            $idParecer = $parecer->enc_id;
            $this->projetosmodel->updateParecer($this->input->post(), $idParecer);
            echo "<script>alert('PARECER INSERIDO');window.location.href = '" . site_url() . "/adm/ProjetosAdm/nupexPendentes/'</script>";
        } else {
            $projetos = $this->projetosmodel->getById($idProjeto);
            $this->dados['dados'] = $projetos;
            $this->dados['anexos'] = $this->projetosmodel->getAnexosByProj($idProjeto);
            $this->dados['pagina'] = 'adm/projetos/nupexPendentesAval';
            $this->dados['titulo'] = 'alunos';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function nupexAcademica() {
        $this->checaPerfil('COORD.NUPEX');
        $projetos = $this->projetosmodel->getProjetos('academica', '');
        $this->dados['projetos'] = $projetos;
        $this->dados['pagina'] = 'adm/projetos/nupexAcademica';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function nupexFinanceiro() {
        $this->checaPerfil('COORD.NUPEX');
        $projetos = $this->projetosmodel->getProjetos('financeiro', '');
        $this->dados['projetos'] = $projetos;
        $this->dados['pagina'] = 'adm/projetos/nupexFinanceiro';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function nupexAprovados() {
        $this->checaPerfil('COORD.NUPEX');
        $projetos = $this->projetosmodel->getProjetos('aprovados', 'todos');
        $this->dados['projetos'] = $projetos;
        $this->dados['pagina'] = 'adm/projetos/nupexAprovados';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function nupexRejeitados() {
        $this->checaPerfil('COORD.NUPEX');
        $projetos = $this->projetosmodel->getProjetos('todos', 'indeferido');
        $this->dados['projetos'] = $projetos;
        $this->dados['pagina'] = 'adm/projetos/nupexRejeitados';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function nupexTodos() {
        $this->checaPerfil('COORD.NUPEX');
        $projetos = $this->projetosmodel->getProjetos('todos', 'todos');
        $this->dados['projetos'] = $projetos;
        $this->dados['pagina'] = 'adm/projetos/nupexTodos';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    /* academica  */

    public function academicaPendentes() {
        $this->checaPerfil('PROGPEX');
        $projetos = $this->projetosmodel->getProjetos('academica', '');
        $this->dados['projetos'] = $projetos;
        $this->dados['pagina'] = 'adm/projetos/academicaPendentes';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function academicaPendentesAval($idProjeto) {
        $this->checaPerfil('PROGPEX');
        if ($this->input->post()) {
            $idSetor = 29; /* ger.academica */
            $parecer = $this->projetosmodel->getParecerBySetor($idProjeto, $idSetor);
            $idParecer = $parecer->enc_id;
            $this->projetosmodel->updateParecer($this->input->post(), $idParecer);
            echo "<script>alert('PARECER INSERIDO');window.location.href = '" . site_url() . "/adm/ProjetosAdm/academicaPendentes'</script>";
        } else {
            $projetos = $this->projetosmodel->getById($idProjeto);
            $this->dados['dados'] = $projetos;
            $this->dados['anexos'] = $this->projetosmodel->getAnexosByProj($idProjeto);
            $this->dados['pagina'] = 'adm/projetos/academicaPendentesAval';
            $this->dados['titulo'] = 'alunos';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function academicaAprovados() {
        $this->checaPerfil('PROGPEX');
        $projetos = $this->projetosmodel->getProjetos('academica', 'deferido');
        $this->dados['projetos'] = $projetos;
        $this->dados['pagina'] = 'adm/projetos/academicaAprovados';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function academicaRejetados() {
        $this->checaPerfil('PROGPEX');
        $projetos = $this->projetosmodel->getProjetos('academica', 'indeferido');
        $this->dados['projetos'] = $projetos;
        $this->dados['pagina'] = 'adm/projetos/academicaRejetados';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    /* academica  fim */

    public function financeiroPendentes() {
        $this->checaPerfil('COORD.FINANCEIRO');
        $projetos = $this->projetosmodel->getProjetos('financeiro', '');
        $this->dados['projetos'] = $projetos;
        $this->dados['pagina'] = 'adm/projetos/financeiroPendentes';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function financeiroPendentesAval($idProjeto) {
        $this->checaPerfil('COORD.FINANCEIRO');
        if ($this->input->post()) {
            $idSetor = 16; /* ger.academica */
            $parecer = $this->projetosmodel->getParecerBySetor($idProjeto, $idSetor);
            $idParecer = $parecer->enc_id;
            $this->projetosmodel->updateParecer($this->input->post(), $idParecer);
            echo "<script>alert('PARECER INSERIDO');window.location.href = '" . site_url() . "/adm/ProjetosAdm/financeiroPendentes'</script>";
        } else {
            $projetos = $this->projetosmodel->getById($idProjeto);
            $this->dados['dados'] = $projetos;
            $this->dados['anexos'] = $this->projetosmodel->getAnexosByProj($idProjeto);
            $this->dados['pagina'] = 'adm/projetos/financeiroPendentesAval';
            $this->dados['titulo'] = 'alunos';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function financeiroAprovados() {
        $this->checaPerfil('COORD.FINANCEIRO');
        $projetos = $this->projetosmodel->getProjetos('financeiro', 'deferido');
        $this->dados['projetos'] = $projetos;
        $this->dados['pagina'] = 'adm/projetos/financeiroAprovados';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function financeiroRejeitados() {
        $this->checaPerfil('COORD.FINANCEIRO');
        $projetos = $this->projetosmodel->getProjetos('financeiro', 'indeferido');
        $this->dados['projetos'] = $projetos;
        $this->dados['pagina'] = 'adm/projetos/financeiroRejeitados';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function academicaDetalhar($idProjeto) {
        $this->checaPerfil('COORD.FINANCEIRO');
        $projetos = $this->projetosmodel->getById($idProjeto);
        $this->dados['dados'] = $projetos;
        $this->dados['idProjeto'] = $idProjeto;

        $this->dados['anexos'] = $this->projetosmodel->getAnexosByProj($idProjeto);
        $this->dados['pagina'] = 'adm/projetos/academicaDetalhar';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function financeiroDetalhar($idProjeto) {
        $this->checaPerfil('PROGPEX');
        $projetos = $this->projetosmodel->getById($idProjeto);
        $this->dados['dados'] = $projetos;
        $this->dados['anexos'] = $this->projetosmodel->getAnexosByProj($idProjeto);
        $this->dados['pagina'] = 'adm/projetos/financeiroDetalhar';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function nupexRejeitadosDetalhar($idProjeto) {
        $this->checaPerfil('COORD.NUPEX');
        $projetos = $this->projetosmodel->getById($idProjeto);
        $this->dados['dados'] = $projetos;
        $this->dados['anexos'] = $this->projetosmodel->getAnexosByProj($idProjeto);
        $this->dados['pagina'] = 'adm/projetos/nupexRejeitadosDetalhar';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function nupexDetalhar($idProjeto) {
        $this->checaPerfil('COORD.NUPEX');
        $projetos = $this->projetosmodel->getById($idProjeto);
        $this->dados['dados'] = $projetos;
        $this->dados['anexos'] = $this->projetosmodel->getAnexosByProj($idProjeto);
        $this->dados['pagina'] = 'adm/projetos/nupexDetalhar';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function cipePendentes() {
        $this->checaPerfil('COORD.NUPEX');
        $projetos = $this->projetosmodel->getProjetos('cipe', '');
        $this->dados['projetos'] = $projetos;
        $this->dados['pagina'] = 'adm/projetos/cipePendentes';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function cipeAprovados() {
        $this->checaPerfil('COORD.NUPEX');
        $projetos = $this->projetosmodel->getProjetos('cipe', 'deferido');
        $this->dados['projetos'] = $projetos;
        $this->dados['pagina'] = 'adm/projetos/cipeAprovados';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function cipeRejeitados() {
        $this->checaPerfil('COORD.NUPEX');
        $projetos = $this->projetosmodel->getProjetos('cipe', 'indeferido');
        $this->dados['projetos'] = $projetos;
        $this->dados['pagina'] = 'adm/projetos/cipeRejeitados';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function cipePendentesAval($idProjeto) {
        $this->checaPerfil('COORD.NUPEX');
        if ($this->input->post()) {
            $idSetor = 30; /* cipe */
            $parecer = $this->projetosmodel->getParecerBySetor($idProjeto, $idSetor);
            $idParecer = $parecer->enc_id;
            $this->projetosmodel->updateParecer($this->input->post(), $idParecer);
            echo "<script>alert('PARECER INSERIDO');window.location.href = '" . site_url() . "/adm/ProjetosAdm/cipePendentes'</script>";
        } else {
            $projetos = $this->projetosmodel->getById($idProjeto);
            $this->dados['dados'] = $projetos;
            $this->dados['parecerNupex'] = $this->projetosmodel->getParecer($idProjeto, 18);
            $this->dados['anexos'] = $this->projetosmodel->getAnexosByProj($idProjeto);
            $this->dados['pagina'] = 'adm/projetos/cipePendentesAval';
            $this->dados['titulo'] = 'alunos';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    private function checaPerfil($nivel) {
        if ($this->perfil == $nivel || $this->perfil == 'TI') {
            return true;
        } else {
            echo "<script>alert('ACESSO RESTRITO');window.location.href = '" . site_url('/adm/painel') . "'</script>";
            exit();
        }
    }

    public function download($idDownload) {
        $this->load->helper('download');
        $pastaAnexos = '../../intranet_files/projetos/';
        $download = $this->projetosmodel->getDownload($idDownload);
        echo $download->ane_arquivo;
        $arquivo = $pastaAnexos . $download->ane_arquivo;
        force_download($arquivo, null);
    }

}
