<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/** CRUD Controller para alunos* */
class CapacitaCursos extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();
    private $idUsuario;
    private $grupo;
    private $setor;
    private $idSetor;

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth', 'form_validation'));
        $this->auth->verificaloginAdm();
        $this->load->model('capacitamodel', '', true);
        $this->load->model('usuariosmodel', '', true);
        $this->load->model('capacitacursosmodel', '', true);

        $this->idUsuario = $this->session->userdata('usu_id');
        $this->grupo = $this->session->userdata('grupo');
        $this->idSetor = $this->session->userdata('setor');
        $this->setor = $this->session->userdata('setorNome');
    }

    private function check_grupo($grupo) {
        if ($this->grupo == $grupo) {
            return true;
        } else {
            echo "<script>window.location.href='" . site_url('adm/painel') . "'</script>";
            exit();
        }
    }

    public function index() {
        if ($this->grupo == 'administrativo') {
            echo "<script>alert('Somente para Coordenadores');window.location.href='" . site_url('adm/capacita/adm') . "'</script>";
        } elseif ($this->grupo == 'coord_setor') {
            echo "<script>window.location.href='" . site_url('adm/capacita/coord_setor') . "'</script>";
        } elseif ($this->grupo == 'coord_curso') {
            echo "<script>window.location.href='" . site_url('adm/capacita/coord_curso') . "'</script>";
        } else {
            echo "<script>alert('Cadastro Incompleto, Favor entrar em contato com o RH para atualizar seu cadastro');window.location.href='" . site_url('adm/painel') . "'</script>";
            exit();
        }
    }

    public function adm() {
        $this->check_grupo('coord_setor');
        //$this->check_grupo('administrativo');
        $this->dados['idSetor'] = $this->idSetor;
        $this->dados['idUsuario'] = $this->idUsuario;
        $this->dados['pagina'] = 'adm/capacita/adm';
        $this->dados['titulo'] = 'COORDENAÇÃO DE SETOR / ' . $this->setor;
        $this->load->view($this->_tpl, $this->dados);
    }

    public function agendaAdm($idGrupo) {
        $this->check_grupo('coord_setor');
        if ($this->input->post()) {
            $post = $this->input->post();
            $post['age_idCoordenacao'] = $this->session->userdata('login');
            //print_r($post);
            $this->capacitamodel->agendar($post);
            echo "<script>alert('Curso Agendado');window.location.href='" . site_url('adm/capacita/adm') . "'</script>";
        } else {
            //$this->check_grupo('administrativo');
            $cursos = $this->capacitamodel->getCusosBySetor('administrativo', $idGrupo);
            $this->dados['colaboradores'] = $this->usuariosmodel->getArray();
            $this->dados['cursos'] = $cursos;
            $this->dados['pagina'] = 'adm/capacita/agendarAdm';
            $this->dados['titulo'] = 'COORDENAÇÃO DE SETOR / ' . $this->setor;
            $this->load->view($this->_tpl, $this->dados);
        }
    }
    //new method ----
    public function deleteAgendaAdm($id = false) {
        $this->check_grupo('coord_setor');
        if ($id) {
            $this->capacitamodel->delete($id);
            echo "<script>alert('Agendamento Excluído');window.location.href='" . site_url('adm/capacita/adm') . "'</script>";
        } else {
            echo "<script>alert('Você tentou efetuar alguma ação não adequada. Redirecionando...');window.location.href='" . site_url('adm/capacita/adm') . "'</script>";
        }
    }
    public function editCourse($id = false) {
        $this->check_grupo('coord_setor');
        if ($_POST) {
            $this->capacitacursosmodel->updateCourse($_POST, $id);
            echo "<script>window.location.href='" . site_url() . "/adm/capacita/'</script>";
            return;
        } else {
            $this->dados['object'] = $this->capacitacursosmodel->getCourseById($id);
            $this->dados['pagina'] = 'adm/capacita/curso_edit';
            $this->load->view($this->_tpl, $this->dados);
        }
    }
    public function deleteCourse($id = false) {
        $this->check_grupo('coord_setor');
        if ($id) {
            $this->capacitacursosmodel->delete($id);
            echo "<script>alert('Curso excluído');window.location.href='" . site_url('adm/capacita/adm') . "'</script>";
        } else {
            echo "<script>alert('Você tentou efetuar alguma ação não adequada. Redirecionando...');window.location.href='" . site_url('adm/capacita/adm') . "'</script>";
        }
    }
    // ----

    public function coord_setor() {
        $this->check_grupo('coord_setor');
        $this->dados['idUsuario'] = $this->idUsuario;
        $this->dados['pagina'] = 'adm/capacita/coord_setor';
        $this->dados['titulo'] = 'COORDENAÇÃO DE SETOR / ' . $this->setor;
        $this->load->view($this->_tpl, $this->dados);
    }

    public function agendaCoord_setor($idGrupo) {
        $this->check_grupo('coord_setor');
        $cursos = $this->capacitamodel->getCusosBySetor('administrativo', $idGrupo);
        $this->dados['cursos'] = $cursos;
        $this->dados['pagina'] = 'adm/capacita/agendaCoord_setor';
        $this->dados['titulo'] = 'COORDENAÇÃO DE SETOR / ' . $this->setor;
        $this->load->view($this->_tpl, $this->dados);
    }

    public function agendarCoord_setor($idCurso) {
        $this->check_grupo('coord_setor');

        $post['age_idUsuario'] = $this->idUsuario;
        $post['age_idCurso'] = $idCurso;
        $post['age_idCoordenacao'] = $this->session->userdata('setor');

        $this->capacitamodel->agendar($post);
        echo "<script>alert('Curso Agendado');window.location.href='" . site_url('adm/capacita/coord_setor') . "'</script>";
    }

    public function coord_curso() {
        $this->check_grupo('coord_curso');
        $this->dados['idUsuario'] = $this->idUsuario;
        $this->dados['pagina'] = 'adm/capacita/coord_curso';
        $this->dados['titulo'] = 'COORDENAÇÃO DE CURSO';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function agendaCoord_curso($idGrupo) {
        $this->check_grupo('coord_curso');
        $cursos = $this->capacitamodel->getCusosBySetor('administrativo', $idGrupo);
        $this->dados['cursos'] = $cursos;
        $this->dados['pagina'] = 'adm/capacita/agendaCoord_curso';
        $this->dados['titulo'] = 'COORDENAÇÃO DE CURSO';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function agendarCoord_curso($idCurso) {
        $this->check_grupo('coord_curso');
        
        $post['age_idUsuario'] = $this->idUsuario;
        $post['age_idCurso'] = $idCurso;
        $post['age_idCoordenacao'] = $this->session->userdata('setor');
        
        $this->capacitamodel->agendar($post);
        echo "<script>alert('Curso Agendado');window.location.href='" . site_url('adm/capacita/coord_curso') . "'</script>";
    }
    public function cadastrarCurso() {
        if ($_POST) {
            $this->capacitacursosmodel->inserir($_POST);
            echo "<script>window.location.href='" . site_url() . "/adm/capacita/'</script>";
        
        } else {
            $this->dados['pagina'] = 'adm/capacita/curso_create';
            $this->dados['object'] = $this->capacitacursosmodel;
            $this->load->view($this->_tpl, $this->dados);
        }
        
    }

}
