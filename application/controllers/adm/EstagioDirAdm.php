<?php

/** CRUD Controller para estag__locais* */
class EstagioDirAdm extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('estag__locais_dirmodel', '', true);
        $this->load->model('estag__agendados_dirmodel', '', true);
        $this->load->model('estag__vagas_dirmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
        $this->auth->liberado('coordenacao|TI|coordenador');
        //$this->auth->check_logged($this->router->class, $this->router->method);
    }

    /** Lista estag__locais * */
    public function index($tipoEstagio) {
        /*
          if (trim($curso) != 'Direito') {
          echo "<script>alert('SOMENTE PARA ALUNOS DO CURSO DE Direito');window.location.href='" . site_url() . "/aluno/indexaluno/'</script>";
          exit();
          }
         * 
         */
        $tipo = 1;

        if ($tipoEstagio == 1) {
            $this->dados['tipo'] = 'campo';
            $this->dados['tipoExt'] = 'Campo';
        } elseif ($tipoEstagio == 2) {
            $this->dados['tipo'] = 'orientacao';
            $this->dados['tipoExt'] = 'Orientação Acadêmica';
        }
        $this->dados['$tipoEstagio'] = $tipoEstagio;
        $this->dados['locais'] = $this->estag__locais_dirmodel->getLocais();
        $this->dados['pagina'] = 'adm/estagDir/estagDirIndexAdm';
        $this->dados['titulo'] = 'ESTÁGIO Direito';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function aviso() {
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $curso = $this->session->userdata('curso');

        if (trim($curso) != 'Direito') {
            echo "<script>alert('SOMENTE PARA ALUNOS DO CURSO DE DIREITO');window.location.href='" . site_url() . "/aluno/indexaluno/'</script>";
            exit();
        }

        $this->dados['pagina'] = 'aluno/estagDir/estagDirAviso';
        $this->dados['titulo'] = 'estag__locais';
        $this->load->view('aluno/solicitacoes/indexSolicitacao', $this->dados);
    }

    public function seleciona($tipo, $tipoEstagio) {
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $this->dados['idAluno'] = $idAluno;
        $this->dados['nomeAluno'] = $nomeAluno;
        $this->dados['matriculaAluno'] = $matriculaAluno;
        if ($tipo == 1) {
            $this->dados['tipo'] = 'campo';
            $this->dados['tipoExt'] = 'Campo';
        } elseif ($tipo == 2) {
            $this->dados['tipo'] = 'orientacao';
            $this->dados['tipoExt'] = 'Orientação Acadêmica';
        }
        $this->dados['$tipoEstagio'] = $tipoEstagio;
        $this->dados['locais'] = $this->estag__locais_dirmodel->getLocais($tipoEstagio);
        $this->dados['pagina'] = 'aluno/estagDir/estagDirSeleciona';
        $this->dados['titulo'] = 'estag__locais';
        $this->load->view('aluno/solicitacoes/indexSolicitacao', $this->dados);
    }

    public function selecionar($idVaga) {

        $idAluno = $this->session->userdata('alu_id');

        $tipo = $this->estag__vagas_dirmodel->getById($idVaga);

        $vaga = $this->estag__agendados_dirmodel->getByAlunoTipo($idAluno, $tipo->ev_tipo);

        $agendados = $this->estag__agendados_dirmodel->agendadosVaga($idVaga, $tipo->ev_tipo);

        if (($tipo->ev_vagas - $agendados->num_rows()) == 0) {
            echo "alert('VAGAS ESGOTADAS');<script>window.location.href='" . site_url() . "/aluno/EstagioDir/'</script>";
            exit();
        }

        echo $vaga->num_rows();
        if ($vaga->num_rows() > 0) {
            echo "<script>alert('RESERVA DE VAGA JÁ REALIZADA');window.location.href='" . site_url() . "/aluno/EstagioDir/'</script>";
            exit();
        } else {

            $dadosVaga = array(
                'ea_idAluno' => $idAluno,
                'ea_idVaga' => $idVaga,
                'ea_status' => 'reservado',
            );
            $this->estag__agendados_dirmodel->inserir($dadosVaga);
            echo "<script>alert('VAGA RESERVADA');window.location.href='" . site_url() . "/aluno/EstagioDir/'</script>";
        }
    }

    public function delete($idAgenda) {
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->estag__agendados_dirmodel->delete($idAgenda);
            echo "<script>window.location.href = '" . site_url('adm/EstagioDirAdm/index/1') . "'</script>";
            return;
        } else {
            if ($_POST) {
                echo "<script>window.location.href = '" . site_url('adm/EstagioDirAdm/index/1') . "'</script>";
                return;
            }
            $object = $this->estag__agendados_dirmodel->getById($idAgenda);
        }

        $this->dados['pagina'] = 'adm/estagDir/delete';
        $this->dados['titulo'] = 'estag__locais ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $idAgenda;
        $this->load->view($this->_tpl, $this->dados);
    }

    public function cadVaga($idLocal) {
        if ($this->input->post()) {
            $this->estag__vagas_dirmodel->inserir($this->input->post());
            echo "<script>alert('CADASTRADO COM SUCESSO');window.location.href = '" . site_url('adm/EstagioDirAdm/index/1') . "'</script>";
        } else {
            //$this->dados['dados'] = $this->estag__vagas_dirmodel->getByLocal($idLocal);
            $this->dados['dados'] = $this->estag__vagas_dirmodel;
            $this->dados['idLocal'] = $idLocal;
            $this->dados['pagina'] = 'adm/estagDir/cadVaga';
            $this->dados['titulo'] = 'ESTÁGIO Direito / VAGAS ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function editVaga($idLocal, $idVagas) {
        if ($this->input->post()) {
            $this->estag__vagas_dirmodel->update($this->input->post(), $idVagas);
            echo "<script>alert('CADASTRADO COM SUCESSO');window.location.href = '" . site_url('adm/EstagioDirAdm/index/1') . "'</script>";
        } else {
            //$this->dados['dados'] = $this->estag__vagas_dirmodel->getByLocal($idLocal);
            $this->dados['dados'] = $this->estag__vagas_dirmodel->getById($idVagas);
            $this->dados['idLocal'] = $idLocal;
            $this->dados['pagina'] = 'adm/estagDir/cadVaga';
            $this->dados['titulo'] = 'ESTÁGIO Direito / VAGAS ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    
    //@author Victor Scher
    public function cadLocal(){
        if($this->input->post()){
            $this->estag_locais_dirmodel->inserir($this->input->post());
            echo "<script>alert('CADASTRADO COM SUCESSO');window.location.href = '" . site_url('adm/EstagioDirAdm/index/1') . "'</script>";
        } else {
            $this->dados['pagina'] = 'adm/estagDir/cadLocal';
            $this->dados['titulo'] = 'Estágio Direito / Locais';
        }
    }

    //@author Victor Scher
    public function editLocal(){
        if($this->input->post()){
    
        } else {
    
        }
    }

    //@author Victor Scher
    public function deletarLocal(){
        
    }
}
