<?php

/** CRUD Controller para tcc__agendados* */
class Esportelazer extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('esportemodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
        $this->auth->liberado('TI|COORDENACAO');
    }

    public function index($semana = '') {
        $this->dados['semana'] = $semana;
        $this->dados['turnos'] = $this->esportemodel->getTurnos();
        $this->dados['pagina'] = 'adm/esportelazer/esp_indexAdm';
        $this->dados['titulo'] = 'tcc__agendados';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function horCad($idTurno, $dia) {
        if ($this->input->post()) {
            $this->esportemodel->horCad($this->input->post());
            echo "<script>window.location.href='" . site_url('adm/esportelazer') . "'</script>";
        } else {
            $this->dados['idTurno'] = $idTurno;
            $this->dados['dia'] = $dia;
            $this->dados['professores'] = $this->esportemodel->getmodalidades();
            $this->dados['pagina'] = 'adm/esportelazer/horCad';
            $this->dados['titulo'] = 'alunos ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function horDel($idHorario) {
        $response = NULL;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->esportemodel->horDel($idHorario);
            echo "<script>window.location.href = '" . site_url('adm/esportelazer/tccAgendados') . "'</script>";
            return;
        } else {
            if ($_POST) {
                redirect('adm/esportelazer/tccAgendados');
                return;
            }
            $object = $this->esportemodel->getById($idHorario);
        }

        $this->dados['pagina'] = 'adm/esportelazer/horDel';
        $this->dados['titulo'] = 'alunos ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['idHorario'] = $idHorario;
        $this->load->view($this->_tpl, $this->dados);
    }

    public function reservar($idhorario, $data) {
        $dadosAgenda = array(
            'eag_idHorario' => $idhorario,
            'eag_idAluno' => '0',
            'eag_data' => $data,
        );
        $this->esportemodel->inserirAgenda($dadosAgenda);
        echo "<script>window.location.href = '" . site_url('adm/esportelazer') . "'</script>";
    }

}
