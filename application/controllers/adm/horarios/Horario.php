<?php

/** CRUD Controller para hor_horario* */
class Horario extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('hor_horariomodel', 'horariom', true);
        $this->load->model('Hor_turnomodel', 'turno', true);
        $this->load->model('professoresmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
    }

    /** Lista hor_horario * */
    public function index($tipo) {
        $this->auth->liberado('TI|secretaria|coordenacao|recep_coord');
        $this->dados['dados'] = $this->horariom->getReservas($tipo);
        $this->dados['tipo'] = $tipo;
        $this->dados['pagina'] = 'adm/hor_horario/reservas';
        $this->dados['titulo'] = 'hor_horario';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function calendario($tipo, $semana = '') {
        // $this->auth->liberado('TI|secretaria|coordenacao|coordenador|recepcao');
        $this->auth->liberado('TI|secretaria|recepcao|recep_coord');
        $this->dados['turnos'] = $this->turno->getAll($tipo);
        $this->dados['tipo'] = $tipo;
        $this->dados['semana'] = $semana;
        $this->dados['user_setor'] = $this->session->userdata('setor');
        //$this->dados['dados'] = $this->horariom->getBySala($sala);
        $this->dados['pagina'] = 'adm/hor_horario/sala';
        $this->dados['titulo'] = 'hor_horario';
        $this->load->view('adm/hor_horario/sala', $this->dados);
    }

    public function reserva($turno, $sala, $dia, $data, $tipo) {
        $this->dados['tipo'] = $tipo;
        if ($this->input->post()) {
            if ($this->util->data($this->input->post('data')) < date('Y-m-d')) {
                //echo 'data-retroativa';
                echo "<script>alert('NÃO É POSSIVEL AGENDAR SALA EM DATA RETROATIVA');window.location.href='" . site_url('adm/horarios/horario/reserva/' . $turno . '/' . $sala . '/' . $dia . '/' . $data . '/' . $tipo) . "'</script>";
                exit();
            }

            $agend = $this->horariom->getByTurnoSalaDia($turno, $sala, $this->util->data($this->input->post('data')));
            if ($agend->num_rows() > 0) {
                //echo 'reservada';
                echo "<script>alert('NÃO FOI POSSÍVEL AGENDAR A SALA POIS EXISTE AGENDAMENTO PARA ESTE DIA');window.location.href='" . site_url('adm/horarios/horario/calendario/' . $tipo) . "'</script>";
                exit();
            }

            $reserv = $this->horariom->getByHorario($turno, $sala, $dia);
            if ($reserv->num_rows() == 1) {
                //echo 'ocupada:' . $reserv->num_rows();
                echo "<script>alert('NÃO FOI POSSÍVEL AGENDAR A SALA POIS EXISTE AULA NESTE DIA');window.location.href='" . site_url('adm/horarios/horario/calendario/' . $tipo) . "'</script>";
                exit();
            } else {
                $reserv = $this->horariom->reservar($this->input->post('turno'), $this->input->post('sala'), $this->input->post('hor_idProfessor'), $this->input->post('res_disciplina'), $this->input->post('res_turma'), $this->input->post('dia'), $this->input->post('data'), $tipo);
                echo "<script>alert('RESERVA CONFIRMADA');window.location.href='" . site_url('adm/horarios/horario/index/sala') . "'</script>";
            }
        } else {
            $this->dados['professores'] = $this->professoresmodel->getArrayRh();
            $this->dados['sala'] = $this->horariom->getSalaById($sala);
            $this->dados['turno'] = $this->turno->getById($turno);
            $this->dados['dia'] = ($dia);
            $this->dados['data'] = ($data);
            $this->dados['idTurno'] = ($turno);
            $this->dados['data'] = ($data);
            $this->dados['diaSemana'] = $this->getDia($dia);
            $this->dados['pagina'] = 'adm/hor_horario/reservaSala';
            $this->dados['titulo'] = 'hor_horario';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function bloquearsala(){
        if ($this->input->post()) {
            $sala = $this->input->post('sala');
            $dia = $this->input->post('dia');
            $horario = $this->input->post('horario');
            $dataLimite = $this->input->post('dataLimite');
            $this->horariom->bloquearSala($sala, $dia, $horario, $dataLimite);
            echo "<script>alert('SALA BLOQUEADA');window.location.href='" . site_url('adm/horarios/horario/index/sala') . "'</script>";

        } else {
            $horarios = $this->turno->getTurnosByType('lab');
            $salas = $this->horariom->getAllLabs();
            $this->dados['pagina'] = 'adm/hor_horario/bloquearSala';
            $this->dados['salas'] = $salas;
            $this->dados['horarios'] = $horarios;

            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function horarioEdit($sala, $turno, $dia, $tipo) {
        $this->auth->liberado('TI|coordenacao|recep_coord');
        if ($_POST) {
            $this->horariom->inserir($_POST);
            echo "<script>window.location.href='" . site_url('adm/horarios/horario/calendario/' . $tipo) . "'</script>";
        } else {
            $professores = $this->professoresmodel->getArrayRh();
            $this->dados['professores'] = $professores;
            $this->dados['turno'] = $turno;
            $this->dados['dia'] = $dia;
            $this->dados['sala'] = $sala;
            $this->dados['object'] = $this->horariom;
            $this->dados['pagina'] = 'adm/hor_horario/horarioEdit';
            $this->dados['titulo'] = 'hor_horario';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function confirmar($idReserva, $opcao, $tipo) {
        $this->auth->liberado('TI|recepcao|recep_coord');
        if ($opcao == 1) {
            $this->horariom->confirmar($idReserva, 'CONFIRMADA');
        } elseif ($opcao == 2) {
            $this->horariom->confirmar($idReserva, 'CANCELADA');
        }
        echo "<script>window.location.href='" . site_url('adm/horarios/horario/index/' . $tipo) . "'</script>";
    }

    public function editar($id) {
        $msg = null;
        if ($_POST) {
            //$this->form_validation->set_rules('valid', '', 'required');
            // if ($this->form_validation->run() == TRUE) {
            //$object->save();
            $this->hor_horariomodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/hor_horario/'</script>";
            return;
//} else {
//$msg = 'Por favor preencha todos os dados';
// }
        }

        $this->dados['object'] = $this->hor_horariomodel->getById($id);
        $this->dados['pagina'] = 'hor_horario/hor_horario_edit';
        $this->dados['titulo'] = 'hor_horario ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function horarioDel($id, $tipo, $agree = "false") {
        if($agree == "true"){
            $this->horariom->cancelarReserva($id, $tipo);
            echo "<script>window.location.href='" . site_url('adm/horarios/horario/index/' . $tipo) . "'</script>";
        }else{
            $this->auth->liberado('TI|coordenacao|recep_coord');
            $this->dados['pagina'] = 'adm/hor_horario/hor_horario_del';
            $this->dados['reservas'] = $this->horariom->getReservasById($id, $tipo);
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function calendarioLab($sala = 0) {
        $this->auth->liberado('TI|secretaria|coordenacao|coordenador|recepcao|recep_coord');
        $this->dados['turnos'] = $this->turno->getAll();
        $this->dados['tipo'] = 'LAB';
        //$this->dados['dados'] = $this->horariom->getBySala($sala);
        $this->dados['pagina'] = 'adm/hor_horario/sala';
        $this->dados['titulo'] = 'hor_horario';
        $this->load->view('adm/hor_horario/sala', $this->dados);
    }

    private function getDia($dia) {
        switch ($dia):
            case 1:
                return 'SEGUNDA';
                break;
            case 2:
                return 'TERÇA';
                break;
            case 3:
                return 'QUARTA';
                break;
            case 4:
                return 'QUINTA';
                break;
            case 5:
                return 'SEXTA';
                break;
            case 6:
                return 'SÁBADO';
                break;
            case 7:
                return 'DOMINGO';
                break;
        endswitch;
    }   

}