<?php
/** CRUD Controller para hor_turno**/ 
class Hor_turno extends CI_Controller { 

     private $_tpl = 'index.php';
     private $dados = array();

     public function __construct() {
         parent::__construct();
         $this->load->model('hor_turnomodel', '', true);
         $this->load->helper(array('form', 'url'));
         $this->load->library(array('Util', 'pagination'));
         $this->auth->verificaloginAdm();
     }

     /** Lista hor_turno * */
     public function index($inicio = 0) {
         $maximo = 10;
         $config['base_url'] = site_url('/hor_turno/index');
         $config['total_rows'] = $this->hor_turnomodel->contaRegistros();
         $config['per_page'] = $maximo;
         $this->pagination->initialize($config);
         $this->dados['paginacao'] = $this->pagination->create_links();

         $this->dados['dados'] = $this->hor_turnomodel->all($inicio, $maximo);
         $this->dados['pagina'] = 'hor_turno/hor_turno_list';
         $this->dados['titulo'] = 'hor_turno';
         $this->load->view($this->_tpl, $this->dados);
     }

     /** Cria novo(a)  * */
     public function criar() {
         if ($_POST) {
             $this->hor_turnomodel->inserir($_POST);
             echo "<script>window.location.href='" . site_url() . "/hor_turno/'</script>";
         }else{
             $this->dados['object'] = $this->hor_turnomodel;
             $this->dados['pagina'] = 'hor_turno/hor_turno_novo';
             $this->dados['titulo'] = 'hor_turno ';
             $this->load->view($this->_tpl, $this->dados);
          }
      } 
        

        
        

    /**
     * Edit 
     * */
    public function editar($id) {
        $msg = null;
        if ($_POST) {
            //$this->form_validation->set_rules('valid', '', 'required');
            // if ($this->form_validation->run() == TRUE) {
            //$object->save();
            $this->hor_turnomodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/hor_turno/'</script>";
return;
//} else {
//$msg = 'Por favor preencha todos os dados';
// }
}

$this->dados['object'] = $this->hor_turnomodel->getById($id);
$this->dados['pagina'] = 'hor_turno/hor_turno_edit';
$this->dados['titulo'] = 'hor_turno ';
$this->load->view($this->_tpl, $this->dados);
}

/**
* Delete
* */
public function delete($id) {
$response = null;
if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
$response = $this->hor_turnomodel->delete($id);
echo "<script>window.location.href = '" . site_url() . "/hor_turno/'</script>";
return;
} else {
if ($_POST) {
redirect('hor_turno');
return;
}
$object = $this->hor_turnomodel->getById($id);
}

$this->dados['pagina'] = 'hor_turno/hor_turno_del';
$this->dados['titulo'] = 'hor_turno ';
$this->dados['object'] = $object;
$this->dados['response'] = $response;
$this->dados['id'] = $id;
$this->load->view($this->_tpl, $this->dados);
}

}

