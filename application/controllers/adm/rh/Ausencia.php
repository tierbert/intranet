<?php

/** CRUD Controller para rh__ausencia* */
class Ausencia extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();
    private $idUsuario;
    private $setorNome;

    public function __construct() {
        parent::__construct();
        $this->load->model('rh__ausenciamodel', '', true);
        $this->load->model('usuariosmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
        $this->idUsuario = $this->session->userdata('usu_id');
        $this->setorNome = $this->session->userdata('setorNome');
    }

    /** Lista rh__ausencia * */
    public function index($inicio = 0) {
        $this->dados['dados'] = $this->rh__ausenciamodel->getByFunc($this->idUsuario);
        $this->dados['pagina'] = 'adm/rh/ausencia/ausenciaListFunc';
        $this->dados['titulo'] = 'rh__ausencia';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Lista rh__ausencia parecer RH passado id pelo get * */
    public function ausenciaParecRh($id) {
        $this->dados['object'] = $this->rh__ausenciamodel->getById($id);
        /*$this->dados['dados'] = $this->rh__ausenciamodel->getByFunc($this->idUsuario);*/
        $this->dados['pagina'] = 'adm/rh/ausencia/ausenciaParecRh';
        $this->dados['titulo'] = 'rh__ausencia';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Lista rh__ausencia parecer COORDENADOR passado id pelo get * */
    public function ausenciaParecCoord($id) {
        $this->dados['object'] = $this->rh__ausenciamodel->getById($id);
        /*$this->dados['dados'] = $this->rh__ausenciamodel->getByFunc($this->idUsuario);*/
        $this->dados['pagina'] = 'adm/rh/ausencia/ausenciaParecCoord';
        $this->dados['titulo'] = 'rh__ausencia';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Lista rh__ausencia parecer GERENCIA passado id pelo get * */
    public function ausenciaParecGer($id) {
        $this->dados['object'] = $this->rh__ausenciamodel->getById($id);
        /*$this->dados['dados'] = $this->rh__ausenciamodel->getByFunc($this->idUsuario);*/
        $this->dados['pagina'] = 'adm/rh/ausencia/ausenciaParecGer';
        $this->dados['titulo'] = 'rh__ausencia';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Lista rh__ausencia CONCLUIR passado id pelo get * */
    public function ausenciaConc($id) {
        $this->dados['object'] = $this->rh__ausenciamodel->getById($id);
        /*$this->dados['dados'] = $this->rh__ausenciamodel->getByFunc($this->idUsuario);*/
        $this->dados['pagina'] = 'adm/rh/ausencia/ausenciaConc';
        $this->dados['titulo'] = 'rh__ausencia';
        $this->load->view($this->_tpl, $this->dados);
    }





    public function criar() {
        if ($_POST) {
            $this->rh__ausenciamodel->inserir($_POST);
            echo "<script>window.location.href='" . site_url('adm/rh/ausencia') . "'</script>";
        } else {
            $this->dados['object'] = $this->rh__ausenciamodel;
            $this->dados['pagina'] = 'adm/rh/ausencia/ausenciaNovoFunc';
            $this->dados['titulo'] = 'rh__ausencia ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function editar($id) {
        $msg = null;
        if ($_POST) {
            $this->rh__ausenciamodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/rh__ausencia/'</script>";
            return;
        }

        $this->dados['object'] = $this->rh__ausenciamodel->getById($id);
        $this->dados['pagina'] = 'rh__ausencia/rh__ausencia_edit';
        $this->dados['titulo'] = 'rh__ausencia ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function ausListCoord() {
        if (in_array($this->setorNome, $this->util->coordenacoes())) {
            $dadosUsuario = $this->usuariosmodel->getById($this->idUsuario);
            $this->dados['dados'] = $this->rh__ausenciamodel->getBySetor('TI');
            $this->dados['pagina'] = 'adm/rh/ausencia/ausListCoord';
            $this->dados['titulo'] = 'rh__ausencia';
            $this->load->view($this->_tpl, $this->dados);
        } else {
            echo "<script>alert('SEM PREMISSAO DE ACESSO');window.location.href='" . site_url('adm/rh/iniciorh') . "'</script>";
        }
    }

    public function ausCoordEdit($id) {
        if (in_array($this->setorNome, $this->util->coordenacoes())) {
            $dadosUsuario = $this->usuariosmodel->getById($this->idUsuario);
            if ($_POST) {
                $this->rh__ausenciamodel->update($_POST, $id);
                echo "<script>window.location.href = '" . site_url('adm/rh/ausencia/ausListCoord') . "'</script>";
                return;
            } else {
                $this->dados['object'] = $this->rh__ausenciamodel->getById($id);
                $this->dados['pagina'] = 'adm/rh/ausencia/ausCoordEdit';
                $this->dados['titulo'] = 'rh__ausencia';
                $this->load->view($this->_tpl, $this->dados);
            }
        } else {
            echo "<script>alert('SEM PREMISSAO DE ACESSO');window.location.href='" . site_url('adm/rh/iniciorh') . "'</script>";
        }
    }

    public function ausListRh() {
        if (in_array($this->setorNome, array('RH_COORD'))) {
            $dadosUsuario = $this->usuariosmodel->getById($this->idUsuario);
            $this->dados['dados'] = $this->rh__ausenciamodel->getDeferidasRh();
            $this->dados['pagina'] = 'adm/rh/ausencia/ausListRh';
            $this->dados['titulo'] = 'rh__ausencia';
            $this->load->view($this->_tpl, $this->dados);
        } else {
            echo "<script>alert('SEM PREMISSAO DE ACESSO');window.location.href='" . site_url('adm/rh/iniciorh') . "'</script>";
        }
    }

    public function ausRhEdit($id) {
        if (in_array($this->setorNome, array('RH_COORD'))) {
            if ($_POST) {
                $this->rh__ausenciamodel->update($_POST, $id);
                echo "<script>window.location.href = '" . site_url('adm/rh/ausencia/ausListRh') . "'</script>";
                return;
            } else {
                $this->dados['object'] = $this->rh__ausenciamodel->getById($id);
                $this->dados['pagina'] = 'adm/rh/ausencia/ausRhEdit';
                $this->dados['titulo'] = 'rh__ausencia';
                $this->load->view($this->_tpl, $this->dados);
            }
        } else {
            echo "<script>alert('SEM PREMISSAO DE ACESSO');window.location.href='" . site_url('adm/rh/iniciorh') . "'</script>";
        }
    }

    public function ausListDirec() {
        if (in_array($this->setorNome, array('DIRECAO'))) {
            $dadosUsuario = $this->usuariosmodel->getById($this->idUsuario);
            $this->dados['dados'] = $this->rh__ausenciamodel->getDeferidasDirec();
            $this->dados['pagina'] = 'adm/rh/ausencia/ausListDirec';
            $this->dados['titulo'] = 'rh__ausencia';
            $this->load->view($this->_tpl, $this->dados);
        } else {
            echo "<script>alert('SEM PREMISSAO DE ACESSO');window.location.href='" . site_url('adm/rh/iniciorh') . "'</script>";
        }
    }

    public function ausDirecEdit($id) {
        if (in_array($this->setorNome, array('DIRECAO'))) {
            if ($_POST) {
                $this->rh__ausenciamodel->update($_POST, $id);
                echo "<script>window.location.href = '" . site_url('adm/rh/ausencia/ausListDirec') . "'</script>";
                return;
            } else {
                $this->dados['object'] = $this->rh__ausenciamodel->getById($id);
                $this->dados['pagina'] = 'adm/rh/ausencia/ausDirecEdit';
                $this->dados['titulo'] = 'rh__ausencia';
                $this->load->view($this->_tpl, $this->dados);
            }
        } else {
            echo "<script>alert('SEM PREMISSAO DE ACESSO');window.location.href='" . site_url('adm/rh/iniciorh') . "'</script>";
        }
    }

}
