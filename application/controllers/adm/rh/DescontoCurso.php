<?php

/** CRUD Controller para rh__descontoCurso* */
class DescontoCurso extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();
    private $idUsuario;
    private $tipoCursos;

    public function __construct() {
        parent::__construct();
        $this->load->model('rh__descontocursomodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
        $this->idUsuario = $this->session->userdata('usu_id');
        $this->tipoCursos = array(
            '' => 'SELECIONE O TIPO DE CURSO',
            'GRADUACAO' => 'GRADUAÇÃO',
            'POS_GRADUACAO' => 'POS-GRADUAÇÃO LATO SENSU',
            'MESTRADO' => 'POS-GRADUAÇÃO STRICTO SENSU',
        );
        $this->idUsuario = $this->session->userdata('usu_id');
        $this->setorNome = $this->session->userdata('setorNome');
    }

    /** Lista rh__descontoCurso * */
    public function descFuncList() {
        $this->dados['dados'] = $this->rh__descontocursomodel->getByFunc($this->idUsuario);
        $this->dados['pagina'] = 'adm/rh/descontocurso/descFuncList';
        $this->dados['titulo'] = 'rh__descontoCurso';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Lista parecer rh descontoCurso com id pelo get * */
    public function descFuncParecRh($id) {
        $this->dados['object'] = $this->rh__descontocursomodel->getById($id);
        $this->dados['pagina'] = 'adm/rh/descontocurso/descFuncParecRh';
        $this->dados['titulo'] = 'rh__descontoCurso ';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Lista parecer GERENCIA descontoCurso com id pelo get * */
    public function descFuncParecGer($id) {
        $this->dados['object'] = $this->rh__descontocursomodel->getById($id);
        $this->dados['pagina'] = 'adm/rh/descontocurso/descFuncParecGer';
        $this->dados['titulo'] = 'rh__descontoCurso ';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Lista parecer concluido descontoCurso com id pelo get * */
    public function descFuncParecCon($id) {
        $this->dados['object'] = $this->rh__descontocursomodel->getById($id);
        $this->dados['pagina'] = 'adm/rh/descontocurso/descFuncParecCon';
        $this->dados['titulo'] = 'rh__descontoCurso ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function descFuncNovo() {
        if ($_POST) {
            $this->rh__descontocursomodel->inserir($_POST);
            echo "<script>window.location.href='" . site_url('adm/rh/descontoCurso/descFuncList') . "'</script>";
        } else {
            $this->dados['tipoCursos'] = $this->tipoCursos;
            $this->dados['object'] = $this->rh__descontocursomodel;
            $this->dados['pagina'] = 'adm/rh/descontocurso/descFuncNovo';
            $this->dados['titulo'] = 'rh__descontoCurso ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function editarRh($id) {
        if (in_array($this->setorNome, array('RH_COORD'))) {
            $msg = null;
            if ($_POST) {
                $this->rh__descontocursomodel->update($_POST, $id);
                echo "<script>window.location.href = '" . site_url('adm/rh/descontoCurso/descFuncRhList') . "'</script>";
                return;
            }

            $this->dados['object'] = $this->rh__descontocursomodel->getById($id);
            $this->dados['pagina'] = 'adm/rh/descontocurso/editarRh';
            $this->dados['titulo'] = 'rh__descontoCurso ';
            $this->load->view($this->_tpl, $this->dados);
        } else {
            echo "<script>alert('SEM PREMISSAO DE ACESSO');window.location.href='" . site_url('adm/rh/iniciorh') . "'</script>";
        }
    }

    public function descFuncRhList() {
        if (in_array($this->setorNome, array('RH_COORD', 'CONVENIOS_COORD'))) {
            $this->dados['dados'] = $this->rh__descontocursomodel->all();
            $this->dados['pagina'] = 'adm/rh/descontocurso/descFuncRhList';
            $this->dados['titulo'] = 'rh__descontoCurso';
            $this->load->view($this->_tpl, $this->dados);
        } else {
            echo "<script>alert('SEM PREMISSAO DE ACESSO');window.location.href='" . site_url('adm/rh/iniciorh') . "'</script>";
        }
    }

    public function descFuncPosList() {
        if (in_array($this->setorNome, array('POSGRADUACAO', 'POSGRADUACAO_COORD'))) {
            $this->dados['dados'] = $this->rh__descontocursomodel->getByPos();
            $this->dados['pagina'] = 'adm/rh/descontocurso/descFuncPosList';
            $this->dados['titulo'] = 'rh__descontoCurso';
            $this->load->view($this->_tpl, $this->dados);
        } else {
            echo "<script>alert('SEM PREMISSAO DE ACESSO');window.location.href='" . site_url('adm/rh/iniciorh') . "'</script>";
        }
    }

    public function editarPos($id) {
        if (in_array($this->setorNome, array('POSGRADUACAO', 'POSGRADUACAO_COORD'))) {
            $msg = null;
            if ($_POST) {
                $this->rh__descontocursomodel->update($_POST, $id);
                echo "<script>window.location.href = '" . site_url('adm/rh/descontoCurso/descFuncPosList') . "'</script>";
                return;
            }

            $this->dados['object'] = $this->rh__descontocursomodel->getById($id);
            $this->dados['pagina'] = 'adm/rh/descontocurso/editarPos';
            $this->dados['titulo'] = 'rh__descontoCurso ';
            $this->load->view($this->_tpl, $this->dados);
        } else {
            echo "<script>alert('SEM PREMISSAO DE ACESSO');window.location.href='" . site_url('adm/rh/iniciorh') . "'</script>";
        }
    }

}
