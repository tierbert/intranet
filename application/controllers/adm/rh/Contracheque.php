<?php

date_default_timezone_set('America/Bahia');

/** CRUD Controller para rh_contracheque* */
class Contracheque extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('professoresmodel', '', true);
        $this->load->model('rh_contrachequemodel', '', true);
        $this->load->helper(array('form', 'url', 'string', 'download'));
        $this->load->library(array('Util', 'pagination', 'auth', 'upload'));
        $this->auth->verificaloginAdm();
    }

    /** Lista rh_contracheque * */
    public function index($inicio = 0) {
        $maximo = 10;
        $config['base_url'] = site_url('/rh_contracheque/index');
        $config['total_rows'] = $this->rh_contrachequemodel->contaRegistros();
        $config['per_page'] = $maximo;
        $this->pagination->initialize($config);
        $this->dados['paginacao'] = $this->pagination->create_links();

        $this->dados['dados'] = $this->rh_contrachequemodel->all($inicio, $maximo);
        $this->dados['pagina'] = 'adm/rh/contracheque/contracheque_list';
        $this->dados['titulo'] = 'rh_contracheque';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Cria novo(a)  * */
    public function criar() {
        if ($_POST) {
            $this->rh_contrachequemodel->inserir($_POST);
            echo "<script>window.location.href='" . site_url() . "/rh_contracheque/'</script>";
        } else {
            $this->dados['object'] = $this->rh_contrachequemodel;
            $this->dados['pagina'] = 'rh_contracheque/rh_contracheque_novo';
            $this->dados['titulo'] = 'rh_contracheque ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    /**
     * Edit 
     * */
    public function editar($id) {
        $msg = null;
        if ($_POST) {
            //$this->form_validation->set_rules('valid', '', 'required');
            // if ($this->form_validation->run() == TRUE) {
            //$object->save();
            $this->rh_contrachequemodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/rh_contracheque/'</script>";
            return;
        }

        $this->dados['object'] = $this->rh_contrachequemodel->getById($id);
        $this->dados['pagina'] = 'rh_contracheque/rh_contracheque_edit';
        $this->dados['titulo'] = 'rh_contracheque ';
        $this->load->view($this->_tpl, $this->dados);
    }

    /**
     * Delete
     * */
    private function delete($id) {
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->rh_contrachequemodel->delete($id);
            echo "<script>window.location.href = '" . site_url() . "/rh_contracheque/'</script>";
            return;
        } else {
            if ($_POST) {
                redirect('rh_contracheque');
                return;
            }
            $object = $this->rh_contrachequemodel->getById($id);
        }

        $this->dados['pagina'] = 'rh_contracheque/rh_contracheque_del';
        $this->dados['titulo'] = 'rh_contracheque ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $id;
        $this->load->view($this->_tpl, $this->dados);
    }

    public function mes($mes = '', $ano = '', $tipo = '') {
        if ($this->input->post()) {
            $this->dados['mes'] = $this->input->post('mes');
            $this->dados['ano'] = $this->input->post('ano');
            $this->dados['professores'] = $this->professoresmodel->todos();
        } else {
            $this->dados['mes'] = date('m');
            $this->dados['ano'] = date('Y');
        }

        $this->dados['dados'] = $this->rh_contrachequemodel->all($mes, $tipo);
        $this->dados['pagina'] = 'adm/rh/contracheque/mes';
        $this->dados['titulo'] = 'rh_contracheque';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function upload() {
        $folder = random_string('alpha');
        $path = "../../arquivosIntranet/uploads/" . $folder;
        if (!is_dir($path)) {
            mkdir($path, 0777, $recursive = true);
        }
        $configUpload['upload_path'] = $path;
        $configUpload['allowed_types'] = 'jpg|png|gif|pdf|zip|rar|doc|xls';
        $configUpload['encrypt_name'] = TRUE;
        $regRh = $this->input->post('reg');
        $mes = $this->input->post('mes');
        $ano = $this->input->post('ano');
        $idContracheque = $this->rh_contrachequemodel->getIdCompetencia($regRh, $mes, $ano);
        $this->upload->initialize($configUpload);
        if (!$this->upload->do_upload('arquivo')) {
            $data = array('error' => $this->upload->display_errors());
            $this->load->view('adm/rh/contracheque/uploadErro', $data);
        } else {
            $data['dadosArquivo'] = $this->upload->data();
            $arquivo = $folder . '/' . $data['dadosArquivo']['file_name'];
            $dados = array(
                'rhc_arquivo' => $arquivo
            );
            $this->rh_contrachequemodel->update($dados, $idContracheque);
            echo "<script>window.location.href = '" . site_url('adm/rh/contracheque/index') . "'</script>";
        }
    }

    public function Download($id) {
        $arquivo = $this->rh_contrachequemodel->getbyId($id);
        $arq = $arquivo->rhc_arquivo;
        $caminho = '../../arquivosIntranet/uploads/' . $arq;
        //echo $caminho;
        force_download($caminho, null);
    }

    /*
      public function download1($id) {
      $arquivo = $this->rh_contrachequemodel->getbyId($id);
      //echo $arquivo->rhc_arquivo;
      //$caminho = base_url() . '../../arquivosintranet/upload/' . $arquivo->rhc_arquivo;
      //$caminho = '/var/www/arquivosintranet/upload/' . $arquivo->rhc_arquivo;
      //$arq = str_replace('/', '\\', $arquivo->rhc_arquivo);
      $arq = $arquivo->rhc_arquivo;
      $caminho = '../../arquivosIntranet/uploads/' . $arq;
      echo $caminho;
      $this->dados['caminho'] = $caminho;
      $this->dados['pagina'] = 'adm/rh/contracheque/download';
      $this->dados['titulo'] = 'rh_contracheque';
      $this->load->view('adm/rh/contracheque/download', $this->dados);
      }
     * 
     */
}
