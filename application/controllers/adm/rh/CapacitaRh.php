<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/** CRUD Controller para alunos* */
class CapacitaRh extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();
    private $idUsuario;
    private $grupo;
    private $setor;
    private $idSetor;

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth', 'form_validation'));
        $this->auth->verificaloginAdm();
        $this->load->model('capacitamodel', '', true);
        $this->load->model('usuariosmodel', '', true);

        $this->auth->liberado('TI|RH');

        $this->idUsuario = $this->session->userdata('usu_id');
        $this->grupo = $this->session->userdata('grupo');
        $this->idSetor = $this->session->userdata('setor');
        $this->setor = $this->session->userdata('setorNome');
    }

    public function index() {
        $cursos = $this->capacitamodel->getCursos();
        $this->dados['cursos'] = $cursos;
        $this->dados['pagina'] = 'adm/rh/capacita/index';
        $this->dados['titulo'] = 'COORDENAÇÃO DE SETOR / ' . $this->setor;
        $this->load->view($this->_tpl, $this->dados);
    }

    public function delete($id) {
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->capacitamodel->delete($id);
            echo "<script>window.location.href = '" . site_url('adm/rh/capacitaRh') . "'</script>";
            return;
        } else {
            if ($_POST) {
                echo "<script>window.location.href = '" . site_url('adm/rh/capacitaRh') . "'</script>";
                return;
            }
            $object = $this->capacitamodel->getById($id);
        }

        $this->dados['pagina'] = 'adm/rh/capacita/delete';
        $this->dados['titulo'] = 'professores ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $id;
        $this->load->view($this->_tpl, $this->dados);
    }

}
