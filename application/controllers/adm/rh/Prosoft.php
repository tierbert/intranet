<?php

date_default_timezone_set('America/Bahia');

/** CRUD Controller para rh_contracheque* */
class Prosoft extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('professoresmodel', '', true);
        $this->load->model('prosoftmodel', '', true);
        $this->load->helper(array('form', 'url', 'string', 'download'));
        $this->load->library(array('Util', 'pagination', 'auth', 'upload', 'form_validation'));
        $this->auth->verificaloginAdm();
        $this->auth->liberado('TI|RH');
    }

    /** Lista rh_contracheque * */
    public function index($cadastrado = 0) {
        $this->dados['cadastrado'] = $cadastrado;
        $this->dados['competencias'] = $this->prosoftmodel->getCompetencias();
        $this->dados['pagina'] = 'adm/rh/prosoft/inicio';
        $this->dados['titulo'] = 'rh_contracheque';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function compCad($repetirComp = '') {
        $this->form_validation->set_rules('com_competencia', 'COMPETÊNCIA', 'required|is_unique[rh__competencias.com_competencia]', array('is_unique' => 'COMPETÊNCIA JA CADASTRADA'));
        if ($this->form_validation->run() == TRUE) {
            $idNovaComp = $this->prosoftmodel->compCad($this->input->post());
            if ($repetirComp != '') {
                $dadosComp = $this->prosoftmodel->getByCompetencia($repetirComp);
                $dadosNovaComp = array();
                foreach ($dadosComp->result() as $dados) {
                    $dadosNovaComp[] = array(
                        'cd_idCompetencia' => $idNovaComp,
                        'cd_idProfessor' => $dados->cd_idProfessor,
                        'cd_valorIntegral' => $dados->cd_valorIntegral,
                        'cd_valorHora' => $dados->cd_valorHora,
                        'cd_titulacao' => $dados->cd_titulacao,
                        'cd_regime' => $dados->cd_regime,
                        'cd_chProporcional' => $dados->cd_chProporcional,
                        'cd_chAulaRegIntegral' => $dados->cd_chAulaRegIntegral,
                        'cd_chAulaRegParcial' => $dados->cd_chAulaRegParcial,
                        'cd_chAulaRegHorista' => $dados->cd_chAulaRegHorista,
                        'cd_chExtraSala' => $dados->cd_chExtraSala,
                        'cd_chTotal' => $dados->cd_chTotal,
                        'cd_chAulaRegIntegralTotal' => $dados->cd_chAulaRegIntegralTotal,
                        'cd__chAulaRegParcialTotal' => $dados->cd__chAulaRegParcialTotal,
                        'cd__chAulaHorista' => $dados->cd__chAulaHorista,
                        'cd__dsrAulaHorAula' => $dados->cd__dsrAulaHorAula,
                        'cd__atividadeExtraClasse' => $dados->cd__atividadeExtraClasse,
                        'cd_justificativa' => $dados->cd_justificativa,
                    );
                }
                $this->db->insert_batch('rh__competenciaDados', $dadosNovaComp);
            }
            echo "<script>window.location.href='" . site_url('adm/rh/prosoft/index/1') . "'</script>";
        } else {
            if ($repetirComp != '') {
                $this->dados['repetirComp'] = $this->prosoftmodel->getCompbyId($repetirComp);
            }
            $this->dados['pagina'] = 'adm/rh/prosoft/compCad';
            $this->dados['titulo'] = 'alunos ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function baseCalc() {
        $this->dados['baseCalculo'] = $this->prosoftmodel->baseCalculo();
        $this->dados['pagina'] = 'adm/rh/prosoft/baseCalc';
        $this->dados['titulo'] = 'rh_contracheque';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function baseCalcEdit($idBase) {
        if ($this->input->post()) {
            $this->prosoftmodel->compCad($this->input->post());
            echo "<script>window.location.href='" . site_url('adm/rh/prosoft') . "'</script>";
        } else {
            $this->dados['baseCalculo'] = $this->prosoftmodel->getBaseCalculo();
            $this->dados['pagina'] = 'adm/rh/prosoft/baseCalcEdit';
            $this->dados['titulo'] = 'alunos ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function calcular($idProf = '') {
        if ($idProf != '') {
            $this->dados['professor'] = $this->professoresmodel->getById($idProf);
        }

        $this->dados['baseCalculo'] = $this->prosoftmodel->getBaseCalculo();
        $this->dados['pagina'] = 'adm/rh/prosoft/calcular';
        $this->dados['titulo'] = 'rh_contracheque';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function calcularProf($idCompetencia, $idCompDados, $idProf) {
        if ($this->input->post()) {
            $post = $this->input->post();

            $valores = $this->calculo($post);

            $dados = array(
                'cd_titulacao' => $post['cd_titulacao'],
                'cd_regime' => $post['cd_regime'],
                'cd_chProporcional' => isset($post['cd_chProporcional']) ? $post['cd_chProporcional'] : '',
                'cd_chAulaRegIntegral' => isset($post['cd_chAulaRegIntegral']) ? $post['cd_chAulaRegIntegral'] : '',
                'cd_chAulaRegParcial' => isset($post['cd_chAulaRegParcial']) ? $post['cd_chAulaRegParcial'] : '',
                'cd_chAulaRegHorista' => isset($post['cd_chAulaRegHorista']) ? $post['cd_chAulaRegHorista'] : '',
                'cd_chExtraSala' => $valores['cd_chExtraSala'],
                'cd_chTotal' => $valores['cd_chTotal'],
                'cd_chAulaRegIntegralTotal' => $valores['cd_chAulaRegIntegralTotal'],
                'cd__chAulaRegParcialTotal' => $valores['cd__chAulaRegParcialTotal'],
                'cd__chAulaHorista' => $valores['cd__chAulaHorista'],
                'cd__dsrAulaHorAula' => $valores['cd__dsrAulaHorAula'],
                'cd__atividadeExtraClasse' => $valores['cd__atividadeExtraClasse'],
            );

            /* CHES CHT CHARI CHARP CHAH DSR AEC */

            /*
              echo '<pre>';
              print_r($dados);
              echo '</pre>';
             */

            $this->prosoftmodel->updateCompDados($dados, $idCompDados);
            echo "<script>window.location.href='" . site_url('adm/rh/prosoft/competenciaEdit/' . $idCompetencia) . "'</script>";
        } else {
            if ($idProf != '') {
                $this->dados['professor'] = $this->professoresmodel->getById($idProf);
            }

            $this->dados['compDados'] = $this->prosoftmodel->getCdById($idCompDados);
            $this->dados['baseCalculo'] = $this->prosoftmodel->getBaseCalculo();
            $this->dados['idCompetencia'] = $idCompetencia;
            $this->dados['pagina'] = 'adm/rh/prosoft/calcular';
            $this->dados['titulo'] = 'rh_contracheque';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    private function calculo($post) {
        $regime = $post['cd_regime'];
        $titulacao = $post['cd_titulacao'];

        $chProporcional = isset($post['cd_chProporcional']) ? $post['cd_chProporcional'] : (isset($post['totalHoras']) ? $post['totalHoras'] : '');
        $chAulaRegIntegral = isset($post['cd_chAulaRegIntegral']) ? $post['cd_chAulaRegIntegral'] : (isset($post['totalHoras']) ? $post['totalHoras'] : '');
        $chAulaRegParcial = isset($post['cd_chAulaRegParcial']) ? $post['cd_chAulaRegParcial'] : (isset($post['totalHoras']) ? $post['totalHoras'] : '');
        $chAulaRegHorista = isset($post['cd_chAulaRegHorista']) ? $post['cd_chAulaRegHorista'] : (isset($post['totalHoras']) ? $post['totalHoras'] : '');

        $baseCalculo = $this->prosoftmodel->getBaseCalculo();

        $graduado_integral = $baseCalculo['graduado_integral'];
        $graduado_hora = $baseCalculo['graduado_hora'];
        $especialista_integral = $baseCalculo['especialista_integral'];
        $especialista_hora = $baseCalculo['especialista_hora'];
        $mestre_integral = $baseCalculo['mestre_integral'];
        $mestre_hora = $baseCalculo['mestre_hora'];
        $doutor_integral = $baseCalculo['doutor_integral'];
        $doutor_hora = $baseCalculo['doutor_hora'];

        $mestradoValor = $baseCalculo['mestrado'];
        $sup_estagioValor = $baseCalculo['sup_estagio'];

        $valorIntegral = $titulacao == 'GRADUADO' ? $graduado_integral : ($titulacao == 'ESPECIALISTA' ? $especialista_integral : ($titulacao == 'MESTRE' ? $mestre_integral : ($titulacao == 'DOUTOR' ? $doutor_integral : ($titulacao == 'MESTRADO' ? $mestradoValor : ''))));
        $valorHora = $titulacao == 'GRADUADO' ? $graduado_hora : ($titulacao == 'ESPECIALISTA' ? $especialista_hora : ($titulacao == 'MESTRE' ? $mestre_hora : ($titulacao == 'DOUTOR' ? $doutor_hora : ($titulacao == 'SUP_ESTAGIO' ? $sup_estagioValor : ''))));

        /* echo $regime . ' - ' . $titulacao . ' - ' . $valorIntegral . ' - ' . $valorHora . ' -- '; */

        $CHES = '';
        $CHT = '';
        $CHARI = '';
        $CHARP = '';
        $CHAH = '';
        $DSR = '';
        $AEC = '';

        if ($regime == 'INTEGRAL') {
            $CHES = '';
            $CHT = 44;
            $CHARI = $valorIntegral;

            if ($chAulaRegIntegral == '00') {
                $DSR = round($valorIntegral / 6, 2);
                $CHARI = round($valorIntegral - $DSR, 2);
            } else {
                $CHAH = round($valorHora * $chAulaRegIntegral * 4.5, 2);
                $DSR = round($CHAH / 6, 2);

                $AEC = $valorIntegral - $CHAH - $DSR;
            }
        } elseif ($regime == 'PARCIAL_REGULAR') {

            $CHT = round($chAulaRegParcial / 0.75);
            $CHES = $CHT - $chAulaRegParcial;
            $CHARP = round($valorHora * $chAulaRegParcial * 4.5, 2);
            $DSR = round($CHARP / 6, 2);
            $AEC = round((($CHARP + $DSR) * 11) / 100, 2);
        } elseif ($regime == 'PARCIAL_PROPORCIONAL') {

            $proporcao = round(($chProporcional * 100) / 44, 2);
            $CHARP = round(($valorIntegral * $proporcao) / 100, 2);

            $CHAH = round($valorHora * $chAulaRegParcial * 4.5, 2);
            $DSR = round($CHAH / 6, 2);

            $AEC = $CHARP - $CHAH - $DSR;
        } elseif ($regime == 'HORISTA') {
            if ($titulacao == 'SUP_ESTAGIO') {
                $CHAH = round($valorHora * $chAulaRegHorista * 4, 2);
                $DSR = round($CHAH / 6, 2);
                $CHAH = round($CHAH - $DSR, 2);
            } else {
                $CHAH = round($valorHora * $chAulaRegHorista * 4.5, 2);
                $DSR = round($CHAH / 6, 2);
            }
        }

        $dados = array(
            'cd_chExtraSala' => $CHES,
            'cd_chTotal' => $CHT,
            'cd_chAulaRegIntegralTotal' => $CHARI,
            'cd__chAulaRegParcialTotal' => $CHARP,
            'cd__chAulaHorista' => $CHAH,
            'cd__dsrAulaHorAula' => $DSR,
            'cd__atividadeExtraClasse' => $AEC,
        );
        return $dados;
    }

    public function professores() {
        $this->dados['professores'] = $this->professoresmodel->ativos();
        $this->dados['pagina'] = 'adm/rh/prosoft/professores';
        $this->dados['titulo'] = 'rh_contracheque';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function profEditar($idProf) {
        if ($_POST) {
            $this->professoresmodel->update($_POST, $idProf);
            echo "<script>alert('CADASTRO ATUALIZADO'); window.location.href = '" . site_url('adm/rh/prosoft/professores') . "'</script>";
            return;
        }

        $this->dados['aulas'] = $this->prosoftmodel->getAulasArray();
        $this->dados['divisoes'] = $this->prosoftmodel->getDivisaoArray();
        $this->dados['object'] = $this->professoresmodel->getById($idProf);
        $this->dados['pagina'] = 'adm/rh/prosoft/profEditar';
        $this->dados['titulo'] = 'professores ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function competenciaEdit($idCompetencia) {
        $this->dados['idCompetencia'] = $idCompetencia;
        $this->dados['baseCalculo'] = $this->prosoftmodel->getBaseCalculo();
        $this->dados['totais'] = $this->prosoftmodel->getByCompRegime($idCompetencia);
        $this->dados['dadosCompetencia'] = $this->prosoftmodel->getByCompetencia($idCompetencia);
        $this->dados['pagina'] = 'adm/rh/prosoft/competenciaEdit';
        $this->dados['titulo'] = 'professores ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function imprimir($idCompetencia) {
        $this->dados['idCompetencia'] = $idCompetencia;
        $this->dados['baseCalculo'] = $this->prosoftmodel->getBaseCalculo();
        $this->dados['totais'] = $this->prosoftmodel->getByCompRegime($idCompetencia);
        $this->dados['dadosCompetencia'] = $this->prosoftmodel->getByCompetencia($idCompetencia);
        $this->dados['pagina'] = 'adm/rh/prosoft/imprimir';
        $this->dados['titulo'] = 'professores ';
        $this->load->view('adm/rh/prosoft/imprimir', $this->dados);
    }

    public function importar($idCompetencia) {
        if ($this->input->post()) {

            /* LEITURA DO ARQUIVO XML EXPORTADO PELO TIMETABLES */
            $nome = $_FILES['xml']['tmp_name'];
            $fp = fopen($nome, 'r');
            $arquivo = (fread($fp, filesize($nome)));
            $xml = new SimpleXMLElement($arquivo);
            /* */

            /* IMPORTA PROFESSORES */
            $prof = array();
            foreach ($xml->teachers->teacher as $node) {
                $dadosTeachers = $node->attributes();
                $prof["$dadosTeachers->id"] = array(
                    'idProf' => "$dadosTeachers->id",
                    'nome' => "$dadosTeachers->name",
                    'lessons' => '',
                    'total' => 0,
                );
            }
            /* */

            $cards = array();
            foreach ($xml->cards->card as $node) {
                $dados = $node->attributes();
                $idCard = "$dados->teacherids";

                /* echo strpos($idCard, ',').'<br>'; */
                if (strpos($idCard, ',') > 0) {
                    $ids = explode(',', $idCard);
                    foreach ($ids as $id) {
                        if (key_exists($id, $cards)) {
                            $cards[$id] += 1;
                        } else {
                            $cards[$id] = 1;
                        }
                    }
                } else {
                    if (key_exists($idCard, $cards)) {
                        $cards[$idCard] += 1;
                    } else {
                        $cards[$idCard] = 1;
                    }
                }
            }

            /* INSERE O TOTAL DE HORAS NO ARRAY PROFESSORES */
            foreach ($prof as $pro) {
                $idProf = $pro['idProf'];
                if (isset($cards[$pro['idProf']])) {
                    /* echo $cards[$idProf]; */
                    $prof[$idProf]['total'] += $cards[$idProf];
                }
            }
            /* */

            $inseridos = 0;
            $atualizados = 0;
            $erros = array();

            foreach ($prof as $p) {
                if ($p['idProf'][0] == '*') {
                    /* echo $p['idProf'] . '|' . $p['nome'] . '|' . $p['total'] . '<br>'; */
                    $erros[] = '<td>Erro 001</td><td>Código de RH informado no TIMETABLES incorreto</td><td>' . $p['idProf'] . ' </td><td> ' . $p['nome'] . '</td>';
                    continue;
                } else {
                    /* $codProfTimeTables = 9999999999999999; */
                    $codProfTimeTables = $p['idProf'];
                    $dadosProf = $this->professoresmodel->getByCod($codProfTimeTables);

                    if ($dadosProf == NULL) {
                        /* var_dump($dadosProf); */
                        $erros[] = '<td>Erro 002 </td><td>Professor não encontrado na base de dados da intranet.</td><td> ' . $p['idProf'] . ' </td><td> ' . $p['nome'] . '</td>';
                    } else {
                        $codProf = $dadosProf->pro_registroRh;
                        $titulacao = $dadosProf->pro_titulacao;
                        $regime = $dadosProf->pro_regime;
                        $divisao = $dadosProf->pro_idDivisao;
                        $totalHoras = $p['total'];

                        if ($titulacao == '' || $regime == '' || $totalHoras == 0) {
                            $divisao == '-';
                            $messagen = '';
                            if ($totalHoras == 0) {
                                $erros[] = '<td>Erro:006 </td><td> Quantidade de horas informada no Timetables iqual a zero </td><td>' . $p['idProf'] . ' </td><td> ' . $p['nome'] . '</td>';
                            } else {
                                $titulacao == '' ? $messagen = '003 </td><td> Titulação' : ($regime == '' ? $messagen = '004 </td><td> Regime' : ($divisao == '' ? $messagen = '005 </td><td> Divisão' : '' ));
                                $erros[] = '<td>Erro:' . $messagen . '  do professor não foi informado(a) na base de dados da intranet. </td><td> ' . $p['idProf'] . ' </td><td> ' . $p['nome'] . '</td>';
                            }
                        } else {
                            /* echo $codProfTimeTables . '|' . $codProf . ' - ' . $dadosProf->pro_nome . ' - ' . $titulacao . ' - ' . $regime . ' - ' . $divisao . ' - ' . $totalHoras; */
                            $post = array(
                                'cd_regime' => $regime,
                                'cd_titulacao' => $titulacao,
                                'totalHoras' => $totalHoras,
                            );

                            $valores = $this->calculo($post);

                            $dados = array(
                                'cd_idCompetencia' => $idCompetencia,
                                'cd_idProfessor' => $codProf,
                                'cd_titulacao' => $post['cd_titulacao'],
                                'cd_regime' => $post['cd_regime'],
                                'cd_chProporcional' => $regime == '' ? '' : '',
                                'cd_chAulaRegIntegral' => $regime == '' ? '' : '',
                                'cd_chAulaRegParcial' => $regime == 'PARCIAL_PROPORCIONAL' ? $totalHoras : ($regime == 'PARCIAL_REGULAR' ? $totalHoras : ''),
                                'cd_chAulaRegHorista' => $regime == 'HORISTA' ? $totalHoras : '',
                                'cd_chExtraSala' => $valores['cd_chExtraSala'],
                                'cd_chTotal' => $valores['cd_chTotal'],
                                'cd_chAulaRegIntegralTotal' => $valores['cd_chAulaRegIntegralTotal'],
                                'cd__chAulaRegParcialTotal' => $valores['cd__chAulaRegParcialTotal'],
                                'cd__chAulaHorista' => $valores['cd__chAulaHorista'],
                                'cd__dsrAulaHorAula' => $valores['cd__dsrAulaHorAula'],
                                'cd__atividadeExtraClasse' => $valores['cd__atividadeExtraClasse'],
                            );

                            /* $this->prosoftmodel->updateCompDados($dados, $idCompDados); */

                            $registro = $this->prosoftmodel->getProfByComp($idCompetencia, $codProf);

                            if ($registro->num_rows() == 0) {
                                $this->prosoftmodel->insertProfByComp($idCompetencia, $dados);
                                $inseridos++;
                            } else {
                                $this->prosoftmodel->updateCompDados($dados, $registro->row('cd_id'));
                                $atualizados++;
                            }
                        }
                    }
                }
            }


            echo '<hr>';
            echo 'Total Inseridos: ' . $inseridos . '<br>';
            echo 'Total Atualizados: ' . $atualizados . '<br>';
            echo 'Total de Erros: ' . count($erros);
            echo '<hr>';
            echo 'Erros:<br>';
            echo "<table style='font-size: 15px;' border='1'>";
            foreach ($erros as $erro) {
                echo '<tr>' . $erro . '</tr>';
            }
            echo '</table>';
            echo '<hr>';
            echo "<a href='" . site_url('adm/rh/prosoft/competenciaEdit/' . $idCompetencia) . "'>VOLTAR</a>";

            /*
              echo '<pre>';
              print_r($prof);
              echo '</pre>';
              echo '<hr>';
             * 
             */
        } else {
            $this->dados['pagina'] = 'adm/rh/prosoft/importar';
            $this->dados['titulo'] = 'professores ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function removeProfComp($idCompetencia, $idCd) {
        $this->auth->liberado('TI');
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->prosoftmodel->deleteCd($idCd);
            echo "<script>window.location.href = '" . site_url('adm/rh/prosoft/competenciaEdit/' . $idCompetencia) . "'</script>";
            return;
        } else {
            if ($_POST) {
                echo "<script>window.location.href = '" . site_url('adm/rh/prosoft/competenciaEdit/' . $idCompetencia) . "'</script>";
                return;
            }
            $object = $this->prosoftmodel->getCdById($idCd);
        }

        $this->dados['pagina'] = 'adm/rh/prosoft/removeProfComp';
        $this->dados['titulo'] = 'horario ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $idCd;
        $this->load->view($this->_tpl, $this->dados);
    }

    public function compInsere($idCd) {
        if ($this->input->post()) {
            $post = $this->input->post();

            $valores = $this->calculo($post);

            $dados = array(
                'cd_idCompetencia' => $idCd,
                'cd_idProfessor' => $post['cd_idProfessor'],
                'cd_titulacao' => $post['cd_titulacao'],
                'cd_regime' => $post['cd_regime'],
                'cd_justificativa' => isset($post['cd_justificativa']) ? $post['cd_justificativa'] : '',
                'cd_chProporcional' => isset($post['cd_chProporcional']) ? $post['cd_chProporcional'] : '',
                'cd_chAulaRegIntegral' => isset($post['cd_chAulaRegIntegral']) ? $post['cd_chAulaRegIntegral'] : '',
                'cd_chAulaRegParcial' => isset($post['cd_chAulaRegParcial']) ? $post['cd_chAulaRegParcial'] : '',
                'cd_chAulaRegHorista' => isset($post['cd_chAulaRegHorista']) ? $post['cd_chAulaRegHorista'] : '',
                'cd_chExtraSala' => $valores['cd_chExtraSala'],
                'cd_chTotal' => $valores['cd_chTotal'],
                'cd_chAulaRegIntegralTotal' => $valores['cd_chAulaRegIntegralTotal'],
                'cd__chAulaRegParcialTotal' => $valores['cd__chAulaRegParcialTotal'],
                'cd__chAulaHorista' => $valores['cd__chAulaHorista'],
                'cd__dsrAulaHorAula' => $valores['cd__dsrAulaHorAula'],
                'cd__atividadeExtraClasse' => $valores['cd__atividadeExtraClasse'],
            );

            /* CHES CHT CHARI CHARP CHAH DSR AEC */

            /*
              echo '<hr>';
              echo '<pre>';
              print_r($dados);
              echo '</pre>';
             * 
             */

            $this->prosoftmodel->insertProfByComp($idCd, $dados);
            echo "<script>alert('PROFESSOR INSERIDO NA COMPETÊNCIA'); window.location.href = '" . site_url('adm/rh/prosoft/competenciaEdit/' . $idCd) . "'</script>";
        } else {
            $this->dados['baseCalculo'] = $this->prosoftmodel->getBaseCalculo();
            $this->dados['professores'] = $this->professoresmodel->getArrayRh('ATIVOS');
            $this->dados['funcao'] = 'inserir';
            $this->dados['idCd'] = $idCd;
            $this->dados['pagina'] = 'adm/rh/prosoft/calcular';
            $this->dados['titulo'] = 'rh_contracheque';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function exportar($idCompetencia) {
        $baseCalculo = $this->prosoftmodel->getBaseCalculo();
        $dadosCompetencia = $this->prosoftmodel->getByCompetencia($idCompetencia);

        $baseCalculo = $this->prosoftmodel->getBaseCalculo();
        $linhas = array();

        $codEmpresa = '0112';
        /* $codEmpresa = '5000'; */
        foreach ($dadosCompetencia->result() as $dado) {
            if ($dado->cd_titulacao == 'GRADUADO') {
                $valorIntegral = $baseCalculo['graduado_integral'];
            } elseif ($dado->cd_titulacao == 'ESPECIALISTA') {
                $valorIntegral = $baseCalculo['especialista_integral'];
            } elseif ($dado->cd_titulacao == 'MESTRE') {
                $valorIntegral = $baseCalculo['mestre_integral'];
            } elseif ($dado->cd_titulacao == 'DOUTOR') {
                $valorIntegral = $baseCalculo['doutor_integral'];
            } elseif ($dado->cd_titulacao == 'MESTRADO') {
                $valorIntegral = $baseCalculo['mestrado'];
            } elseif ($dado->cd_titulacao == 'SUP_ESTAGIO') {
                $valorIntegral = 0.00;
            }

            $dsrIntegral = round($valorIntegral / 6, 2);
            $valorIntegralSemDsr = $valorIntegral - $dsrIntegral;
            $dsrIntegral = round($valorIntegral / 6, 2);
            $regime = $dado->cd_regime;
            $divisao = '';

            $chAulaRegParcialTotal = $dado->cd_chAulaRegIntegral;

            $CHES = '';
            $CHT = '';
            $CHARI = $dado->cd_chAulaRegIntegral;
            $CHARP = '';
            $CHRegIntegralTotal = $dado->cd_chAulaRegIntegralTotal;
            $CHAH = $dado->cd__chAulaHorista;
            $DSR = $dado->cd__dsrAulaHorAula;
            $AEC = $dado->cd__atividadeExtraClasse;

            $valor = '';
            $evento = '';
            if ($regime == 'INTEGRAL') {
                /*
                  032 - REG. INTEGRAL
                  066 - ATIV. EXTR. CLASSE
                  090 - DSR S/ REGIME
                 */

                if ($CHARI == '00' || $CHARI == '') {
                    $valor = $dado->cd_chAulaRegIntegral;
                    $linhas[] = $codEmpresa
                            . str_pad($dado->pro_registroRh, 5, "0", STR_PAD_LEFT)
                            . '032'
                            . $this->formataValor($CHRegIntegralTotal)
                            . '000'
                            . $divisao;

                    $linhas[] = $codEmpresa
                            . str_pad($dado->pro_registroRh, 5, "0", STR_PAD_LEFT)
                            . '090'
                            . $this->formataValor($DSR)
                            . '000'
                            . $divisao;
                } else {
                    $valor = $dado->cd_chAulaRegIntegral;
                    $linhas[] = $codEmpresa
                            . str_pad($dado->pro_registroRh, 5, "0", STR_PAD_LEFT)
                            . '032'
                            . $this->formataValor($CHAH)
                            . '000'
                            . $divisao;

                    $linhas[] = $codEmpresa
                            . str_pad($dado->pro_registroRh, 5, "0", STR_PAD_LEFT)
                            . '066'
                            . $this->formataValor($AEC)
                            . '000'
                            . $divisao;

                    $linhas[] = $codEmpresa
                            . str_pad($dado->pro_registroRh, 5, "0", STR_PAD_LEFT)
                            . '090'
                            . $this->formataValor($DSR)
                            . '000'
                            . $divisao;
                }
                /* FIM REGIME INTEGRAL */
            } elseif ($regime == 'PARCIAL_PROPORCIONAL') {
                /*
                  061 - REG. PARC. PROPORCIONAL
                  066 - ATIV. EXTR. CLASSE
                  090 - DSR S/ REGIME
                 */
                $valor = $dado->cd__chAulaRegParcialTotal;
                $dsr = $dado->cd__dsrAulaHorAula;
                $linhas[] = $codEmpresa
                        . str_pad($dado->pro_registroRh, 5, "0", STR_PAD_LEFT)
                        . '061'
                        . $this->formataValor($CHAH)
                        . '000'
                        . $divisao;

                $linhas[] = $codEmpresa
                        . str_pad($dado->pro_registroRh, 5, "0", STR_PAD_LEFT)
                        . '066'
                        . $this->formataValor($AEC)
                        . '000'
                        . $divisao;

                $linhas[] = $codEmpresa
                        . str_pad($dado->pro_registroRh, 5, "0", STR_PAD_LEFT)
                        . '090'
                        . $this->formataValor($DSR)
                        . '000'
                        . $divisao;

                /* FIM REGIME P. PROPORCIONAL */
            } elseif ($regime == 'PARCIAL_REGULAR') {
                $valorRegParcialTotal = $dado->cd__chAulaRegParcialTotal;
                $valorRegParcialDsr = round($valorRegParcialTotal / 6, 2);
                $valorRegParcialAEC = round((($dado->cd__chAulaRegParcialTotal + $dado->cd__dsrAulaHorAula) * 0.11), 2);

                $linhas[] = $codEmpresa
                        . str_pad($dado->pro_registroRh, 5, "0", STR_PAD_LEFT)
                        . '031'
                        . $this->formataValor($valorRegParcialTotal)
                        . '000'
                        . $divisao;

                $linhas[] = $codEmpresa
                        . str_pad($dado->pro_registroRh, 5, "0", STR_PAD_LEFT)
                        . '090'
                        . $this->formataValor($valorRegParcialDsr)
                        . '000'
                        . $divisao;

                $valorRegParcialAEC = number_format($valorRegParcialAEC, 2, '.', '');

                $linhas[] = $codEmpresa
                        . str_pad($dado->pro_registroRh, 5, "0", STR_PAD_LEFT)
                        . '066'
                        . $this->formataValor($valorRegParcialAEC)
                        . '000'
                        . $divisao;
            } elseif ($regime == 'HORISTA') {
                if ($dado->cd_titulacao == 'SUP_ESTAGIO') {
                    $eventoHor = '072';
                } else {
                    $eventoHor = '065';
                }
                $valor = $dado->cd__chAulaHorista;
                $dsrhorista = round($valor / 6, 2);
                $linhas[] = $codEmpresa
                        . str_pad($dado->pro_registroRh, 5, "0", STR_PAD_LEFT)
                        . $eventoHor
                        . $this->formataValor($valor)
                        . '000'
                        . $divisao;

                $linhas[] = $codEmpresa
                        . str_pad($dado->pro_registroRh, 5, "0", STR_PAD_LEFT)
                        . '004'
                        . $this->formataValor($dsrhorista)
                        . '000'
                        . $divisao;
            }
        }

        $nomeComp = str_replace('/', '', $dadosCompetencia->row('com_competencia'));

        $file_name = $nomeComp . '.txt';
        $pasta = FCPATH . 'assets/intranet/';
        $file = fopen($pasta . $file_name, "w");
        foreach ($linhas as $linha) {
            fwrite($file, $linha . "\r\n");
        }

        fclose($file);

        echo 'ARQUIVO GERADO - COMPETÊNCIA: ' . $dadosCompetencia->row('com_competencia') . '<br><br>';
        echo "<a target='_blanck' href='" . base_url('assets/intranet/' . $file_name) . "'>BAIXAR ARQUIVO</a>";
        echo '<br><br>';
        echo "<a target='_blanck' href='" . site_url('adm/rh/prosoft/competenciaEdit/' . $idCompetencia) . "'>VOLTAR</a>";
        echo '<br><br>';

        foreach ($linhas as $linha) {
            echo $this->mask($linha, '####-#####-###-####.###.###,##-###') . '<br>';
        }

        /*
          header("Content-Type: application/save");
          header("Content-Length:" . filesize($file_name));
          header('Content-Disposition: attachment; filename = "' . $file_name . '"');
          header("Content-Transfer-Encoding: binary");
          header('Expires: 0');
          header('Pragma: no-cache ');

          $fp = fopen($file_name, "r");
          fpassthru($fp);
         * 
         */
//fclose($fp);
    }

    public function formataValor($valor) {
        $valor = number_format($valor, 2, '', '');
        $valor2 = str_pad($valor, 12, "0", STR_PAD_LEFT);
        $valor3 = str_replace('.', '', $valor2);
        return $valor3;
    }

    public function arendodar($valor) {
        return round($valor, 2);
    }

    function mask($val, $mask) {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k]))
                    $maskared .= $val[$k++];
            }
            else {
                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

}
