<?php

/** CRUD Controller para rh__ajustePonto* */
class AjustePonto extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();
    private $idUsuario;

    public function __construct() {
        parent::__construct();
        $this->load->model('rh__ajustepontomodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
        $this->idUsuario = $this->session->userdata('usu_id');
        $this->setorNome = $this->session->userdata('setorNome');
    }

    /** Lista rh__ajustePonto * */
    public function index($inicio = 0) {
        $this->dados['dados'] = $this->rh__ajustepontomodel->getByFunc($this->idUsuario);
        $this->dados['pagina'] = 'adm/rh/ajusteponto/ajusteListFunc';
        $this->dados['titulo'] = 'rh__ajustePonto';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Lista rh__ajustePonto ajuste parecer coordenador* */
    public function ajusteParecCoord($id) {
        $this->dados['object'] = $this->rh__ajustepontomodel->getById($id);
        /* $this->dados['dados'] = $this->rh__ajustepontomodel->getByFunc($this->idUsuario); */
        $this->dados['pagina'] = 'adm/rh/ajusteponto/ajusteParecCoord';
        $this->dados['titulo'] = 'rh__ajustePonto';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Lista rh__ajustePonto ajuste parecer coordenador* */
    public function ajusteParecRh($id) {
        $this->dados['object'] = $this->rh__ajustepontomodel->getById($id);
        /* $this->dados['dados'] = $this->rh__ajustepontomodel->getByFunc($this->idUsuario); */
        $this->dados['pagina'] = 'adm/rh/ajusteponto/ajusteParecRh';
        $this->dados['titulo'] = 'rh__ajustePonto';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Lista rh__ajustePonto ajuste finaliza* */
    public function ajusteConc($id) {
        $this->dados['object'] = $this->rh__ajustepontomodel->getById($id);
        /* $this->dados['dados'] = $this->rh__ajustepontomodel->getByFunc($this->idUsuario); */
        $this->dados['pagina'] = 'adm/rh/ajusteponto/ajusteConc';
        $this->dados['titulo'] = 'rh__ajustePonto';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Cria novo(a)  * */
    public function criar() {
        if ($_POST) {
            $this->rh__ajustepontomodel->inserir($_POST);
            echo "<script>window.location.href='" . site_url('adm/rh/ajustePonto') . "'</script>";
        } else {
            $this->dados['object'] = $this->rh__ajustepontomodel;
            $this->dados['pagina'] = 'adm/rh/ajusteponto/ajusteNovoFunc';
            $this->dados['titulo'] = 'rh__ajustePonto ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function editar($id) {
        $msg = null;
        if ($_POST) {
            $this->rh__ajustepontomodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/rh__ajusteponto/'</script>";
            return;
        }

        $this->dados['object'] = $this->rh__ajustepontomodel->getById($id);
        $this->dados['pagina'] = 'rh__ajusteponto/rh__ajusteponto_edit';
        $this->dados['titulo'] = 'rh__ajustePonto ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function ajusCoordList() {
        if (in_array($this->setorNome, $this->util->coordenacoes())) {
            $this->dados['dados'] = $this->rh__ajustepontomodel->getByCoord($this->setorNome);
            $this->dados['pagina'] = 'adm/rh/ajusteponto/ajusCoordList';
            $this->dados['titulo'] = 'rh__ajustePonto';
            $this->load->view($this->_tpl, $this->dados);
        } else {
            echo "<script>alert('SEM PREMISSAO DE ACESSO');window.location.href='" . site_url('adm/rh/iniciorh') . "'</script>";
        }
    }

    public function ajusCoordEdit($id) {
        if (in_array($this->setorNome, $this->util->coordenacoes())) {
            if ($this->input->post()) {
                $this->rh__ajustepontomodel->update($this->input->post(), $id);
                echo "<script>window.location.href = '" . site_url('adm/rh/ajustePonto/ajusCoordList') . "'</script>";
                return;
            }
            $this->dados['object'] = $this->rh__ajustepontomodel->getById($id);
            $this->dados['pagina'] = 'adm/rh/ajusteponto/ajusCoordEdit';
            $this->dados['titulo'] = 'rh__ajustePonto ';
            $this->load->view($this->_tpl, $this->dados);
        } else {
            echo "<script>alert('SEM PREMISSAO DE ACESSO');window.location.href='" . site_url('adm/rh/iniciorh') . "'</script>";
        }
    }

    public function ajusRhList() {
        if (in_array($this->setorNome, array('RH_COORD'))) {
            $this->dados['dados'] = $this->rh__ajustepontomodel->getByRh();
            $this->dados['pagina'] = 'adm/rh/ajusteponto/ajusRhList';
            $this->dados['titulo'] = 'rh__ajustePonto';
            $this->load->view($this->_tpl, $this->dados);
        } else {
            echo "<script>alert('SEM PREMISSAO DE ACESSO');window.location.href='" . site_url('adm/rh/iniciorh') . "'</script>";
        }
    }

    public function ajusRhEdit($id) {
        if (in_array($this->setorNome, array('RH_COORD'))) {
            if ($this->input->post()) {
                $this->rh__ajustepontomodel->update($this->input->post(), $id);
                echo "<script>window.location.href = '" . site_url('adm/rh/ajustePonto/ajusRhList') . "'</script>";
                return;
            } else {
                $this->dados['object'] = $this->rh__ajustepontomodel->getById($id);
                $this->dados['pagina'] = 'adm/rh/ajusteponto/ajusRhEdit';
                $this->dados['titulo'] = 'rh__ajustePonto ';
                $this->load->view($this->_tpl, $this->dados);
            }
        } else {
            echo "<script>alert('SEM PREMISSAO DE ACESSO');window.location.href='" . site_url('adm/rh/iniciorh') . "'</script>";
        }
    }

}
