<?php

/** CRUD Controller para rh__adiantamento* */
class Selecao extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();
    private $idUsuario;
    private $setorNome;

    public function __construct() {
        parent::__construct();
        $this->load->model('Rh_selecaomodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
        $this->idUsuario = $this->session->userdata('usu_id');
        $this->setorNome = $this->session->userdata('setorNome');
        $this->auth->liberado('TI|RH');
    }

    public function index() {
        $this->dados['dados'] = $this->Rh_selecaomodel->getDocentes();
        $this->dados['pagina'] = 'adm/rh/selecao/indexSelecao';
        $this->dados['titulo'] = 'rh__adiantamento';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function detalharDocente($id) {
        $this->dados['dados'] = $this->Rh_selecaomodel->getById($id);
        $this->dados['pagina'] = 'adm/rh/selecao/detalharDocente';
        $this->dados['titulo'] = 'rh__adiantamento';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function imprimir($id) {
        $this->dados['dados'] = $this->Rh_selecaomodel->getById($id);
        $this->dados['pagina'] = 'adm/rh/selecao/imprimir';
        $this->dados['titulo'] = 'rh__adiantamento';
        $html = $this->load->view('adm/rh/selecao/imprimir', $this->dados, true);

        $mpdf = new mPDF();
        $html = $this->load->view('adm/rh/selecao/imprimir', $this->dados, TRUE);
        //echo $html;
        $mpdf->SetFooter('{PAGENO}');
        $mpdf->writeHTML($html);
        $mpdf->Output();
    }

    /** Lista rh__adiantamento Funcionario Parecer RH 
      public function adiantamentoFuncParec() {
      $this->dados['dados'] = $this->rh__adiantamentomodel->getByFunc($this->idUsuario);
      $this->dados['pagina'] = 'adm/rh/adiantamento/adiantamentoFuncParec';
      $this->dados['titulo'] = 'rh__adiantamento';
      $this->load->view($this->_tpl, $this->dados);
      } */

    /** Lista rh__adiantamento Funcionario Parecer RH  que leva o id via post* */
    public function adiantamentoFuncParec($idAdiantamento) {
        $this->dados['object'] = $this->rh__adiantamentomodel->getById($idAdiantamento);
        /* $this->dados['dados'] = $this->rh__adiantamentomodel->getByFunc($this->idUsuario); */
        $this->dados['pagina'] = 'adm/rh/adiantamento/adiantamentoFuncParec';
        $this->dados['titulo'] = 'rh__adiantamento';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Lista rh__adiantamento Funcionario Parecer Finan * */
    public function adiantamentoFuncParecFinan($idAdiantamento) {
        $this->dados['object'] = $this->rh__adiantamentomodel->getById($idAdiantamento);
        /* $this->dados['dados'] = $this->rh__adiantamentomodel->getByFunc($this->idUsuario); */
        $this->dados['pagina'] = 'adm/rh/adiantamento/adiantamentoFuncParecFinan';
        $this->dados['titulo'] = 'rh__adiantamento';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Lista rh__adiantamento Funcionario Parecer Concluido * */
    public function adiantamentoFuncConc($idAdiantamento) {
        $this->dados['object'] = $this->rh__adiantamentomodel->getById($idAdiantamento);
        //$this->dados['dados'] = $this->rh__adiantamentomodel->getByFunc($this->idUsuario);
        $this->dados['pagina'] = 'adm/rh/adiantamento/adiantamentoFuncConc';
        $this->dados['titulo'] = 'rh__adiantamento';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Cria novo(a)  * */
    public function adiantamentoFuncNovo() {
        if ($_POST) {
            $this->rh__adiantamentomodel->inserir($_POST);
            echo "<script>window.location.href='" . site_url('adm/rh/adiantamento/adiantamentoFunc') . "'</script>";
        } else {
            $this->dados['object'] = $this->rh__adiantamentomodel;
            $this->dados['pagina'] = 'adm/rh/adiantamento/adiantamentoFuncNovo';
            $this->dados['titulo'] = 'rh__adiantamento ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function adiantamentoRh() {
        if (in_array($this->setorNome, array('RH_COORD'))) {
            $this->dados['dados'] = $this->rh__adiantamentomodel->all();
            $this->dados['pagina'] = 'adm/rh/adiantamento/adiantamentoRh';
            $this->dados['titulo'] = 'rh__adiantamento';
            $this->load->view($this->_tpl, $this->dados);
        } else {
            echo "<script>alert('SEM PREMISSAO DE ACESSO');window.location.href='" . site_url('adm/rh/iniciorh') . "'</script>";
        }
    }

    public function editarRh($idAdiantamento) {
        if (in_array($this->setorNome, array('RH_COORD'))) {
            $msg = null;
            if ($_POST) {
                $this->rh__adiantamentomodel->update($_POST, $idAdiantamento);
                echo "<script>window.location.href='" . site_url('adm/rh/adiantamento/adiantamentoRh') . "'</script>";
                return;
            }

            $this->dados['object'] = $this->rh__adiantamentomodel->getById($idAdiantamento);
            $this->dados['pagina'] = 'adm/rh/adiantamento/adiantamentoEditRh';
            $this->dados['titulo'] = 'rh__adiantamento ';
            $this->load->view($this->_tpl, $this->dados);
        } else {
            echo "<script>alert('SEM PREMISSAO DE ACESSO');window.location.href='" . site_url('adm/rh/iniciorh') . "'</script>";
        }
    }

    public function adiantamentorFinancList() {
        if (in_array($this->setorNome, array('FINANCEIRO', 'FINANCEIRO_COORD'))) {
            $this->dados['dados'] = $this->rh__adiantamentomodel->getDeferidos();
            $this->dados['pagina'] = 'adm/rh/adiantamento/adiantamentorFinancList';
            $this->dados['titulo'] = 'rh__adiantamento';
            $this->load->view($this->_tpl, $this->dados);
        } else {
            echo "<script>alert('SEM PREMISSAO DE ACESSO');window.location.href='" . site_url('adm/rh/iniciorh') . "'</script>";
        }
    }

    public function editarFinanc($idAdiantamento) {
        if (in_array($this->setorNome, array('FINANCEIRO', 'FINANCEIRO_COORD'))) {
            $msg = null;
            if ($_POST) {
                $this->rh__adiantamentomodel->update($_POST, $idAdiantamento);
                echo "<script>window.location.href='" . site_url('adm/rh/adiantamento/adiantamentorFinancList') . "'</script>";
                return;
            }

            $this->dados['object'] = $this->rh__adiantamentomodel->getById($idAdiantamento);
            $this->dados['pagina'] = 'adm/rh/adiantamento/editarFinanc';
            $this->dados['titulo'] = 'rh__adiantamento ';
            $this->load->view($this->_tpl, $this->dados);
        } else {
            echo "<script>alert('SEM PREMISSAO DE ACESSO');window.location.href='" . site_url('adm/rh/iniciorh') . "'</script>";
        }
    }

}
