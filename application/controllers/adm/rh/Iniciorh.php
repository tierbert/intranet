<?php

date_default_timezone_set('America/Bahia');

/** CRUD Controller para rh_contracheque* */
class Iniciorh extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('professoresmodel', '', true);
        $this->load->model('rh_contrachequemodel', '', true);
        $this->load->helper(array('form', 'url', 'string', 'download'));
        $this->load->library(array('Util', 'pagination', 'auth', 'upload'));
        $this->auth->verificaloginAdm();
    }

    /** Lista rh_contracheque * */
    public function index($inicio = 0) {
        $this->dados['pagina'] = 'adm/rh/inicioRh';
        $this->dados['titulo'] = 'rh_contracheque';
        $this->load->view($this->_tpl, $this->dados);
    }

}
