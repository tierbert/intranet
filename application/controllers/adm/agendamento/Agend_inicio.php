<?php

/** CRUD Controller para agendamento__horario* */
class Agend_inicio extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('agendamento__horariomodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
    }

    /** Lista agendamento__horario * */
    public function index($inicio = 0) {


        $this->dados['pagina'] = 'adm/agendamento/agend_inicio';
        $this->dados['titulo'] = 'agendamento__horario';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function senhas($tipo) {
        $this->auth->liberado('TI|secretaria|coordenacao|financeiro');
        $this->dados['tipo'] = $tipo;
        $this->dados['pagina'] = 'adm/agendamento/senhas';
        $this->dados['titulo'] = 'agendamento__horario';
        $this->load->view($this->_tpl, $this->dados);
    }

}
