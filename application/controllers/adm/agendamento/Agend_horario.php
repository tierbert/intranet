<?php

/** CRUD Controller para agendamento__horario* */
class Agend_horario extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('cursosmodel', '', true);
        $this->load->model('agendamento__horariomodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
    }

    /** Lista agendamento__horario * */
    public function index($inicio = 0) {
        $this->auth->liberado('TI|coordenacao|TCC');
        $maximo = 80;
        $config['base_url'] = site_url('/adm/agendamento/agend_horario/index');
        $config['total_rows'] = $this->agendamento__horariomodel->contaRegistros();
        $config['per_page'] = $maximo;
        $this->pagination->initialize($config);
        $this->dados['paginacao'] = $this->pagination->create_links();

        $this->dados['dados'] = $this->agendamento__horariomodel->all($inicio, $maximo);
        $this->dados['pagina'] = 'adm/agendamento/agend_horario/agend_horario_list';
        $this->dados['titulo'] = 'agendamento__horario';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Cria novo(a)  * */
    public function criar() {
        $this->auth->liberado('TI|coordenacao|TCC');
        if ($_POST) {
            $this->agendamento__horariomodel->inserir($_POST);
            echo "<script>window.location.href='" . site_url('adm/agendamento/agend_horario') . "'</script>";
        } else {
            $this->dados['cursos'] = $this->cursosmodel->getArray();
            $this->dados['object'] = $this->agendamento__horariomodel;
            $this->dados['pagina'] = 'adm/agendamento/agend_horario/agend__horario_novo';
            $this->dados['titulo'] = 'Novo Horário ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    /**
     * Edit 
     * */
    public function editar($id) {
        $this->auth->liberado('TI|coordenacao|TCC');
        $msg = null;
        if ($_POST) {
            $this->agendamento__horariomodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/adm/agendamento/agend_horario/'</script>";
            return;
        }

        $this->dados['object'] = $this->agendamento__horariomodel->getById($id);
        $this->dados['pagina'] = 'adm/agendamento/agend_horario/agend__horario_edit';
        $this->dados['titulo'] = 'Editar Horário ';
        $this->load->view($this->_tpl, $this->dados);
    }

    /**
     * Delete
     * */
    public function delete($id) {
        $this->auth->liberado('TI|coordenacao|TCC');
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->agendamento__horariomodel->delete($id);
            echo "<script>window.location.href = '" . site_url('adm/agendamento/agend_horario') . "'</script>";
            return;
        } else {
            if ($_POST) {
                redirect('adm/agendamento/agendamento_horario');
                return;
            }
            $object = $this->agendamento__horariomodel->getById($id);
        }

        $this->dados['pagina'] = 'adm/agendamento/agend_horario/agend__horario_del';
        $this->dados['titulo'] = 'agendamento__horario ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $id;
        $this->load->view($this->_tpl, $this->dados);
    }

}
