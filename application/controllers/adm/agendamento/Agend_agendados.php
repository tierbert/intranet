<?php

/** CRUD Controller para agendamento__agendados* */
class Agend_agendados extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('agendamento__agendadosmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
    }

    /** Lista agendamento__agendados * */
    public function index($inicio = 0) {
        $this->auth->liberado('TI|secretaria|coordenacao|financeiro');
        $maximo = 80;
        $config['base_url'] = site_url('/adm/agendamento/agend_agendados/index');
        $config['total_rows'] = $this->agendamento__agendadosmodel->contaRegistros();
        $config['per_page'] = $maximo;
        $this->pagination->initialize($config);
        $this->dados['paginacao'] = $this->pagination->create_links();

        if ($this->input->post()) {
            $this->dados['dados'] = $this->agendamento__agendadosmodel->all($inicio, $maximo, $this->input->post());
            $this->dados['post'] = $this->input->post();
        } else {
            $this->dados['dados'] = $this->agendamento__agendadosmodel->all($inicio, $maximo, $this->input->post());
        }

        $this->dados['pagina'] = 'adm/agendamento/agend_agendados/agendados_list';
        $this->dados['titulo'] = 'Agendados';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Cria novo(a)  * */
    private function criar() {
        $this->auth->liberado('TI|secretaria|coordenacao|financeiro');
        if ($_POST) {
            $this->agendamento__agendadosmodel->inserir($_POST);
            echo "<script>window.location.href='" . site_url() . "/agendamento__agendados/'</script>";
        } else {
            $this->dados['object'] = $this->agendamento__agendadosmodel;
            $this->dados['pagina'] = 'adm/agendamento__agendados/agendamento__agendados_novo';
            $this->dados['titulo'] = 'agendamento__agendados ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    private function editar($id) {
        $this->auth->liberado('TI|coordenacao|TCC');
        $msg = null;
        if ($_POST) {
            $this->agendamento__agendadosmodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/agendamento__agendados/'</script>";
            return;
        }

        $this->dados['object'] = $this->agendamento__agendadosmodel->getById($id);
        $this->dados['pagina'] = 'agendamento__agendados/agendamento__agendados_edit';
        $this->dados['titulo'] = 'agendamento__agendados ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function delete($id) {
        $this->auth->liberado('TI|secretaria|coordenacao|financeiro');
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->agendamento__agendadosmodel->delete($id);
            echo "<script>window.location.href = '" . site_url() . "/adm/agendamento/agend_agendados/'</script>";
            return;
        } else {
            if ($_POST) {
                redirect('adm/agendamento/agend_agendados');
                return;
            }
            $object = $this->agendamento__agendadosmodel->getById($id);
        }

        $this->dados['pagina'] = 'adm/agendamento/agend_agendados/agendados_del';
        $this->dados['titulo'] = 'agendamento__agendados ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $id;
        $this->load->view($this->_tpl, $this->dados);
    }

    public function conferido($id) {
        $this->agendamento__agendadosmodel->update(array('aha_conferido' => 'sim'), $id);
        echo "<script>window.location.href='" . site_url('adm/agendamento/agend_agendados/') . "'</script>";
    }

}
