<?php

/** CRUD Controller para cursos* */
class Cursos extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('cursosmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
        $this->auth->liberado('TI');
    }

    /** Lista cursos * */
    public function index($inicio = 0) {
        $maximo = 20;
        $config['base_url'] = site_url('/cursos/index');
        $config['total_rows'] = $this->cursosmodel->contaRegistros();
        $config['per_page'] = $maximo;
        $this->pagination->initialize($config);
        $this->dados['paginacao'] = $this->pagination->create_links();

        $this->dados['dados'] = $this->cursosmodel->all($inicio, $maximo);
        $this->dados['pagina'] = 'adm/cursos/cursos_list';
        $this->dados['titulo'] = 'cursos';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Cria novo(a)  * */
    public function criar() {
        if ($_POST) {
            $this->cursosmodel->inserir($_POST);
            echo "<script>window.location.href='" . site_url() . "/adm/cursos/'</script>";
        } else {
            $this->dados['object'] = $this->cursosmodel;
            $this->dados['pagina'] = 'adm/cursos/cursos_novo';
            $this->dados['titulo'] = 'cursos ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    /**
     * Edit 
     * */
    public function editar($id) {
        $msg = null;
        if ($_POST) {
            $this->cursosmodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/adm/cursos/'</script>";
            return;
        }

        $this->dados['object'] = $this->cursosmodel->getById($id);
        $this->dados['pagina'] = 'adm/cursos/cursos_edit';
        $this->dados['titulo'] = 'cursos ';
        $this->load->view($this->_tpl, $this->dados);
    }

    /**
     * Delete
     * */
    public function delete($id) {
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->cursosmodel->delete($id);
            echo "<script>window.location.href = '" . site_url() . "/adm/cursos/'</script>";
            return;
        } else {
            if ($_POST) {
                redirect('cursos');
                return;
            }
            $object = $this->cursosmodel->getById($id);
        }

        $this->dados['pagina'] = 'adm/cursos/cursos_del';
        $this->dados['titulo'] = 'cursos ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $id;
        $this->load->view($this->_tpl, $this->dados);
    }

    public function listByTipo() {
        $tipo = $this->input->post('tipo');
        $dados = $this->cursosmodel->getByTipo($tipo);

        echo "<option value=''>SELECIONE O CURSO</option>";
        foreach ($dados->result() as $curso) {
            echo "<option>" . mb_strtoupper($curso->cur_nome) . "</option>";
        }
    }

}
