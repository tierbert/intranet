<?php
/** CRUD Controller para estag__locais**/ 
class Estag__locais extends CI_Controller { 

     private $_tpl = 'index.php';
     private $dados = array();

     public function __construct() {
         parent::__construct();
         $this->load->model('estag__locaismodel', '', true);
         $this->load->helper(array('form', 'url'));
         $this->load->library(array('Util', 'pagination'));
         //$this->auth->check_logged($this->router->class, $this->router->method);
     }

     /** Lista estag__locais * */
     public function index($inicio = 0) {
         $maximo = 10;
         $config['base_url'] = site_url('/estag__locais/index');
         $config['total_rows'] = $this->estag__locaismodel->contaRegistros();
         $config['per_page'] = $maximo;
         $this->pagination->initialize($config);
         $this->dados['paginacao'] = $this->pagination->create_links();

         $this->dados['dados'] = $this->estag__locaismodel->all($inicio, $maximo);
         $this->dados['pagina'] = 'estag__locais/estag__locais_list';
         $this->dados['titulo'] = 'estag__locais';
         $this->load->view($this->_tpl, $this->dados);
     }

     /** Cria novo(a)  * */
     public function criar() {
         if ($_POST) {
             $this->estag__locaismodel->inserir($_POST);
             echo "<script>window.location.href='" . site_url() . "/estag__locais/'</script>";
         }else{
             $this->dados['object'] = $this->estag__locaismodel;
             $this->dados['pagina'] = 'estag__locais/estag__locais_novo';
             $this->dados['titulo'] = 'estag__locais ';
             $this->load->view($this->_tpl, $this->dados);
          }
      } 
        

        
        

    /**
     * Edit 
     * */
    public function editar($id) {
        $msg = null;
        if ($_POST) {
            //$this->form_validation->set_rules('valid', '', 'required');
            // if ($this->form_validation->run() == TRUE) {
            //$object->save();
            $this->estag__locaismodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/estag__locais/'</script>";
return;
//} else {
//$msg = 'Por favor preencha todos os dados';
// }
}

$this->dados['object'] = $this->estag__locaismodel->getById($id);
$this->dados['pagina'] = 'estag__locais/estag__locais_edit';
$this->dados['titulo'] = 'estag__locais ';
$this->load->view($this->_tpl, $this->dados);
}

/**
* Delete
* */
public function delete($id) {
$response = null;
if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
$response = $this->estag__locaismodel->delete($id);
echo "<script>window.location.href = '" . site_url() . "/estag__locais/'</script>";
return;
} else {
if ($_POST) {
redirect('estag__locais');
return;
}
$object = $this->estag__locaismodel->getById($id);
}

$this->dados['pagina'] = 'estag__locais/estag__locais_del';
$this->dados['titulo'] = 'estag__locais ';
$this->dados['object'] = $object;
$this->dados['response'] = $response;
$this->dados['id'] = $id;
$this->load->view($this->_tpl, $this->dados);
}

}

