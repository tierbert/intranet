<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bkp extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index($db = '') {
        $this->load->dbutil();
        $prefs = array(
            //'tables' => array('table1', 'table2'), // Array of tables to backup.
            // 'ignore' => array(), // List of tables to omit from the backup
            'format' => 'gzip', // gzip, zip, txt
            'filename' => 'mybackup.zip', // File name - NEEDED ONLY WITH ZIP FILES
            'add_drop' => TRUE, // Whether to add DROP TABLE statements to backup file
            'add_insert' => TRUE, // Whether to add INSERT data to backup file
            'newline' => "\n"                         // Newline character used in backup file
        );
        $backup = $this->dbutil->backup($prefs);
        $this->load->helper('file');
        $pastaBkp = '../../intranet_files/bkps/';
        if (!is_dir($pastaBkp)) {
            mkdir($pastaBkp);
        }
        $nomeBkp = 'BKP_INTRANET_' . date('Y-m-d') . '.zip';
        write_file($pastaBkp . $nomeBkp, $backup);
    }

}
