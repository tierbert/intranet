<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Importacao extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('proflifemodel', '', true);
        $this->load->model('professoresmodel', '', true);
        $this->load->model('alunosmodel', '', true);
        $this->load->model('cursosmodel', '', true);
        $this->load->model('disciplinasmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination'));
        $this->load->helper('file');
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginAdm();
        //$this->auth->liberado('TI');
    }

    public function index() {
        $this->index2();
    }

    private function index2() {
        $texto = read_file('./arquivos/20191.csv');
        $texto = mb_convert_encoding($texto, 'UTF-8', mb_detect_encoding($texto, 'UTF-8, ISO-8859-1', true));

        $this->db->query("delete from horario where hor_semestre = '2019-1'");

        $var1 = explode("\n", $texto);
        $disciplinas = array();
        echo '<hr>';
        //unset($var1[0]);

        $i = 25000;
        $linhas = array();
        foreach ($var1 as $var2) {
            //echo $i . '-';
            $var3 = explode(";", $var2);
            $linhas[] = array(
                'hor_id' => $i,
                'hor_disciplina' => trim($var3[0]),
                'hor_turno' => trim($var3[2]),
                'hor_prof' => trim($var3[3]),
                'hor_semestre' => '2019-1',
            );
            $i++;
            //break;
        }
        //echo '<pre>';
        //print_r($linhas);
        echo count($linhas);
        $this->db->insert_batch('horario', $linhas);
    }

    private function horario() {
        $texto = read_file('./arquivos/20182.csv');
        //echo $texto;
        $linhas = explode("\n", $texto);

        //print_r($linhas);
        print_r(explode('|', $linhas[0]));
        unset($linhas[0]);
        foreach ($linhas as $linha) {
            $linhasArray = explode('|', $linha);

            //print_r($linhasArray);
            $dadosHorario[] = array(
                'hor_idSala' => $linhasArray[0],
                'hor_idTurno' => $linhasArray[1],
                'hor_dia' => $linhasArray[3],
            );
            break;
        }
        //print_r($dadosHorario);
        //$this->db->insert_batch('hor_horario', $dadosHorario);
    }

    private function index22() {
        $texto = read_file('./arquivos/teste.csv');
        $texto = mb_convert_encoding($texto, 'UTF-8', mb_detect_encoding($texto, 'UTF-8, ISO-8859-1', true));

//$this->db->query("delete from horario");
//var_dump($texto);

        $var1 = explode("\n", $texto);
        $disciplinas = array();
        foreach ($var1 as $var2) {
            $var3 = explode("|", $var2);

            $total = count($var3);
//echo $total .'- ';
//$loops = $total / 4;
//echo $loops;

            for ($i = 0; $i <= $total; $i += 4) {
//echo $i;
//echo $var3[$i];
                $chave = '';

                if (!isset($var3[$i + 3])) {
                    continue;
                }

                $chave = trim($var3[$i] . '-' . $var3[$i + 1]);
                $chave = str_replace(' ', '', $chave);

//var_dump($chave);

                if (!key_exists($chave, $disciplinas)) {
                    $disciplinas[$chave] = array(
                        'hor_disciplina' => $var3[$i],
                        'hor_turno' => $var3[$i + 1],
                        'hor_turma' => $var3[$i + 2],
                        'hor_prof' => $var3[$i + 3],
                        'hor_semestre' => '20171'
                    );

//                   var_dump($var3);
//                  echo '<hr>';
                }
//echo '<hr>';
            }
//break;
        }
        foreach ($disciplinas as $disc) {
            var_dump($disc);
            echo '<hr>';
        }

//var_dump($disciplinas);
//$this->db->insert_batch('horario', $disciplinas);
    }

    private function prof() {
        $dados1 = $this->db->query("select pl_profCpf from profLife4");

//$dados2 = $dados->result();
        $prof = array();
        foreach ($dados1->result() as $dad) {
            $dados2 = $this->db->query("select * from profLife where pl_profCpf = '$dad->pl_profCpf' ");
            echo $dad->pl_profCpf;
            echo $dados2->row('pl_profNome');
            echo "<hr>";

            $dados = array();
            $dados['pro_nome'] = $dados2->row('pl_profNome');
            $dados['pro_cpf'] = $dad->pl_profCpf;
            $dados['pro_senha'] = $dad->pl_profCpf;


            $this->professoresmodel->inserir($dados);
        }
    }

    private function professores() {
        $texto = read_file('./arquivos/disc_prof.txt');
        $texto = mb_convert_encoding($texto, 'UTF-8', mb_detect_encoding($texto, 'UTF-8, ISO-8859-1', true));

//echo $texto;
        $this->db->query("delete from profLife");

        $array = explode("\n", $texto);

        foreach ($array as $dadosArray) {
            $dadosLinha = explode('|', $dadosArray);

            foreach ($dadosLinha as $dado) {
//echo $dado;
                var_dump($dado);
            }
            echo '<hr>';

            $cpf = substr($dadosLinha[3], -1);
//var_dump($cpf);


            $dados = array();
            $dados['pl_discNome'] = $dadosLinha[0];
            $dados['pl_discCodigo'] = $dadosLinha[1];
            $dados['pl_profNome'] = $dadosLinha[2];
            $dados['pl_profCpf'] = $dadosLinha[3];

            $this->proflifemodel->inserir($dados);
        }
    }

    private function alunos() {
        $texto = read_file('./arquivos/alunos20171');
        $texto = mb_convert_encoding($texto, 'UTF-8', mb_detect_encoding($texto, 'UTF-8, ISO-8859-1', true));

//var_dump( $texto);

        $array = explode("\n", $texto);

//$this->db->query("delete from alunos");
        $linhas = array();
        foreach ($array as $dadosArray) {
            $dadosLinha = explode(';', $dadosArray);

            foreach ($dadosLinha as $dado) {
// echo $dado;
            }
//echo '<hr>';


            $dados = array();
            $dados['alu_matricula'] = $dadosLinha[0];
            $dados['alu_nome'] = $dadosLinha[1];
            $dados['alu_cpf'] = substr(($dadosLinha[2]), 0, -1);
            $dados['alu_senha'] = substr(($dadosLinha[2]), 0, -1);

            $linhas[] = $dados;

//$this->alunosmodel->inserir($dados);
        }
//$this->db->insert_batch('alunos', $linhas);
    }

    private function disciplinas() {
        $texto = read_file('./arquivos/disciplinas.csv');
        $texto = mb_convert_encoding($texto, 'UTF-8', mb_detect_encoding($texto, 'UTF-8, ISO-8859-1', true));

//echo $texto;

        $array = explode("\n", $texto);

        foreach ($array as $dadosArray) {
            $dadosLinha = explode('|', $dadosArray);

            foreach ($dadosLinha as $dado) {
                echo $dado;
            }
            echo '<hr>';

//$this->db->query("delete from professores");
            $dados = array();
            $dados['dis_idCurso'] = $dadosLinha[4];
            $dados['dis_codigo'] = $dadosLinha[0];
            $dados['dis_nome'] = $dadosLinha[1];
            $dados['dis_turma'] = $dadosLinha[2];
            $dados['dis_nivel'] = $dadosLinha[3];

            $this->disciplinasmodel->inserir($dados);
        }
    }

    private function cursos() {
        $texto = read_file('./arquivos/cursos.csv');
        $texto = mb_convert_encoding($texto, 'UTF-8', mb_detect_encoding($texto, 'UTF-8, ISO-8859-1', true));

//echo $texto;

        $array = explode("\n", $texto);

        foreach ($array as $dadosArray) {
            $dadosLinha = explode('|', $dadosArray);

            foreach ($dadosLinha as $dado) {
                echo $dado;
            }
            echo '<hr>';

//$this->db->query("delete from professores");
            $dados = array();
            $dados['cur_id'] = $dadosLinha[0];
            $dados['cur_nome'] = $dadosLinha[1];
            $this->cursosmodel->inserir($dados);
        }
    }

    private function teste() {
        $texto = read_file('./arquivos/cursos.csv');
        $texto = mb_convert_encoding($texto, 'UTF-8', mb_detect_encoding($texto, 'UTF-8, ISO-8859-1', true));

        echo $texto;

        $func = explode('Funcionario:', $texto);
//$this->db->query("delete from professores");
        $i = 0;

        foreach ($func as $fun) {
            if ($i == 0) {
                $i++;
                continue;
            }
            strlen($fun);
            if ($fun != "") {
                $pos1 = strripos($fun, 'CPF');
                $pos2 = strripos($fun, 'Funcao');
                $pos3 = strripos($fun, 'Grupo');
                $pos4 = $pos3 - $pos2 - 8;
                $cod = substr($fun, 1, 5);
                $cod = intval($cod);
                $cpf = substr($fun, $pos1 + 5, 14);
                $nome = substr($fun, 7, $pos1 - 7);
                $funcao = substr($fun, $pos2 + 8, $pos4);

//echo strtolower($funcao) . strripos(strtolower($funcao), 'professor');

                $prof = (strripos(strtolower($funcao), 'prof'));
                $coord = (strripos(strtolower($funcao), 'coorden'));

//var_dump($prof);

                if ($prof !== false || $coord !== false) {
//echo 'PROFESSOR';
                    echo "codigo: $cod<br>";
                    echo "Nome: $nome<br>";
                    echo "cpf: $cpf<br>";
                    echo "Funcao: $funcao<br>";

                    $senha = substr($cpf, 0, 3) . substr($cpf, 4, 1);

                    $dados = array();
                    $dados['pro_nome'] = $nome;
                    $dados['pro_cpf'] = $cpf;
                    $dados['pro_registroRh'] = $cod;
                    $dados['pro_senha'] = $senha;

                    $this->professoresmodel->inserir($dados);

                    echo '<hr>';
//break;
                } else {
//echo 'pula';
                }
            }
        }
    }

    private function senhaProf() {
        $texto = read_file(base_url() . 'arquivos/senhaProf.csv');
        $texto = mb_convert_encoding($texto, 'UTF-8', mb_detect_encoding($texto, 'UTF-8, ISO-8859-1', true));
        $dados = explode("\n", $texto);
        foreach ($dados as $dado) {
            echo $dado;
            $prof = explode(',', $dado);

            $id = $prof[0];
            $cpf = str_replace('.', '', $prof[1]);
            $cpf = str_replace('-', '', $cpf);

            echo $id . ' - ' . $cpf;

            $salaLivre = $this->load->database('salalivre', true);
            $pro = $salaLivre->query("update professores set pro_senhaIntra = '$cpf' where pro_id = " . $id);
//echo $pro->row('pro_cpf');



            echo '<hr>';
//break;
        }
    }

}
