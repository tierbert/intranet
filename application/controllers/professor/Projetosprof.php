<?php

/** CRUD Controller para tcc__horarios* */
class Projetosprof extends CI_Controller {

    private $_tpl = 'indexProfessor.php';
    private $dados = array();
    private $idProfessor;

    public function __construct() {
        parent::__construct();
        $this->load->model('projetosmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));

        $this->idProfessor = $this->session->userdata('usu_id');
        $this->auth->verificaloginProf();
    }

    public function index() {
        $this->dados['dados'] = $this->projetosmodel->getByProf($this->idProfessor);
        $this->dados['pagina'] = 'professor/projetos/indexProjProf';
        $this->dados['titulo'] = 'MEUS PROJETOS';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function criar() {
        if ($this->input->post()) {
            $pastaAnexos = '../../intranet_files/projetos/';
            if (!is_dir($pastaAnexos)) {
                mkdir($pastaAnexos);
            }
            $obs = $this->input->post('obs');
            $idProjeto = $this->projetosmodel->inserir($this->input->post(), $this->idProfessor);

            $total = count($_FILES['anexos']['name']);
            for ($i = 0; $i < $total; $i++) {
                //echo $i . '<hr>';
                $tmpFilePath = $_FILES['anexos']['tmp_name'][$i];
                if ($tmpFilePath != "") {
                    $nomeArquivo = 'proj_' . $idProjeto . '_' . urlencode($_FILES['anexos']['name'][$i]);
                    $newFilePath = $pastaAnexos . $nomeArquivo;
                    if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                        $dadosAnexo = array(
                            'ane_idProjeto' => $idProjeto,
                            'ane_tipo' => '',
                            'ane_obs' => $obs[$i],
                            'ane_arquivo' => $nomeArquivo,
                        );

                        $this->projetosmodel->inserirAnexo($dadosAnexo);
                    }
                }
            }
            echo "<script>alert('PROJETO ENCAMINHADO PARA ANÁLISE');window.location.href='" . site_url('professor/projetosprof/') . "'</script>";
        } else {
            $this->dados['pagina'] = 'professor/projetos/criarProjProf';
            $this->dados['titulo'] = 'ENVIO DE NOTA FISCAL';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

}
