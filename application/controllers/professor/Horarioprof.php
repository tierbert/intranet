<?php

/*  Também utilizado por estagiarios  */

class Horarioprof extends CI_Controller {

    private $_tpl = 'indexProfessor.php';
    private $dados = array();
    private $tipo;

    public function __construct() {
        parent::__construct();
        $this->load->model('hor_horariomodel', 'horariom', true);
        $this->load->model('Hor_turnomodel', 'turnom', true);
        $this->load->model('alunosmodel', 'turnom', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));

        $loginprof = $this->session->userdata('loginPainelProf');
        $loginAluno = $this->session->userdata('loginAluno');

        if ($loginprof != NULL) {
            $this->tipo = 'professor';
            $this->_tpl = 'indexProfessor.php';

            $this->auth->verificaloginProf();
            $this->auth->liberado('professor');
        } elseif ($loginAluno != NULL) {
            $this->tipo = 'aluno';

            $this->_tpl = 'indexAluno.php';
            $this->verificaLogin();

            if ($this->session->userdata('estagiario') == 'SIM') {
                
            } else {
                echo "<script>alert('SOMENTE PARA ESTAGIÁRIOS');window.location.href='" . site_url('aluno/indexaluno') . "'</script>";
                exit();
            }
        }
    }

    public function index($sala = 0) {
        if ($this->tipo == 'professor') {
            $regRh = $this->session->userdata('usu_regRh');
        } elseif ($this->tipo == 'aluno') {
            $regRh = '9999';
        }
        //$this->dados['turnos'] = $this->turnom->getAll();
        $this->dados['tipo'] = $this->tipo;
        $this->dados['dados'] = $this->horariom->getByProf($regRh);
        $this->dados['pagina'] = 'professor/reservasProf';
        $this->dados['titulo'] = 'hor_horario';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function calendario($tipo, $semana = '') {
       
            $this->dados['semana'] = $semana;
            $this->dados['tipo'] = $this->tipo;
            $this->dados['turnos'] = $this->turnom->getAll($tipo);
            //$this->dados['dados'] = $this->horariom->getBySala($sala);
            $this->dados['pagina'] = 'professor/hor_horario/sala';
            $this->dados['tipo'] = $tipo;
            $this->dados['titulo'] = 'hor_horario';
            $this->load->view('professor/salaProf', $this->dados);
       
    }

    public function reserva($turno, $sala, $dia, $data, $tipo) {
        $this->dados['tipo'] = $this->tipo;
        if ($this->input->post()) {

            if ($this->util->data($this->input->post('data')) < date('Y-m-d')) {
                //echo 'data-retroativa';
                echo "<script>alert('NÃO É POSSIVEL AGENDAR SALA EM DATA RETROATIVA');window.location.href='" . site_url('professor//horarioprof/reserva/' . $turno . '/' . $sala . '/' . $dia . '/' . $data . '/' . $tipo) . "'</script>";
                exit();
            }

            $agend = $this->horariom->getByTurnoSalaDia($turno, $sala, $this->util->data($this->input->post('data')));
            if ($agend->num_rows() > 0) {
                //echo 'reservada';
                echo "<script>alert('NÃO FOI POSSÍVEL AGENDAR A SALA POIS EXISTE AGENDAMENTO PARA ESTE DIA');window.location.href='" . site_url('professor//horarioprof/calendario/' . $tipo) . "'</script>";
                exit();
            }

            $reserv = $this->horariom->getByHorario($turno, $sala, $dia);
            if ($reserv->num_rows() == 1) {
                //echo 'ocupada:' . $reserv->num_rows();
                echo "<script>alert('NÃO FOI POSSÍVEL AGENDAR A SALA POIS EXISTE AULA NESTE DIA');window.location.href='" . site_url('professor//horarioprof/calendario/' . $tipo) . "'</script>";
                exit();
            } else {
                //var_dump($this->input->post());
                if ($this->tipo == 'professor') {
                    $regRh = $this->input->post('regRh');
                } else {
                    $regRh = '0000';
                }

                $reserv = $this->horariom->reservarV2($this->input->post('turno'), $this->input->post('sala'), $regRh, $this->input->post('dia'), $this->input->post('data'), $this->tipo);

                echo "<script>alert('RESERVA AGUARDANDO CONFIRMAÇÃO');window.location.href='" . site_url('professor/horarioprof') . "'</script>";
                
            }
        } else {
            $this->dados['sala'] = $this->horariom->getSalaById($sala);
            $this->dados['turno'] = $this->turnom->getById($turno);
            $this->dados['dia'] = ($dia);
            $this->dados['data'] = ($data);
            $this->dados['idTurno'] = ($turno);
            $this->dados['regRh'] = $this->session->userdata('usu_regRh');
            $this->dados['data'] = ($data);
            $this->dados['diaSemana'] = $this->getDia($dia);
            $this->dados['pagina'] = 'professor/reservaProf';
            $this->dados['titulo'] = 'hor_horario';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    private function getDia($dia) {
        switch ($dia):
            case 1:
                return 'SEGUNDA';
                break;
            case 2:
                return 'TERÇA';
                break;
            case 3:
                return 'QUARTA';
                break;
            case 4:
                return 'QUINTA';
                break;
            case 5:
                return 'SEXTA';
                break;
            case 6:
                return 'SÁBADO';
                break;
            case 7:
                return 'DOMINGO';
                break;
        endswitch;
    }

    public function verificaLogin() {
        $logado = $this->session->userdata('loginAluno');
        $idAluno = $this->session->userdata('idAluno');
        if ($logado != 'logado') {
            echo "<script>window.location.href='" . site_url() . "'</script>";
        }
    }

}
