<?php
/** CRUD Controller para tcc__agendados**/ 
class TccAgendados extends CI_Controller { 

     private $_tpl = 'indexProfessor.php';
     private $dados = array();

     public function __construct() {
         parent::__construct();
         $this->load->model('tcc__agendadosmodel', '', true);
         $this->load->helper(array('form', 'url'));
         $this->load->library(array('Util', 'pagination'));
         $this->auth->verificaloginProf();
     }

     /** Lista tcc__agendados * */
     public function index($inicio = 0) {
         $maximo = 10;
         $config['base_url'] = site_url('/tcc__agendados/index');
         $config['total_rows'] = $this->tcc__agendadosmodel->contaRegistros();
         $config['per_page'] = $maximo;
         $this->pagination->initialize($config);
         $this->dados['paginacao'] = $this->pagination->create_links();

         $this->dados['dados'] = $this->tcc__agendadosmodel->all($inicio, $maximo);
         $this->dados['pagina'] = 'tcc__agendados/tcc__agendados_list';
         $this->dados['titulo'] = 'tcc__agendados';
         $this->load->view($this->_tpl, $this->dados);
     }

     /** Cria novo(a)  * */
     public function criar() {
         if ($_POST) {
             $this->tcc__agendadosmodel->inserir($_POST);
             echo "<script>window.location.href='" . site_url() . "/tcc__agendados/'</script>";
         }else{
             $this->dados['object'] = $this->tcc__agendadosmodel;
             $this->dados['pagina'] = 'tcc__agendados/tcc__agendados_novo';
             $this->dados['titulo'] = 'tcc__agendados ';
             $this->load->view($this->_tpl, $this->dados);
          }
      } 
        

        
        

    /**
     * Edit 
     * */
    public function editar($id) {
        $msg = null;
        if ($_POST) {
            //$this->form_validation->set_rules('valid', '', 'required');
            // if ($this->form_validation->run() == TRUE) {
            //$object->save();
            $this->tcc__agendadosmodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/tcc__agendados/'</script>";
return;
//} else {
//$msg = 'Por favor preencha todos os dados';
// }
}

$this->dados['object'] = $this->tcc__agendadosmodel->getById($id);
$this->dados['pagina'] = 'tcc__agendados/tcc__agendados_edit';
$this->dados['titulo'] = 'tcc__agendados ';
$this->load->view($this->_tpl, $this->dados);
}

/**
* Delete
* */
public function delete($id) {
$response = null;
if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
$response = $this->tcc__agendadosmodel->delete($id);
echo "<script>window.location.href = '" . site_url() . "/tcc__agendados/'</script>";
return;
} else {
if ($_POST) {
redirect('tcc__agendados');
return;
}
$object = $this->tcc__agendadosmodel->getById($id);
}

$this->dados['pagina'] = 'tcc__agendados/tcc__agendados_del';
$this->dados['titulo'] = 'tcc__agendados ';
$this->dados['object'] = $object;
$this->dados['response'] = $response;
$this->dados['id'] = $id;
$this->load->view($this->_tpl, $this->dados);
}

}

