<?php
/** CRUD Controller para tcc__horarios**/ 
class TccHorarios extends CI_Controller { 

     private $_tpl = 'indexProfessor.php';
     private $dados = array();

     public function __construct() {
         parent::__construct();
         $this->load->model('tcc__horariosmodel', '', true);
         $this->load->helper(array('form', 'url'));
         $this->load->library(array('Util', 'pagination'));
         $this->auth->verificaloginProf();
     }

     /** Lista tcc__horarios * */
     public function index($inicio = 0) {
         $maximo = 10;
         $config['base_url'] = site_url('/tcc__horarios/index');
         $config['total_rows'] = $this->tcc__horariosmodel->contaRegistros();
         $config['per_page'] = $maximo;
         $this->pagination->initialize($config);
         $this->dados['paginacao'] = $this->pagination->create_links();

         $this->dados['dados'] = $this->tcc__horariosmodel->all($inicio, $maximo);
         $this->dados['pagina'] = 'tcc__horarios/tcc__horarios_list';
         $this->dados['titulo'] = 'tcc__horarios';
         $this->load->view($this->_tpl, $this->dados);
     }

     /** Cria novo(a)  * */
     public function criar() {
         if ($_POST) {
             $this->tcc__horariosmodel->inserir($_POST);
             echo "<script>window.location.href='" . site_url() . "/tcc__horarios/'</script>";
         }else{
             $this->dados['object'] = $this->tcc__horariosmodel;
             $this->dados['pagina'] = 'tcc__horarios/tcc__horarios_novo';
             $this->dados['titulo'] = 'tcc__horarios ';
             $this->load->view($this->_tpl, $this->dados);
          }
      } 
        

        
        

    /**
     * Edit 
     * */
    public function editar($id) {
        $msg = null;
        if ($_POST) {
            //$this->form_validation->set_rules('valid', '', 'required');
            // if ($this->form_validation->run() == TRUE) {
            //$object->save();
            $this->tcc__horariosmodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/tcc__horarios/'</script>";
return;
//} else {
//$msg = 'Por favor preencha todos os dados';
// }
}

$this->dados['object'] = $this->tcc__horariosmodel->getById($id);
$this->dados['pagina'] = 'tcc__horarios/tcc__horarios_edit';
$this->dados['titulo'] = 'tcc__horarios ';
$this->load->view($this->_tpl, $this->dados);
}

/**
* Delete
* */
public function delete($id) {
$response = null;
if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
$response = $this->tcc__horariosmodel->delete($id);
echo "<script>window.location.href = '" . site_url() . "/tcc__horarios/'</script>";
return;
} else {
if ($_POST) {
redirect('tcc__horarios');
return;
}
$object = $this->tcc__horariosmodel->getById($id);
}

$this->dados['pagina'] = 'tcc__horarios/tcc__horarios_del';
$this->dados['titulo'] = 'tcc__horarios ';
$this->dados['object'] = $object;
$this->dados['response'] = $response;
$this->dados['id'] = $id;
$this->load->view($this->_tpl, $this->dados);
}

}

