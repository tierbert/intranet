<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/** CRUD Controller para professores* */
class Painelprof extends CI_Controller {

    private $_tpl = 'indexProfessor.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('tcc__agendadosmodel', 'tcaa', true);
        $this->load->model('tcc__horariosmodel', 'tcch', true);
        $this->load->model('agendamento__horariomodel', '', true);
        $this->load->model('agendamento__agendadosmodel', '', true);
        $this->load->model('rh_contrachequemodel', '', true);
        $this->load->model('solicitacoesmodel', '', true);
        $this->load->model('horariomodel', '', true);
        $this->load->model('professoresmodel', '', true);
        $this->load->model('tcc__agendadosmodel', 'tca', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination', 'auth'));
        $this->auth->verificaloginProf();
    }

    public function index() {
        $this->auth->liberado('professor');
        $this->dados['pagina'] = 'professor/painelProf';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function senha() {
        if ($_POST) {
            $idusuario = $this->session->userdata('usu_id');
            $_POST['pro_trocaSenha'] = 1;
            $this->professoresmodel->update($_POST, $idusuario);
            echo "<script>window.location.href='" . site_url('professor/painelprof') . "'</script>";
            return;
        }
        $this->dados['usu_id'] = $this->session->userdata('usu_id');
        $this->dados['pagina'] = 'professor/senhaProf';
        $this->dados['titulo'] = 'alunos ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function solicitacoes() {
        $this->auth->liberado('professor');
        //$this->dados['pagina'] = 'professor/painelProf';
        $this->dados['pagina'] = 'professor/solicitProf';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function listaAssProf() {
        $this->auth->liberado('professor');

        $idusuario = $this->session->userdata('usu_id');
        $nomeProf = $this->session->userdata('usu_nome');
        $this->dados['nomeProf'] = $nomeProf;

        $disciplinas = $this->horariomodel->getByProf($idusuario, '2019-1');
        $this->dados['disciplinas'] = $disciplinas;
        //$this->dados['totais'] = $this->solicitacoesmodel->totais();
        //$professores = $this->horariomodel->professores();


        $this->dados['pagina'] = 'professor/listaAssProf';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function contracheque() {
        $this->auth->liberado('professor');

        $idusuario = $this->session->userdata('usu_id');
        $regRh = $this->session->userdata('usu_regRh');

        $this->dados['dados'] = $this->rh_contrachequemodel->getByFunc($regRh);

        $this->dados['pagina'] = 'professor/contracheques';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function contrachequeBaixar() {
        $idusuario = $this->session->userdata('usu_id');

        $disciplinas = $this->horariomodel->getByProf($idusuario);
        $this->dados['disciplinas'] = $disciplinas;
        //$this->dados['totais'] = $this->solicitacoesmodel->totais();
        //$professores = $this->horariomodel->professores();


        $this->dados['pagina'] = 'professor/contracheque';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function agendados($semana = '') {
        $this->auth->liberado('professor');

        $idProf = $this->session->userdata('usu_id');
        //$this->dados['horarios'] = $this->tcch->getByProf($idProf);




        if ($this->input->post()) {
            $post = $this->input->post();

            $data = date('Y-m-d H:i:s', $post['tca_data']);
            $semana = date('W', $post['tca_data']);

            $post['tca_semana'] = $semana;
            $post['tca_data'] = $data;
            $post['tca_idAluno'] = 0;

//            $agenda = $this->tcch->getByHorarioData($post['tca_idHorario'], $post['tca_data']);

            $this->tcch->alunHorCad($post);
            echo "<script>alert('HORARIO RESERVADO');window.location.href='" . site_url('professor/painelprof/agendados/') . "'</script>";
            //print_r($post);
        } else {
            $this->dados['semana'] = $semana;
            $this->dados['turnos'] = $this->tcch->getTurnos();
            $this->dados['pagina'] = 'professor/agendados';
            $this->dados['titulo'] = 'agendamento__horario';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function horarioTccNovo() {
        $this->auth->liberado('professor');
        $idProf = $this->session->userdata('usu_id');
        $this->dados['idProf'] = $idProf;
        if ($_POST) {
            $this->tcch->inserir($_POST, $idProf);
            echo "<script>window.location.href='" . site_url('professor/painelprof/agendados') . "'</script>";
        } else {
            $this->dados['object'] = $this->tcch;
            $this->dados['pagina'] = 'professor/horarioTccNovo';
            $this->dados['titulo'] = 'agendamento__horario';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function horarioTccEdit($id) {
        $this->auth->liberado('professor');
        $idProf = $this->session->userdata('usu_id');
        $this->dados['idProf'] = $idProf;
        if ($_POST) {
            $this->tcch->update($_POST, $id);
            echo "<script>window.location.href='" . site_url('professor/painelprof/agendados') . "'</script>";
        } else {
            $this->dados['object'] = $this->tcch->getById($id);
            $this->dados['pagina'] = 'professor/horarioTccEdit';
            $this->dados['titulo'] = 'agendamento__horario';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    function sair() {
        $this->session->sess_destroy();
        echo "<script>window.location.href='" . site_url('inicio/loginProf') . "'</script>";
    }

    public function videoswa() {
        $this->auth->liberado('professor');


        $this->dados['pagina'] = 'professor/videosSwa';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function agendadosListTcc($idhorario) {
        $this->auth->liberado('professor');

        $idProf = $this->session->userdata('usu_id');
        $this->dados['agendados'] = $this->tcaa->getByhorario($idhorario);
        $this->dados['pagina'] = 'professor/agendadosList';
        $this->dados['titulo'] = 'agendamento__horario';
        $this->load->view($this->_tpl, $this->dados);
    }

}
