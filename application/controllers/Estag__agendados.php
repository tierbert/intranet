<?php

/** CRUD Controller para estag__agendados* */
class Estag__agendados extends CI_Controller {

    private $_tpl = 'index.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('estag__agendadosmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination'));
        //$this->auth->check_logged($this->router->class, $this->router->method);
    }

    /** Lista estag__agendados * */
    public function index($inicio = 0) {
        $maximo = 10;
        $config['base_url'] = site_url('/estag__agendados/index');
        $config['total_rows'] = $this->estag__agendadosmodel->contaRegistros();
        $config['per_page'] = $maximo;
        $this->pagination->initialize($config);
        $this->dados['paginacao'] = $this->pagination->create_links();

        $this->dados['dados'] = $this->estag__agendadosmodel->all($inicio, $maximo);
        $this->dados['pagina'] = 'estag__agendados/estag__agendados_list';
        $this->dados['titulo'] = 'estag__agendados';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Cria novo(a)  * */
    public function criar() {
        if ($_POST) {
            $this->estag__agendadosmodel->inserir($_POST);
            echo "<script>window.location.href='" . site_url() . "/estag__agendados/'</script>";
        } else {
            $this->dados['object'] = $this->estag__agendadosmodel;
            $this->dados['pagina'] = 'estag__agendados/estag__agendados_novo';
            $this->dados['titulo'] = 'estag__agendados ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    /**
     * Edit 
     * */
    public function editar($id) {
        $msg = null;
        if ($_POST) {
            //$this->form_validation->set_rules('valid', '', 'required');
            // if ($this->form_validation->run() == TRUE) {
            //$object->save();
            $this->estag__agendadosmodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/estag__agendados/'</script>";
            return;
//} else {
//$msg = 'Por favor preencha todos os dados';
// }
        }

        $this->dados['object'] = $this->estag__agendadosmodel->getById($id);
        $this->dados['pagina'] = 'estag__agendados/estag__agendados_edit';
        $this->dados['titulo'] = 'estag__agendados ';
        $this->load->view($this->_tpl, $this->dados);
    }

    /**
     * Delete
     * */
    public function delete($id) {
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->estag__agendadosmodel->delete($id);
            echo "<script>window.location.href = '" . site_url() . "/estag__agendados/'</script>";
            return;
        } else {
            if ($_POST) {
                redirect('estag__agendados');
                return;
            }
            $object = $this->estag__agendadosmodel->getById($id);
        }

        $this->dados['pagina'] = 'estag__agendados/estag__agendados_del';
        $this->dados['titulo'] = 'estag__agendados ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $id;
        $this->load->view($this->_tpl, $this->dados);
    }
    
    

}
