<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sistemas extends CI_Controller {

    private $_tpl = 'index.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination'));
        $this->load->model('sistemasmodel', '', true);
        //$config['base_url'] = 'http://localhost/projectname/';
    }

    public function index() {

    }

    public function cadCargoAdministrativo() {
        $this->dados['pagina'] = 'sistemas/cadCargoAdministrativo';
        $this->dados['titulo'] = 'alunos';
        $this->load->view('sistemas/indexSistemas', $this->dados);
    }

    public function cadCargoDocente() {
        if ($this->input->post()) {
            $this->sistemasmodel->cadCargoAdministrativo($this->input->post());
            $this->dados['pagina'] = 'sistemas/cadCargoDocenteFim';
            $this->load->view('sistemas/indexSistemas', $this->dados);
        } else {
            $this->dados['pagina'] = 'sistemas/cadCargoDocente';
            $this->dados['titulo'] = 'alunos';
            $this->load->view('sistemas/indexSistemas', $this->dados);
        }
    }

    public function acessibilidade() {
        if ($this->input->post()) {
            $this->sistemasmodel->acessibilidade($this->input->post());
            $this->dados['pagina'] = 'sistemas/acessibilidadeFim';
            $this->load->view('sistemas/indexSistemas', $this->dados);
        } else {
            $this->dados['pagina'] = 'sistemas/acessibilidade';
            $this->dados['titulo'] = '';
            $this->load->view('sistemas/indexSistemas', $this->dados);
        }
    }

    public function horarios() {
        $this->load->helper('file');
        $arquivo = read_file(base_url() . 'assets/arquivos/Classes.html');
        $horSelecionado = 1; //$_POST['horario'];
        $dados = explode('<p>', $arquivo);
        $horarios = array();
        foreach ($dados as $key => $value) {
            if ($key == 0) {
                continue;
            }
            $pos = strpos($dados[$key], '<h1>');
            $pos2 = strpos($dados[$key], '</h1>');
            $horario = substr($dados[$key], $pos, $pos2);
            $horario = strip_tags($horario);
            if ($horario != '') {
                $horarios[$key] = $horario;
            }
        }

        $this->dados['dados'] = $dados;
        $this->dados['horSelecionado'] = 1;
        $this->dados['horario'] = $this->input->post('horario');
        $this->dados['horarios'] = $horarios;

        $this->dados['pagina'] = 'sistemas/horario';
        $this->dados['titulo'] = 'alunos';
        $this->load->view('sistemas/horario', $this->dados);
    }

    public function tvs() {
        $this->dados['pagina'] = 'sistemas/posgraduacao';
        $this->dados['titulo'] = 'alunos';
        $this->load->view('sistemas/tvs', $this->dados);
    }

    public function tv($numCorredor = '') {
        if ($numCorredor == '') {
            echo "<script>window.location.href = '" . site_url('sistemas/tvs') . "'</script>";
        } else {
            $this->dados['numCorredor'] = $numCorredor;
            $this->load->view('sistemas/tv', $this->dados);
        }
    }

    public function agenda() {


header('Access-Control-Allow-Origin: *');
        $dados = $this->db->get('agenda');
        echo json_encode($dados->result_array());
    }

    public function posgraduacao($codCurso = '') {
        if ($this->input->post()) {
            $this->sistemasmodel->posgraduacaoCad($this->input->post());
            $this->dados['pagina'] = 'sistemas/cadCargoDocenteFim';

            $this->enviarCadPos($this->input->post());

            $this->load->view('sistemas/indexSistemas', $this->dados);
        } else {
            if ($codCurso != '') {
                $this->dados['curso'] = $this->sistemasmodel->posgraduacaoGetCursosByCod($codCurso);
            } else {
                $this->dados['curso'] = '';
            }

            $this->dados['cursos'] = $this->sistemasmodel->posgraduacaoCursosArray();
            $this->dados['pagina'] = 'sistemas/posgraduacao';
            $this->dados['titulo'] = 'alunos';
            $this->load->view('sistemas/posgraduacao', $this->dados);
        }
    }

    private function enviarCadPos($dados) {
        $html = ""
            . "<html>"
            . "<body>"
            . "<h3>UNIFG - CADASTRO PÓS-GRADUAÇÃO</h3>"
            . "<br><br>"
            . "Curso : " . $dados['pos_curso']
            . "<br>"
            . "Nome : " . $dados['pos_nome']
            . "<br>"
            . "Data Nascimento : " . $dados['pos_dataNascimento']
            . "<br>"
            . "Nacionalidade : " . $dados['pos_nacionalidade']
            . "<br>"
            . "Naturalidade : " . $dados['pos_naturalidade']
            . "<br>"
            . "Cpf : " . $dados['pos_cpf']
            . "<br>"
            . "RG : " . $dados['pos_rg']
            . "<br>"
            . "Orgao Emissor : " . $dados['pos_orgaoEmissor']
            . "<br>"
            . "Estado Civil : " . $dados['pos_estadoSivio']
            . "<br>"
            . "Emp Trabalho : " . $dados['pos_empTrabalho']
            . "<br>"
            . "Telefone : " . $dados['pos_telefone']
            . "<br>"
            . "Celular: " . $dados['pos_celular']
            . "<br>"
            . "Nome Pai: " . $dados['pos_pai']
            . "<br>"
            . "Nome Mãe: " . $dados['pos_mae']
            . "<br>"
            . "Endereco : " . $dados['pos_endereco']
            . "<br>"
            . "Cep : " . $dados['pos_cep']
            . "<br>"
            . "Email : " . $dados['pos_email']
            . "<br>"
            . "Graduacao : " . $dados['pos_graduacao']
            . "<br>"
            . "Instituicao : " . $dados['pos_instituicao']
            . "<br>"
            . "Forma de Pagamento : " . $dados['pos_formaPagamento']
            . "</body>"
            . "<html>";

        //echo $html;
        $pasta = APPPATH . 'third_party/PHPMailer-master/src/PHPMailer.php';
        $smtp = APPPATH . 'third_party/PHPMailer-master/src/SMTP.php';
        $exception = APPPATH . 'third_party/PHPMailer-master/src/Exception.php';

        require_once($pasta);
        require_once($smtp);
        require_once($exception);

        // var_dump($pasta);
        $mail = new PHPMailer\PHPMailer\PHPMailer();

        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->Host = "smtp.gmail.com";
        $mail->SMTPAuth = true;
        $mail->Username = "posgraduacao@centrouniversitariounifg.edu.br";
        $mail->Password = "gbi@2018";
        $mail->SMTPSecure = "tls";
        $mail->Port = 587;
        $mail->From = "posgraduacao@centrouniversitariounifg.edu.br";
        $mail->FromName = "CADASTRO POS-GRADUACAO";
        $mail->addAddress('posgraduacao@centrouniversitariounifg.edu.br', 'CADASTRO POS-GRADUACAO');
        $mail->isHTML(true);
        $mail->Subject = "CADASTRO POS-GRADUACAO";
        $mail->Body = $html;
        //$mail->AltBody = "This is the plain text version of the email content";
        if (!$mail->send()) {
            //echo "<br>Mailer Error: " . $mail->ErrorInfo;
        } else {
            //echo "E-mail enviado com sucesso.<br> <a href='" . site_url() . "'>Voltar<a>";
        }
    }

}
