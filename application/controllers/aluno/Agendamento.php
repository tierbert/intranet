<?php

/** CRUD Controller para alunos* */
class Agendamento extends CI_Controller {

    private $_tpl = 'indexAluno.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        //echo "<script>window.location.href='" . site_url() . "/aluno/indexaluno'</script>";
        $this->load->model('Agendamento__horariomodel', '', true);
        $this->load->model('Agendamento__agendadosmodel', '', true);
        $this->load->model('solicitacao_disciplinasmodel', '', true);
        $this->load->model('solicitacoesmodel', '', true);
        $this->load->model('horariomodel', '', true);
        $this->load->model('alunosmodel', '', true);

        $this->load->library(array('Util', 'pagination'));
    }

    public function index() {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');

        $agendado = $this->Agendamento__agendadosmodel->getByAluno($idAluno);

        $this->dados['idAluno'] = $idAluno;
        $this->dados['nomeAluno'] = $nomeAluno;
        $this->dados['matriculaAluno'] = $matriculaAluno;
        $this->dados['pagina'] = 'aluno/agendamento/agenda';
        $this->dados['titulo'] = 'alunos';
        //$this->load->view($this->_tpl, $this->dados);
        $this->load->view($this->_tpl, $this->dados);
    }

    public function agendar($tipo, $diaSelec = '') {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $idCurso = $this->session->userdata('alu_idCurso');
        $curso = $this->session->userdata('curso');

        $agendado = $this->Agendamento__agendadosmodel->getByAluno($idAluno, $tipo);
        //echo count($agendado);
        if (count($agendado) > 0) {
            // echo 'erro';
            echo "<script>alert('Agendamento já realizado para este setor');window.location.href='" . site_url() . "/aluno/agendamento'</script>";
            exit();
            //echo "<script>alert('Máximo de 2 agendamentos');window.location.href='" . site_url() . "/aluno/agendamento'</script>";
        }
        if ($this->input->post()) {
            $horario = $this->Agendamento__horariomodel->getByid($this->input->post('idHorario'));
            $dados = array(
                'aha_idAluno' => $idAluno,
                'aha_idHorario' => $this->input->post('idHorario')
            );
            $this->Agendamento__agendadosmodel->inserir($dados);
            if ($this->input->post('financeiro') == 1) {
                $this->agendarFinanceiro($idAluno, $horario->ahh_data);
            }
            echo "<script>window.location.href='" . site_url() . "/aluno/agendamento'</script>";
            exit();
        } else {

            $this->dados['diaSelec'] = $diaSelec;
            $this->dados['dias'] = $this->Agendamento__horariomodel->getTipoDias($tipo, $diaSelec);
            //$this->dados['horarios'] = $this->Agendamento__horariomodel->getByTipoDia($tipo, $diaSelec);

            $this->dados['horarios'] = $this->Agendamento__horariomodel->dias($tipo);

            $this->dados['curso'] = $curso;
            $this->dados['idCurso'] = $idCurso;
            $this->dados['tipo'] = $tipo;
            $this->dados['nomeAluno'] = $nomeAluno;
            $this->dados['matriculaAluno'] = $matriculaAluno;
            $this->dados['pagina'] = 'aluno/agendamento/agendar';
            $this->dados['titulo'] = 'alunos';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function agendarFinanceiro($idAluno, $data) {
        $agendadoFinan = $this->Agendamento__agendadosmodel->getByAluno($idAluno, 2);
        echo count($agendadoFinan);
        if (count($agendadoFinan) == 0) {
            $vagas = $this->Agendamento__agendadosmodel->vagas(2, $data);
            $total = $vagas->row('ahh_vagas') - $vagas->row('agendados');
            if ($total >= 0) {
                $dados = array(
                    'aha_idAluno' => $idAluno,
                    'aha_idHorario' => $vagas->row('ahh_id')
                );
                $this->Agendamento__agendadosmodel->inserir($dados);
            } else {
                echo "<script>alert('Não há vagas para o setor financeiro neste dia');</script>";
            }
        }
    }

    public function agend_cancel($idAgendamento) {
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->Agendamento__agendadosmodel->delete($idAgendamento);
            redirect('aluno/agendamento');
            return;
        } else {
            if ($_POST) {
                redirect('aluno/agendamento');
                return;
            }
            $object = $this->Agendamento__agendadosmodel->getById($idAgendamento);
        }

        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $this->dados['idAluno'] = $idAluno;
        $this->dados['nomeAluno'] = $nomeAluno;
        $this->dados['matriculaAluno'] = $matriculaAluno;
        $this->dados['pagina'] = 'aluno/agendamento/agend_del';
        $this->dados['titulo'] = 'agenda ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $idAgendamento;
        $this->load->view($this->_tpl, $this->dados);
    }

    public function verificaLogin() {
        $logado = $this->session->userdata('loginAluno');
        $idAluno = $this->session->userdata('idAluno');
        if ($logado != 'logado') {
            echo "<script>window.location.href='" . site_url() . "'</script>";
        }
    }

    private function agendarDia($tipo, $dia) {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $idCurso = $this->session->userdata('alu_idCurso');
        $curso = $this->session->userdata('curso');


        $horarios = $this->Agendamento__horariomodel->getByTipoDia($tipo, $dia);
        $this->dados['horarios'] = $horarios;

        $this->dados['tipo'] = $tipo;
        $this->dados['dia'] = $dia;
        $this->dados['curso'] = $curso;
        $this->dados['idCurso'] = $idCurso;
        $this->dados['tipo'] = $tipo;
        $this->dados['nomeAluno'] = $nomeAluno;
        $this->dados['matriculaAluno'] = $matriculaAluno;
        $this->dados['pagina'] = 'aluno/agendamento/agendarDia';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function agendaDia($idHorario) {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $idCurso = $this->session->userdata('alu_idCurso');
        $curso = $this->session->userdata('curso');

        $horario = $this->Agendamento__horariomodel->getByid($idHorario);


        if ($horario == NULL) {
            echo "horario nao encontrado";
            echo "<script>alert('Horário não encontrado');window.location.href='" . site_url() . "/aluno/agendamento'</script>";
            exit();
        }



        $tipo = $horario->ahh_tipo;
        $agendado = $this->Agendamento__agendadosmodel->getByAlunoTcc($idAluno, $tipo);
        if ($agendado->num_rows() > 1) {
            //echo "realizado";
            echo "<script>alert('Agendamento já realizado para este setor');window.location.href='" . site_url() . "/aluno/agendamento'</script>";
            exit();
        } else {
            $dados = array(
                'aha_idAluno' => $idAluno,
                'aha_idHorario' => $idHorario
            );
            $this->Agendamento__agendadosmodel->inserir($dados);
            if ($this->input->post('financeiro') == 1) {
                $this->agendarFinanceiro($idAluno, $horario->ahh_data);
            }
            echo "ok";
            echo "<script>alert('Agendamento realizado');window.location.href='" . site_url() . "/aluno/agendamento'</script>";
            exit();
        }
    }

}
