<?php

/** CRUD Controller para estag__locais* */
class EstagioPsic extends CI_Controller {

    private $_tpl = 'indexAluno.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('estag__locaismodel', '', true);
        $this->load->model('estag__agendadosmodel', '', true);
        $this->load->model('estag__vagasmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination'));
        //$this->auth->check_logged($this->router->class, $this->router->method);
        //echo "<script>alert('FINALIZADO');window.location.href='" . site_url() . "/aluno/indexaluno/'</script>";
    }

    /** Lista estag__locais * */
    public function index($inicio = 0) {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $curso = $this->session->userdata('curso');


        if (trim($curso) != 'PSICOLOGIA') {
            //echo "<script>alert('SOMENTE PARA ALUNOS DO CURSO DE PSICOLOGIA');window.location.href='" . site_url() . "/aluno/indexaluno/'</script>";
            //exit();
        }

        $this->dados['idAluno'] = $idAluno;
        $this->dados['nomeAluno'] = $nomeAluno;
        $this->dados['matriculaAluno'] = $matriculaAluno;

        // $this->dados['dados'] = $this->estag__locaismodel->all($inicio, $maximo);
        $this->dados['pagina'] = 'aluno/estagPsic/estagPsicIndex';
        $this->dados['titulo'] = 'estag__locais';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function aviso() {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $curso = $this->session->userdata('curso');

        if (trim($curso) != 'PSICOLOGIA') {
            echo "<script>alert('SOMENTE PARA ALUNOS DO CURSO DE PSICOLOGIA');window.location.href='" . site_url() . "/aluno/indexaluno/'</script>";
            exit();
        }


        $this->dados['pagina'] = 'aluno/estagPsic/estagPsicAviso';
        $this->dados['titulo'] = 'estag__locais';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function seleciona($tipo, $tipoEstagio) {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $this->dados['idAluno'] = $idAluno;
        $this->dados['nomeAluno'] = $nomeAluno;
        $this->dados['matriculaAluno'] = $matriculaAluno;
        if ($tipo == 1) {
            $this->dados['tipo'] = 'campo';
            $this->dados['tipoExt'] = 'Campo';
        } elseif ($tipo == 2) {
            $this->dados['tipo'] = 'orientacao';
            $this->dados['tipoExt'] = 'Orientação Acadêmica';
        }
        $this->dados['$tipoEstagio'] = $tipoEstagio;
        $this->dados['locais'] = $this->estag__locaismodel->getLocais($tipoEstagio);
        $this->dados['pagina'] = 'aluno/estagPsic/estagPsicSeleciona';
        $this->dados['titulo'] = 'estag__locais';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function selecionar($idVaga) {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');

        $tipo = $this->estag__vagasmodel->getById($idVaga);

        $vaga = $this->estag__agendadosmodel->getByAlunoTipo($idAluno, $tipo->ev_tipo);

        $agendados = $this->estag__agendadosmodel->agendadosVaga($idVaga, $tipo->ev_tipo);

        if (($tipo->ev_vagas - $agendados->num_rows()) == 0) {
            echo "alert('VAGAS ESGOTADAS');<script>window.location.href='" . site_url() . "/aluno/EstagioPsic/'</script>";
            exit();
        }

        echo $vaga->num_rows();
        if ($vaga->num_rows() > 0) {
            echo "<script>alert('RESERVA DE VAGA JÁ REALIZADA');window.location.href='" . site_url() . "/aluno/EstagioPsic/'</script>";
            exit();
        } else {

            $dadosVaga = array(
                'ea_idAluno' => $idAluno,
                'ea_idVaga' => $idVaga,
                'ea_status' => 'reservado',
            );
            $this->estag__agendadosmodel->inserir($dadosVaga);
            echo "<script>alert('VAGA RESERVADA');window.location.href='" . site_url() . "/aluno/EstagioPsic/'</script>";
        }
    }

    /** Cria novo(a)  * */
    public function criar() {
        if ($_POST) {
            $this->estag__locaismodel->inserir($_POST);
            echo "<script>window.location.href='" . site_url() . "/estag__locais/'</script>";
        } else {
            $this->dados['object'] = $this->estag__locaismodel;
            $this->dados['pagina'] = 'estag__locais/estag__locais_novo';
            $this->dados['titulo'] = 'estag__locais ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    /**
     * Edit 
     * */
    public function editar($id) {
        $msg = null;
        if ($_POST) {
            //$this->form_validation->set_rules('valid', '', 'required');
            // if ($this->form_validation->run() == TRUE) {
            //$object->save();
            $this->estag__locaismodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/estag__locais/'</script>";
            return;
//} else {
//$msg = 'Por favor preencha todos os dados';
// }
        }

        $this->dados['object'] = $this->estag__locaismodel->getById($id);
        $this->dados['pagina'] = 'estag__locais/estag__locais_edit';
        $this->dados['titulo'] = 'estag__locais ';
        $this->load->view($this->_tpl, $this->dados);
    }

    /**
     * Delete
     * */
    public function delete($id) {
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->estag__locaismodel->delete($id);
            echo "<script>window.location.href = '" . site_url() . "/estag__locais/'</script>";
            return;
        } else {
            if ($_POST) {
                redirect('estag__locais');
                return;
            }
            $object = $this->estag__locaismodel->getById($id);
        }

        $this->dados['pagina'] = 'estag__locais/estag__locais_del';
        $this->dados['titulo'] = 'estag__locais ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $id;
        $this->load->view($this->_tpl, $this->dados);
    }

    public function verificaLogin() {
        $logado = $this->session->userdata('loginAluno');
        $idAluno = $this->session->userdata('idAluno');
        if ($logado != 'logado') {
            echo "<script>window.location.href='" . site_url() . "'</script>";
            exit();
        }
    }

}
