<?php

/** CRUD Controller para alunos* */
class Solicitacoesalu extends CI_Controller {

    private $_tpl = 'indexAluno.php';
    private $dados = array();
    private $semestre;

    public function __construct() {
        parent::__construct();
        $this->load->model('solicitacao_disciplinasmodel', '', true);
        $this->load->model('solicitacoesmodel', '', true);
        $this->load->model('horariomodel', '', true);
        $this->load->model('alunosmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination'));

        $this->semestre = '2019-1';
    }

    public function index() {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $solicitacoes = $this->solicitacoesmodel->listarAluno($idAluno );
        $this->dados['nomeAluno'] = $nomeAluno;
        $this->dados['matriculaAluno'] = $matriculaAluno;
        $this->dados['solicitacoes'] = $solicitacoes;
        $this->dados['pagina'] = 'aluno/solicitacoes/solicitAluno';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function verificaLogin() {
        $logado = $this->session->userdata('loginAluno');
        $idAluno = $this->session->userdata('idAluno');
        if ($logado != 'logado') {
            echo "<script>window.location.href='" . site_url() . "'</script>";
            exit();
        }
    }

    private function listarSolicitacoes() {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');
        $solicitacoes = $this->solicitacoesmodel->listarAluno($idAluno);
        $nomeAluno = $this->session->userdata('alu_nome');
        $this->dados['nomeAluno'] = $nomeAluno;
        $this->dados['solicitacoes'] = $solicitacoes;
        $this->dados['pagina'] = 'aluno/solicitacoes/listarSolicAlu';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function solicitacao() {
        $this->verificaLogin();
        //echo "<script>alert('Solicitaçoes encerradas');window.location.href='" . site_url('aluno/solicitacoesalu') . "'</script>";
        $this->dados['pagina'] = 'aluno/solicitacoes/aviso';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
        //exit();
    }

    public function solicitacao20191() {
        $this->verificaLogin();

        if ($this->input->post()) {
            //$disciplinas = $this->horariomodel->solicitacao($this->input->post());
            //$this->dados['post'] = $this->input->post();
            //echo "<script>window.location.href='" . site_url('aluno/solicitacoesalu/finalizaSolic') . "'</script>";
            exit();
        } else {
            $disciplinas = $this->horariomodel->solicitacao(NULL);

            $selecionadas = $this->session->userdata('disciplinas');

            if ($selecionadas == NULL) {
                $this->inicia();
            }

            //print_r($disciplinas->result_array());

            $nomeAluno = $this->session->userdata('alu_nome');
            $matriculaAluno = $this->session->userdata('alu_matricula');
            $this->dados['nomeAluno'] = $nomeAluno;
            $this->dados['matriculaAluno'] = $matriculaAluno;

            $this->dados['selecionadas'] = $selecionadas;
            $professores = $this->horariomodel->professores();
            $this->dados['professores'] = $professores;
            $this->dados['disciplinas'] = $disciplinas;
            $this->dados['pagina'] = 'aluno/solicitacoes/solic_nov_aluno';
            $this->dados['titulo'] = 'alunos';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function inicia() {
        $this->verificaLogin();
        $this->session->set_userdata(
                array(
                    'disciplinas' => array()
                )
        );
//echo "<script>window.location.href='" . site_url('aluno/solicitacao') . "'</script>";
    }

    public function finalizaSolic() {
        $this->verificaLogin();
        //if ($this->input->post()) {
        //print_r($this->session->userdata('disciplinas'));
        $discplinas = $this->session->userdata('disciplinas');
        if (count($discplinas) == 0) {
            //echo '0';
            echo "<script>alert('Nenhuma disciplina selecionada');window.location.href='" . site_url('aluno/solicitacoesalu/solicitacao') . "'</script>";
            exit();
        }
        $idAluno = $this->session->userdata('alu_id');
        $idSolicitacao = $this->solicitacoesmodel->inserir($discplinas, $idAluno);
        $this->session->unset_userdata('disciplinas');
        echo "<script>window.location.href='" . site_url('aluno/solicitacoesalu/solicFinaliza/' . $idSolicitacao) . "'</script>";
        /*
          } else {
          echo "<script>alert('Nenhuma disciplina selecionada');window.location.href='" . site_url('aluno/solicitacoesalu/solicitacao') . "'</script>";
          }
         * 
         */
    }

    public function solicFinaliza($idSolicitacao) {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');
        $confere = $this->solicitacoesmodel->getBy("sol_id = $idSolicitacao and sol_idAluno = $idAluno");
        if ($confere->num_rows() != 0) {
            $nomeAluno = $this->session->userdata('alu_nome');
            $matriculaAluno = $this->session->userdata('alu_matricula');
            $this->dados['nomeAluno'] = $nomeAluno;
            $this->dados['matriculaAluno'] = $matriculaAluno;
            $this->dados['idSolicitacao'] = $idSolicitacao;
            $this->dados['pagina'] = 'aluno/solicitacoes/solicFinaliza';
            $this->dados['titulo'] = 'alunos';
            $this->load->view($this->_tpl, $this->dados);
        } else {
            echo "<script>window.location.href='" . site_url('aluno/') . "'</script>";
        }
    }

    public function cancelSolic($idSolicitacao) {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');
        $confere = $this->solicitacoesmodel->getBy("sol_id = $idSolicitacao and sol_idAluno = $idAluno");
        if ($confere->num_rows() != 0) {
            if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
                $id = $this->input->post('id');
                $this->solicitacoesmodel->cancelaAluno($id);
                echo "<script>alert('Solicitacao cancelada');window.location.href = '" . site_url('aluno/Solicitacoesalu') . "'</script>";
            } else {
                if ($_POST) {
                    echo "<script>window.location.href='" . site_url('aluno/Solicitacoesalu') . "'</script>";
                    return;
                }
                $this->dados['idSolicitacao'] = $idSolicitacao;
                $this->dados['pagina'] = 'aluno/solicitacoes/cancelSolic';
                $this->dados['titulo'] = 'alunos';
                $this->load->view($this->_tpl, $this->dados);
            }
        } else {
            echo "<script>window.location.href='" . site_url('aluno/') . "'</script>";
        }
    }

    public function comprovante($idSolicitacao) {
        $this->verificaLogin();
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $idAluno = $this->session->userdata('alu_id');
        $confere = $this->solicitacoesmodel->getBy("sol_id = $idSolicitacao and sol_idAluno = $idAluno");

        if ($confere->num_rows() == 0) {
            echo "<script>alert('Erro ao imprimir comprovante');window.location.href='" . site_url('aluno') . "'</script>";
        } else {
            $this->dados['nomeAluno'] = $nomeAluno;
            $this->dados['matriculaAluno'] = $matriculaAluno;
            $disciplinas = $this->solicitacao_disciplinasmodel->getBySolic($idSolicitacao);
            $this->dados['idSolicitacao'] = $idSolicitacao;
            $this->dados['disciplinas'] = $disciplinas;
            $this->dados['pagina'] = 'aluno/solicitacoes/comprovante';
            $this->dados['titulo'] = 'alunos';

            $mpdf = new mPDF();
            $html = $this->load->view('aluno/solicitacoes/comprovante', $this->dados, TRUE);
            $mpdf->SetFooter('{PAGENO}');
            $mpdf->writeHTML($html);
            $mpdf->Output();
        }
    }

    public function comprovanteReimprime() {
        $this->verificaLogin();

        $idSolicitacao = $this->input->post('id');
        $idAluno = $this->session->userdata('alu_id');
        $confere = $this->solicitacoesmodel->getBy("sol_id = $idSolicitacao and sol_idAluno = $idAluno");

        if ($confere->num_rows() == 0) {
            echo "<script>alert('Erro ao imprimir comprovante');window.location.href='" . site_url('aluno') . "'</script>";
        } else {

            $nomeAluno = $this->session->userdata('alu_nome');
            $matriculaAluno = $this->session->userdata('alu_matricula');
            $this->dados['nomeAluno'] = $nomeAluno;
            $this->dados['matriculaAluno'] = $matriculaAluno;
            $disciplinas = $this->solicitacao_disciplinasmodel->getBySolic($idSolicitacao);
            $this->dados['idSolicitacao'] = $idSolicitacao;
            $this->dados['disciplinas'] = $disciplinas;
            $this->dados['pagina'] = 'aluno/comprovante';
            $this->dados['titulo'] = 'alunos';

            $mpdf = new mPDF();
            $html = $this->load->view('aluno/comprovante', $this->dados, TRUE);
            $mpdf->SetFooter('{PAGENO}');
            $mpdf->writeHTML($html);
            $mpdf->Output();
        }
    }

    /*
      public function senha() {
      $this->verificaLogin();
      //$this->auth->liberado('TI|secretaria');
      if ($_POST) {
      $idAluno = $this->session->userdata('alu_id');
      $_POST['alu_trocaSenha'] = 1;
      $this->alunosmodel->update($_POST, $idAluno);
      echo "<script>window.location.href='" . site_url('aluno') . "'</script>";
      return;
      }

      //$this->dados['object'] = $this->alunosmodel->getById($id);
      $this->dados['alu_id'] = $this->session->userdata('alu_id');
      $this->dados['pagina'] = 'aluno/senhaAlu';
      $this->dados['titulo'] = 'alunos ';
      $this->load->view($this->_tpl, $this->dados);
      }
     * 
     */

    public function api_getDisciplinas() {
        $this->verificaLogin();
        $turma = $this->input->post('turma');

        $selecionadas = $this->session->userdata('disciplinas');

        $disciplinas = $this->db
                ->where('hor_semestre', $this->semestre)
                ->where('hor_turno', $turma)
                ->get('horario');

        foreach ($disciplinas->result() as $disc) {
            if (in_array($disc->hor_id, $selecionadas)) {
                $botao = '<td>Selecionado</td>';
            } else {
                $botao = "<td> <a onclick='add({$disc->hor_id})' class='btn btn-info' style='margin: 10px;'><span class='glyphicon glyphicon-plus'></span></a></td>";
            }
            echo '<tr>';
            echo '<td><strong>' . $disc->hor_disciplina . '</strong><br> PROFESSOR: ' . $disc->hor_prof . '</td>';
            //echo "<td> <a onclick='add({$disc->hor_id})' class='btn btn-info' style='margin: 10px;'><span class='glyphicon glyphicon-plus'></span></a></td>";
            echo $botao;
            echo '</tr>';
        }
    }

    public function api_getSelecionadas() {
        $this->verificaLogin();

        $discplinas = $this->session->userdata('disciplinas');

        foreach ($discplinas as $disc) {
            $disciplina = $this->horariomodel->getById($disc);
            //echo $disc;
            echo '<tr>';
            echo '<td class="discSelecionada"><strong>' . $disciplina->hor_disciplina . '</strong><br> PROFESSOR: ' . $disciplina->hor_prof . '</td>';
            echo "<td>{$disciplina->hor_turno}</td>";
            echo "<td> <a onclick='remove({$disc})' class='btn btn-danger' style='margin: 0px;'><span class='glyphicon glyphicon-trash'></span></a></td>";
            echo '</tr>';
        }
    }

    public function api_add() {
        $this->verificaLogin();
        $idDisciplina = $this->input->post('idDisciplina');
        $discplinas = $this->session->userdata('disciplinas');
        if (!in_array($idDisciplina, $discplinas)) {
            array_push($discplinas, $idDisciplina);
            $this->session->set_userdata(array('disciplinas' => $discplinas));
        }
    }

    public function api_remove() {
        $this->verificaLogin();
        $idDisciplina = $this->input->post('idDisciplina');
        $discplinas = $this->session->userdata('disciplinas');

        $key = array_search($idDisciplina, $discplinas);
        unset($discplinas[$key]);
        $this->session->set_userdata(
                array(
                    'disciplinas' => $discplinas
                )
        );
    }

}
