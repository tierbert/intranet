<?php

/** CRUD Controller para alunos* */
class Esportelazer extends CI_Controller {

    private $_tpl = 'indexAluno.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('esportemodel', '', true);
        $this->load->model('alunosmodel', '', true);
        $this->load->library(array('Util', 'pagination'));
    }

    public function index() {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');

        $this->dados['reservas'] = $this->esportemodel->getReservasByAluno($idAluno);

        $this->dados['idAluno'] = $idAluno;
        $this->dados['nomeAluno'] = $nomeAluno;
        $this->dados['matriculaAluno'] = $matriculaAluno;
        $this->dados['pagina'] = 'aluno/esportelazer/agenda';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function agendar() {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $idCurso = $this->session->userdata('alu_idCurso');
        $curso = $this->session->userdata('curso');
        if ($this->input->post()) {
            $post = $this->input->post();
            $agenda = $this->esportemodel->getByDataAluno(($post['eag_data']), $idAluno);
            //echo $agenda->num_rows();
            if ($agenda->num_rows() > 0) {
                echo "<script>alert('RESERVA NÃO REALIZADA. É PERMITIDO APENAS 01 (UMA) RESERVA POR DIA.');window.location.href='" . site_url('aluno/esportelazer/agendar') . "'</script>";
                exit();
            } else {
                $strTime = strtotime(date('Y-m-d'));
                $dataTime = strtotime($post['eag_data']);
                if ($dataTime < $strTime) {
                    echo "<script>alert('NÃO É PERMITIDO RESERVA EM DATA RETROATIVA');window.location.href='" . site_url('aluno/esportelazer/agendar') . "'</script>";
                    exit();
                }
                $post['eag_idAluno'] = $idAluno;
                $this->esportemodel->inserirAgenda($post);
                echo "<script>alert('HORÁRIO RESERVADO');window.location.href='" . site_url('aluno/esportelazer') . "'</script>";
            }
        } else {
            //$this->dados['semana'] = $semana;
            $this->dados['turnos'] = $this->esportemodel->getTurnos();
            $this->dados['curso'] = $curso;
            $this->dados['idCurso'] = $idCurso;
            $this->dados['nomeAluno'] = $nomeAluno;
            $this->dados['matriculaAluno'] = $matriculaAluno;
            $this->dados['pagina'] = 'aluno/esportelazer/agendar';
            $this->dados['titulo'] = 'alunos';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function agendaConfirm($idHorario) {
        $this->verificaLogin();
        $dadosHorario = $this->esportemodel->getReservaById($idHorario);
        $data = $dadosHorario->ere_data;

        //echo date('W', strtotime($data));
        //echo date('w');

        /* pega os dias da semana atual */
        $day = date('w', strtotime($data));
        $domingo = date('Y-m-d', strtotime($data . '+' . (0 - $day) . ' days')); //segunda
        $sabado = date('Y-m-d', strtotime($data . '+' . (6 - $day) . ' days')); //sabado
        //$domingo = date('m-d-Y', strtotime('-' . $day . ' days'));
        //echo '<hr>';
        //echo $domingo . ' - ' . $sabado;
        //echo '<hr>';

        $idAluno = $this->session->userdata('alu_id');
        $agendados = $this->esportemodel->getBySemana($idAluno, $domingo, $sabado);

        //echo '<hr>';
        //echo $agendados->num_rows();

        if ($agendados->num_rows() > 0) {
            echo "<script>alert('RESERVA NÃO REALIZADA. É PERMITIDO APENAS 01 (UMA) RESERVA POR SEMANA.');window.location.href='" . site_url('aluno/esportelazer') . "'</script>";
        } else {
            if ($this->input->post()) {
                $this->esportemodel->reservar($idHorario, $idAluno);
                echo "<script>window.location.href='" . site_url('aluno/esportelazer') . "'</script>";
            } else {
                $nomeAluno = $this->session->userdata('alu_nome');
                $matriculaAluno = $this->session->userdata('alu_matricula');
                $idCurso = $this->session->userdata('alu_idCurso');
                $curso = $this->session->userdata('curso');

                //$this->dados['horarios'] = $this->esportemodel->getHorariosReserva();
                $this->dados['idhorario'] = $idHorario;
                $this->dados['curso'] = $curso;
                $this->dados['idCurso'] = $idCurso;
                $this->dados['nomeAluno'] = $nomeAluno;
                $this->dados['matriculaAluno'] = $matriculaAluno;
                $this->dados['pagina'] = 'aluno/esportelazer/agendaConfirm';
                $this->dados['titulo'] = 'alunos';
                $this->load->view($this->_tpl, $this->dados);
            }
        }
    }

    public function verificaLogin() {
        $logado = $this->session->userdata('loginAluno');
        $idAluno = $this->session->userdata('idAluno');
        if ($logado != 'logado') {
            echo "<script>window.location.href='" . site_url() . "'</script>";
        }
    }

}
