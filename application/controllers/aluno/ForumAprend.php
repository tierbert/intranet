<?php

/** CRUD Controller para alunos* */
class ForumAprend extends CI_Controller {

    private $_tpl = 'indexAluno.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('forunscppmodel', 'fcpp', true);
        $this->load->model('alunosmodel', '', true);
        $this->load->library(array('Util', 'pagination'));
    }

    public function index() {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $this->dados['idAluno'] = $idAluno;
        $this->dados['nomeAluno'] = $nomeAluno;
        $this->dados['matriculaAluno'] = $matriculaAluno;
        $this->dados['agendas'] = $this->fcpp->getByAluno($idAluno);
        $this->dados['pagina'] = 'aluno/forumAprend/agendaForAprend';
        $this->dados['titulo'] = 'alunos';
        //$this->load->view($this->_tpl, $this->dados);
        $this->load->view($this->_tpl, $this->dados);
    }

    public function agendarForAprend($semana = '') {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $idCurso = $this->session->userdata('alu_idCurso');
        $curso = $this->session->userdata('curso');

        if ($this->input->post()) {
            $post = $this->input->post();

            $data = date('Y-m-d H:i:s', $post['tca_data']);
            $semana = date('W', $post['tca_data']);

            $post['tca_semana'] = $semana;
            $post['tca_data'] = $data;
            $post['tca_idAluno'] = $idAluno;

            $idHorario = $post['tca_idHorario'];
            $dadosHorario = $this->fcpp->getHorById($idHorario);
            $vagas = $dadosHorario->hor_vagas;
            $agendados = $this->fcpp->getByHorarioData($idHorario, $data);

            echo $agendados->num_rows() . '<br>';
            if ($agendados->num_rows() >= $vagas) {
                echo 'erro vagas';
                echo "<script>alert('Apenas um agendamento na mesma semana');window.location.href='" . site_url() . "/aluno/forumAprend/agendarTcc'</script>";
            } else {
                echo 'ok';
                $this->fcpp->alunHorCad($post);
                echo "<script>alert('HORARIO AGENDADO');window.location.href='" . site_url('aluno/forumAprend') . "'</script>";
            }
            //print_r($post);
        } else {
            $this->dados['semana'] = $semana;
            $this->dados['turnos'] = $this->fcpp->getTurnos('APRENDIZAGEM');
            $this->dados['curso'] = $curso;
            $this->dados['idCurso'] = $idCurso;
            $this->dados['idAluno'] = $idAluno;
            $this->dados['nomeAluno'] = $nomeAluno;
            $this->dados['matriculaAluno'] = $matriculaAluno;
            $this->dados['pagina'] = 'aluno/forumAprend/agendarForAprend';
            $this->dados['titulo'] = 'alunos';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function agend_cancel($idAgendamento) {
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->Agendamento__agendadosmodel->delete($idAgendamento);
            redirect('aluno/agendamento');
            return;
        } else {
            if ($_POST) {
                redirect('aluno/agendamento');
                return;
            }
            $object = $this->Agendamento__agendadosmodel->getById($idAgendamento);
        }

        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $this->dados['idAluno'] = $idAluno;
        $this->dados['nomeAluno'] = $nomeAluno;
        $this->dados['matriculaAluno'] = $matriculaAluno;
        $this->dados['pagina'] = 'aluno/agendamento/agend_del';
        $this->dados['titulo'] = 'agenda ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $idAgendamento;
        $this->load->view($this->_tpl, $this->dados);
    }

    public function verificaLogin() {
        $logado = $this->session->userdata('loginAluno');
        $idAluno = $this->session->userdata('idAluno');
        if ($logado != 'logado') {
            echo "<script>window.location.href='" . site_url() . "'</script>";
        }
    }

    public function agendarHorarioTcc($idHorario) {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $idCurso = $this->session->userdata('alu_idCurso');
        $curso = $this->session->userdata('curso');

        $horario = $this->tch->getById($idHorario);
        //echo $horario->tch_numSemana;
        $agendados = $this->tca->getByAluSemana($idAluno, $horario->tch_numSemana);

        if ($agendados->num_rows() >= 1) {
            echo "<script>alert('Apenas um agendamento na mesma semana');window.location.href='" . site_url() . "/aluno/forumAprend/agendarTcc'</script>";
        } else {
            $this->tca->inserir($idAluno, $idHorario);
            echo "<script>alert('Agendamento realizado');window.location.href='" . site_url() . "/aluno/tcc'</script>";
        }
    }

}
