<?php

/** CRUD Controller para alunos* */
class Cpp extends CI_Controller {

    private $_tpl = 'indexAluno.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('cpp__horariosmodel', 'tch', true);
        $this->load->model('cpp__agendadosmodel', 'tca', true);
        $this->load->model('alunosmodel', '', true);
        $this->load->library(array('Util', 'pagination'));
    }

    public function index() {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $this->dados['idAluno'] = $idAluno;
        $this->dados['nomeAluno'] = $nomeAluno;
        $this->dados['matriculaAluno'] = $matriculaAluno;
        $this->dados['pagina'] = 'aluno/cpp/agendaTcc';
        $this->dados['titulo'] = 'alunos';
        //$this->load->view($this->_tpl, $this->dados);
        $this->load->view($this->_tpl, $this->dados);
    }

    public function agendarCpp() {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $idCurso = $this->session->userdata('alu_idCurso');
        $curso = $this->session->userdata('curso');

        $this->dados['horarios'] = $this->tch->getHorarios();
        $this->dados['curso'] = $curso;
        $this->dados['idCurso'] = $idCurso;
        $this->dados['nomeAluno'] = $nomeAluno;
        $this->dados['matriculaAluno'] = $matriculaAluno;
        $this->dados['pagina'] = 'aluno/cpp/agendarTcc';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function agend_cancel($idAgendamento) {
        $this->verificaLogin();
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->Agendamento__agendadosmodel->delete($idAgendamento);
            redirect('aluno/agendamento');
            return;
        } else {
            if ($_POST) {
                redirect('aluno/agendamento');
                return;
            }
            $object = $this->Agendamento__agendadosmodel->getById($idAgendamento);
        }

        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $this->dados['idAluno'] = $idAluno;
        $this->dados['nomeAluno'] = $nomeAluno;
        $this->dados['matriculaAluno'] = $matriculaAluno;
        $this->dados['pagina'] = 'aluno/agendamento/agend_del';
        $this->dados['titulo'] = 'agenda ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $idAgendamento;
        $this->load->view($this->_tpl, $this->dados);
    }

    public function verificaLogin() {
        $logado = $this->session->userdata('loginAluno');
        $idAluno = $this->session->userdata('idAluno');
        if ($logado != 'logado') {
            echo "<script>window.location.href='" . site_url() . "'</script>";
        }
    }

    public function agendarHorarioCpp($idHorario) {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $idCurso = $this->session->userdata('alu_idCurso');
        $curso = $this->session->userdata('curso');

        $horario = $this->tch->getById($idHorario);
        //echo $horario->tch_numSemana;
        $agendados = $this->tca->getByAlunoHorario($idAluno, $idHorario);

        if ($agendados->num_rows() >= 1) {
            //echo '1';
            echo "<script>alert('JÁ AGENDADO PARA ESTE HORÁRIO');window.location.href='" . site_url() . "/aluno/cpp/agendarCpp'</script>";
        } else {
            $this->tca->inserir($idAluno, $idHorario);
            echo "<script>alert('Agendamento realizado');window.location.href='" . site_url() . "/aluno/cpp'</script>";
        }
    }

}
