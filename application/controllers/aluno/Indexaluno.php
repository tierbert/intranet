<?php

/** CRUD Controller para alunos* */
class Indexaluno extends CI_Controller {

    private $_tpl = 'indexAluno.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('solicitacao_disciplinasmodel', '', true);
        $this->load->model('solicitacoesmodel', '', true);
        $this->load->model('horariomodel', '', true);
        $this->load->model('alunosmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination'));
        //$this->auth->check_logged($this->router->class, $this->router->method);
    }

    public function index() {
        $this->verificaLogin();
        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $solicitacoes = $this->solicitacoesmodel->listarAluno($idAluno);
        $this->dados['nomeAluno'] = $nomeAluno;
        $this->dados['matriculaAluno'] = $matriculaAluno;
        $this->dados['solicitacoes'] = $solicitacoes;
        $this->dados['pagina'] = 'aluno/inicioAluno';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function verificaLogin() {
        $logado = $this->session->userdata('loginAluno');
        $idAluno = $this->session->userdata('idAluno');
        if ($logado != 'logado') {
            echo "<script>window.location.href='" . site_url() . "'</script>";
        }
    }

    function sair() {
        $this->session->sess_destroy();
        echo "<script>window.location.href='" . site_url() . "'</script>";
    }

    public function senha() {
        $this->verificaLogin();
        //$this->auth->liberado('TI|secretaria');
        if ($_POST) {
            $idAluno = $this->session->userdata('alu_id');
            $_POST['alu_trocaSenha'] = 1;
            $this->alunosmodel->update($_POST, $idAluno);
            echo "<script>window.location.href='" . site_url('aluno/indexaluno') . "'</script>";
            return;
        }

        $idAluno = $this->session->userdata('alu_id');
        $nomeAluno = $this->session->userdata('alu_nome');
        $matriculaAluno = $this->session->userdata('alu_matricula');
        $this->dados['nomeAluno'] = $nomeAluno;
        $this->dados['matriculaAluno'] = $matriculaAluno;
        $this->dados['alu_id'] = $this->session->userdata('alu_id');
        $this->dados['pagina'] = 'aluno/senhaAlu';
        $this->dados['titulo'] = 'alunos ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function alunoVideos() {
        $this->verificaLogin();
        $this->dados['pagina'] = 'aluno/alunosVideos';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function docsEstagio() {
        $this->verificaLogin();
        $this->dados['pagina'] = 'aluno/docsEstagio';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function mestrado() {
        /*
          $url = 'http://www.revistadostribunais.com.br/maf/api/v1/authenticate.json?sp=GUANAMBI-2';
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_HEADER, 0);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_URL, $url);
          $data = curl_exec($ch);
          $dados = json_decode($data);
          curl_close($ch);
         */

        $url = 'http://www.revistadostribunais.com.br/maf/api/v1/authenticate.json?sp=GUANAMBI-2';
        $json = file_get_contents($url);
        $dados = json_decode($json);

        if (isset($dados->error)) {
            echo "<script>alert('OCORREU UM ERRO AO ACESSAR O SERVIÇO, POR FAVOR TENTE NOVAMENTE EM ALGUNS INSTANTES');window.location.href='" . site_url('aluno/indexaluno') . "'</script>";
        } else {
            $token = $dados->token;
            $urlRevista = "http://www.revistadostribunais.com.br/maf/app/authentication/signon?sso-token=$token";
            //echo $urlRevista;
            echo "<script>window.location.href='" . $urlRevista . "'</script>";
        }
    }

    public function docsMaterial() {
        $this->verificaLogin();
        $this->dados['pagina'] = 'aluno/docsMaterial';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function docsMaterialCurso() {
        $this->verificaLogin();
        $this->dados['pagina'] = 'aluno/docsMaterialCurso';
        $this->dados['titulo'] = 'alunos';
        $this->load->view($this->_tpl, $this->dados);
    }

}
