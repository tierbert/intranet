<?php

/** CRUD Controller para estag__vagas* */
class Estag__vagas extends CI_Controller {

    private $_tpl = 'index.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('estag__vagasmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination'));
        //$this->auth->check_logged($this->router->class, $this->router->method);
    }

    /** Lista estag__vagas * */
    public function index($inicio = 0) {
        $maximo = 10;
        $config['base_url'] = site_url('/estag__vagas/index');
        $config['total_rows'] = $this->estag__vagasmodel->contaRegistros();
        $config['per_page'] = $maximo;
        $this->pagination->initialize($config);
        $this->dados['paginacao'] = $this->pagination->create_links();

        $this->dados['dados'] = $this->estag__vagasmodel->all($inicio, $maximo);
        $this->dados['pagina'] = 'estag__vagas/estag__vagas_list';
        $this->dados['titulo'] = 'estag__vagas';
        $this->load->view($this->_tpl, $this->dados);
    }

    /** Cria novo(a)  * */
    public function criar() {
        if ($_POST) {
            $this->estag__vagasmodel->inserir($_POST);
            echo "<script>window.location.href='" . site_url() . "/estag__vagas/'</script>";
        } else {
            $this->dados['object'] = $this->estag__vagasmodel;
            $this->dados['pagina'] = 'estag__vagas/estag__vagas_novo';
            $this->dados['titulo'] = 'estag__vagas ';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function editar($id) {
        $msg = null;
        if ($_POST) {
            $this->estag__vagasmodel->update($_POST, $id);
            echo "<script>window.location.href = '" . site_url() . "/estag__vagas/'</script>";
            return;
        }

        $this->dados['object'] = $this->estag__vagasmodel->getById($id);
        $this->dados['pagina'] = 'estag__vagas/estag__vagas_edit';
        $this->dados['titulo'] = 'estag__vagas ';
        $this->load->view($this->_tpl, $this->dados);
    }

    public function delete($id) {
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->estag__vagasmodel->delete($id);
            echo "<script>window.location.href = '" . site_url() . "/estag__vagas/'</script>";
            return;
        } else {
            if ($_POST) {
                redirect('estag__vagas');
                return;
            }
            $object = $this->estag__vagasmodel->getById($id);
        }

        $this->dados['pagina'] = 'estag__vagas/estag__vagas_del';
        $this->dados['titulo'] = 'estag__vagas ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $id;
        $this->load->view($this->_tpl, $this->dados);
    }

}
