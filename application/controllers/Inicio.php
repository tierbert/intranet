<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

    private $_tpl = 'index.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('professoresmodel', '', true);
        $this->load->model('alunosmodel', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('Util', 'pagination'));
        //$this->auth->check_logged($this->router->class, $this->router->method);
        //$this->config->set_item('base_url','http://10.1.1.200/intranet') ;
    }

    public function index($login = '') {

        //echo $_SERVER['REMOTE_ADDR'];
        /*
          $ip = $_SERVER['REMOTE_ADDR'];
          $details = json_decode(file_get_contents("http://ipinfo.io/json"));
          //echo $details->city; // -> "Mountain View"
          var_dump($details);
          echo $details->ip;
         */

        if ($this->input->post()) {
            $tipo = $this->input->post('tipo');

            if ($tipo == 1) {
                $this->loginAl();
            } elseif ($tipo == 2) {
                $this->loginProf();
            } elseif ($tipo == 3) {
                $this->loginAdm();
            }
        } else {
            $this->dados['login'] = $login;
            //$this->dados['pagina'] = 'inicio/indexInicio';
            $this->dados['titulo'] = 'alunos';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    public function loginAluno($agenda = '') {
        if ($this->input->post()) {
            $this->loginAl();
        } else {
            $this->dados['agenda'] = $agenda;
            $this->dados['pagina'] = 'inicio';
            $this->dados['titulo'] = 'alunos';
            $this->load->view($this->_tpl, $this->dados);
        }
    }

    private function loginAl() {
        $matricula = $this->input->post('matricula');
        $senha = $this->input->post('senha', true);

        $matricula = addslashes($matricula);
        $senha = addslashes($senha);

        $this->db->where("alu_matricula", $matricula);
        $verificaMatricula = $this->db->get('alunos');
        if ($verificaMatricula->num_rows() == 0) {
            echo "<script>alert('Matrícula não encontrada, verifique se você digitou corretamente a sua matrícula, caso o erro persista, procure a secretaria!');"
            . "window.location.href='" . site_url() . "'</script>";
        }

        $this->db->join('cursos', 'alu_idCurso = cur_codigo', 'left');
        $this->db->where("alu_matricula", $matricula);
        $this->db->where("alu_senha", $senha);
        $usuarios = $this->db->get('alunos')->row();

        if ($usuarios != NULL) {
            $this->session->set_userdata(
                    array(
                        'loginAluno' => 'logado',
                        'alu_id' => $usuarios->alu_id,
                        'alu_nome' => $usuarios->alu_nome,
                        'alu_matricula' => $usuarios->alu_matricula,
                        'alu_idCurso' => $usuarios->alu_idCurso,
                        'curso' => $usuarios->cur_nome,
                        'estagiario' => $usuarios->alu_estagiario,
                    )
            );

            $troca = $usuarios->alu_trocaSenha;
            if ($troca == 0) {
                echo "<script>alert('Prezado aluno você deverá alterar a sua senha para o primeiro acesso');"
                . "window.location.href='" . site_url('aluno/indexaluno/senha') . "'</script>";
            } else {
                echo "<script>window.location.href='" . site_url('aluno/indexaluno') . "'</script>";
            }
        } else {
            $this->session->unset_userdata('login');
            echo "<script>alert('Nº de matricula ou senha incorretos');"
            . "window.location.href='" . site_url() . "'</script>";
        }
    }

    public function loginAdm() {
        if ($this->input->post()) {
            $login = $this->input->post('matricula');
            $senha = $this->input->post('senha', true);

            $login = addslashes($login);
            $senha = addslashes($senha);

            $this->db->where("usu_login", $login);
            $this->db->where("usu_senha", $senha);
            $this->db->join("rh__setores", 'usu_setor = set_id', 'left');
            $usuarios = $this->db->get('usuarios')->row();
            //echo $this->db->last_query();
            //var_dump($usuarios);

            if ($usuarios != NULL) {
                $this->session->set_userdata(
                        array(
                            'loginPainelAdm' => 'loginPainelAdm',
                            'loginTipo' => 'adm',
                            'usu_id' => $usuarios->usu_id,
                            'usu_nome' => $usuarios->usu_nome,
                            'usu_perfil' => $usuarios->usu_perfil,
                            'setor' => $usuarios->usu_setor,
                            'setorNome' => $usuarios->set_nome,
                            'grupo' => $usuarios->usu_grupo,
                        )
                );

                $troca = $usuarios->usu_trocaSenha;
                //echo $troca;
                if ($troca == 0) {
                    echo "<script>alert('Prezado colaborador você deverá alterar a sua senha para o primeiro acesso');"
                    . "window.location.href='" . site_url('adm/painel/senha') . "'</script>";
                } else {
                    echo "<script>window.location.href='" . site_url() . "/adm/painel'</script>";
                }
            } else {
                //echo 'erro';
                $this->session->sess_destroy();
                echo "<script>alert('Login ou senha incorretos');window.location.href='" . site_url() . "'</script>";
            }
        } else {
            $this->dados['pagina'] = 'adm/painel/inicioPainel';
            $this->dados['titulo'] = 'alunos';
            $this->load->view('adm/painel/inicioPainel', $this->dados);
        }
    }

    public function loginProf() {
        if ($this->input->post()) {
            $login = $this->input->post('matricula', TRUE);
            $senha = $this->input->post('senha', true);

            $login = addslashes($login);
            $senha = addslashes($senha);

            $usuarios = $this->professoresmodel->login($login, $senha);

            if ($usuarios != NULL) {
                $this->session->set_userdata(
                        array(
                            'loginPainelProf' => 'loginPainelProf',
                            'loginTipo' => 'professor',
                            'usu_id' => $usuarios->pro_id,
                            'usu_nome' => $usuarios->pro_nome,
                            'usu_regRh' => $usuarios->pro_registroRh,
                            'usu_perfil' => 'professor'
                        )
                );

                $troca = $usuarios->pro_trocaSenha;
                //echo $troca;
                if ($troca == 0) {
                    echo "<script>alert('Prezado professor você deverá alterar a sua senha para o primeiro acesso');"
                    . "window.location.href='" . site_url('professor/painelprof/senha') . "'</script>";
                    //echo 'erro senha1';
                } else {
                    //echo 'erro senha2';
                    echo "<script>window.location.href='" . site_url() . "/professor/painelprof/'</script>";
                }
            } else {
                $this->session->unset_userdata('login');
                //echo 'erro senha';
                echo "<script>alert('Login ou senha incorretos');window.location.href='" . site_url() . "'</script>";
            }
        } else {
            $this->dados['pagina'] = 'professor/inicioProf';
            $this->dados['titulo'] = 'alunos';
            $this->load->view('professor/inicioProf', $this->dados);
        }
    }

    public function recuperaAl() {
        if ($this->input->post()) {
            $matricula = $this->input->post('matricula');
            $matricula = addslashes($matricula);

            $dados = $this->alunosmodel->getBy("alu_matricula = '$matricula'");

            if ($dados->row('alu_trocaSenha') == 0) {
                echo "<script>alert('Não foi possível recuperar sua senha. Você ainda não realizou o primeiro acesso');"
                . "window.location.href='" . site_url('') . "'</script>";
            } else {
                if ($dados->num_rows() > 0) {
                    $html = ""
                            . "<html>"
                            . "<body>"
                            . "<h3>FACULDADE GUANAMBI</h3>"
                            . "<h4>Recuperação de senha</h4>"
                            . ""
                            . "Olá: " . $dados->row('alu_nome')
                            . "<br><br>"
                            . "Você solicitou a recuperação de senha do sistema interno da Faculdade Guanambi. Segue dados:"
                            . "<br><br>"
                            . "Matrícula: " . $dados->row('alu_matricula')
                            . "<br>"
                            . "Senha: " . $dados->row('alu_senha')
                            . "</body>"
                            . "<html>";

                    //echo $html;


                    $mail = new PHPMailer;
                    $mail->SMTPDebug = 0;
                    $mail->isSMTP();
                    $mail->Host = "smtp.gmail.com";
                    $mail->SMTPAuth = true;
                    $mail->Username = "facgbi@gmail.com";
                    $mail->Password = "@02193945$";
                    $mail->SMTPSecure = "tls";
                    $mail->Port = 587;
                    $mail->From = "sistemas@faculdadeguanambi.edu.br";
                    $mail->FromName = "Faculdade Guanambi";
                    $mail->addAddress($dados->row('alu_email'), $dados->row('alu_nome'));
                    $mail->isHTML(true);
                    $mail->Subject = "Faculdade Guanambi";
                    $mail->Body = $html;
                    //$mail->AltBody = "This is the plain text version of the email content";
                    if (!$mail->send()) {
                        echo "Mailer Error: " . $mail->ErrorInfo;
                    } else {
                        echo "E-mail enviado com sucesso.<br> <a href='" . site_url() . "'>Voltar<a>";
                    }
                } else {
                    echo "<script>alert('Email nao encontrado');"
                    . "window.location.href='" . site_url('inicio/recuperaAl') . "'</script>";
                }
            }
        } else {
            $this->dados['pagina'] = 'inicio/recuperaAl';
            $this->dados['titulo'] = 'alunos';
            $this->load->view('recuperaSenha', $this->dados);
        }
    }

    private function curso() {
        $alunos = $this->alunosmodel->all2();
        foreach ($alunos->result() as $aluno) {
            $idCurso = substr($aluno->alu_matricula, 3, 3);
            echo $idCurso . '<br>';
            $this->db->where('alu_id', $aluno->alu_id);
            $this->db->set(array('alu_idCurso' => $idCurso))->update('alunos');
            //break;
            flush();
        }
    }

}
