<?php

/** CRUD Controller para Agenda* */
class Agenda extends CI_Controller {

    private $_tpl = 'indexPainel.php';
    private $dados = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('modelagenda', '', true);
        $this->load->helper(array('form', 'url'));
        $this->load->library('Util');
    }

    /**
     * Cria novo(a) Agenda
     * */
    public function criar() {
        $msg = null;
        if ($_POST) {
            //$this->form_validation->set_rules('valid', '', 'required');
            // if ($this->form_validation->run() == TRUE) {
            //$object->save();
            $this->modelagenda->inserir($_POST);
            redirect('agenda');
            return;
            //} else {
            //$msg = 'Por favor preencha todos os dados';
            // }
        }

        $this->dados['object'] = $this->modelagenda;
        $this->dados['pagina'] = 'agenda/novo';
        $this->dados['titulo'] = 'agenda ';
        $this->load->view($this->_tpl, $this->dados);
    }

    /**
     * Edit Agenda
     * */
    public function editar($id) {
        $msg = null;
        if ($_POST) {
            //$this->form_validation->set_rules('valid', '', 'required');
            // if ($this->form_validation->run() == TRUE) {
            //$object->save();
            $this->modelagenda->update($_POST, $id);
            redirect('agenda');
            return;
            //} else {
            //$msg = 'Por favor preencha todos os dados';
            // }
        }

        $this->dados['object'] = $this->modelagenda->getById($id);
        $this->dados['pagina'] = 'agenda/editar';
        $this->dados['titulo'] = 'agenda ';
        $this->load->view($this->_tpl, $this->dados);
    }

    /**
     * Delete Agenda
     * */
    public function delete($id) {
        $response = null;
        if ($_POST && isset($_POST['agree']) && $_POST['agree'] == 'Sim') {
            $response = $this->modelagenda->delete($id);
            redirect('agenda');
            return;
        } else {
            if ($_POST) {
                redirect('agenda');
                return;
            }
            $object = $this->modelagenda->getById($id);
        }

        $this->dados['pagina'] = 'agenda/delete';
        $this->dados['titulo'] = 'agenda ';
        $this->dados['object'] = $object;
        $this->dados['response'] = $response;
        $this->dados['id'] = $id;
        $this->load->view($this->_tpl, $this->dados);
    }

    /**
     * List objects of Agenda
     * */
    public function index() {
        $this->dados['objects'] = $this->modelagenda->all();
        $this->dados['pagina'] = 'agenda/list';
        $this->dados['titulo'] = 'agenda ';
        $this->load->view($this->_tpl, $this->dados);
    }

}
