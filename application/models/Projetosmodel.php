<?php

/* * * Model cpp__horarios * */

class Projetosmodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getByProf($idProf) {
        //$this->db->order_by('pro_data', 'desc');
        return $this->db->get_where('proj__projetos', array('pro_idProfessor' => $idProf));
    }

    public function inserir($post, $idProf) {
        //$dados['tch_data'] = $this->util->data($dados['tch_data']);

        $custo = str_replace(",", "", $post['custo']);

        $dadosProjeto['pro_idProfessor'] = $idProf;
        $dadosProjeto['pro_titulo'] = $post['titulo'];
        $dadosProjeto['pro_dataApresentacao'] = $this->util->data($post['data']);
        $dadosProjeto['pro_custo'] = $custo;
        $dadosProjeto['pro_obs'] = $post['observacoes'];
        $dadosProjeto['pro_tipo'] = $post['pro_tipo'];


        $this->db->set($dadosProjeto)->insert('proj__projetos');

        $idProjeto = $this->db->insert_id();

        $parecer = array(
            array(
                'enc_idProjeto' => $idProjeto,
                'enc_idSetor' => '18', //COORD NUPEX
            ),
            array(
                'enc_idProjeto' => $idProjeto,
                'enc_idSetor' => '29', //GER ACADEMICA
            ),
            array(
                'enc_idProjeto' => $idProjeto,
                'enc_idSetor' => '16', //GER FINANCEIRO
            ),
            array(
                'enc_idProjeto' => $idProjeto,
                'enc_idSetor' => '30', //CIPE
            ),
        );

        $this->db->insert_batch('proj__encaminhamentos', $parecer);

        return $idProjeto;
    }

    public function inserirAnexo($dadosProjeto) {
        //$dados['tch_data'] = $this->util->data($dados['tch_data']);
        /*
          $dadosProjeto['ane_idProjeto'] = $dadosProjeto['ane_idProjeto'];
          $dadosProjeto['ane_tipo'] = $dadosProjeto['ane_tipo'];
          $dadosProjeto['ane_obs'] = $dadosProjeto['ane_obs'];
          $dadosProjeto['ane_arquivo'] = $dadosProjeto['ane_arquivo'];
         * 
         */
        $this->db->set($dadosProjeto)->insert('proj__anexos');

        //return $this->db->insert_id();
    }

    public function getAnexosByProj($idProjeto) {
        return $this->db->get_where('proj__anexos', array('ane_idProjeto' => $idProjeto));
    }

    public function getParecerBySetor($idProjeto, $idSetor) {
        $this->db->where('enc_idProjeto', $idProjeto);
        $this->db->where('enc_idSetor', $idSetor);
        return $this->db->get('proj__encaminhamentos')->row();
    }

    public function updateParecer($post, $idParecer) {
        $post['enc_dataParecer'] = date('Y-m-d H-i-s');
        $this->db->where('enc_id', $idParecer);
        $this->db->set($post)->update('proj__encaminhamentos');
    }

    public function update($data, $id) {
        $data['tch_data'] = $this->util->data($data['tch_data']);
        $data['tch_numSemana'] = date("W", strtotime($data['tch_data']));
        $this->db->where('tch_id', $id);
        $this->db->set($data)->update('cpp__horarios');
    }

    function contaRegistros() {
        return $this->db->count_all_results('cpp__horarios');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('proj__projetos', array('pro_id' => $id));
    }

    public function getById($id) {
        return $this->db->get_where('proj__projetos', array('pro_id' => $id))->row();
    }

    public function getBy($where) {
        return $this->db->get_where('proj__projetos', $where);
    }

    public function getProjetos($setor, $status) {
        //var_dump($setor);
        //var_dump($status);

        $this->db->select('proj__projetos.*');

        $this->db->select("(select enc_parecer from proj__encaminhamentos where enc_idProjeto = pro_id and enc_idSetor = 18) as encNupex");
        $this->db->select("(select enc_parecer from proj__encaminhamentos where enc_idProjeto = pro_id and enc_idSetor = 29) as encAcademica");
        $this->db->select("(select enc_parecer from proj__encaminhamentos where enc_idProjeto = pro_id and enc_idSetor = 16) as encFinanceiro");
        $this->db->select("(select enc_parecer from proj__encaminhamentos where enc_idProjeto = pro_id and enc_idSetor = 30) as encCipe");

        if ($setor == 'nupex' && strtolower($status) == NULL) {
            $this->db->having('encNupex');
            $this->db->having('encAcademica');
            $this->db->having('encFinanceiro');
        } elseif ($setor == 'academica' && strtolower($status) == '') {
            $this->db->having('encNupex', 'DEFERIDO');
            $this->db->having('encAcademica');
            $this->db->having('encFinanceiro');

            $this->db->where('pro_tipo', 'ATIVIDADE_COMPLEMENTAR');
        } elseif ($setor == 'academica' && strtolower($status) == 'deferido') {
            $this->db->having('encNupex', 'DEFERIDO');
            $this->db->having('encAcademica', 'DEFERIDO');
            $this->db->having('encFinanceiro');
        } elseif ($setor == 'academica' && strtolower($status) == 'indeferido') {
            $this->db->having('encNupex', 'DEFERIDO');
            $this->db->having('encAcademica', 'INDEFERIDO');
            $this->db->having('encFinanceiro');
        } elseif ($setor == 'financeiro' && strtolower($status) == '') {
            $this->db->having('encNupex', 'DEFERIDO');
            $this->db->having('encAcademica', 'DEFERIDO');
            $this->db->having('encFinanceiro');
        } elseif ($setor == 'financeiro' && strtolower($status) == 'deferido') {
            //echo $setor . ' - ' . $status . 'ok';
            $this->db->having('encNupex', 'DEFERIDO');
            $this->db->having('encAcademica', 'DEFERIDO');
            $this->db->having('encFinanceiro', 'DEFERIDO');
        } elseif ($setor == 'financeiro' && strtolower($status) == 'indeferido') {
            //echo $setor . ' - ' . $status . 'ok';
            $this->db->having('encNupex', 'DEFERIDO');
            $this->db->having('encAcademica', 'DEFERIDO');
            $this->db->having('encFinanceiro', 'INDEFERIDO');
        } elseif ($setor == 'todos' && strtolower($status) == 'indeferido') {
            $this->db->having('encNupex', 'INDEFERIDO');
            $this->db->or_having('encAcademica', 'INDEFERIDO');
            $this->db->or_having('encFinanceiro', 'INDEFERIDO');
            $this->db->or_having('encCipe', 'INDEFERIDO');
        } elseif ($setor == 'aprovados' && strtolower($status) == 'todos') {
            $this->db->having('encNupex', 'DEFERIDO');
            $this->db->having('encAcademica', 'DEFERIDO');
            $this->db->having('encFinanceiro', 'DEFERIDO');
        } elseif ($setor == 'cipe' && strtolower($status) == '') {
            $this->db->having('encNupex', 'DEFERIDO');
            $this->db->having('encCipe');
            $this->db->where('pro_tipo', 'PESQUISA_EXTENSAO');
        } elseif ($setor == 'cipe' && strtolower($status) == 'deferido') {
            $this->db->having('encNupex', 'DEFERIDO');
            $this->db->having('encCipe', 'DEFERIDO');
            $this->db->where('pro_tipo', 'PESQUISA_EXTENSAO');
        } elseif ($setor == 'cipe' && strtolower($status) == 'indeferido') {
            $this->db->having('encNupex', 'DEFERIDO');
            $this->db->having('encCipe', 'INDEFERIDO');
            $this->db->where('pro_tipo', 'PESQUISA_EXTENSAO');
        } elseif ($setor == 'todos' && strtolower($status) == 'todos') {
            
        } else {
            echo $setor . ' - ' . $status . ' false';
            return false;
        }

        //$this->db->join('salalivre.professores','proj__projetos.pro_idProfessor = salalivre.professores.pro_id');
        $dados = $this->db->get('proj__projetos');

        //echo $this->db->last_query();
        //var_dump($dados->result_array());


        return $dados;
    }

    public function getDownload($idDownload) {
        $this->db->where('ane_id', $idDownload);
        $dados = $this->db->get('proj__anexos');

        return $dados->row();
    }

    public function getParecer($idProjeto, $idSetor) {
        $this->db->where('enc_idSetor', $idSetor);
        $this->db->where('enc_idProjeto', $idProjeto);
        $dados = $this->db->get('proj__encaminhamentos');

        echo $this->db->last_query();
        
        return $dados->row();
    }

}
