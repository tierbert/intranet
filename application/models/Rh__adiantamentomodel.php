<?php

/* * * Model rh__adiantamento * */

class Rh__adiantamentomodel extends CI_Model {

    /**  Campos* */
    var $adi_id;
    var $adi_idUsuario;
    var $adi_data;
    var $adi_valor;
    var $adi_obs;
    var $adi_rhParecer;
    var $adi_rhDataParecer;
    var $adi_rhIntegralParcela;
    var $adi_quantParcelas;
    var $adi_mesParcela;
    var $adi_financParecer;
    var $adi_financDataAgendado;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados) {
        $dados['adi_idUsuario'] = $this->session->userdata('usu_id');
        $dados['adi_data'] = $this->util->data($dados['adi_data']);
        $dados['adi_valor'] = str_replace('.', '', $dados['adi_valor']);
        $dados['adi_valor'] = str_replace(',', '.', $dados['adi_valor']);
        $this->db->set($dados)->insert('rh__adiantamento');
    }

    public function update($data, $id) {
        if (isset($data['adi_rhDataParecer'])) {
            $data['adi_rhDataParecer'] = $this->util->data($data['adi_rhDataParecer']);
        }
        if (isset($data['adi_financDataAgendado'])) {
            $data['adi_financDataAgendado'] = $this->util->data($data['adi_financDataAgendado']);
        }
        $this->db->where('adi_id', $id);
        $this->db->set($data)->update('rh__adiantamento');
    }

    function contaRegistros() {
        return $this->db->count_all_results('rh__adiantamento');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('rh__adiantamento', array('adi_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all() {
        $this->db->where('adi_rhParecer IS NULL');
        $this->db->join('usuarios', 'adi_idUsuario = usu_id', 'left');
        //$this->db->order_by('');
        return $this->db->get('rh__adiantamento');
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        $this->db->join('usuarios', 'adi_idUsuario = usu_id', 'left');
        return $this->db->get_where('rh__adiantamento', array('adi_id' => $id))->row();
    }


    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('rh__adiantamento', $where);
    }

    public function getByFunc($idFunc = '', $idProf = '') {
        if ($idFunc != '') {
            $this->db->where('adi_idUsuario', $idFunc);
        }

        return $this->db->get('rh__adiantamento')->result();
    }

    public function getDeferidos() {
        $this->db->join('usuarios', 'adi_idUsuario = usu_id', 'left');
        return $this->db->get_where('rh__adiantamento', array('adi_rhParecer' => 'DEFERIDO'));
    }

  
}
