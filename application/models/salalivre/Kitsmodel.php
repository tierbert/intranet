<?php

/* * * Model kits * */

class Kitsmodel extends CI_Model {

    /**  Campos* */
    var $kit_id;
    var $kit_numero;
    var $kit_conteudo;
    var $kit_status;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados) {
        $this->db->set($dados)->insert('kits');
    }

    public function update($data, $id) {
        $this->db->where('kit_id', $id);
        $this->db->set($data)->update('kits');
    }

    function contaRegistros() {
        return $this->db->count_all_results('kits');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('kits', array('kit_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim, $tipo) {
        $this->db->where('kit_tipo', $tipo);
        return $this->db->get('kits', $fim, $ini)->result();
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('kits', array('kit_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('kits', $where);
    }

    public function getArray($tipo = '') {

        if ($tipo != '') {
            $this->db->where('kit_tipo', $tipo);
        }

        $this->db->order_by('kit_numero');
        $dados = $this->db->get('salalivre.kits');

        $result = array('' => '');
        foreach ($dados->result() as $value) {
            $result[$value->kit_id] = '00' . $value->kit_numero;
        }
        return $result;
    }

    public function pendentes($tipo = '') {
        if ($tipo != '') {
            $this->db->where('fre_tipo', $tipo);
        }

        $this->db->where('fre_fim is null');
        $this->db->join('salalivre.salas', 'fre_idSala = sal_id', 'left');
        $this->db->join('salalivre.kits', 'fre_idKit = kit_id', 'left');
        $this->db->join('salalivre.professores', 'fre_idProf = pro_id');
        $this->db->order_by('pro_nome');
        $dados = $this->db->get('salalivre.frequencia');

        return $dados;
    }

}
