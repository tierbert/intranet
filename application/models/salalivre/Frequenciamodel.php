<?php

/* * * Model frequencia * */

class Frequenciamodel extends CI_Model {

    /**  Campos* */
    var $fre_id;
    var $fre_idProf;
    var $fre_idKit;
    var $fre_inicio;
    var $fre_fim;
    var $fre_ip;
    var $fre_obs1;
    var $fre_obs2;
    var $fre_justificativa;
    var $fre_parecer;
    var $fre_dataParecer;
    var $fre_idSala;

    public function __construct() {
        parent::__construct();
    }

    public function getBy($idProf, $tipo = '') {
        if ($tipo != '') {
            $this->db->where("fre_tipo", $tipo);
        }
        $this->db->where("fre_idProf", $idProf);
        $this->db->where("fre_fim is null");
        $this->db->join('salalivre.professores', 'fre_idProf = pro_id');
        $this->db->join('salalivre.kits', 'fre_idKit = kit_id', 'left');
        $this->db->join('salalivre.salas', 'fre_idSala = sal_id', 'left');
        $dados = $this->db->get('salalivre.frequencia');

        //echo $this->db->last_query();

        return $dados;
    }

    public function inserir($dados) {
        $dados['fre_idProf'] = $this->session->userdata('pro_id');
        $this->db->set($dados)->insert('salalivre.frequencia');
    }

    public function update($data, $id) {
        $this->db->where('fre_id', $id);
        $this->db->set($data)->update('salalivre.frequencia');
    }

    function contaRegistros() {
        return $this->db->count_all_results('salalivre.frequencia');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('frequencia', array('fre_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($post) {

        if ($post && $post['nome'] != '') {
            $this->db->where("pro_nome like '%$post[nome]%'");
        }
        if ($post && $post['kit'] != '') {
            $this->db->where("kit_numero", $post['kit']);
        }
        if ($post && $post['dataInicial'] != '' && $post['dataFinal'] != '') {
            $dataInicial = $this->util->data($post['dataInicial']);
            $data1 = $this->util->data($post['dataFinal']);
            $data2 = explode('-', $data1);
            $data2[2] = $data2[2] + 1;
            $dataFim = implode('-', $data2);
            $this->db->where("fre_inicio between '$dataInicial' AND '$dataFim'");
        }

        $this->db->join('kits', 'fre_idKit = kit_id');
        $this->db->join('professores', 'fre_idProf = pro_id');
        $this->db->order_by('fre_inicio');
        $dados = $this->db->get('salalivre.frequencia')->result();

        //echo $this->db->last_query();

        return $dados;
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('frequencia', array('fre_id' => $id))->row();
    }

    public function devolveKit($id) {
        date_default_timezone_set('America/Bahia');
        $date = date('Y/m/d H:i:s ', time());
        $data['fre_fim'] = $date;
        $this->db->where('fre_id', $id);
        $this->db->set($data)->update('salalivre.frequencia');
    }

    public function retirarKit($idKit) {
        $dados['fre_tipo'] = 'sala';
        $dados['fre_idKit'] = $idKit;
        $dados['fre_idProf'] = $this->session->userdata('pro_id');
        $this->db->set($dados)->insert('salalivre.frequencia');
        //echo $this->db->last_query();
    }

    public function retirarLab($idLab) {
        $dados['fre_tipo'] = 'lab';
        $dados['fre_idSala'] = $idLab;
        $dados['fre_idProf'] = $this->session->userdata('pro_id');
        $this->db->set($dados)->insert('frequencia');
        //echo $this->db->last_query();
    }

    public function getByData() {

        $this->db->where("fre_inicio between '2016-08-18' AND '2016-09-18'");
        $this->db->join('kits', 'fre_idKit = kit_id');
        $this->db->join('professores', 'fre_idProf = pro_id');
        $this->db->order_by('fre_inicio');
        return $this->db->get('salalivre.frequencia')->result();
    }

}
