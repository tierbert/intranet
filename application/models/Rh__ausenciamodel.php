<?php

/* * * Model rh__ausencia * */

class Rh__ausenciamodel extends CI_Model {

    /**  Campos* */
    var $aus_id;
    var $aus_idUsuario;
    var $aus_idProfessor;
    var $aus_dataInicial;
    var $aus_dataFinal;
    var $aus_motivo;
    var $aus_coordParecer;
    var $aus_coordObservacoes;
    var $aus_gerParecer;
    var $aus_gerObservacoes;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados) {
        $dados['aus_idUsuario'] = $this->session->userdata('usu_id');
        $dados['aus_dataInicial'] = $this->util->data($dados['aus_dataInicial']);
        $dados['aus_dataFinal'] = $this->util->data($dados['aus_dataFinal']);
        $this->db->set($dados)->insert('rh__ausencia');
    }

    public function update($data, $id) {
        $this->db->where('aus_id', $id);
        $this->db->set($data)->update('rh__ausencia');
    }

    function contaRegistros() {
        return $this->db->count_all_results('rh__ausencia');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('rh__ausencia', array('aus_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim) {
        return $this->db->get('rh__ausencia', $fim, $ini)->result();
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('rh__ausencia', array('aus_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('rh__ausencia', $where);
    }

    public function getByFunc($idFunc) {
        $this->db->where('aus_idUsuario', $idFunc);
        return $this->db->get('rh__ausencia');
    }

    public function getBySetor($setor) {
        $this->db->join('usuarios', 'aus_idUsuario = usu_id');
        $this->db->join('rh__setores', 'usu_setor = set_id');
        $this->db->where('set_nome', $setor);
        return $this->db->get('rh__ausencia');
    }

    public function getDeferidasRh() {
        $this->db->join('usuarios', 'aus_idUsuario = usu_id');
        $this->db->join('rh__setores', 'usu_setor = set_id');
        $this->db->where('aus_coordParecer is not null');
        return $this->db->get('rh__ausencia');
    }

    public function getDeferidasDirec() {
        $this->db->join('usuarios', 'aus_idUsuario = usu_id');
        $this->db->join('rh__setores', 'usu_setor = set_id');
        $this->db->where('aus_rhParecer is not null');
        return $this->db->get('rh__ausencia');
    }


}
