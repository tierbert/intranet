<?php

/* * * Model estag__locais * */

class Estag__locaismodel extends CI_Model {

    /**  Campos* */
    var $el_id;
    var $el_nome;
    var $el_status;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados) {
        $this->db->set($dados)->insert('estag__locais');
    }

    public function update($data, $id) {
        $this->db->where('el_id', $id);
        $this->db->set($data)->update('estag__locais');
    }

    function contaRegistros() {
        return $this->db->count_all_results('estag__locais');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('estag__locais', array('el_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim) {
        return $this->db->get('estag__locais', $fim, $ini)->result();
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('estag__locais', array('el_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('estag__locais', $where);
    }

    public function getLocais($tipoEstagio = '') {
        if ($tipoEstagio != '') {
            $this->db->where('el_tipo', $tipoEstagio);
        }

        $this->db->order_by('el_nome');
        return $this->db->get('estag__locais');
    }

}
