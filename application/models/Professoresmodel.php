<?php

/* * * Model professores * */

class Professoresmodel extends CI_Model {

    var $salaLivre;

    /**  Campos* */
    var $pro_id;
    var $pro_codigo;
    var $pro_nome;
    var $pro_cpf;
    var $pro_login;
    var $pro_nascimento;
    var $pro_email;
    var $pro_registroRh;
    var $pro_senha;

    public function __construct() {
        parent::__construct();
        $this->salaLivre = $this->load->database('salalivre', true);
    }

    public function inserir($dados) {
        $this->salaLivre->set($dados)->insert('professores');
    }

    public function update($data, $id) {
        $this->salaLivre->where('pro_id', $id);
        $this->salaLivre->set($data)->update('professores');
    }

    function contaRegistros() {
        return $this->salaLivre->count_all_results('professores');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('professores', array('pro_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim, $post) {

        if ($post) {
            $this->salaLivre->like('pro_nome', $post['nome']);
        }

        if ($post['pro_registroRh'] != '') {
            $this->salaLivre->where('pro_registroRh', $post['pro_registroRh']);
        }

        //return $this->db->get('professores', $fim, $ini)->result();
        return $this->salaLivre->get('professores', $fim, $ini)->result();
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->salaLivre->get_where('professores', array('pro_id' => $id))->row();
    }

    public function getByCod($cod) {
        return $this->salaLivre->get_where('professores', array('pro_registroRh' => intval($cod)))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('professores', $where);
    }

    public function getArray($status = '') {

        if ($status != '') {
            //$this->salaLivre->where('pro_status', $status);
        }

        $this->salaLivre->order_by('pro_nome');
        $prof = $this->salaLivre->get('professores')->result_array();
        $dados = array();
        $dados[''] = '';
        foreach ($prof as $key => $prof) {
            $dados[$prof['pro_id']] = $prof['pro_nome'] . ' - Cod: ' . $prof['pro_id'];
        }
        return $dados;
    }

    public function getArrayRh() {
        $this->salaLivre->order_by('pro_nome');
        $prof = $this->salaLivre->get('professores')->result_array();
        $dados = array();
        $dados[''] = '';
        foreach ($prof as $key => $prof) {
            if($prof['pro_registroRh'] != '' || $prof['pro_registroRh'] != NULL ){
                $dados[$prof['pro_registroRh']] = $prof['pro_nome'] . ' - Cod: ' . $prof['pro_id'];
            }
            
        }
        return $dados;
    }

    public function login($login, $senha) {
        $this->salaLivre->where("pro_registroRh", $login);
        $this->salaLivre->where("pro_senhaIntra", $senha);
        return $this->salaLivre->get('professores')->row();
    }

    public function todos() {
        //return $this->db->get('professores', $fim, $ini)->result();
        return $this->salaLivre->get('professores');
    }

    public function ativos() {
        //$this->salaLivre->order_by('pro_nome');
        //$this->salaLivre->where("pro_status != 'INATIVO'");
        return $this->salaLivre->get('professores');
    }

}

/*
 
 ALTER TABLE `professores` CHANGE `pro_status` `pro_status` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'ATIVO'; 
 UPDATE `professores` set pro_status = 'ATIVO';
 ALTER TABLE `professores` ADD `pro_regime` VARCHAR(45) NULL AFTER `pro_titulacao`, ADD `pro_chProporcional` DOUBLE NULL AFTER `pro_regime`, ADD `pro_chAulaRegIntegral` DOUBLE NULL AFTER `pro_chProporcional`, ADD `pro_chAulaRegParcial` DOUBLE NULL AFTER `pro_chAulaRegIntegral`, ADD `pro_chAulaRegHorista` DOUBLE NULL AFTER `pro_chAulaRegParcial`;
  
 */
