<?php

/* * * Model cpp__agendados * */

class Cpp__agendadosmodel extends CI_Model {

    /**  Campos* */
    var $tca_id;
    var $tca_idHorario;
    var $tca_idAluno;
    var $tca_data;
    var $tca_status;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($idAluno, $idHorario) {
        $dados['tca_idAluno'] = $idAluno;
        $dados['tca_idHorario'] = $idHorario;
        $this->db->set($dados)->insert('cpp__agendados');
    }

    public function update($data, $id) {
        $this->db->where('tca_id', $id);
        $this->db->set($data)->update('cpp__agendados');
    }

    function contaRegistros() {
        return $this->db->count_all_results('cpp__agendados');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('cpp__agendados', array('tca_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim) {
        return $this->db->get('cpp__agendados', $fim, $ini)->result();
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('cpp__agendados', array('tca_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('cpp__agendados', $where);
    }

    public function getByAluno($idAluno) {
        $this->db->join('cpp__horarios', 'tca_idHorario = tch_id');
        return $this->db->get_where('cpp__agendados', array('tca_idAluno' => $idAluno));
    }

    public function getByhorario($idHorario) {
        $this->db->join('alunos', 'tca_idAluno = alu_id');
        $this->db->join('cpp__horarios', 'tca_idHorario = tch_id');
        return $this->db->get_where('cpp__agendados', array('tca_idHorario' => $idHorario));
    }

    public function getByAlunoHorario($idAluno, $idHorario) {
        $this->db->join('cpp__horarios', 'tca_idHorario = tch_id');
        $this->db->where('tca_idAluno', $idAluno);
        $this->db->where('tca_idHorario', $idHorario);
        //$this->db->where('tch_numSemana', $numSemana);
        return $this->db->get('cpp__agendados');
    }

}
