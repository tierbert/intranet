<?php

/* * * Model alunos * */

class Programasmodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function cadPosList() {
        $this->db->join('cad__posgraduacao_cursos', 'pos_curso = pc_codigo', 'left');
        $this->db->order_by('pos_dataCadastro', 'DESC');
        return $this->db->get('cad__posgraduacao');
    }

    public function cadPosGetById($id) {
        $this->db->where('pos_id', $id);
        $this->db->join('cad__posgraduacao_cursos', 'pos_curso = pc_codigo', 'left');
        return $this->db->get('cad__posgraduacao')->row();
    }

}
