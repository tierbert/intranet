<?php

/* * * Model usuarios * */

class Usuariosmodel extends CI_Model {

    /**  Campos* */
    var $usu_id;
    var $usu_nome;
    var $usu_senha;
    var $usu_login;
    var $usu_perfil;
    var $usu_setor;
    var $usu_grupo;
    var $usu_status;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados) {
        $this->db->set($dados)->insert('usuarios');
    }

    public function update($data, $id) {
        $this->db->where('usu_id', $id);
        $this->db->set($data)->update('usuarios');
    }

    function contaRegistros() {
        return $this->db->count_all_results('usuarios');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('usuarios', array('usu_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim, $post) {
        if ($post['regRh']) {
            $this->db->where('usu_login', $post['regRh']);
        }
        if ($post['nome']) {
            $this->db->like('usu_nome', $post['nome']);
        }

        if ($post['perfil']) {
            $this->db->like('usu_perfil', $post['perfil']);
        }
        $this->db->order_by('usu_nome');
        return $this->db->get('usuarios', $fim, $ini)->result();
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        $this->db->join('rh__setores', 'usu_setor = set_id', 'left');
        return $this->db->get_where('usuarios', array('usu_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('usuarios', $where);
    }

    public function getByRegRh($regRh) {
        return $this->db->get_where('usuarios', array('usu_login', $regRh));
    }

    public function getSetoresArray() {
        $this->db->order_by('set_nome');
        $dados = $this->db->get('rh__setores');

        $result[''] = 'SELECIONE O SETOR DE LOTAÇÃO';
        foreach ($dados->result() as $dado) {
            $result[$dado->set_id] = $dado->set_nome;
        }
        return $result;
    }

    public function getArray() {
        $this->db->order_by('usu_nome');
        $this->db->where('usu_status', 'ATIVO');
        $dados = $this->db->get('usuarios');

        $result[''] = 'SELECIONE O COLABORADOR';
        foreach ($dados->result() as $dado) {
            $result[$dado->usu_id] = $dado->usu_nome;
        }
        return $result;
    }

}
