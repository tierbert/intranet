<?php

/* * * Model estag__agendados_dir * */

class Estag__agendados_dirmodel extends CI_Model {

    /**  Campos* */
    var $ea_id;
    var $ea_idAluno;
    var $ea_idVaga;
    var $ea_status;
    var $ea_data;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados) {
        $this->db->set($dados)->insert('estag__agendados_dir');
    }

    public function update($data, $id) {
        $this->db->where('ea_id', $id);
        $this->db->set($data)->update('estag__agendados_dir');
    }

    function contaRegistros() {
        return $this->db->count_all_results('estag__agendados_dir');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('estag__agendados_dir', array('ea_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim) {
        return $this->db->get('estag__agendados_dir', $fim, $ini)->result();
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('estag__agendados_dir', array('ea_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('estag__agendados_dir', $where);
    }

    public function getByAlunoTipo($idAluno, $tipo){
        //Deprecated
        $this->db->where('ev_tipo', $tipo);
        $this->db->where('ea_idAluno', $idAluno);
        $this->db->join('estag__vagas_dir', 'ev_id = ea_idVaga');
        $this->db->join('estag__locais_dir', 'el_id = ev_idLocal');
        $dados = $this->db->get('estag__agendados_dir');

        //echo $this->db->last_query();

        return $dados;
    }

    public function getTipoLocal($idAluno, $tipo, $tipoLocal){
        $this->db->where('ev_tipo', $tipo);
        $this->db->where('ea_idAluno', $idAluno);
        $this->db->where('el_tipo', $tipoLocal);
        $this->db->join('estag__vagas_dir', 'ev_id = ea_idVaga');
        $this->db->join('estag__locais_dir', 'el_id = ev_idLocal');
        $dados = $this->db->get('estag__agendados_dir');

        //echo $this->db->last_query();

        return $dados;
    }

    public function agendadosVaga($idVaga, $tipo) {
        //$this->db->where('ev_tipo', $tipo);
        $this->db->join('alunos', 'alu_id = ea_idAluno');
        $this->db->join('estag__vagas_dir', 'ev_id = ea_idVaga');
        //$this->db->join('estag__locais', 'el_id = ev_idLocal');
        $this->db->where('ea_idVaga', $idVaga);
        return $this->db->get('estag__agendados_dir');

        //echo $this->db->last_query();
    }

}
