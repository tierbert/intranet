<?php

/* * * Model forum__horarios * */

class Forunscppmodel extends CI_Model {

    /**  Campos* */
    var $tch_id;
    var $tch_idProfessor;
    var $tch_data;
    var $tch_vagas;
    var $tch_horario;
    var $tch_numSemana;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados, $idProf) {
        $dados['tch_idProfessor'] = $idProf;
        $dados['tch_data'] = $this->util->data($dados['tch_data']);
        $dados['tch_numSemana'] = date("W", strtotime($dados['tch_data']));
        $this->db->set($dados)->insert('forum__horarios');
    }

    public function update($data, $id) {
        $data['tch_data'] = $this->util->data($data['tch_data']);
        $data['tch_numSemana'] = date("W", strtotime($data['tch_data']));
        $this->db->where('tch_id', $id);
        $this->db->set($data)->update('forumforum__horarios');
    }

    function contaRegistros() {
        return $this->db->count_all_results('forum__horarios');
    }

    /**
     * Delete object
     * */
    public function horDel($id) {
        return $this->db->delete('forum__horario', array('hor_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim) {
        return $this->db->get('forum__horarios', $fim, $ini)->result();
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('forum__horario', array('hor_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('forum__horarios', $where);
    }

    public function getByProf($idProf) {
        $this->db->order_by('tch_data');
        return $this->db->get_where('forum__horarios', array('tch_idProfessor' => $idProf));
    }

    public function getTurnos($tipo) {
        $this->db->where('tur_tipo', $tipo);
        $dados = $this->db->get('forum__turno');

        return $dados;
    }

    public function getHorario($idTurno, $dia) {
        //$this->db->join('forum__professores', 'hor_professor = tcp_id', 'left');
        $this->db->where('hor_idTurno', $idTurno);
        $this->db->where('hor_dia', $dia);
        $dados = $this->db->get('forum__horario');

        return $dados;
    }

    public function getProfArray() {
        $this->db->order_by('tcp_nome');
        $prof = $this->db->get('forum__professores')->result_array();
        $dados = array();
        $dados[''] = '';
        foreach ($prof as $key => $prof) {
            $dados[$prof['tcp_id']] = $prof['tcp_nome'];
        }
        return $dados;
    }

    public function horCad($post) {
        $this->db->set($post)->insert('forum__horario');
    }

    public function alunHorCad($post) {
        $this->db->set($post)->insert('forum__agendados');
    }

    public function getByAlunData($idHorario, $idAluno, $data) {
        $this->db->where('tca_idHorario', $idHorario);
        $this->db->where('tca_idAluno', $idAluno);
        $this->db->where('tca_data', $data);
        $dados = $this->db->get('forum__agendados');

        return $dados;
    }

    public function getByHorarioData($idHorario, $data, $tipo = 'aluno') {
        $this->db->where('tca_idHorario', $idHorario);
        $this->db->where('tca_data', $data);

        if ($tipo == 'aluno') {
            $this->db->join('alunos', 'alu_id = tca_idAluno', 'left');
        } elseif ($tipo == 'prof') {
            $this->db->join('salalivre.professores', 'pro_registroRh = tca_idAluno', 'left');
        }


        $dados = $this->db->get('forum__agendados');

        //echo $this->db->last_query();

        return $dados;
    }

    public function getHorById($id) {
        $this->db->where('hor_id', $id);
        return $this->db->get('forum__horario')->row();
    }

    public function getByAluSemana($idAluno, $numSemana) {
        $this->db->where('tca_idAluno', $idAluno);
        $this->db->where('tca_semana', $numSemana);
        return $this->db->get('forum__agendados');
    }

    public function getByAluno($idAluno) {
        $this->db->join('forum__horario', 'tca_idHorario = hor_id', 'left');
        $this->db->join('forum__turno', 'hor_idTurno = tur_id', 'left');
        //$this->db->join('forum__professores', 'hor_professor = tcp_id', 'left');
        $this->db->where('tca_idAluno', $idAluno);
        $dados = $this->db->get('forum__agendados');

        return $dados;
    }

    public function getByAlunoHorarioData($idAluno, $idHorario, $data) {
        $this->db->join('forum__horario', 'tca_idHorario = hor_id', 'left');
        $this->db->join('forum__turno', 'hor_idTurno = tur_id', 'left');
        //$this->db->join('forum__professores', 'hor_professor = tcp_id', 'left');
        $this->db->where('tca_data', $data);
        $this->db->where('hor_id', $idHorario);
        $this->db->where('tca_idAluno', $idAluno);
        $dados = $this->db->get('forum__agendados');

        //echo $this->db->last_query();

        return $dados;
    }

}
