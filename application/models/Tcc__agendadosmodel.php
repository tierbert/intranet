<?php

/* * * Model tcc__agendados * */

class Tcc__agendadosmodel extends CI_Model {

    /**  Campos* */
    var $tca_id;
    var $tca_idHorario;
    var $tca_idAluno;
    var $tca_data;
    var $tca_status;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($idAluno, $idHorario) {
        $dados['tca_idAluno'] = $idAluno;
        $dados['tca_idHorario'] = $idHorario;
        $this->db->set($dados)->insert('tcc__agendados');
    }

    public function update($data, $id) {
        $this->db->where('tca_id', $id);
        $this->db->set($data)->update('tcc__agendados');
    }

    function contaRegistros() {
        return $this->db->count_all_results('tcc__agendados');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('tcc__agendados', array('tca_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim) {
        return $this->db->get('tcc__agendados', $fim, $ini)->result();
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('tcc__agendados', array('tca_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('tcc__agendados', $where);
    }

    public function getByAluno($idAluno) {
        $this->db->join('tcc__horarios', 'tca_idHorario = tch_id');
        return $this->db->get_where('tcc__agendados', array('tca_idAluno' => $idAluno));
    }

    public function getByhorario($idHorario) {
        $this->db->join('alunos', 'tca_idAluno = alu_id');
        $this->db->join('tcc__horarios', 'tca_idHorario = tch_id');
        return $this->db->get_where('tcc__agendados', array('tca_idHorario' => $idHorario));
    }

    public function getByAluSemana($idAluno, $numSemana) {
        $this->db->where('tca_idAluno', $idAluno);
        $this->db->where('tca_semana', $numSemana);
        return $this->db->get('tcc__agendados');
    }

    public function getAllByProf() {
        //$this->db->join('tcc__horarios', 'tca_idHorario = tch_id', 'left');
        $this->db->where('tch_data >= CURDATE()');
        $this->db->join('salalivre.professores', 'pro_id = tch_idProfessor', 'left');
        $this->db->order_by('tch_data, pro_nome');
        return $this->db->get('tcc__horarios');
    }

}
