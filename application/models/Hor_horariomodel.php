<?php

/* * * Model hor_horario * */

class Hor_horariomodel extends CI_Model {

    /**  Campos* */
    var $hor_id;
    var $hor_idSala;
    var $hor_idTurno;
    var $hor_idProfessor;
    var $hor_dia;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados) {
        $this->db->set($dados)->insert('hor_horario');
    }

    public function update($data, $id) {
        $this->db->where('hor_id', $id);
        $this->db->set($data)->update('hor_horario');
    }

    function contaRegistros() {
        return $this->db->count_all_results('hor_horario');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('hor_horario', array('hor_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all() {
        return $this->db->get('hor_horario');
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('hor_horario', array('hor_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('hor_horario', $where);
    }

    public function getBySala($sala) {
        
    }

    public function getByTurnoSalaDia($idTurno, $idSala, $data) {
        $this->db->where('res_data', $data);
        $this->db->where('res_idTurno', $idTurno);
        $this->db->where('res_idSala', $idSala);
        $this->db->where_in('res_status', array('AGUARDANDO', 'CONFIRMADA'));
        //$this->db->join('salalivre.professores', 'hor_idProfessor = pro_registroRh', 'left');
        $dados = $this->db->get('hor_reserva');

        //echo $this->db->last_query();

        return $dados;
    }

    public function getSalas($turno, $dia, $tipo, $status, $segunda, $sabado) {

        /* pega os dias da semana */
        $segunda = $this->util->data($segunda);
        $sabado = $this->util->data2($sabado);

        /*
          $day = date('w');
          $segunda = date('Y-m-d', strtotime('+' . (1 - $day) . ' days')); //segunda
          $sabado = date('Y-m-d', strtotime('+' . (6 - $day) . ' days')); //sabado
         * 
         */
        //$domingo = date('m-d-Y', strtotime('-' . $day . ' days'));

        $this->db->select('salalivre.salas.*');
        $this->db->select('max(horario.hor_id) as hor');
        $this->db->select('max(horario.hor_disciplina) as disciplina');
        $this->db->select('max(horario.hor_turmas) as turmas');
        $this->db->select('max(reserva.res_id) as res_id');
        $this->db->select('max(reserva.res_status) as res_status');
        $this->db->select('max(prof.pro_id) as pro_id');
        $this->db->select('max(prof.pro_nome) as pro_nome');

        $this->db->join('intranet.hor_horario as horario', "hor_idSala = sal_id and hor_dia = $dia and hor_idTurno = $turno", 'left');
        $this->db->join('intranet.hor_reserva as reserva', "res_idSala = sal_id and res_idDia = $dia and res_idTurno = $turno and (res_data BETWEEN '$segunda' and '$sabado')", 'left');
        $this->db->join('salalivre.professores as prof', 'pro_registroRh = hor_idProfessor', 'left');

        $this->db->where('sal_tipo', $tipo);
        $this->db->where('sal_status', $status);

        $this->db->group_by('sal_id');
        $this->db->order_by('sal_grupo, sal_nome');

        $dados = $this->db->get('salalivre.salas');

        //echo $this->db->last_query();

        return $dados;
    }

    public function getSalasV2($turno, $dia, $tipo, $segunda, $sabado) {

        /* pega os dias da semana */
        $segunda = $this->util->data($segunda);
        $sabado = $this->util->data2($sabado);

        /*
          $day = date('w');
          $segunda = date('Y-m-d', strtotime('+' . (1 - $day) . ' days')); //segunda
          $sabado = date('Y-m-d', strtotime('+' . (6 - $day) . ' days')); //sabado
         * 
         */
        //$domingo = date('m-d-Y', strtotime('-' . $day . ' days'));

        $this->db->select('salalivre.salas.*');
        $this->db->select('max(horario.hor_id) as hor');
        $this->db->select('max(horario.hor_disciplina) as disciplina');
        $this->db->select('max(horario.hor_turmas) as turmas');
        $this->db->select('max(reserva.res_id) as res_id');
        $this->db->select('max(reserva.res_status) as res_status');
        $this->db->select('max(prof.pro_id) as pro_id');
        $this->db->select('max(prof.pro_nome) as pro_nome');

        $this->db->join('intranet.hor_horario as horario', "hor_idSala = sal_id and hor_dia = $dia and hor_idTurno = $turno", 'left');
        $this->db->join('intranet.hor_reserva as reserva', "res_idSala = sal_id and res_idDia = $dia and res_idTurno = $turno and (res_data BETWEEN '$segunda' and '$sabado')", 'left');
        $this->db->join('salalivre.professores as prof', 'pro_registroRh = hor_idProfessor', 'left');

        $this->db->where('sal_tipo', $tipo);

        $this->db->group_by('sal_id');
        $this->db->order_by('sal_grupo, sal_nome');

        $dados = $this->db->get('salalivre.salas');

        //echo $this->db->last_query();

        return $dados;
    }

    public function getAllLabs(){
        $this->db->select('salalivre.salas.*');

        $this->db->where('sal_tipo = "LAB" OR sal_tipo="LABSAUDE2"');
        $this->db->where('sal_status', 'ATIVA');

        $this->db->order_by('sal_grupo, sal_nome');

        $dados = $this->db->get('salalivre.salas')->result();

        return $dados;

    }
    public function getByHorario($idTurno, $idSala, $dia) {
        $this->db->where('hor_dia', $dia);
        $this->db->where('hor_idTurno', $idTurno);
        $this->db->where('hor_idSala', $idSala);
        $this->db->join('salalivre.professores', 'hor_idProfessor = pro_registroRh', 'left');
        $dados = $this->db->get('hor_horario');

        //echo $this->db->last_query();

        return $dados;
    }

    public function cancelarReserva($id){
        $this->db->where('hor_reserva.res_id', $id);
        $this->db->set('res_status', 'CANCELADA');
        $this->db->update('hor_reserva');
    }

    public function getSalaById($idSala) {
        return $this->db->get_where('salalivre.salas', array('sal_id' => $idSala))->row();
    }
                            // turno, sala, rgRh, dia, data, tipo
    public function reservar($turno, $sala, $regRh, $disciplina, $turma, $dia, $data, $tipo) {
        $dados = array(
            'res_idSala' => $sala,
            'res_idTurno' => $turno,
            'res_idProfessor' => $regRh,
            'res_disciplina' => $disciplina,
            'res_turma' => $turma,
            'res_idDia' => $dia,
            'res_data' => $this->util->data($data),
            'res_estagiario' => $tipo == 'aluno' ? $this->session->userdata('alu_nome') : 'teste',
        );
        $this->db->set($dados)->insert('hor_reserva');
    }
     public function reservarV2($turno, $sala, $regRh, $dia, $data, $tipo) {
        $dados = array(
            'res_idSala' => $sala,
            'res_idTurno' => $turno,
            'res_idProfessor' => $regRh,
            'res_idDia' => $dia,
            'res_data' => $this->util->data($data),
            'res_estagiario' => $tipo == 'aluno' ? $this->session->userdata('alu_nome') : 'teste',
        );
        $this->db->set($dados)->insert('hor_reserva');
    }

    public function getAgendaByDia($turno, $sala, $dia, $data) {
        $this->db->where('res_idDia', $dia);
        $this->db->where('res_idTurno', $turno);
        $this->db->where('res_idSala', $sala);
        $this->db->where('res_data', $this->util->data($data));
        //$this->db->where('res_status', 'AGUARDANDO');
        return $this->db->get('hor_reserva');
    }

    public function getByProf($regRh) {
        $this->db->join('hor_turno', 'res_idTurno = tur_id', 'left');
        $this->db->join('salalivre.salas', 'res_idSala = sal_id', 'left');
        $this->db->join('salalivre.professores', 'res_idProfessor = pro_registroRh', 'left');
        $this->db->where('res_idProfessor', $regRh);
        $this->db->where('res_data >= CURDATE()');
        $this->db->order_by('res_data,sal_nome');
        return $this->db->get('hor_reserva');
    }

    public function getReservas($tipo) {
        $this->db->select('hor_reserva.*');
        $this->db->select('hor_turno.tur_horario');
        $this->db->select('salalivre.professores.pro_nome');
        $this->db->select('salalivre.salas.sal_nome');

        $this->db->from('intranet.hor_turno');
        $this->db->from('salalivre.professores');
        $this->db->from('salalivre.salas');

        $this->db->where('hor_reserva.res_idTurno = hor_turno.tur_id');
        $this->db->where('hor_reserva.res_idSala = salalivre.salas.sal_id');
        $this->db->where('hor_reserva.res_idProfessor = salalivre.professores.pro_registroRh');
        $this->db->where('res_data >= CURDATE()');
        $this->db->where('salalivre.salas.sal_tipo', $tipo);

        $this->db->order_by('res_data', 'desc');
        $this->db->order_by('res_idTurno', 'asc');
        $dados = $this->db->get('intranet.hor_reserva');

        //echo $this->db->last_query();

        return $dados;
    }

    public function getReservasById($res_id, $tipo) {
        $this->db->select('hor_reserva.*');
        $this->db->select('hor_turno.tur_horario');
        $this->db->select('salalivre.professores.pro_nome');
        $this->db->select('salalivre.salas.sal_nome');

        $this->db->from('intranet.hor_turno');
        $this->db->from('salalivre.professores');
        $this->db->from('salalivre.salas');

        $this->db->where('hor_reserva.res_idTurno = hor_turno.tur_id');
        $this->db->where('hor_reserva.res_idSala = salalivre.salas.sal_id');
        $this->db->where('hor_reserva.res_idProfessor = salalivre.professores.pro_registroRh');
        $this->db->where('res_data >= CURDATE()');
        $this->db->where('salalivre.salas.sal_tipo', $tipo);
        $this->db->where('hor_reserva.res_id', $res_id);

        $this->db->order_by('res_data', 'desc');
        $this->db->order_by('res_idTurno', 'asc');
        $dados = $this->db->get('intranet.hor_reserva')->row();

        //echo $this->db->last_query();

        return $dados;
    }

    public function confirmar($idReserva, $status) {
        $data = array('res_status' => $status);
        $this->db->where('res_id', $idReserva);
        $this->db->set($data)->update('hor_reserva');
    }
    public function bloquearSala($sala, $dias, $horarios, $dataLimite){
        
        // pega todos os dias até o fim da data
        $daysToBlock = array();
        $objsToInsert = array();
        $endDate = strtotime($dataLimite);
        // $dayId = date('w', strtotime($dia)) -1;
        foreach ($dias as $dia) { 
            $dayId = date('w', strtotime($dia)) -1;
            for($i = strtotime($dia, strtotime('today')); $i <= $endDate; $i = strtotime('+1 week', $i)){
                $dayToBlock = date('Y-m-d', $i);
                 foreach ($horarios as $horario) { 
                   
                        array_push($objsToInsert, 
                            array(
                                'res_idSala' => $sala,
                                'res_status' => 'BLOQUEADA',
                                'res_data' => $dayToBlock,
                                'res_idTurno' => $horario,
                                'res_idDia' => $dayId
                            )
                        );
                    
                }
            }
        }


        
        // var_dump($objsToInsert);
        foreach ($objsToInsert as $obj) {
            $this->db->set($obj)->insert('hor_reserva');
        }
        

    }

}
