<?php
/*** Model rh_contracheque **/

class Rh_contrachequemodel extends CI_Model{

     /**  Campos**/
     var $rhc_id;
     var $rhc_registroRh;
     var $rhc_mes;
     var $rhc_ano;
     var $rhc_arquivo;
     var $rhc_observacao;


     public function __construct(){
         parent::__construct();
     }

     public function inserir($dados){
         $this->db->set($dados)->insert('rh_contracheque');
     }

    
    public function update($data, $id) {
        $this->db->where('rhc_id', $id);
        $this->db->set($data)->update('rh_contracheque');
    }
    
     function contaRegistros() {
        return $this->db->count_all_results('rh_contracheque');
    }
    

    /**
    * Delete object
    **/
    public function delete($id){
	return $this->db->delete('rh_contracheque', array('rhc_id' => $id));
    }
	/**
    * Return all objects
    **/
    public function all($ini, $fim){
    	return $this->db->get('rh_contracheque', $fim, $ini)->result();
    }
    /**
    * Return object that has $id
    **/
    public function getById($id){
		return $this->db->get_where('rh_contracheque', array('rhc_id' => $id))->row();
	}
	/**
    * Return object filtered by $where
    * $where must be an array
    **/
    public function getBy($where){
	return $this->db->get_where('rh_contracheque', $where);
    }
}
