<?php

/**
 * Model that represents Agenda at database
 * */
class Capacitacursosmodel extends CI_Model {

    var $cur_id;
    var $cur_name;
    var $cur_type;

    public function __construct() {
        parent::__construct();
    }

    public function getCursos() {
        $this->db->order_by('cur_name');
        return $this->db->get('capac_grupo_cursos')->result();
    }

    public function getById($id) {
        $this->db->where('age_id', $id);
        return $this->db->get('capac_agendados')->row();
    }

    public function delete($id) {
        return $this->db->delete('capac_grupo_cursos', array('cur_id' => $id));
    }
    public function inserir($dados) {
        $this->db->set($dados)->insert('capac_grupo_cursos');
    }

    public function all() {
        $this->db->order_by('cur_id');
        return $this->db->get('capac_grupo_cursos')->result();

    }
    public function updateCourse($data, $id) {
        $this->db->where('cur_id', $id);
        $this->db->set($data)->update('capac_grupo_cursos');
    }
    public function getCourseById($id) {
        $this->db->where('cur_id', $id);
        return $this->db->get('capac_grupo_cursos')->row();
    }

}
