<?php
/**
* Model that represents Agenda at database
**/
class ModelAgenda extends CI_Model{
	/**
	* Model Fields	
	**/
    var $ag_id;
    var $ag_nome;
    var $ag_numero;
    var $ag_cidade;
    var $ag_obs;


    public function __construct(){
	parent::__construct();
    }
   
    /**
    * Persist object
    **/
    public function inserir($dados){
            $this->db->set($dados)->insert('agenda');
    }

    public function update($data, $id) {
        $this->db->where('ag_id', $id);
        $this->db->set($data)->update(agenda);
    }

    /**
    * Delete object
    **/
    public function delete($id){
	return $this->db->delete('agenda', array('ag_id' => $id));
    }
	/**
    * Return all objects
    **/
    public function all(){
    	return $this->db->get('agenda')->result();
    }
    /**
    * Return object that has $id
    **/
    public function getById($id){
		return $this->db->get_where('agenda', array('ag_id' => $id))->row();
	}
	/**
    * Return object filtered by $where
    * $where must be an array
    **/
    public function getBy($where){
	return $this->db->get_where('agenda', $where);
    }
}
