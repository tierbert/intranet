<?php

/* * * Model  * */

class Prosoftmodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getCompetencias() {
        return $this->db->get('rh__competencias');
    }

    public function getByCompetencia($idCompetencia) {
        $this->db->join('salalivre.professores', 'pro_registroRh = cd_idProfessor', 'left');
        $this->db->join('rh__competencias', 'cd_idCompetencia = com_id', 'left');
        $this->db->where('cd_idCompetencia', $idCompetencia);
        $this->db->order_by('pro_nome');
        $dados = $this->db->get('rh__competenciaDados');

        //echo $this->db->last_query();

        return $dados;
    }

    public function updateCompDados($post, $id) {
        $this->db->where('cd_id', $id);
        $this->db->set($post)->update('rh__competenciaDados');
    }

    public function getBaseCalculo() {
        $graduado = $this->db->get_where('rh__baseCalculo', "bc_titulacao = 'GRADUADO'")->row();
        $espc = $this->db->get_where('rh__baseCalculo', "bc_titulacao = 'ESPECIALISTA'")->row();
        $mest = $this->db->get_where('rh__baseCalculo', "bc_titulacao = 'MESTRE'")->row();
        $doc = $this->db->get_where('rh__baseCalculo', "bc_titulacao = 'DOUTOR'")->row();
        $supEstag = $this->db->get_where('rh__baseCalculo', "bc_titulacao = 'SUP_ESTAGIO'")->row();
        $mestrado = $this->db->get_where('rh__baseCalculo', "bc_titulacao = 'MESTRADO'")->row();
        $base = array();

        $base['graduado_integral'] = $graduado->bc_valorRegIntegral;
        $base['graduado_hora'] = $graduado->bc_valorHoraAula;

        $base['especialista_integral'] = $espc->bc_valorRegIntegral;
        $base['especialista_hora'] = $espc->bc_valorHoraAula;

        $base['mestre_integral'] = $mest->bc_valorRegIntegral;
        $base['mestre_hora'] = $mest->bc_valorHoraAula;

        $base['doutor_integral'] = $doc->bc_valorRegIntegral;
        $base['doutor_hora'] = $doc->bc_valorHoraAula;

        $base['mestrado'] = $mestrado->bc_valorRegIntegral;

        $base['sup_estagio'] = $supEstag->bc_valorHoraAula;

        //print_r($base);

        return $base;
    }

    public function getByCompRegime($idCompetencia) {
        $this->db->select('count(pro_regime) as totalRegime');
        //$this->db->select('count(pro_titulacao) as totalTitulacao');
        $this->db->select('pro_regime');
        $this->db->join('salalivre.professores', 'pro_registroRh = cd_idProfessor', 'left');
        $this->db->where('cd_idCompetencia', $idCompetencia);
        $this->db->group_by('pro_regime');
        //$this->db->order_by('pro_nome');
        $dados = $this->db->get('rh__competenciaDados');

        //echo $this->db->last_query();

        $totais = array();
        foreach ($dados->result() as $dado) {
            //echo $dado->pro_titulacao;
            $totais[$dado->pro_regime] = $dado->totalRegime;
        }
        return $totais;
    }

    public function baseCalculo() {
        $base = $this->db->get('rh__baseCalculo');

        return $base;
    }

    public function getCdById($idCd) {
        $this->db->where('cd_id', $idCd);
        return $this->db->get('rh__competenciaDados')->row();
    }

    public function deleteCd($idCd) {
        return $this->db->delete('rh__competenciaDados', array('cd_id' => $idCd));
    }

    public function getProfByComp($idCompetencia, $idProf) {
        $this->db->where('cd_idCompetencia', $idCompetencia);
        $this->db->where('cd_idProfessor', $idProf);
        return $this->db->get('rh__competenciaDados');
    }

    public function insertProfByComp($idCd, $post) {
        $this->db->set($post)->insert('rh__competenciaDados');

        //echo $this->db->last_query();
    }

    public function getAulasbyCod($codigo) {
        $this->db->where('aul_codigo', $codigo);
        $prof = $this->db->get('prosoft_aulas')->row();
        return $prof;
    }

    public function getAulasArray() {

        //$this->salaLivre->order_by('pro_nome');
        $prof = $this->db->get('prosoft_aulas')->result_array();
        $dados = array();
        $dados[''] = '';
        foreach ($prof as $key => $prof) {
            $dados[$prof['aul_codigo']] = $prof['aul_codigo'] . ' - ' . $prof['aul_nome'];
        }
        return $dados;
    }

    public function getDivisaobyCod($codigo) {
        $this->db->where('div_codigo', $codigo);
        $prof = $this->db->get('prosoft_divisao')->row();
        return $prof;
    }

    public function getCompbyId($id) {
        $this->db->where('com_id', $id);
        $dados = $this->db->get('rh__competencias')->row();

        //echo $this->db->last_query();

        return $dados;
    }

    public function getDivisaoArray() {

        //$this->salaLivre->order_by('pro_nome');
        $prof = $this->db->get('prosoft_divisao')->result_array();
        $dados = array();
        $dados[''] = '';
        foreach ($prof as $key => $prof) {
            $dados[$prof['div_codigo']] = $prof['div_codigo'] . ' - ' . $prof['div_descricao'];
        }
        return $dados;
    }

    public function compCad($post) {
        $this->db->set($post)->insert('rh__competencias');

        return $this->db->insert_id();
    }

}
