<?php

/* * * Model solicitacao_disciplinas * */

class Solicitacao_disciplinasmodel extends CI_Model {

    /**  Campos* */
    var $sd_id;
    var $sd_idSolicitacao;
    var $sd_idDisciplina;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados) {
        $this->db->set($dados)->insert('solicitacao_disciplinas');
    }

    public function update($data, $id) {
        $this->db->where('sd_id', $id);
        $this->db->set($data)->update('solicitacao_disciplinas');
    }

    function contaRegistros() {
        return $this->db->count_all_results('solicitacao_disciplinas');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('solicitacao_disciplinas', array('sd_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim) {
        return $this->db->get('solicitacao_disciplinas', $fim, $ini)->result();
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('solicitacao_disciplinas', array('sd_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('solicitacao_disciplinas', $where);
    }

    public function getBySolic($idSolicitacao) {
        $this->db->join('horario', 'hor_id = sd_idDisciplina');
        return $this->db->get_where('solicitacao_disciplinas', array('sd_idSolicitacao' => $idSolicitacao));
    }

 

}
