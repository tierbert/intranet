<?php

/* * * Model solicitacoes * */

class Solicitacoesmodel extends CI_Model {

    /**  Campos* */
    var $sol_id;
    var $sol_idAluno;
    var $sol_data;
    var $sol_valor;
    var $sol_dataPagamento;
    var $sol_status;
    var $semestre = '2019-1';

    public function __construct() {
        parent::__construct();
    }

    public function inserir($disciplinas, $idAluno) {

        $dadosSolic['sol_idAluno'] = $idAluno;
        $dadosSolic['sol_valor'] = count($disciplinas) * 75;
        $dadosSolic['sol_semestre'] = $this->semestre;

        $this->db->set($dadosSolic)->insert('solicitacoes');

        $idSolicitacao = $this->db->insert_id();

        foreach ($disciplinas as $disciplina) {
            $dadosDisc['sd_idSolicitacao'] = $idSolicitacao;
            $dadosDisc['sd_idDisciplina'] = $disciplina;
            $this->db->set($dadosDisc)->insert('solicitacao_disciplinas');
        }

        return $idSolicitacao;
    }

    public function update($data, $id) {
        $idUsuario = $this->session->userdata('usu_id');

        $data['sol_dataPagamento'] = $this->util->data($data['sol_dataPagamento']);
        $data['sol_idUsuarioBaixa'] = $idUsuario;
        $this->db->where('sol_id', $id);
        $this->db->set($data)->update('solicitacoes');
    }

    public function cancelaAluno($id) {
        $dados['sol_status'] = 'cancelado';
        $this->db->where('sol_id', $id);
        $this->db->set($dados)->update('solicitacoes');
    }

    function contaRegistros() {
        return $this->db->count_all_results('solicitacoes');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('solicitacoes', array('sol_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim, $post) {
        if ($post && $post['id'] != '') {
            $this->db->where('sol_id', $post['id']);
        }

        if ($post && $post['nome'] != '') {
            $this->db->like('alu_nome', $post['nome']);
        }

        // $this->db->where('sol_semestre', $this->semestre);
        
        $this->db->join('alunos', 'alu_id = sol_idAluno', 'left');
        $dados = $this->db->get('solicitacoes', $fim, $ini);

        //echo $this->db->last_query();

        return $dados;
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        $this->db->join('alunos', 'alu_id = sol_idAluno');
        return $this->db->get_where('solicitacoes', array('sol_id' => $id));
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('solicitacoes', $where);
    }

    public function listarAluno($idAluno) {
        //$this->db->where("sol_status != 'cancelado'");
        $this->db->where("sol_semestre", $this->semestre);
        $this->db->where("sol_idAluno", $idAluno);
        return $this->db->get('solicitacoes');
    }

    public function totais() {
        // $dados = $this->db->query("SELECT count(*) as total, sol_status FROM `solicitacoes` where sol_semestre = '$this->semestre' group by sol_status");
        $dados = $this->db->query("SELECT count(*) as total, sol_status FROM `solicitacoes`  group by sol_status");


        $totais = array();
        foreach ($dados->result() as $dado) {
            $totais[$dado->sol_status] = $dado->total;
        }
        return $totais;
    }

    public function totaisDisciplina($idDisciplina, $semestre) {


        $this->db->select('sol_status');
        $this->db->select('count(*) as total');
        $this->db->where('sd_idDisciplina', $idDisciplina);

        if ($semestre != '') {
            $this->db->where("sol_semestre", $semestre);
        }

        $this->db->join('solicitacoes', 'sol_id = sd_idSolicitacao');
        $this->db->group_by('sol_status');
        $dados = $this->db->get('solicitacao_disciplinas');

        //echo $dados->num_rows();

        $totais = array();
        foreach ($dados->result() as $dado) {
            $totais[$dado->sol_status] = $dado->total;
            //  echo $dado->sol_status .' - '.$dado->total . '<br>';
        }

        return $totais;
    }

    public function relatStatus($idDisciplina, $status, $semestre) {
        if ($status != 'TODOS') {
            if ($status == 'AGUARDANDO') {
                $this->db->where('sol_status', NULL);
            } else {
                $this->db->where('sol_status', $status);
            }
        }
        if ($semestre != '') {
            $this->db->where('sol_semestre', $semestre);
        }
        $this->db->where('hor_id', $idDisciplina);
        $this->db->join('solicitacoes', 'sol_id = sd_idSolicitacao');
        $this->db->join('horario', 'hor_id = sd_idDisciplina');
        $this->db->join('alunos', 'solicitacoes.sol_idAluno = alu_id');
        return $this->db->get('solicitacao_disciplinas');
    }

    public function relatPagos($idDisciplina) {
        $this->db->where("sol_semestre", $this->semestre);
        $this->db->where('hor_id', $idDisciplina);
        $names = array('pago', 'baixado');
        $this->db->where_in('sol_status', $names);
        $this->db->join('solicitacoes', 'sol_id = sd_idSolicitacao');
        $this->db->join('horario', 'hor_id = sd_idDisciplina');
        $this->db->join('alunos', 'solicitacoes.sol_idAluno = alu_id');
        $this->db->order_by('alu_nome');
        return $this->db->get('solicitacao_disciplinas');
    }

}
