<?php

/* * * Model agendamento__horario * */

class Agendamento__horariomodel extends CI_Model {

    /**  Campos* */
    var $ahh_id;
    var $ahh_data;
    var $ahh_turno;
    var $ahh_vagas;
    var $ahh_tipo;
    var $ahh_curso;
    var $ahh_professor;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados) {
        $dados['ahh_data'] = $this->util->data($dados['ahh_data']);
        $this->db->set($dados)->insert('agendamento__horario');
    }

    public function update($data, $id) {
        $data['ahh_data'] = $this->util->data($data['ahh_data']);
        $this->db->where('ahh_id', $id);
        $this->db->set($data)->update('agendamento__horario');
    }

    function contaRegistros() {
        return $this->db->count_all_results('agendamento__horario');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('agendamento__horario', array('ahh_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim) {
        $this->db->order_by('ahh_tipo, ahh_data, ahh_turno');
        return $this->db->get('agendamento__horario', $fim, $ini);
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('agendamento__horario', array('ahh_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('agendamento__horario', $where);
    }

    public function getTipoDias($tipo, $dia) {
        $this->db->select('ahh_data');
        $this->db->where('ahh_tipo', $tipo);
        //$this->db->where('ahh_data', '2018-01-' . $dia);
        $this->db->order_by('ahh_data');
        $this->db->group_by('ahh_data');
        $dados = $this->db->get('agendamento__horario');

        //echo $this->db->last_query();

        return $dados;
    }

    public function getByTipoDia($tipo, $dia) {

        $this->db->where('ahh_tipo', $tipo);
        $this->db->where('ahh_data', '2018-01-' . $dia);
        $this->db->order_by('ahh_turno');
        $dados = $this->db->get('agendamento__horario');

        //echo $this->db->last_query();

        return $dados;
    }

    public function getByTipoDiaCurso($tipo, $idCurso) {
        //$this->db->select('ahh_data');
        $this->db->where('ahh_tipo', $tipo);
        if ($tipo == 1) {
            $this->db->where('cur_codigo', $idCurso);
        }
        $this->db->where("ahh_data >= '" . date('Y-m-d') . "'");
        $this->db->join('cursos', 'cur_id = ahh_curso', 'left');
        $this->db->order_by('ahh_data');
        //$this->db->group_by('ahh_data');
        $dados = $this->db->get('agendamento__horario');

        //echo $this->db->last_query();

        return $dados;
    }

    public function getByTipoDiaCursoTcc($tipo, $idCurso) {
        //$this->db->select('ahh_data');
        $this->db->where('ahh_tipo', $tipo);
        if ($tipo == 1) {
            $this->db->where('cur_codigo', $idCurso);
        }
        $this->db->where("ahh_data >= '" . date('Y-m-d') . "'");
        $this->db->where("ahh_data < CURRENT_DATE() + 7");
        $this->db->join('cursos', 'cur_id = ahh_curso', 'left');
        $this->db->order_by('ahh_data');
        //$this->db->group_by('ahh_data');
        $dados = $this->db->get('agendamento__horario');

        //echo $this->db->last_query();

        return $dados;
    }

    public function dias($tipo, $curso = '') {

        if ($curso != '') {
            $this->db->where("curso", $curso);
        }

        $this->db->where("ahh_data >= current_date()");
        $this->db->where('ahh_tipo', $tipo);
        $this->db->order_by('ahh_data');
        $this->db->group_by('ahh_data');
        return $this->db->get('agendamento__horario');
    }

}
