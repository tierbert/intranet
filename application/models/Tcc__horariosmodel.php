<?php

/* * * Model tcc__horarios * */

class Tcc__horariosmodel extends CI_Model {

    /**  Campos* */
    var $tch_id;
    var $tch_idProfessor;
    var $tch_data;
    var $tch_vagas;
    var $tch_horario;
    var $tch_numSemana;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados, $idProf) {
        $dados['tch_idProfessor'] = $idProf;
        $dados['tch_data'] = $this->util->data($dados['tch_data']);
        $dados['tch_numSemana'] = date("W", strtotime($dados['tch_data']));
        $this->db->set($dados)->insert('tcc__horarios');
    }

    public function update($data, $id) {
        $data['tch_data'] = $this->util->data($data['tch_data']);
        $data['tch_numSemana'] = date("W", strtotime($data['tch_data']));
        $this->db->where('tch_id', $id);
        $this->db->set($data)->update('tcc__horarios');
    }

    function contaRegistros() {
        return $this->db->count_all_results('tcc__horarios');
    }

    /**
     * Delete object
     * */
    public function horDel($id) {
        return $this->db->delete('tcc__horario', array('hor_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim) {
        return $this->db->get('tcc__horarios', $fim, $ini)->result();
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('tcc__horario', array('hor_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('tcc__horarios', $where);
    }

    public function getByProf($idProf) {
        $this->db->order_by('tch_data');
        return $this->db->get_where('tcc__horarios', array('tch_idProfessor' => $idProf));
    }

    public function getHorarios() {
        //$this->db->select('ahh_data');
        //$this->db->where('ahh_tipo', $tipo);

        $this->db->where("tch_data >= '" . date('Y-m-d') . "'");
        $this->db->where("tch_data < CURRENT_DATE() + 7");
        $this->db->order_by('tch_data');
        $dados = $this->db->get('tcc__horarios');

        //echo $this->db->last_query();

        return $dados;
    }

    public function getTurnos() {
        $this->db->order_by('tur_horario');
        $dados = $this->db->get('tcc__turno');
        return $dados;
    }

    public function getHorario($idTurno, $dia) {
        $this->db->join('tcc__professores', 'hor_professor = tcp_id', 'left');
        $this->db->where('hor_idTurno', $idTurno);
        $this->db->where('hor_dia', $dia);
        $dados = $this->db->get('tcc__horario');

        return $dados;
    }

    public function getProfArray() {
        $this->db->order_by('tcp_nome');
        $prof = $this->db->get('tcc__professores')->result_array();
        $dados = array();
        $dados[''] = '';
        foreach ($prof as $key => $prof) {
            $dados[$prof['tcp_id']] = $prof['tcp_nome'];
        }
        return $dados;
    }

    public function horCad($post) {
        $this->db->set($post)->insert('tcc__horario');
    }

    public function alunHorCad($post) {
        $this->db->set($post)->insert('tcc__agendados');
    }

    public function getByAlunData($idHorario, $idAluno, $data) {
        $this->db->where('tca_idHorario', $idHorario);
        $this->db->where('tca_idAluno', $idAluno);
        $this->db->where('tca_data', $data);
        $dados = $this->db->get('tcc__agendados');

        return $dados;
    }

    public function getByHorarioData($idHorario, $data) {
        $this->db->where('tca_idHorario', $idHorario);
        $this->db->where('tca_data', $data);
        $this->db->join('alunos', 'alu_id = tca_idAluno', 'left');
        $dados = $this->db->get('tcc__agendados');

        // echo $this->db->last_query();

        return $dados;
    }

    public function getByAluno($idAluno) {
        $this->db->join('tcc__horario', 'tca_idHorario = hor_id', 'left');
        $this->db->join('tcc__turno', 'hor_idTurno = tur_id', 'left');
        $this->db->join('tcc__professores', 'hor_professor = tcp_id', 'left');
        $this->db->where('tca_idAluno', $idAluno);
        $dados = $this->db->get('tcc__agendados');

        return $dados;
    }

    public function getAgendById($id) {
        return $this->db->get_where('tcc__agendados', array('tca_id' => $id))->row();
    }

    public function reservDel($id) {
        return $this->db->delete('tcc__agendados', array('tca_id' => $id));
    }

}
