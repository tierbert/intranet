<?php

/* * * Model estag__agendados * */

class Estag__agendadosmodel extends CI_Model {

    /**  Campos* */
    var $ea_id;
    var $ea_idAluno;
    var $ea_idVaga;
    var $ea_status;
    var $ea_data;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados) {
        $this->db->set($dados)->insert('estag__agendados');
    }

    public function update($data, $id) {
        $this->db->where('ea_id', $id);
        $this->db->set($data)->update('estag__agendados');
    }

    function contaRegistros() {
        return $this->db->count_all_results('estag__agendados');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('estag__agendados', array('ea_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim) {
        return $this->db->get('estag__agendados', $fim, $ini)->result();
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('estag__agendados', array('ea_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('estag__agendados', $where);
    }

    public function getByAlunoTipo($idAluno, $tipo) {
        $this->db->where('ev_tipo', $tipo);
        $this->db->where('ea_idAluno', $idAluno);
        $this->db->join('estag__vagas', 'ev_id = ea_idVaga');
        $this->db->join('estag__locais', 'el_id = ev_idLocal');
        $dados = $this->db->get('estag__agendados');

        //echo $this->db->last_query();

        return $dados;
    }

    public function agendadosVaga($idVaga, $tipo) {
        //$this->db->where('ev_tipo', $tipo);
        $this->db->join('alunos', 'alu_id = ea_idAluno');
        $this->db->join('estag__vagas', 'ev_id = ea_idVaga');
        //$this->db->join('estag__locais', 'el_id = ev_idLocal');
        $this->db->where('ea_idVaga', $idVaga);
        $dados = $this->db->get('estag__agendados');

        //echo $this->db->last_query();

        return $dados;
    }

}
