<?php

/* * * Model salalivre.salas * */

class Salasmodel extends CI_Model {

    /**  Campos* */
    var $sal_id;
    var $sal_nome;
    var $sal_abreviatura;
    var $sal_tipo;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados) {
        $this->db->set($dados)->insert('salalivre.salas');
    }

    public function update($data, $id) {
        $this->db->where('sal_id', $id);
        $this->db->set($data)->update('salalivre.salas');
    }

    function contaRegistros() {
        return $this->db->count_all_results('salalivre.salas');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('salalivre.salas', array('alu_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim, $post) {
        if ($post && $post['nome'] != '') {
            $this->db->like('alu_nome', $post['nome']);
        }
        if ($post && $post['matricula'] != '') {
            $this->db->where('alu_matricula', $post['matricula']);
        }
        $this->db->order_by('sal_tipo, sal_nome');
        return $this->db->get('salalivre.salas', $fim, $ini);
    }

    public function all2() {
        $this->db->order_by('sal_tipo, sal_nome');
        return $this->db->get('salalivre.salas');
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('salalivre.salas', array('sal_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('salalivre.salas', $where);
    }

}
