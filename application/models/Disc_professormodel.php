<?php
/*** Model disc_professor **/

class Disc_professormodel extends CI_Model{

     /**  Campos**/
     var $dp_id;
     var $dp_idDisciplina;
     var $dp_idProfessor;
     var $dp_cpfProfessor;


     public function __construct(){
         parent::__construct();
     }

     public function inserir($dados){
         $this->db->set($dados)->insert('disc_professor');
     }

    
    public function update($data, $id) {
        $this->db->where('dp_id', $id);
        $this->db->set($data)->update('disc_professor');
    }
    
     function contaRegistros() {
        return $this->db->count_all_results('disc_professor');
    }
    

    /**
    * Delete object
    **/
    public function delete($id){
	return $this->db->delete('disc_professor', array('dp_id' => $id));
    }
	/**
    * Return all objects
    **/
    public function all($ini, $fim){
    	return $this->db->get('disc_professor', $fim, $ini)->result();
    }
    /**
    * Return object that has $id
    **/
    public function getById($id){
		return $this->db->get_where('disc_professor', array('dp_id' => $id))->row();
	}
	/**
    * Return object filtered by $where
    * $where must be an array
    **/
    public function getBy($where){
	return $this->db->get_where('disc_professor', $where);
    }
}
