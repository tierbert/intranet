<?php

/* * * Model cpp__horarios * */

class Cpp__horariosmodel extends CI_Model {

    /**  Campos* */
    var $tch_id;
    var $tch_idProfessor;
    var $tch_data;
    var $tch_vagas;
    var $tch_horario;
    var $tch_numSemana;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados, $idProf) {
        //$dados['tch_idProfessor'] = $idProf;
        $dados['tch_data'] = $this->util->data($dados['tch_data']);
        $dados['tch_numSemana'] = date("W", strtotime($dados['tch_data']));
        $this->db->set($dados)->insert('cpp__horarios');
    }

    public function update($data, $id) {
        $data['tch_data'] = $this->util->data($data['tch_data']);
        $data['tch_numSemana'] = date("W", strtotime($data['tch_data']));
        $this->db->where('tch_id', $id);
        $this->db->set($data)->update('cpp__horarios');
    }

    function contaRegistros() {
        return $this->db->count_all_results('cpp__horarios');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('cpp__horarios', array('tch_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all() {
        $this->db->order_by('tch_data','desc');
        return $this->db->get('cpp__horarios');
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('cpp__horarios', array('tch_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('cpp__horarios', $where);
    }

    public function getByProf($idProf) {
        $this->db->order_by('tch_data');
        return $this->db->get_where('cpp__horarios', array('tch_idProfessor' => $idProf));
    }

    public function getHorarios() {
        $this->db->where("tch_data >= '" . date('Y-m-d') . "'");
        //$this->db->where("tch_data < CURRENT_DATE() + 7");
        $this->db->order_by('tch_data');
        $dados = $this->db->get('cpp__horarios');

        //echo $this->db->last_query();

        return $dados;
    }

}
