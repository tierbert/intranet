<?php

/* * * Model rh__descontoCurso * */

class Rh__descontocursomodel extends CI_Model {

    /**  Campos* */
    var $dc_id;
    var $dc_idUsuario;
    var $dc_idProfessor;
    var $dc_tipoCurso;
    var $dc_beneficiario;
    var $dc_familiarNome;
    var $dc_familiarParentesco;
    var $dc_curso;
    var $dc_semestre;
    var $dc_observacoes;
    var $dc_rhTempoServico;
    var $dc_gerParecer;
    var $dc_gerPercentual;
    var $dc_gerObservacoes;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados) {
        $dados['dc_idUsuario'] = $this->session->userdata('usu_id');
        $this->db->set($dados)->insert('rh__descontoCurso');
    }

    public function update($data, $id) {
        $this->db->where('dc_id', $id);
        $this->db->set($data)->update('rh__descontoCurso');
    }

    function contaRegistros() {
        return $this->db->count_all_results('rh__descontoCurso');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('rh__descontoCurso', array('dc_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all() {
        return $this->db->get('rh__descontoCurso');
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        $this->db->join('usuarios', 'usu_id = dc_idUsuario');
        return $this->db->get_where('rh__descontoCurso', array('dc_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('rh__descontoCurso', $where);
    }

    public function getByFunc($idFunc = '', $idProf = '') {
        if ($idFunc != '') {
            $this->db->where('  dc_idUsuario', $idFunc);
        }

        return $this->db->get('rh__descontoCurso');
    }

    public function getByPos() {
        $this->db->where('dc_rhParecer', 'DEFERIDO');
        $this->db->where('dc_tipoCurso', 'POS_GRADUACAO');
        return $this->db->get('rh__descontoCurso');
    }

    public function getByMest() {
        $this->db->where('dc_rhParecer', 'DEFERIDO');
        $this->db->where('dc_tipoCurso', 'MESTRADO');
        return $this->db->get('rh__descontoCurso');
    }

}
