<?php

/* * * Model horario * */

class Horariomodel extends CI_Model {

    /**  Campos* */
    var $hor_id;
    var $hor_disciplina;
    var $hor_turno;
    var $hor_turma;
    var $hor_prof;
    var $semestre = '2019-1';

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados) {
        $this->db->set($dados)->insert('horario');
    }

    public function update($data, $id) {
        $this->db->where('hor_id', $id);
        $this->db->set($data)->update('horario');
    }

    function contaRegistros() {
        return $this->db->count_all_results('horario');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('horario', array('hor_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim, $post) {
        if ($post) {
            if ($post['nome']) {
                $this->db->like('hor_prof', $post['nome']);
            }
        }

        $this->db->where('hor_semestre', $this->semestre);
        $this->db->order_by('hor_prof');
        return $this->db->get('horario', $fim, $ini)->result();
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('horario', array('hor_id' => $id))->row();
    }

    public function getBy($where) {
        return $this->db->get_where('horario', $where);
    }

    public function professores() {
        $this->db->select('hor_prof');
        $this->db->group_by('hor_prof');
        $this->db->where('hor_semestre', $this->semestre);
        $this->db->order_by('hor_prof');
        $prof = $this->db->get('horario')->result_array();
        $dados = array();
        $dados[0] = 'Pesquise por Professor';
        //var_dump($prof);
        foreach ($prof as $pro) {
            $dados[$pro['hor_prof']] = $pro['hor_prof'];
        }
        return $dados;
    }

    public function professoresArray() {
        $this->db->select('hor_prof, hor_idProfessor');
        $this->db->group_by('hor_prof');
        $this->db->order_by('hor_prof');
        $prof = $this->db->get('horario')->result_array();
        $dados = array();
        $dados[0] = 'Pesquise por Professor';
        //var_dump($prof);
        foreach ($prof as $pro) {
            $dados[$pro['hor_idProfessor']] = $pro['hor_prof'];
        }
        return $dados;
    }

    public function solicitacao($post) {
        if ($post['disciplina'] != '') {
            $this->db->like('hor_disciplina', $post['disciplina']);
        }
        //var_dump($post['prof']);
        if ($post['prof'] != '0') {
            $this->db->like('hor_prof', $post['prof']);
        }

        //$this->db->where('hor_status', 1);
        $this->db->where('hor_semestre', $this->semestre);
        $this->db->order_by('hor_disciplina');
        return $this->db->get('horario');
    }

    public function solicitacaoAssi($post, $semestre) {
        if ($post['disciplina'] != '') {
            $this->db->like('hor_disciplina', $post['disciplina']);
        }
        if ($post['prof'] != '0') {
            $this->db->like('hor_prof', $post['prof']);
        }

        if ($semestre != '') {
            $this->db->where('hor_semestre', $semestre);
        }



        return $this->db->get('horario');
    }

    public function getByProf($idProf, $semestre) {
        if ($semestre != '') {
            $this->db->where('hor_semestre', $semestre);
        }

        $dados = $this->db->get_where('horario', array('hor_idProfessor' => $idProf));

        //echo $this->db->last_query();

        return $dados;
    }

}
