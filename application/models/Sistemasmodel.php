<?php

/* * * Model solicitacoes * */

class Sistemasmodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function cadCargoAdministrativo($post) {

        $post['sp_graduacaoInicio'] = $this->util->data($post['sp_graduacaoInicio']);
        $post['sp_graduacaoFim'] = $this->util->data($post['sp_graduacaoFim']);
        $post['sp_especializacaoInicio'] = $this->util->data($post['sp_especializacaoInicio']);
        $post['sp_especializacaoFim'] = $this->util->data($post['sp_especializacaoFim']);
        $post['sp_mestradoInicio'] = $this->util->data($post['sp_mestradoInicio']);
        $post['sp_mestradoFim'] = $this->util->data($post['sp_mestradoFim']);
        $post['sp_doutoradoInicio'] = $this->util->data($post['sp_doutoradoInicio']);
        $post['sp_doutoradoFim'] = $this->util->data($post['sp_doutoradoFim']);

        $this->db->set($post)->insert('rh__selecaoProfessores');
    }

    public function update($data, $id) {
        $idUsuario = $this->session->userdata('usu_id');

        $data['sol_dataPagamento'] = $this->util->data($data['sol_dataPagamento']);
        $data['sol_idUsuarioBaixa'] = $idUsuario;
        $this->db->where('sol_id', $id);
        $this->db->set($data)->update('solicitacoes');
    }

    public function delete($id) {
        return $this->db->delete('solicitacoes', array('sol_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim, $post) {
        if ($post && $post['id'] != '') {
            $this->db->where('sol_id', $post['id']);
        }

        if ($post && $post['nome'] != '') {
            $this->db->like('alu_nome', $post['nome']);
        }

        $this->db->where('sol_semestre', '20172');
        $this->db->join('alunos', 'alu_id = sol_idAluno', 'left');
        $dados = $this->db->get('solicitacoes', $fim, $ini);

        //echo $this->db->last_query();

        return $dados;
    }

    public function getById($id) {
        $this->db->join('alunos', 'alu_id = sol_idAluno');
        return $this->db->get_where('solicitacoes', array('sol_id' => $id));
    }

    public function posgraduacaoGetCursosByCod($codCurso) {
        $this->db->where('pc_codigo', $codCurso);
        $cursos = $this->db->get('cad__posgraduacao_cursos');

        return $cursos;
    }

    public function posgraduacaoCad($post) {
        $post['pos_dataNascimento'] = $this->util->data($post['pos_dataNascimento']);
        $this->db->set($post)->insert('cad__posgraduacao');
    }

    public function posgraduacaoCursosArray() {
        $this->db->order_by('pc_nomeCurso');
        $cursos = $this->db->get('cad__posgraduacao_cursos');

        $dados[''] = 'ESCOLHA O CURSO!';
        foreach ($cursos->result() as $curso) {
            $dados[$curso->pc_codigo] = $curso->pc_nomeCurso;
        }
        return $dados;
    }

}
