<?php

/**
 * Model that represents Agenda at database
 * */
class Esportemodel extends CI_Model {
    /* tabelas */

    var $tb_agendados = "esp__agendados";
    var $tb_horarios = "esp__horario";
    var $tb_modalidades = "esp__modalidades";
    var $tb_turnos = "esp__turno";

    public function __construct() {
        parent::__construct();
    }

    public function getTurnos() {
        $this->db->order_by('tur_horario');
        $dados = $this->db->get($this->tb_turnos);
        return $dados;
    }

    public function getHorario($idTurno, $dia) {
        //$this->db->join('tcc__professores', 'hor_professor = tcp_id', 'left');
        $this->db->where('hor_idTurno', $idTurno);
        $this->db->where('hor_dia', $dia);
        $dados = $this->db->get($this->tb_horarios);

        return $dados;
    }

    public function getmodalidades() {
        $dados = $this->db->get($this->tb_modalidades);
        $dadosArray[''] = "Escolha a Modalidade";
        foreach ($dados->result() as $dado) {
            $dadosArray[$dado->emo_id] = $dado->emo_nome;
        }
        return $dadosArray;
    }

    public function horCad($post) {
        $this->db->set($post)->insert($this->tb_horarios);
    }

    public function getByHorarioData($idHorario, $data) {
        $this->db->where('eag_idHorario', $idHorario);
        $this->db->where('eag_data', $data);
        $this->db->join('alunos', 'alu_id = eag_idAluno', 'left');
        $dados = $this->db->get($this->tb_agendados);

        // echo $this->db->last_query();

        return $dados;
    }

    public function inserirAgenda($post) {
        $this->db->set($post)->insert($this->tb_agendados);
    }

    public function getReservasByAluno($idAluno) {
        $this->db->where('eag_idAluno', $idAluno);
        //$this->db->join('alunos', 'alu_id = tca_idAluno', 'left');
        $this->db->join('esp__horario', 'eag_idHorario = hor_id', 'left');
        $this->db->join('esp__turno', 'hor_idTurno = tur_id', 'left');
        $dados = $this->db->get($this->tb_agendados);

        // echo $this->db->last_query();

        return $dados;
    }

    public function getByDataAluno($data, $idAluno) {
        $this->db->where('eag_idAluno', $idAluno);
        $this->db->where('eag_data', $data);
        $dados = $this->db->get($this->tb_agendados);

        //echo $this->db->last_query();

        return $dados;
    }

}
