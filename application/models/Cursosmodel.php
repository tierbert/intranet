<?php

/* * * Model cursos * */

class Cursosmodel extends CI_Model {

    /**  Campos* */
    var $cur_id;
    var $cur_nome;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados) {
        $this->db->set($dados)->insert('cursos');
    }

    public function update($data, $id) {
        $this->db->where('cur_id', $id);
        $this->db->set($data)->update('cursos');
    }

    function contaRegistros() {
        return $this->db->count_all_results('cursos');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('cursos', array('cur_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim) {
        return $this->db->get('cursos', $fim, $ini)->result();
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('cursos', array('cur_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('cursos', $where);
    }

    public function getArray() {
        $this->db->order_by('cur_nome');
        $prof = $this->db->get('cursos')->result_array();
        $dados = array();
        $dados[''] = '';
        foreach ($prof as $key => $prof) {
            $dados[$prof['cur_id']] = $prof['cur_nome'];
        }
        return $dados;
    }

    public function getByTipo($tipo) {
        $this->db->where('cur_tipo', $tipo);
        $this->db->order_by('cur_nome');
        $dados = $this->db->get('cursos');

        //echo $this->db->last_query();

        return $dados;
    }

}
