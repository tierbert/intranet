<?php
/*** Model disciplinas **/

class Disciplinasmodel extends CI_Model{

     /**  Campos**/
     var $dis_id;
     var $dis_idCurso;
     var $dis_codigo;
     var $dis_nome;
     var $dis_turma;
     var $dis_nivel;
     var $dis_obs;
     var $dis_dataProva;
     var $dis_horaProva;
     var $dis_salaProva;
     var $dis_idProfessor;


     public function __construct(){
         parent::__construct();
     }

     public function inserir($dados){
         $this->db->set($dados)->insert('disciplinas');
     }

    
    public function update($data, $id) {
        $this->db->where('dis_id', $id);
        $this->db->set($data)->update('disciplinas');
    }
    
     function contaRegistros() {
        return $this->db->count_all_results('disciplinas');
    }
    

    /**
    * Delete object
    **/
    public function delete($id){
	return $this->db->delete('disciplinas', array('dis_id' => $id));
    }
	/**
    * Return all objects
    **/
    public function all($ini, $fim){
        
        $this->db->join('disc_professor','dis_codigo = dp_idDisciplina');
        $this->db->join('professores','dp_cpfProfessor = pro_cpf');
        $this->db->join('cursos','dis_idCurso = cur_id');
        
    	return $this->db->get('disciplinas', $fim, $ini)->result();
    }
    /**
    * Return object that has $id
    **/
    public function getById($id){
		return $this->db->get_where('disciplinas', array('dis_id' => $id))->row();
	}
	/**
    * Return object filtered by $where
    * $where must be an array
    **/
    public function getBy($where){
	return $this->db->get_where('disciplinas', $where);
    }
}
