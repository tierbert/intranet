<?php

/* * * Model rh__ajustePonto * */

class Rh__ajustepontomodel extends CI_Model {

    /**  Campos* */
    var $ap_id;
    var $ap_idUsuario;
    var $ap_idProfessor;
    var $ap_dataAjuste;
    var $ap_correcao;
    var $ap_justificativa;
    var $ap_coordParecer;
    var $ap_coordObservacoes;
    var $ap_rhParecer;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados) {
        $dados['ap_idUsuario'] = $this->session->userdata('usu_id');
        $dados['ap_dataAjuste'] = $this->util->data($dados['ap_dataAjuste']);
        $this->db->set($dados)->insert('rh__ajustePonto');
    }

    public function update($data, $id) {
        $this->db->where('ap_id', $id);
        $this->db->set($data)->update('rh__ajustePonto');
    }

    function contaRegistros() {
        return $this->db->count_all_results('rh__ajustePonto');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('rh__ajustePonto', array('ap_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim) {
        return $this->db->get('rh__ajustePonto', $fim, $ini)->result();
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('rh__ajustePonto', array('ap_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('rh__ajustePonto', $where);
    }

    public function getByFunc($idFunc) {
        return $this->db->get_where('rh__ajustePonto', array('ap_idUsuario' => $idFunc));
    }

    public function getByCoord($setor) {
        $this->db->where('set_nome', $setor);
        $this->db->join('usuarios', 'ap_idUsuario = usu_id');
        $this->db->join('rh__setores', 'usu_setor = set_id');
        return $this->db->get('rh__ajustePonto');
    }

    public function getByRh() {
        $this->db->where('ap_coordParecer is not null');
        return $this->db->get('rh__ajustePonto');
    }

}
