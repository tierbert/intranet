<?php

/* * * Model hor_turno * */

class Hor_turnomodel extends CI_Model {

    /**  Campos* */
    var $tur_id;
    var $tur_turno;
    var $tur_horario;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados) {
        $this->db->set($dados)->insert('hor_turno');
    }

    public function update($data, $id) {
        $this->db->where('tur_id', $id);
        $this->db->set($data)->update('hor_turno');
    }

    function contaRegistros() {
        return $this->db->count_all_results('hor_turno');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('hor_turno', array('tur_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function getAll($tipo) {
        if ($tipo == 'lab') {
            $tipo = 'sala';
        }

        $this->db->where('tur_tipo', strtoupper($tipo));
        return $this->db->get('hor_turno');
    }

    public function getTurnosByType($tipo){
        if($tipo == 'lab'){
            $this->db->where('tur_tipo', 'LABSAUDE2')
                ->or_where('tur_tipo', 'LABSAUDE');    
        } else {
            $this->db->where('tur_tipo', $tipo);    
        }
        
        return $this->db->get('hor_turno')->result();
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('hor_turno', array('tur_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('hor_turno', $where);
    }

}
