<?php
/*** Model profLife **/

class Proflifemodel extends CI_Model{

     /**  Campos**/
     var $pl_id;
     var $pl_discNome;
     var $pl_discCodigo;
     var $pl_profNome;
     var $pl_profCpf;


     public function __construct(){
         parent::__construct();
     }

     public function inserir($dados){
         $this->db->set($dados)->insert('profLife');
     }

    
    public function update($data, $id) {
        $this->db->where('pl_id', $id);
        $this->db->set($data)->update('profLife');
    }
    
     function contaRegistros() {
        return $this->db->count_all_results('profLife');
    }
    

    /**
    * Delete object
    **/
    public function delete($id){
	return $this->db->delete('profLife', array('pl_id' => $id));
    }
	/**
    * Return all objects
    **/
    public function all($ini, $fim){
    	return $this->db->get('profLife', $fim, $ini)->result();
    }
    /**
    * Return object that has $id
    **/
    public function getById($id){
		return $this->db->get_where('profLife', array('pl_id' => $id))->row();
	}
	/**
    * Return object filtered by $where
    * $where must be an array
    **/
    public function getBy($where){
	return $this->db->get_where('profLife', $where);
    }
}
