<?php

/* * * Model alunos * */

class Alunosmodel extends CI_Model {

    /**  Campos* */
    var $alu_id;
    var $alu_matricula;
    var $alu_nome;
    var $alu_cpf;
    var $alu_senha;
    var $alu_idCurso;
    var $alu_email;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados) {
        $this->db->set($dados)->insert('alunos');
    }

    public function update($data, $id) {
        $this->db->where('alu_id', $id);
        $this->db->set($data)->update('alunos');
    }

    function contaRegistros() {
        return $this->db->count_all_results('alunos');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('alunos', array('alu_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim, $post) {
        if ($post && $post['nome'] != '') {
            $this->db->like('alu_nome', $post['nome']);
        }
        if ($post && $post['matricula'] != '') {
            $this->db->where('alu_matricula', $post['matricula']);
        }
        return $this->db->get('alunos', $fim, $ini);
    }

    public function all2() {
        return $this->db->get('alunos');
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('alunos', array('alu_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('alunos', $where);
    }

}
