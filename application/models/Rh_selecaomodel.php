<?php

/* * * Model solicitacoes * */

class Rh_selecaomodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function cadCargoAdministrativo($post) {

        $post['sp_graduacaoInicio'] = $this->util->data($post['sp_graduacaoInicio']);
        $post['sp_graduacaoFim'] = $this->util->data($post['sp_graduacaoFim']);
        $post['sp_especializacaoInicio'] = $this->util->data($post['sp_especializacaoInicio']);
        $post['sp_especializacaoFim'] = $this->util->data($post['sp_especializacaoFim']);
        $post['sp_mestradoInicio'] = $this->util->data($post['sp_mestradoInicio']);
        $post['sp_mestradoFim'] = $this->util->data($post['sp_mestradoFim']);
        $post['sp_doutoradoInicio'] = $this->util->data($post['sp_doutoradoInicio']);
        $post['sp_doutoradoFim'] = $this->util->data($post['sp_doutoradoFim']);

        $this->db->set($post)->insert('rh__selecaoProfessores');
    }

    public function getDocentes() {
        $this->db->order_by('sp_dataCriacao', 'desc');
        return $this->db->get('rh__selecaoProfessores');
    }

    public function getById($id) {
        return $this->db->get_where('rh__selecaoProfessores', array('sp_id' => $id))->row();
    }

    public function update($data, $id) {
        $idUsuario = $this->session->userdata('usu_id');

        $data['sol_dataPagamento'] = $this->util->data($data['sol_dataPagamento']);
        $data['sol_idUsuarioBaixa'] = $idUsuario;
        $this->db->where('sol_id', $id);
        $this->db->set($data)->update('solicitacoes');
    }

    public function delete($id) {
        return $this->db->delete('solicitacoes', array('sol_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim, $post) {
        if ($post && $post['id'] != '') {
            $this->db->where('sol_id', $post['id']);
        }

        if ($post && $post['nome'] != '') {
            $this->db->like('alu_nome', $post['nome']);
        }

        $this->db->where('sol_semestre', '20172');
        $this->db->join('alunos', 'alu_id = sol_idAluno', 'left');
        $dados = $this->db->get('solicitacoes', $fim, $ini);

        //echo $this->db->last_query();

        return $dados;
    }

}
