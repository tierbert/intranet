<?php

/* * * Model estag__vagas_dir * */

class Estag__vagas_dirmodel extends CI_Model {

    /**  Campos* */
    var $ev_id;
    var $ev_idLocal;
    var $ev_periodo;
    var $ev_vagas;
    var $ev_tipo;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados) {
        $this->db->set($dados)->insert('estag__vagas_dir');
    }

    public function update($data, $id) {
        $this->db->where('ev_id', $id);
        $this->db->set($data)->update('estag__vagas_dir');
    }

    function contaRegistros() {
        return $this->db->count_all_results('estag__vagas_dir');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('estag__vagas_dir', array('ev_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim) {
        return $this->db->get('estag__vagas_dir', $fim, $ini)->result();
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('estag__vagas_dir', array('ev_id' => $id))->row();
    }

    /**
     * Return object filtered by $where
     * $where must be an array
     * */
    public function getBy($where) {
        return $this->db->get_where('estag__vagas_dir', $where);
    }

    public function getBylocal($idLocal, $tipo = '') {
        if ($tipo != '') {
            $this->db->where('ev_tipo', $tipo);
        }
        $this->db->where('ev_idLocal', $idLocal);
        $this->db->join('estag__locais_dir', 'el_id = ev_idLocal');
        return $this->db->get('estag__vagas_dir');
    }

}
