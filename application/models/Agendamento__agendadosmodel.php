<?php

/* * * Model agendamento__agendados * */

class Agendamento__agendadosmodel extends CI_Model {

    /**  Campos* */
    var $aha_id;
    var $aha_idHorario;
    var $aha_idAluno;
    var $aha_conferido;
    var $aha_dataCriacao;

    public function __construct() {
        parent::__construct();
    }

    public function inserir($dados) {
        $this->db->set($dados)->insert('agendamento__agendados');
    }

    public function update($data, $id) {
        $this->db->where('aha_id', $id);
        $this->db->set($data)->update('agendamento__agendados');
    }

    function contaRegistros() {
        return $this->db->count_all_results('agendamento__agendados');
    }

    /**
     * Delete object
     * */
    public function delete($id) {
        return $this->db->delete('agendamento__agendados', array('aha_id' => $id));
    }

    /**
     * Return all objects
     * */
    public function all($ini, $fim, $post) {

        if ($post && $post['nome'] != '') {
            $this->db->like('alu_nome', $post['nome']);
        }

        if ($post && $post['data'] != '') {
            $this->db->like('ahh_data', $this->util->data($post['data']));
        }

        $this->db->join('agendamento__horario', 'aha_idHorario = ahh_id', 'left');
        $this->db->join('agendamento__tipo', 'ahh_tipo = aht_tipo', 'left');
        $this->db->join('alunos', 'alu_id = aha_idAluno', 'left');
        $this->db->order_by('ahh_data, ahh_turno');
        return $this->db->get('agendamento__agendados', $fim, $ini)->result();
    }

    /**
     * Return object that has $id
     * */
    public function getById($id) {
        return $this->db->get_where('agendamento__agendados', array('aha_id' => $id))->row();
    }

    public function getBy($where) {
        return $this->db->get_where('agendamento__agendados', $where);
    }

    public function getByHorario($idHorario, $idCurso = '') {

        if ($idCurso != '') {
            $this->db->where('alu_idCurso', $idCurso);
        }

        $this->db->join('alunos', 'aha_idAluno = alu_id', 'left');
        $this->db->where('aha_idHorario', $idHorario);
        return $this->db->get('agendamento__agendados');
    }

    public function getByAluno($idAluno, $tipo = '') {
        if ($tipo != '') {
            $this->db->where('ahh_tipo', $tipo);
        }
        $this->db->join('agendamento__horario', 'aha_idHorario = ahh_id');
        return $this->db->get_where('agendamento__agendados', array('aha_idAluno' => $idAluno))->row();
    }

    //TCC
    public function getByAlunoTcc($idAluno, $tipo = '') {
        if ($tipo != '') {
            $this->db->where('ahh_tipo', $tipo);
        }
        $this->db->join('agendamento__horario', 'aha_idHorario = ahh_id');
        return $this->db->get_where('agendamento__agendados', array('aha_idAluno' => $idAluno));
    }

    public function vagas($tipo = '', $data = '') {
        $this->db->select("agendamento__horario.*");
        $this->db->select("(select count(*) from agendamento__agendados where aha_idhorario = ahh_id) AS agendados");

        if ($tipo != '') {
            $this->db->where('ahh_tipo', $tipo);
        }

        if ($data != '') {
            $this->db->where('ahh_data', $data);
        }

        $dados = $this->db->get('agendamento__horario');

        //echo $this->db->last_query();

        return $dados;
    }

}
