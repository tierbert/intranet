<?php

/**
 * Model that represents Agenda at database
 * */
class Capacitamodel extends CI_Model {

    var $ag_id;
    var $ag_nome;
    var $ag_numero;
    var $ag_cidade;
    var $ag_obs;
    var $cur_setor;
    var $cur_idGrupo;
    var $cur_curso;
    var $cur_vagas;
    var $cur_sala;
    var $cur_horario;
    var $cur_profissional;
    var $cur_ch;

    public function __construct() {
        parent::__construct();
    }

    public function getCusosBySetor($setor, $idGrupo) {
        //$this->db->where('cur_setor', $setor);
        $this->db->where('cur_idGrupo', $idGrupo);
        return $this->db->get('capac_cursos');
    }

    public function getSetorGrupo($setor, $idGrupo) {
        $this->db->where('age_idCoordenacao', $setor);
        $this->db->where('cur_idGrupo', $idGrupo);
        $this->db->join('capac_cursos', 'cur_id = age_idCurso');
        $this->db->join('usuarios', 'usu_id = age_idusuario', 'left');
        $this->db->order_by('usu_nome');
        return $this->db->get('capac_agendados');
    }

    public function getInscCurso($idCurso) {
        $this->db->where('age_idCurso', $idCurso);
        $this->db->join('capac_cursos', 'cur_id = age_idCurso');
        $this->db->join('usuarios', 'usu_id = age_idusuario', 'left');
        $this->db->join('rh__setores', 'set_id = usu_setor', 'left');
        $this->db->order_by('set_nome, usu_nome');
        return $this->db->get('capac_agendados');
    }

    public function agendar($dados) {
        $this->db->set($dados)->insert('capac_agendados');
    }

    public function getInscritos($idCurso) {
        $this->db->where('age_idCurso', $idCurso);
        return $this->db->get('capac_agendados');
    }

    public function getUsuarioGrupo($idUsuario, $idGrupo) {
        $this->db->where('age_idUsuario', $idUsuario);
        $this->db->where('cur_idGrupo', $idGrupo);
        $this->db->join('capac_cursos', 'cur_id = age_idCurso');
        return $this->db->get('capac_agendados');
    }

    public function getCursos() {
        $this->db->order_by('cur_curso');
        return $this->db->get('capac_cursos');
    }

    public function getById($id) {
        $this->db->where('age_id', $id);
        return $this->db->get('capac_agendados')->row();
    }
    public function getClassById($id) {
        $this->db->where('cur_id', $id);
        return $this->db->get('capac_cursos')->row();
    }
    public function delete($id) {
        return $this->db->delete('capac_agendados', array('age_id' => $id));
    }
    public function deleteclass($id) {
        return $this->db->delete('capac_cursos', array('cur_id' => $id));
    }
    public function inserir($dados) {
        $this->db->set($dados)->insert('capac_cursos');
    }
    public function updateClass($data, $id) {
        $this->db->where('cur_id', $id);
        $this->db->set($data)->update('capac_cursos');
    }

}
