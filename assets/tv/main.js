

$(function () {
    $('#tvbox').tv({
        play: 3000,
        //pause: 2500,
        mousehoverPause: false,
        transitionFX: 'fade',
        onClickNext: true,
        hideNextPrev: true,
        remote: false,
        crossfade: false
    });
});