<?php
include './conexaoMysql.php';
$funcoes = new crud();

$setores = array(
    'FARMÁCIA',
    'ADM.GTI.CONT',
    'ENFERMAGEM',
    'PSICOLOGIA',
    'ENGENHARIA',
    'FISIOTERAPIA',
    'NUTRIÇÃO',
    'DIREITO'
);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <!--[if IE]>
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <![endif]-->
        <title>FILA</title>
        <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <link href="assets/css/style.css" rel="stylesheet" />
    </head>
    <body>
        <section class="menu-section"></section>
        <div class="content-wrapper2">
            <div class="container">
                <div class="row">

                    <div class="col-md-3" >
                        <div class="alert alert-info text-center" >
                            <h2><b> <?php echo $setores[0] ?></b> </h2> 
                            <hr />
                            <h3>Senha</h3>
                            <div class="senha1">

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3" >
                        <div class="alert alert-info text-center">
                            <h2><b> <?php echo $setores[1] ?></b> </h2> 
                            <hr />
                            <h3>Senha</h3>
                            <div class="senha2">

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3" >
                        <div class="alert alert-info text-center">
                            <h2><b> <?php echo $setores[2] ?></b> </h2> 
                            <hr />
                            <h3>Senha</h3>
                            <div class="senha3">

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3" >
                        <div class="alert alert-info text-center">
                            <h2><b> <?php echo $setores[3] ?></b> </h2> 
                            <hr />
                            <h3>Senha</h3>
                            <div class="senha4">

                            </div>
                        </div>
                    </div>

                </div>  

                <div class="row">
                    <div class="col-md-3" >
                        <div class="alert alert-info text-center" >
                            <h2><b> <?php echo $setores[4] ?></b> </h2> 
                            <hr />
                            <h3>Senha</h3>
                            <div class="senha5">

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3" >
                        <div class="alert alert-info text-center">
                            <h2><b> <?php echo $setores[5] ?></b> </h2> 
                            <hr />
                            <h3>Senha</h3>
                            <div class="senha6">

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3" >
                        <div class="alert alert-info text-center">
                            <h2><b> <?php echo $setores[6] ?></b> </h2> 
                            <hr />
                            <h3>Senha</h3>
                            <div class="senha7">

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3" >
                        <div class="alert alert-info text-center">
                            <h2><b> <?php echo $setores[7] ?></b> </h2> 
                            <hr />
                            <h3>Senha</h3>
                            <div class="senha8">

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <!-- CONTENT-WRAPPER SECTION END-->
        <section class="footer-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                    </div>

                </div>
            </div>
        </section>
        <script src="assets/js/jquery-1.10.2.js"></script>
        <script src="assets/js/bootstrap.js"></script>
        <script src="assets/js/custom.js"></script>
    </body>
</html>




<script src="assets/js/jquery-1.10.2.js"></script>
<script src="assets/jquery.jclock.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        loop1();
        loop2();
        loop3();
        loop4();
        loop5();
        loop6();
        loop7();
        loop8();

        $(".piscar").css("opacity", "0.4");//define opacidade inicial
        setInterval(function () {
            if ($(".piscar").css("opacity") == 0) {
                $(".piscar").css("opacity", "1");
            } else {
                $(".piscar").css("opacity", "0");
            }
        }, 200);

        $('.som').click(function () {
            $('embed').remove();
            $('body').append('<embed src="assets/toque1.mp3" autostart="true" hidden="true" loop="false">');
        });


        var options = {
            timeNotation: '24h',
            am_pm: true,
            fontFamily: 'Verdana, Times New Roman',
            fontSize: '60px',
            foreground: 'blue'
        }
        $('.jclock').jclock(options);

    });
    var loop1 = function () {
        $.post('setorRefresh.php', {setor: '<?php echo $setores[0] ?>'}, function (resultado) {
            $('.senha1').html(resultado);
        });
        setTimeout('loop1()', 4000);
    }

    var loop2 = function () {
        $.post('setorRefresh.php', {setor: '<?php echo $setores[1] ?>'}, function (resultado) {
            $('.senha2').html(resultado);
        });
        setTimeout('loop2()', 5000);
    }

    var loop3 = function () {
        $.post('setorRefresh.php', {setor: '<?php echo $setores[2] ?>'}, function (resultado) {
            $('.senha3').html(resultado);
        });
        setTimeout('loop3()', 4000);
    }

    var loop4 = function () {
        $.post('setorRefresh.php', {setor: '<?php echo $setores[3] ?>'}, function (resultado) {
            $('.senha4').html(resultado);
        });
        setTimeout('loop4()', 5000);
    }

    var loop5 = function () {
        $.post('setorRefresh.php', {setor: '<?php echo $setores[4] ?>'}, function (resultado) {
            $('.senha5').html(resultado);
        });
        setTimeout('loop5()', 4000);
    }


    var loop6 = function () {
        $.post('setorRefresh.php', {setor: '<?php echo $setores[5] ?>'}, function (resultado) {
            $('.senha6').html(resultado);
        });
        setTimeout('loop6()', 5000);
    }

    var loop7 = function () {
        $.post('setorRefresh.php', {setor: '<?php echo $setores[6] ?>'}, function (resultado) {
            $('.senha7').html(resultado);
        });
        setTimeout('loop7()', 4000);
    }

    var loop8 = function () {
        $.post('setorRefresh.php', {setor: '<?php echo $setores[7] ?>'}, function (resultado) {
            $('.senha8').html(resultado);
        });
        setTimeout('loop8()', 5000);
    }


</script>


<audio src="assets/toque.mp3" id="mp3" preload="auto"></audio>
<input type="button" value="Clique para tocar o MP3" onClick="playMP3();"  id="btSom" style="display: none;"/>

<script type="text/javascript">
    function playMP3() {
        document.getElementById("mp3").play();

    }
</script>