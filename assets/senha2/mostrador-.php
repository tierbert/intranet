<?php
include './conexaoMysql.php';
$funcoes = new crud();
$setores = array(
    'FARMÁCIA',
    'ADM.GTI.CONT',
    'ENFERMAGEM',
    'PSICOLOGIA',
    'ENGENHARIA',
    'FISIOTERAPIA',
    'NUTRIÇÃO',
    'DIREITO'
);  
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <!--[if IE]>
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <![endif]-->
        <title>FILA</title>
        <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <link href="assets/css/style.css" rel="stylesheet" />
    </head>
    <body>
        <section class="menu-section"></section>
        <div class="content-wrapper2">
            <div class="container">
                <div class="row">

                    <div class="col-md-4" >
                        <div class="alert alert-info text-center" >
                            <h2><b>FIES</b> </h2> 
                            <hr />
                            <h3>Senha</h3>
                            <div class="senha1">

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4" >
                        <div class="alert alert-info text-center">
                            <h2><b> </b></h2>
                            <hr />
                            <h3>Senha</h3>
                            <div class="senha2">

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4" >
                        <div class="alert alert-info text-center">
                            <h2><b> </b></h2>
                            <hr />
                            <h3>Senha</h3>
                            <div class="senha3">

                            </div>
                        </div>
                    </div>

                    

                </div>  

                <section class="menu-section"></section>


                <div class="row">
                    <div class="col-md-6 col-sm-8 col-xs-12">
                        <div id="carousel-example" class="carousel slide slide-bdr" data-ride="carousel" >

                            <div class="carousel-inner">
                                <div class="item active">

                                    <img src="assets/img/1.jpg" alt="" />

                                </div>
                                <div class="item">
                                    <img src="assets/img/2.jpg" alt="" />

                                </div>
                                <div class="item">
                                    <img src="assets/img/3.jpg" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12" >
                        <div class="jclock well"></div>
                        <img src="assets/img/banner.jpg" width="100%">
                    </div>

                </div>

            </div>
        </div>
        <!-- CONTENT-WRAPPER SECTION END-->
        <section class="footer-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                    </div>

                </div>
            </div>
        </section>
        <script src="assets/js/jquery-1.10.2.js"></script>
        <script src="assets/js/bootstrap.js"></script>
        <script src="assets/js/custom.js"></script>
    </body>
</html>




<script src="assets/js/jquery-1.10.2.js"></script>
<script src="assets/jquery.jclock.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        loop();
        loop2();
        loop3();

        $(".piscar").css("opacity", "0.4");//define opacidade inicial
        setInterval(function () {
            if ($(".piscar").css("opacity") == 0) {
                $(".piscar").css("opacity", "1");
            } else {
                $(".piscar").css("opacity", "0");
            }
        }, 200);

        $('.som').click(function () {
            $('embed').remove();
            $('body').append('<embed src="assets/toque1.mp3" autostart="true" hidden="true" loop="false">');
        });


        var options = {
            timeNotation: '24h',
            am_pm: true,
            fontFamily: 'Verdana, Times New Roman',
            fontSize: '60px',
            foreground: 'blue'
        }
        $('.jclock').jclock(options);

    });
    var loop = function () {
        $.post('setorRefresh.php', {setor: 'FIES'}, function (resultado) {
            $('.senha1').html(resultado);
        });
        setTimeout('loop()', 3000);
    }

    var loop2 = function () {
        $.post('setorRefresh.php', {setor: 'SECRETARI'}, function (resultado) {
            $('.senha2').html(resultado);
        });
        setTimeout('loop2()', 3000);
    }

    var loop3 = function () {
        $.post('setorRefresh.php', {setor: 'PROTOCOL'}, function (resultado) {
            $('.senha3').html(resultado);
        });
        setTimeout('loop3()', 3000);
    }


</script>


<audio src="assets/toque.mp3" id="mp3" preload="auto"></audio>
<input type="button" value="Clique para tocar o MP3" onClick="playMP3();"  id="btSom" style="display: none;"/>

<script type="text/javascript">
    function playMP3() {
        document.getElementById("mp3").play();

    }
</script>