<?php

class crud {

    public function __construct() {
        include 'dados.php';
    }

    public function sql($sql) {
        $sql = $this->AntiInjection($sql);
        $consulta = mysql_query($sql);
        //$this->exibiSql($sql);
        return $consulta;
    }

    public function numLinha($sql) {
        return mysql_num_rows($this->sql($sql));
    }

    public function ler($tabela, $colunas = "*", $condicao = "") {
        $sql = "select {$colunas} from $tabela {$condicao}";
        $consulta = mysql_query($sql);
        $this->exibiSql($sql);
        return $consulta;
    }

    public function inserir($tabela, $dados, $valores) {
        $sql = "INSERT INTO {$tabela} ({$dados}) VALUES ({$valores})";
        $this->exibiSql($sql);
        $inseri = mysql_query($sql);
        if ($inseri)
            return TRUE;
    }

    public function alterar($tabela, $valores, $condicao) {
        $sql = "UPDATE {$tabela} SET  {$valores} {$condicao}";
        $this->exibiSql($sql);
        //echo "$sql<br>";
        $altera = mysql_query($sql);
        if ($altera)
            return TRUE;
    }

    public function apagar($tabela, $condicao) {
        $sql = "DELETE FROM {$tabela} {$condicao}";
        $this->exibiSql($sql);
        $apagar = mysql_query($sql);
        if ($apagar)
            return TRUE;
    }

    public function inserirForm($tabela, $dados) {
        foreach ($dados as $key => $value) {
            $chave[] = $key;
            $valor[] = "'$value'";
        }
        $chaves = implode(', ', array_values($chave));
        $valores = implode(', ', array_values($valor));
        $sql = sprintf("INSERT INTO $tabela (%s) VALUES (%s)", $chaves, $valores);
        $this->sql($sql);
        //$this->exibiSql($sql);
    }

    function editarForm($tabela, $array, $condicao) {
        foreach ($_POST as $key => $value) {
            $chav[] = "$key = '$value'";
        }
        $chave = implode(', ', array_values($chav));
        $sql = sprintf("UPDATE $tabela  SET %s $condicao", $chave);
        $this->sql($sql);
        //$this->exibiSql($sql);
    }

    public function exibiSql($sql) {
        //echo "$sql<br>";
        $fp = fopen('./log.txt', 'a');
        fwrite($fp, "$sql\n");
        fclose($fp);
    }

   
    public function AntiInjection($sql) {
        $sql = strip_tags($sql); // retirar as tags html
        //$param = mysql_escape_string($param); // Retirar todas tags mysql ex: select, insert, update drop etc...
        //$sql = addslashes($sql);
        $sql = str_replace(" or ", "", $sql);
        //$sql = str_replace("select ", "", $sql);
        //$sql = str_replace("delete ", "", $sql);
        $sql = str_replace("create ", "", $sql);
        $sql = str_replace("drop ", "", $sql);
        //$sql = str_replace("update ", "", $sql);
        $sql = str_replace("drop table", "", $sql);
        $sql = str_replace("show table", "", $sql);
        $sql = str_replace("applet", "", $sql);
        $sql = str_replace("object", "", $sql);
        //$sql = str_replace("'", "", $sql);
        //$sql = str_replace("#", "", $sql);
        //$sql = str_replace("=", "", $sql);
        //$sql = str_replace("--", "", $sql);
        //$sql = str_replace("-", "", $sql);
        $sql = str_replace(";", "", $sql);
        //$sql = str_replace("*", "", $sql);
        //$sql = strip_tags($sql);
        return $sql;
    }

}
