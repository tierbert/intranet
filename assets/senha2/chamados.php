<?php
session_start();
if (!isset($_SESSION['logado'])) {
    header("Location:entrar.php");
}

if (isset($_REQUEST['sair'])) {
    session_destroy();
    header("Location:index.php");
}
include './conexaoMysql.php';
$funcoes = new crud();
extract($_GET);
$senhaSetor1 = mysql_fetch_array($funcoes->sql("select * from chamados where setor = '$setor'"));
$nSetor = $funcoes->numLinha("select * from chamados where setor = '$setor'");
if ($nSetor < 1) {
    $dados = array(
        'alteracao' => 0,
        'senha' => 0,
        'setor' => $setor
    );
    $funcoes->inserirForm('chamados', $dados);
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <!--[if IE]>
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <![endif]-->
        <title>FILA</title>
        <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <link href="assets/css/style.css" rel="stylesheet" />
    </head>
    <body>
        <section class="menu-section"></section>
        <div class="content-wrapper2">
            <div class="container">
                <div class="row">

                    <h3>SETOR: GERÊNCIA DE CRÉDITO</h3>
                    <div class="senha1">
                        <h3>senha: <?php echo $senhaSetor1['senha']; ?></h3>
                        <h4><br>Horario: <?php echo $senhaSetor1['data']; ?></h4>
                    </div>
                    <div class="form-group">
                        <label>Atendente:</label>
                        <input type="text" name="local" id="local" class="form-group">
                        <br>
                        <a class="prox1 btn btn-primary"><i class="glyphicon glyphicon-bullhorn"></i> Próxima Senha</a><br><br>
                        <a class="repetir btn btn-primary"><i class="glyphicon glyphicon-bullhorn"></i> Chamar Novamente</a><br>
                    </div>
                </div>
            </div>
            <!-- CONTENT-WRAPPER SECTION END-->
            <section class="footer-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                        </div>

                    </div>
                </div>
            </section>
            <script src="assets/js/jquery-1.10.2.js"></script>
            <script src="assets/js/bootstrap.js"></script>
            <script src="assets/js/custom.js"></script>
    </body>
</html>


<script src="assets/js/jquery-1.10.2.js"></script>
<script>
    $(document).ready(function () {
        $('.prox1').click(function () {
            //alert('teste')
            var localn = $('#local').val();
            $.post('setor1Alterar.php', {local: localn, setor: '<?php echo $setor; ?>'}, function (resultado) {
                $('.senha1').fadeOut(0);
                $('.senha1').fadeIn(800);
                $('.senha1').load('setor1AltRef.php?setor=<?php echo $setor; ?>');
            });
        });
        $('.repetir').click(function () {
            //alert('teste')
            var localn = $('#local').val();
            $.post('setorRepetir.php', {local: localn, setor: '<?php echo $setor; ?>'}, function (resultado) {
                $('.senha1').fadeOut(0);
                $('.senha1').fadeIn(800);
                $('.senha1').load('setor1AltRef.php?setor=<?php echo $setor; ?>');
            });
        });
    });

</script>